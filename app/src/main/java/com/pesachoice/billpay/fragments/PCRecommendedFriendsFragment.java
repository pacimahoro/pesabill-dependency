package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;

import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCExcludedReferrals;
import com.pesachoice.billpay.model.PCReferralData;
import com.pesachoice.billpay.utils.PCExpendableListViewAdapter;


import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Fragment to show a list of both approvded  and excluded recommended friends
 * @author Desire AHEZA
 */
public class PCRecommendedFriendsFragment extends DialogFragment {

    private PCReferralData pcReferralData = new PCReferralData();
    private List<PCContactInfo> selectedContacts;
    private List<PCContactInfo> allContacts;
    //private PCContactInf

    public PCRecommendedFriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pc_fragment_recommended_friends, container, false);
        List<String> title = new ArrayList<>();
        HashMap<String,List<PCContactInfo>>  mRecommendedNotRecommended=new HashMap<>();
        if (pcReferralData != null) {
            //get first recommended friends
            if (pcReferralData.getReferrals() != null && pcReferralData.getReferrals().size() > 0) {
                title.add(PCPesachoiceConstant.PC_APPROVED_RECOMMENDED_FRIENDS);
                mRecommendedNotRecommended.put(PCPesachoiceConstant.PC_APPROVED_RECOMMENDED_FRIENDS,getSelectedContactsObj(pcReferralData.getReferrals()));
            }
            //then get list of excluded
            if (pcReferralData.getExcludedReferrals() != null && pcReferralData.getExcludedReferrals().size() > 0) {
                title.add(PCPesachoiceConstant.PC_NOT_APPROVED_RECOMMENDED_FRIENDS);
                List<String> number = new ArrayList<>();
                //currently only uses numbers for testing
                for (PCExcludedReferrals excludedReferrals:pcReferralData.getExcludedReferrals()) {
                    number.add(excludedReferrals.getPhoneNumber());
                }
                mRecommendedNotRecommended.put(PCPesachoiceConstant.PC_NOT_APPROVED_RECOMMENDED_FRIENDS,getSelectedContactsObj(number));
            }
        }
        //had to use expandable listview to show both approved and not approved recommended friends
        ExpandableListView listView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        PCExpendableListViewAdapter expandableListViewAdapter = new PCExpendableListViewAdapter(this.getActivity(),title,mRecommendedNotRecommended);
        listView.setAdapter(expandableListViewAdapter);
        //expand group one: recommended uses
        listView.expandGroup(0);
        return view;
    }

    public PCReferralData getPcReferralData() {
        return pcReferralData;
    }

    public List<PCContactInfo> getSelectedContacts() {
        return selectedContacts;
    }

    public void setAllContacts(List<PCContactInfo> allContacts) {
        this.allContacts = allContacts;
    }
    public List<PCContactInfo> getAllContacts() {
        return allContacts;
    }

    public void setSelectedContacts(List<PCContactInfo> selectedContacts) {
        this.selectedContacts = selectedContacts;
    }

    public void setPcReferralData(PCReferralData pcReferralData) {
        this.pcReferralData = pcReferralData;
    }


    /**
     * get a list of selected contacts
     * @return List<PCContactInfo>
     *          list of selected contacts object
     */
    public List<PCContactInfo> getSelectedContactsObj(List<String> mApprovedContacts){
        List<PCContactInfo>  mSelectedContacts = new ArrayList<>();
        for (PCContactInfo pcContactInfo : allContacts){

            if (pcContactInfo != null ){
                String contact = pcContactInfo.getPhoneNumber();
                //TODO:need to find a best way to handle this situation
                if (!StringUtils.isEmpty(contact) && mApprovedContacts != null) {
                    if (contact.indexOf("+") == 0) {
                        contact = contact.substring(1);
                    }
                    for (String mContact : mApprovedContacts) {
                        if (contact.equalsIgnoreCase(mContact)) {
                            mSelectedContacts.add(pcContactInfo);
                            break;
                        }
                    }
                }
            }
        }
        return mSelectedContacts;
    }

}
