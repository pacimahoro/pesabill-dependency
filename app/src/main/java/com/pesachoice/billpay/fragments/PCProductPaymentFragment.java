/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMerchantsProfile;
import com.pesachoice.billpay.model.PCProductPaymentData;
import com.pesachoice.billpay.model.PCProductServiceInfo;
import com.pesachoice.billpay.model.PCServiceInfoFactory;

import java.util.ArrayList;

/**
 * this fragment has views to hold inputs and methods required display a list of product we offer in each country we support
 * @author Pacifique Mahoro
 */
public class PCProductPaymentFragment extends PCTransactionFragment {

    private Spinner providerSpinner;
    private EditText productDescription;
    private EditText productPrice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_retail_transaction, container, false);

        this.setProviderListSpinner(view);

        Button continueBtn = (Button)view.findViewById(R.id.continue_button);
        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }

        return view;
    }

    public void updateMerchantsProfile(PCMerchantsProfile merchantsProfile) {
        View view = getView();

        if (view == null) { return; }

        Spinner providerSpinner = (Spinner)view.findViewById(R.id.account_provider);
        ArrayList<String> operators = null;
        providerSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        if (merchantsProfile != null) {
            operators = (ArrayList<String>)merchantsProfile.getOperatorNamesForService(this.getActionType());
            this.generateOperators(providerSpinner, operators);
        }
    }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.SELL;
    }

    @Override
    public void onClick(View v) {
        try {
            if(validateSellerFragmentInput(v)){

                // set product service info
                this.getServiceInfo(v);

                //set the operator
                //this.getOperatorInfo(v);

                PCPurchaseCheckoutFragment frag = new PCPurchaseCheckoutFragment();

                //open Customer phone&cardInfoFragment
                frag.setTransactionData(this.getTransactionData());
                frag.setServiceType(this.getServiceType());

                FragmentManager fm = this.getActivity().getSupportFragmentManager();
                fm.beginTransaction()
                        .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_phone_number_and_card_fragment))
                        .addToBackStack(null)
                        .commit();
            }
        } catch (PCGenericError error){
            this.presentError(error);
        }
    }


    private boolean validateSellerFragmentInput(View view) throws PCGenericError{
        boolean fargmentValid = false;
        view = getView();
        if (view != null) {
            PCGenericError error;
            providerSpinner = (Spinner) view.findViewById(R.id.account_provider);
            productDescription = (EditText) view.findViewById(R.id.product_name);
            productPrice = (EditText) view.findViewById(R.id.usd_amount);

            if (providerSpinner != null && SELECT_COMPANY.equals(providerSpinner.getSelectedItem())) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("Please Select Company");
                throw error;
            }
            else if (productDescription.length() == 0) {
                productDescription.setError("Product name is required");
            }
            else if(productPrice.length() == 0){
                productPrice.setError("Product price is required");
            }
            else
                fargmentValid = true;
        }
        return fargmentValid;
    }


    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        view = getView();
        if (view == null) { return; }

        PCProductPaymentData transaction = (PCProductPaymentData)this.getTransactionData();

        PCProductServiceInfo serviceInfo = (PCProductServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
        serviceInfo.setReceiverCountry(((PCMainTabActivity) getActivity()).countrySendingTo);
        serviceInfo.setCompanyName((String) providerSpinner.getSelectedItem());
        serviceInfo.setProductDescription(productDescription.getText().toString());

        Double usdAmount = Double.parseDouble(productPrice.getText().toString());
        Double fee = 0.0;
        serviceInfo.setUsd(usdAmount);
        serviceInfo.setTotalUsd(usdAmount + fee);

        PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
        if (countryProfile != null) {
            Double localAmount = countryProfile.convertTolocalCurrency(usdAmount);
            serviceInfo.setTotalLocalCurrency(localAmount);
        }

        transaction.setProductServiceInfo(serviceInfo);
    }
}
