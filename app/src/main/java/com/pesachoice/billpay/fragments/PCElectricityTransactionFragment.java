/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCElectricityPaymentData;
import com.pesachoice.billpay.model.PCElectricityServiceInfo;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCServiceInfoFactory;

/**
 * Represents a pcOnPhoneContactLoad for electricity service
 * @author Pacifique Mahoro
 */
public class PCElectricityTransactionFragment extends PCTransactionFragment {

    public PCElectricityTransactionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_transaction, container, false);

        Button continueBtn = (Button) view.findViewById(R.id.continue_button);
        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }

        // Set these fields we will need them to set the selected contact info.
        fullNameText = (EditText) view.findViewById(R.id.full_name);
        phoneText = (EditText) view.findViewById(R.id.mobile);
        setPhoneTextPrefix(view);
        this.setAddContactsButton(view);
        this.setProviderListSpinner(view);

        return view;
    }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.ELECTRICITY;
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        View view = getView();
        if (view == null) { return; }

        PCGenericError error;
        EditText accountText = (EditText) view.findViewById(R.id.account_number);
        String accountNumber = accountText.getText().toString();
        if (accountNumber.length() == 0) {
            error = new PCGenericError();
            error.setMessage("Account Number is required");
            throw error;
        }

        PCElectricityPaymentData transaction = (PCElectricityPaymentData)this.getTransactionData();
        PCElectricityServiceInfo serviceInfo = (PCElectricityServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
        serviceInfo.setReceiverCountry(((PCMainTabActivity)getActivity()).countrySendingTo);
        serviceInfo.setCashPower(accountNumber);
        serviceInfo.setAccountNumber(accountNumber);
        transaction.setElectricityServiceInfo(serviceInfo);
    }
}
