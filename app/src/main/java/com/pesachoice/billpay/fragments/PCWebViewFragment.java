package com.pesachoice.billpay.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.BinderThread;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.R;

/**
 * Handle the display of requested webpage in webview pcOnPhoneContactLoad
 * A simple {@link Fragment} subclass.
 *
 * @author Desire Aheza
 */
public class PCWebViewFragment extends Fragment {
    private ProgressBar progressBar;

    public PCWebViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        View rootView = inflater.inflate(R.layout.pc_webview_fragment, container, false);
        WebView webView = (WebView) rootView.findViewById(R.id.display_webcontent);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted (WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(ProgressBar.VISIBLE);
            }

            @Override
            public void onPageFinished (WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(ProgressBar.GONE);
            }
        });

        webView.loadUrl(((PCAccountDetailsActivity) getActivity()).getURL());
        return rootView;
    }
}
