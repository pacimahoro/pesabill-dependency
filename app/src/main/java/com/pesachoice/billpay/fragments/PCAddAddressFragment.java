package com.pesachoice.billpay.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCGenericError;

import org.springframework.util.StringUtils;
import org.w3c.dom.Text;

/**
 * Fragment used to collect user's Address
 * A simple {@link Fragment} subclass.
 */
public class PCAddAddressFragment extends DialogFragment {

    private PCBillPaymentData transactionData;
    private final static String CLAZZZ = PCAddAddressFragment.class.getName();
    // TODO: Get rid of this serviceType and instead use the transactionData
    private PCPesabusClient.PCServiceType serviceType;
    private String streetAddressText;
    private String cityNameText;
    private String zipCodeText;
    private String stateNameText;
    private String countryNameText;
    private boolean addressFormHasError = true;
    private View view;

    private PCMainTabActivity activity = new PCMainTabActivity();

    public PCAddAddressFragment() {
        // Required empty public constructor
    }

    public void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public PCPesabusClient.PCServiceType getServiceType() {
        return serviceType;
    }

    public void setTransactionData(PCBillPaymentData transactionData) {
        this.transactionData = transactionData;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.pc_add_address_fragment, null);
        activity = (PCMainTabActivity)getActivity();
        return createAddressDialog();
    }

    private Dialog createAddressDialog() {
        AlertDialog dialog = null;
        try {
            if (this.isVisible()) {
                this.dismiss();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setView(view)
                    .setPositiveButton(R.string.save_address, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //Do nothing here because we override this button later to change the close behaviour.
                            //However, we still need this because on older versions of Android unless we
                            //pass a handler the button doesn't get instantiated
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d(CLAZZZ, "Clicked on cancel on Alert box");
                        }
                    });
            dialog = builder.create();
        } catch (Exception e){

        }
        return dialog;
    }


    private void validateAddressAndOpenCheckOut() {
        try {
            validateInput();
            if (addressFormHasError) {
                addAddressToSender();
                openCheckOut();
            }
        } catch (PCGenericError pcGenericError) {
            presentError(pcGenericError);
        }
    }

    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity)getActivity();

        if (error != null && activity != null) {
            String title = error.getTitle();
            if (title == null) {
                title = "ADD ADDRESS ERROR";
            }
            this.presentError(error, title);
        }
    }

    /**
     * Validate inputs and then set error if input box doesn't have text
     * @throws PCGenericError
     */
    private void validateInput() throws PCGenericError {
        PCGenericError error;
        //TODO: need to discuss with Team if we really need to throw a dialog box in case of error or if we can use built in error displaying
        if (view == null) {
            // TODO: what should we do here?
            return;
        }
        EditText streetAddress = (EditText) view.findViewById(R.id.street_address);
        streetAddressText = getInputInEditText(streetAddress);
        EditText cityName = (EditText) view.findViewById(R.id.city_name);
        cityNameText = getInputInEditText(cityName);
        EditText stateName = (EditText) view.findViewById(R.id.state_name);
        stateNameText = getInputInEditText(stateName);
        EditText zipCode = (EditText) view.findViewById(R.id.zip_code_number);
        zipCodeText = getInputInEditText(zipCode);
        EditText countryName = (EditText) view.findViewById(R.id.country_name);
        countryNameText = getInputInEditText(countryName);
        if (StringUtils.isEmpty(streetAddressText)) {
            addressFormHasError=false;
            streetAddress.setError("Street Address is required");
        }
        else if (StringUtils.isEmpty(cityNameText)) {
            addressFormHasError = false;
            cityName.setError("City name is required");

        }
        else if (StringUtils.isEmpty(zipCodeText)) {
            addressFormHasError = false;
            zipCode.setError("Zip Code is required");

        }
        else if (StringUtils.isEmpty(stateNameText)) {
            addressFormHasError = false;
            stateName.setError("State is required");
        }
        else if (StringUtils.isEmpty(countryNameText)) {
            addressFormHasError = false;
            countryName.setError("Country is required");
        }
        else
            addressFormHasError = true;
    }
    //TODO: this method will be removed currently not being used here
    private PCGenericError createError(String errorText) {
        PCGenericError error = new PCGenericError();
        error.setTitle("Validation Error");
        error.setMessage(errorText);
        return error;
    }

    private String getInputInEditText(EditText editText){
        return editText != null ?editText.getText().toString():null;
    }

    private void addAddressToSender() {
        PCBillingAddress billingAddress = new PCBillingAddress();
        billingAddress.setStreet(streetAddressText);
        billingAddress.setCity(cityNameText);
        billingAddress.setZip(zipCodeText);
        billingAddress.setState(stateNameText);
        billingAddress.setCountry(countryNameText);
        PCMainTabActivity activity = (PCMainTabActivity)getActivity();
        activity.getAppUser().setBillingAddress(billingAddress);
    }

    private void openCheckOut() {
        PCCheckoutFragment frag = new PCCheckoutFragment();
        PCMainTabActivity activity = (PCMainTabActivity)getActivity();

        //Set the sender
        if (activity != null && this.transactionData != null) {
            this.transactionData.setSender(activity.getAppUser());
        }

        frag.setTransactionData(this.transactionData);
        frag.setServiceType(this.getServiceType());

        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_checkout))
                .addToBackStack(null)
                .commit();
    }
    //TODO: this method will be removed currently not being used here
    public void presentError(PCGenericError error, String title) {
        String message = error.getMessage();
        if ( message == null) {
            return;
        }

        final AlertDialog.Builder builderInner = new AlertDialog.Builder(this.getActivity());
        builderInner.setMessage(message);
        builderInner.setTitle(title);

        builderInner.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                try {
                    Log.d(CLAZZZ, "Clicked on OK on Alert box");
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        });

        builderInner.create().show();
    }

    @Override
    public void onStart()
    {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to check if we need to validate inputs and then dismiss the dialog or not here
        final AlertDialog d = (AlertDialog)getDialog();
        if (d != null)
        {
            Button positiveButton = (Button) d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Boolean wantToCloseDialog = false;
                    /*
                        We're validating input and then set a flag that will help to check if we need to
                        to close the address dialog or keep it open because the address form has error(s)
                    */
                    validateAddressAndOpenCheckOut();
                    if (addressFormHasError)
                        wantToCloseDialog = true;
                    if (wantToCloseDialog)
                        d.dismiss();
                    //else dialog stays open. Make sure we have an obvious way to close the dialog especially if you set cancellable to false.
                }
            });
        }
    }
}
