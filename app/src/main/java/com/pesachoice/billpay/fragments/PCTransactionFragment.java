package com.pesachoice.billpay.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.PCOnPhoneContactLoad;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCCurrencyExchangeRateUtil;
import com.pesachoice.billpay.utils.PCFontManager;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCPreferenceHelper;
import com.pesachoice.billpay.utils.PCSpinner;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Creates and handles a transaction view
 *
 * @author Pacifique Mahoro
 */

public abstract class PCTransactionFragment extends Fragment implements PCOnPhoneContactLoad, PConRequiredDataLoaded, PCTransactionComponent, View.OnClickListener {
    public static final String SELECT_COMPANY = "Select Company";
    private PCPesabusClient.PCServiceType serviceType;
    private PCBillPaymentData transaction = new PCBillPaymentData();
    public static final String TRANSACTION_ERROR_TITLE = "Transaction Error";
    private PCContactsAdapter mContactAdapter;
    public static final int CONTACT_REQUEST_CODE = 1;
    static final int PICK_CONTACT = 2;
    private PCContactInfo contactInfo;
    protected String phonePrefix;
    protected EditText fullNameText;
    protected EditText phoneText;
    private LinearLayout transanctionLayout;
    private ProgressBar progressBar;

    @Override
    public void setTransactionData(PCBillPaymentData transaction) {
        this.transaction = transaction;
    }

    @Override
    public PCBillPaymentData getTransactionData() {
        return this.transaction;
    }

    @Override
    public void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public PCPesabusClient.PCServiceType getServiceType() {
        return serviceType;
    }

    public void loadContacts() {
        mContactAdapter = new PCContactsAdapter(getActivity(), R.layout.pc_contact_listitem, new ArrayList<PCContactInfo>());
        // Sets the adapter for the ListView
        PCListAllContactsAsync listViewContactsLoader = new PCListAllContactsAsync(this);
        listViewContactsLoader.execute();
    }

    public void setPhoneTextPrefix(View view) {
        if (view != null) {
            PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
            this.phonePrefix = "";
            if (countryProfile != null) {
                this.phonePrefix = countryProfile.getCountryCode();
            }

            if (phoneText == null)
                phoneText = (EditText) view.findViewById(R.id.mobile);

            phoneText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    String txt = phoneText.getText().toString();
                    String cleanPhone = txt.replaceAll("\\D", "");
                    if (hasFocus && phonePrefix.length() > 0 && !cleanPhone.startsWith(phonePrefix)) {
                        String p = "+" + phonePrefix + " ";
                        phoneText.setText(p);
                        Selection.setSelection(phoneText.getText(), phoneText.getText().length());
                    }
                }
            });

            phoneText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String txt = s.toString();
                    String cleanPhone = txt.replaceAll("\\D", "");
                    if (phonePrefix.length() > 0 && !cleanPhone.startsWith(phonePrefix)) {
                        String p = "+" + phonePrefix + " ";
                        phoneText.setText(p);
                        Selection.setSelection(phoneText.getText(), phoneText.getText().length());
                    }
                }
            });

            phoneText.invalidate();
        }
    }

    public PCContactInfo getContactInfo() {
        return contactInfo;
    }

    @Override
    public void setContactInfo(PCContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    /**
     * Set the provider list spinner element. Each service has its own list of operators
     * that provides that service for a given country.
     * Here we retrieve the list of operators for the selected service and
     * then use it to fill the dropdown menu (spinner).
     *
     * @param view View that contains the spinner
     */
    public void setProviderListSpinner(View view) {
        PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
        Spinner providerSpinner = (Spinner) view.findViewById(R.id.account_provider);
        ArrayList<String> operators = null;
        providerSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        if (this.getTransactionData() != null && PCActivity.PCPaymentProcessingType.CALLING.equals(this.getTransactionData().getPaymentProcessingType())) {
            //TODO:need to find a best way to handle receiver name
            fullNameText.setText(((PCMainTabActivity) getActivity()).getAppUser().getFullName());
            operators = new ArrayList<>();
            operators.add("KIVUTEL");
            generateOperators(providerSpinner, operators);


        } else if (countryProfile != null) {
            operators = (ArrayList<String>) countryProfile.getOperatorNamesForService(this.getActionType());
            generateOperators(providerSpinner, operators);
        } else {
            ((PCMainTabActivity) getActivity()).setTransactionFragment(this);
            this.onDataLoadStart(view);
        }
    }

    public void generateOperators(Spinner providerSpinner, ArrayList<String> operators) {
        if (providerSpinner != null && operators.size() > 0) {
            if (transaction != null && PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS.equals(transaction.getPaymentProcessingType())) {
                if (operators != null)
                    operators.add(0, SELECT_COMPANY);
            }
            String[] operatorNames = operators.toArray(new String[operators.size()]);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.pc_line_item_spinner, operatorNames);// android.R.layout.simple_spinner_item
            providerSpinner.setAdapter(spinnerArrayAdapter);
        }
    }

    /**
     * Sets add contacts button.
     *
     * @param view the view
     * @return the add contacts button
     */
    public boolean setAddContactsButton(View view) {
        boolean msg = false;


        TextView addContactsBtn = new TextView(getContext());
        addContactsBtn = (TextView) view.findViewById(R.id.contacts_btn);

        /**
         * @param view the view
         *this method is checking the value assigned to addContactBtn if its not NULL BY
         * code Analysis we found that the Null pointer is caused by the view parameter we
         * are passing it and which will also affect the addContactsBtn to be NULL.
         *
         */
        if (addContactsBtn == null) {
            return msg;
        }

        /**
         * check if the call is to display calling pcOnPhoneContactLoad that contains UI to be used to refil KIVUTEL credit
         */
        if (this.getTransactionData() != null && PCActivity.PCPaymentProcessingType.CALLING.equals(this.getTransactionData().getPaymentProcessingType())) {
            phoneText.setText(((PCMainTabActivity) getActivity()).getAppUser().getPhoneNumber());
            LinearLayout linearlayout_country = (LinearLayout) view.findViewById(R.id.linearlayout_country);
            if (linearlayout_country != null)
                linearlayout_country.setVisibility(View.GONE);
        } else {
            /**
             * used to display other transanction UI  country and phone
             */
            //TextView country = (TextView) view.findViewById(R.id.country);
            //phoneText.setHint(((PCMainTabActivity)getActivity()).getCountryProfile().getCountryCode()+" 751-231-230");//+256
            // country.setText(((PCMainTabActivity)getActivity()).countrySendingTo);
        }

        if (addContactsBtn != null) {
            Typeface iconFont = PCFontManager.getTypeface(getActivity().getApplicationContext(), PCFontManager.FONTAWESOME);
            PCFontManager.markAsIconContainer(addContactsBtn, iconFont);
            addContactsBtn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = null;
                            //request phone contact
                            intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                            // startActivityForResult(intent, CONTACT_REQUEST_CODE);
                            if (intent != null && intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                //used to provide result back to this pcOnPhoneContactLoad
                                startActivityForResult(intent, PICK_CONTACT);


                            } else {
                                //request phone contact
                                intent = new Intent(getActivity(), PCAccountDetailsActivity.class);
                                intent.putExtra(PCPesabusClient.OPTION, PCPesabusClient.OPTION_CONTACTS_FROM_PHONE);
                                //used to provide result back to this pcOnPhoneContactLoad
                                startActivityForResult(intent, CONTACT_REQUEST_CODE);
                            }
                        }
                    }
            );
            msg = true;
        }

        return msg;
    }

    /**
     * Set user information from phone contact
     *
     * @param contactInfo
     */
    private void setNameAndPhone(PCContactInfo contactInfo) {
        if (contactInfo != null) {
            if (fullNameText != null) {
                fullNameText.setText(contactInfo.getDisplayName());
            }
            if (phoneText != null) {
                phoneText.setText(contactInfo.getPhoneNumber());
            }
        }
    }


    /**
     * Returns the action type of this transaction. It's very similar
     * but different in representation to the ServiceType. See @ServiceType for more details.
     *
     * @return PCData.ActionType
     */
    public abstract PCData.ActionType getActionType();


    @Override
    public void onClick(View v) {

        try {

            this.validateInputs(v);
            PCSpecialUser user = ((PCMainTabActivity) getActivity()).getAppUser();

            this.checkOut();

        } catch (PCGenericError error) {
            this.presentError(error);
        }
    }

    private void openAddAddressFragment() {
        PCAddAddressFragment frag = new PCAddAddressFragment();
        frag.setTransactionData(this.getTransactionData());
        frag.setServiceType(this.getServiceType());
        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        frag.show(fm, "Transaction Confirmation Dialog");

    }

    private void checkOut() {
        PCCheckoutFragment frag = new PCCheckoutFragment();
        PCMainTabActivity activity = (PCMainTabActivity) getActivity();

        //Set the sender
        if (activity != null && this.getTransactionData() != null) {
            PCSpecialUser user = activity.getAppUser();
            if (user != null) {
                if (StringUtils.isEmpty(user.getCountry())) {
                    PCCurrency currency = user.getBaseCurrency();
                    String senderCountry = null;
                    //SET THE COUNTRY OF THE SENDER
                    if (currency != null) {
                        senderCountry = currency.getCountry();
                    } else {
                        senderCountry = PCCurrencyExchangeRateUtil.getCountry(activity);
                    }
                    activity.getAppUser().setCountry(senderCountry);
                }
            }
            this.getTransactionData().setSender(activity.getAppUser());

        }

        frag.setTransactionData(this.getTransactionData());
        frag.setServiceType(this.getServiceType());

        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.grid_container_country, frag, Integer.toString(R.layout.pc_fragment_checkout))
                .addToBackStack(null)
                .commit();
    }

    void showInputDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.pc_validate_user_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final EditText editTextPassword = (EditText) promptView.findViewById(R.id.editTextPassword);
        TextView userEmail = (TextView) promptView.findViewById(R.id.textView_email);
        userEmail.setText(PCPreferenceHelper.getFromPrefs(getActivity(), PCPesachoiceConstant.SHARED_PREF_USER_EMAIL, ""));

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        checkOut();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void validateInputs(View v) throws PCGenericError {
        this.getAndSetReceiverInfo(v);
        this.getOperatorInfo(v);
        this.getServiceInfo(v);
    }


    /**
     * All the pcOnPhoneContactLoad
     * have a receiver info section and this method
     * handles getting input values, validating it.
     *
     * @param v
     * @throws PCGenericError Error in case some required fields are missing or fail validation.
     */
    @Override
    public void getAndSetReceiverInfo(View v) throws PCGenericError {
        PCGenericError error;
        View view = getView();
        if (view == null) {
            // TODO: what should we do here?
            return;
        }

        fullNameText = (EditText) view.findViewById(R.id.full_name);
        String fullName = StringUtils.capitalize(fullNameText.getText().toString());

        phoneText = (EditText) view.findViewById(R.id.mobile);
        String phoneNumber = phoneText.getText().toString();
        phoneNumber = PCGeneralUtils.cleanPhoneString(phoneNumber);

        PCMainTabActivity activity = ((PCMainTabActivity) getActivity());
        String countryCode = null;
        if (activity != null) {
            PCCountryProfile countryProfile = activity.getCountryProfile();
            if (countryProfile != null) {
                countryCode = countryProfile.getCountryCode();
            }
        }

        // Default to UG country code.
        if (countryCode == null) {
            countryCode = "256";
        }

        if (fullName.length() == 0) {
            error = new PCGenericError();
            error.setTitle("Validation Error");
            error.setMessage("Receiver full Name is required");
            throw error;
        }
        if (phoneNumber.length() == 0) {
            error = new PCGenericError();
            error.setTitle("Validation Error");
            error.setMessage("Receiver Phone Number is required");
            throw error;
        }
        if ("Nigeria".equalsIgnoreCase(((PCMainTabActivity) getActivity()).countrySendingTo) && (phoneNumber.length() < 13 || phoneNumber.length() > 13) && !("1".equals(phoneNumber.substring(0, 1)) && phoneNumber.length() == 11)) {
            error = new PCGenericError();
            error.setTitle("Validation Error");
            error.setMessage("A valid receiver number should have 10 digits excluding +" + countryCode + ". Please check the number again");
            throw error;
        } else if (!"Nigeria".equalsIgnoreCase(((PCMainTabActivity) getActivity()).countrySendingTo) && (phoneNumber.length() < 12 || phoneNumber.length() > 12) && !("1".equals(phoneNumber.substring(0, 1)) && phoneNumber.length() == 11)) {
            error = new PCGenericError();
            error.setTitle("Validation Error");
            error.setMessage("A valid receiver number should have 9 digits excluding +" + countryCode + ". Please check the number again");
            throw error;
        }

        String firstName, lastName = "";
        String[] parts = fullName.split(" ");
        firstName = parts[0];

        for (int i = 1; i < parts.length; i++) {
            lastName += parts[i] + " ";
        }
        lastName = lastName.trim();

        PCSpecialUser receiver = new PCSpecialUser();
        receiver.setFullName(fullName);
        receiver.setFirstName(firstName);
        receiver.setLastName(lastName);
        receiver.setPhoneNumber(phoneNumber);

        if (activity != null) {
            PCCountryProfile pcCountryProfile = activity.getCountryProfile();
            if (pcCountryProfile != null) {
                //added receiver country in order to show right exchange rate
                receiver.setCountry(pcCountryProfile.getCountry());
            }
        }
        PCBillPaymentData transaction = this.getTransactionData();
        transaction.setReceiver(receiver);
    }

    @Override
    public void getOperatorInfo(View v) throws PCGenericError {
        View view = getView();
        if (view == null) {
            return;
        }

        PCOperator operator = new PCOperator();
        PCGenericError error;
        /**
         * We're not setting operator in case this transaction is Money transfer, Currently Server is taking care of generating operator
         */
        if ((this.transaction != null && !PCActivity.PCPaymentProcessingType.MONEY_TRANSFER.equals(this.transaction.getPaymentProcessingType()))
                || PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS.equals(this.transaction.getPaymentProcessingType())) {

            PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
            if (!((PCMainTabActivity) getActivity()).getPcOperatorRequest(countryProfile.getCountry()).getCompanyId().equalsIgnoreCase("pesachoice")) {
                List<String> operators = null;
                if (countryProfile != null) {
                    operators = (ArrayList<String>) countryProfile.getOperatorNamesForService(this.getActionType());
                }
                if (operators != null && operators.size() > 0)
                    operator.setName(operators.get(0));
                else {
                    //TODO:what to do here?
                }

            } else {
                Spinner providerSpinner = (Spinner) view.findViewById(R.id.account_provider);
                String providerName = (String) providerSpinner.getSelectedItem();

                if (providerName != null && providerName.length() == 0) {
                    error = new PCGenericError();
                    error.setMessage("Operator Name is required");
                    throw error;
                }
                operator.setName(providerName);
            }

        } else {
            PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
            List<String> operators = null;
            if (countryProfile != null) {
                operators = (ArrayList<String>) countryProfile.getOperatorNamesForService(this.getActionType());
            }
            if (operators != null && operators.size() > 0)
                operator.setName(operators.get(0));
            else {
                //TODO:what to do here?
            }
        }
        PCBillPaymentData transaction = this.getTransactionData();
        transaction.setOperator(operator);
    }

    private void setNameandPhone(PCContactInfo contactInfo) {
        if (contactInfo != null) {
            //user information from phone contact
            fullNameText.setText(contactInfo.getDisplayName());
            Log.e("PhoneN0 :", contactInfo.getPhoneNumber());
            if (contactInfo.getPhoneNumber().startsWith("+")) {
                phoneText.setText(contactInfo.getPhoneNumber());
            } else {
                PCCountryProfile countryProfile = ((PCMainTabActivity) getActivity()).getCountryProfile();
                if (countryProfile.getCountry().equalsIgnoreCase("rwanda")) {
                    String phoneConcant = "+" + countryProfile.getCountryCode() + contactInfo.getPhoneNumber();
                    phoneText.setText(phoneConcant);
                } else if (countryProfile.getCountry().equalsIgnoreCase("uganda")) {
                    String phone = "+" + contactInfo.getPhoneNumber();
                    phone = phone.substring(0, 4) + phone.substring(5, phone.length());
                    phoneText.setText(phone);
                } else if (countryProfile.getCountry().equalsIgnoreCase("kenya")) {
                    String phone = "+" + contactInfo.getPhoneNumber();
                    phone = phone.substring(0, 4) + phone.substring(5, phone.length());
                    phoneText.setText(phone);
                } else if (countryProfile.getCountry().equalsIgnoreCase("Zimbabwe")) {
                    String phone = "+" + contactInfo.getPhoneNumber();
                    phone = phone.substring(0, 4) + phone.substring(5, phone.length());
                    phoneText.setText(phone);
                }
            }
        }
    }

    @Override
    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity) getActivity();

        if (error != null && activity != null) {
            String title = error.getTitle();
            if (title == null) {
                title = TRANSACTION_ERROR_TITLE;
            }
            activity.presentError(error, title);
        }
    }

    @Override
    public void onContactLoadStarted() {

    }

    @Override
    public void onContactLoadFinished(List<PCContactInfo> contacts) {
        mContactAdapter.addAll(contacts);
        mContactAdapter.notifyDataSetChanged();
    }

    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    /**
     * Used to handle results from started activities while expecting response
     *
     * @param requestCode constant to determine particular request
     * @param resultCode
     * @param data        intent with extra data most likely a parcelable
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Cursor cursor = null;
        Cursor phones = null;
        try {
            switch (requestCode) {
                case (PICK_CONTACT):
                    if (resultCode == getActivity().RESULT_OK) {
                        Uri contactData = data.getData();
                        cursor = getActivity().managedQuery(contactData, null, null, null, null);
                        while (cursor.moveToNext()) {
                            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                            String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                            String phoneNumber = "";
                            if ("1".equalsIgnoreCase(hasPhone))
                                hasPhone = "true";
                            else
                                hasPhone = "false";
                            PCContactInfo contactInfo = null;
                            if (Boolean.parseBoolean(hasPhone)) {

                                if (cursor != null && cursor.moveToFirst()) {
                                    phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                }
                                contactInfo = new PCContactInfo(phoneNumber, name, "");
                            } else {
                                PCCountryProfile countrySendingTo = ((PCMainTabActivity) getActivity()).getCountryProfile();
                                phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                if (PCGeneralUtils.formatPhoneNumber(phoneNumber, countrySendingTo.getCountryCode()).equals(phoneNumber)) {
                                    if (!phoneNumber.contains(countrySendingTo.getCountryCode()) && countrySendingTo.getCountry().equalsIgnoreCase("rwanda")) {
                                        phoneNumber = phoneNumber.replaceFirst("[0-9]", "");
                                    } else if (!phoneNumber.contains(countrySendingTo.getCountryCode()) && countrySendingTo.getCountry().equalsIgnoreCase("uganda")) {

                                        phoneNumber = countrySendingTo.getCountryCode() + phoneNumber;
                                    } else if (!phoneNumber.contains(countrySendingTo.getCountryCode()) && countrySendingTo.getCountry().equalsIgnoreCase("kenya")) {
                                        phoneNumber = countrySendingTo.getCountryCode() + phoneNumber;
                                    } else if (!phoneNumber.contains(countrySendingTo.getCountryCode()) && countrySendingTo.getCountry().equalsIgnoreCase("Zimbabwe")) {
                                        phoneNumber = countrySendingTo.getCountryCode() + phoneNumber;
                                    }
                                    contactInfo = new PCContactInfo(phoneNumber, name, "");
                                }

                            }
                            setNameandPhone(contactInfo);
                        }
                    }
                    break;
                case (CONTACT_REQUEST_CODE):
                    if (resultCode == getActivity().RESULT_OK) {
                        PCContactInfo contactInfo = data.getParcelableExtra(PCPesachoiceConstant.CONTACT_RESULTS);
                        if (contactInfo != null)
                            setNameandPhone(contactInfo);
                    }
                    break;
                default:
                    break;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataLoadStart(View view) {
        if (view != null) {
            this.view = view;
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            transanctionLayout = (LinearLayout) view.findViewById(R.id.transaction_view);
            if (progressBar != null)
                progressBar.setVisibility(View.VISIBLE);
            if (transanctionLayout != null)
                transanctionLayout.setVisibility(View.GONE);
        }
    }

    View view;

    @Override
    public void onDataLoadFinished(PCData pcData) {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
        if (transanctionLayout != null)
            transanctionLayout.setVisibility(View.VISIBLE);
        /*
         *if this transaction is money transfer we don't need to setup provider list Spinner
         * Server will take care
         */
        if (this.transaction != null && !PCActivity.PCPaymentProcessingType.MONEY_TRANSFER.equals(this.transaction.getPaymentProcessingType()))
            setProviderListSpinner(view);
    }

    @Override
    public void onCollectionDataLoadFinished(Collection<? extends PCData> pcData) {
        //TODO:Not being used here
    }
}
