package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.pesachoice.billpay.activities.PCOnPhoneContactLoad;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCContactInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * this pcOnPhoneContactLoad is used to display a list of logged in user's contacts
 * @author desire.aheza
 */
public class PCContactsFragment extends Fragment implements PCOnPhoneContactLoad {

    // Defines a variable for the search string
    private String mSearchString;
    // Defines the array to hold values that replace the ?
    private String[] mSelectionArgs = { mSearchString };

    /*
    * Defines an array that contains resource ids for the layout views
    * that get the Cursor column contents. The id is pre-defined in
    * the Android framework, so it is prefaced with "android.R.id"
    */
    private final static int[] TO_IDS = {
            R.id.imageView_contact_photo,R.id.textView_contact_name,R.id.textView_contact_number
    };

    // Define a ListView object
    private ListView mContactsList;

    // An adapter that binds the result Cursor to the ListView
    private PCContactsAdapter mContactAdapter;

    //progress bar
    private ProgressBar mProgressBar;

    // Empty public constructor, required by the system
    public PCContactsFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pc_fragment_contacts,container,false);
        return rootView;
    }

    /**
     * once the activity is created, use it to get the listview and adapt data in it
     * @param savedInstanceState
     */
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mProgressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);
        // Gets the ListView from the View list of the parent activity
        mContactsList =
                (ListView) getActivity().findViewById(R.id.listView_contacts);

        mContactAdapter = new PCContactsAdapter(getActivity(),R.layout.pc_contact_listitem,new ArrayList<PCContactInfo>());
        // Sets the adapter for the ListView
        mContactsList.setAdapter(mContactAdapter);
        PCListAllContactsAsync listViewContactsLoader = new PCListAllContactsAsync(this);
        listViewContactsLoader.execute();
    }

    @Override
    public void onContactLoadStarted() {
       mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onContactLoadFinished(List<PCContactInfo> contacts) {
        mProgressBar.setVisibility(View.GONE);
        mContactAdapter.addAll(contacts);
        mContactAdapter.notifyDataSetChanged();
    }

}

