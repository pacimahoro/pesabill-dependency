/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.PCRecommendFriendsActivity;
import com.pesachoice.billpay.activities.PCTakePhotoIdActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles a pcOnPhoneContactLoad that display information on user account, help, report a problem
 * <p/>
 * //Todo: more clean up work need to be done
 *
 * @author Pacifique Mahoro
 * @author Desire Aheza
 */
public class PCUserAccountFragment extends Fragment {

	private Map<String, String> accountFragementTitles = new LinkedHashMap<>();
	private Map<String, Integer> optionItems = new LinkedHashMap<>();
	private List<PCOption> options = new ArrayList<>();
	private PCOptionsAdapter adapter = null;
	private String callingBalance = "loading...";
	private ListView optionsView = null;
	private View userAccountFragmentView;
	private PCMainTabActivity activity = null;

	public PCUserAccountFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		userAccountFragmentView = inflater.inflate(R.layout.pc_fragment_user_account, container, false);
		activity = (PCMainTabActivity) getActivity();
		optionItems.put(PCPesabusClient.OPTION_MOBILE_NUMBER, R.drawable.icn_call_us);
		optionItems.put(PCPesabusClient.OPTION_REFER_FRIENDS, R.drawable.ic_invite_friends);
		optionItems.put(PCPesabusClient.OPTION_TAKE_PHOTO_ID, R.drawable.ic_take_photo_id);
		optionItems.put(PCPesabusClient.OPTION_PAYMENT, R.drawable.icn_product_payment1);
		optionItems.put(PCPesabusClient.OPTION_PRIVACY, R.drawable.icn_action_privacy_info);
		optionItems.put(PCPesabusClient.OPTION_TERMS_OF_SERVICES, R.drawable.icn_privacy_term);
		optionItems.put(PCPesabusClient.OPTION_CONTACT_US, R.drawable.icn_contact_us);
		optionItems.put(PCPesabusClient.OPTION_LOGOUT, R.drawable.icn_logout);
		if (userAccountFragmentView != null) {
			TextView fullName = (TextView) userAccountFragmentView.findViewById(R.id.full_name);
			TextView email = (TextView) userAccountFragmentView.findViewById(R.id.email);
			optionsView = (ListView) userAccountFragmentView.findViewById(R.id.listView_options);
			PCMainTabActivity mainActivity = (PCMainTabActivity) getActivity();
			PCUser user = mainActivity.getAppUser();
			String fName = "";
			if (user != null) {
				fName = user.getFullName();
				TextDrawable drawable1 = TextDrawable.builder()
						.beginConfig()
						.useFont(Typeface.DEFAULT)
						.fontSize(24)
						.toUpperCase()
						.endConfig()
						.buildRound(getInitial(fName), R.color.sectionBackground);
				ImageView profileImage = (ImageView) userAccountFragmentView.findViewById(R.id.account_profile_image);
				profileImage.setImageDrawable(drawable1);
				fullName.setText(StringUtils.capitalize(fName));
				email.setText(user.getEmail());
			}
			setAdater(makeOption(optionItems));
		}

		return userAccountFragmentView;
	}

	protected void setAdater(List<PCOption> options) {
		if (userAccountFragmentView != null) {
			if (optionsView == null) {
				optionsView = (ListView) userAccountFragmentView.findViewById(R.id.listView_options);
			}
			if (options != null) {
				adapter = new PCOptionsAdapter((PCBaseActivity) this.getActivity(), options);
				optionsView.setAdapter(adapter);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (activity == null) {
			activity = (PCMainTabActivity) getActivity();
		}
		PCBillPaymentData.PCPaymentType paymentType = PCBillPaymentData.PCPaymentType.PAYMENT_CARD;
		if (activity != null && activity.getAppUser() != null) {
			PCPaymentInfo pcPaymentInfo = activity.getAppUser().getPaymentInfo();
			if (pcPaymentInfo != null) {
				paymentType = pcPaymentInfo.getPaymentType();
			}
		}
		this.updatePaymentCardInfo(getView(), paymentType);
	}

	public void updatePaymentCardInfo(View view, PCBillPaymentData.PCPaymentType pcPaymentType) {
		if (view == null) {
			return;
		}
		PCMainTabActivity activity = ((PCMainTabActivity) getActivity());
		if (activity != null && this.adapter != null) {
			PCSpecialUser appUser = activity.getAppUser();
			if (appUser != null) {
				PCPaymentInfo pcPaymentInfo = appUser.getPaymentInfo();
				if (PCBillPaymentData.PCPaymentType.MOBILE_MONEY.equals(pcPaymentType)) {
					this.setPaymentInfoNumber(activity.getAccountInfo(pcPaymentType));
				} else {
					this.setPaymentInfoNumber(appUser.getMaskedCardNumber());
				}
			}
		}
	}

	/**
	 * Used to handle the logout feature. It delegates the work to be handled by the correct activity
	 *
	 * @param view Fragment view for user account
	 */
	public void onLogoutAction(View view) {
		PCMainTabActivity parentActivity = (PCMainTabActivity) getActivity();
		if (parentActivity != null) {
			parentActivity.logoutTheUser();
		}
	}

	public void setCallingCreditDetails(PCCallingServiceInfo callingServiceInfo) {
		if (adapter != null) {
			if (options != null) {
				for (int i = 0; i < options.size(); i++) {
					PCOption option = options.get(i);
					if (PCPesabusClient.OPTION_CALLING_CREDIT.equals(option.getOptionName())) {
						DecimalFormat df2 = new DecimalFormat(".##");
						String balanceValue = df2.format(callingServiceInfo.getBalance());
						balanceValue = ".0".equalsIgnoreCase(balanceValue) ? "0" : balanceValue;
						callingBalance = "$" + balanceValue;
						option.setOptionValue(callingBalance);
						options.set(i, option);
						setAdater(options);
						break;
					}
				}
			}
		}
	}


	public void setPaymentInfoNumber(String paymentInfoValue) {
		if (adapter != null && !StringUtils.isEmpty(paymentInfoValue)) {
			if (options != null) {
				for (int i = 0; i < options.size(); i++) {
					PCOption option = options.get(i);
					if (PCPesabusClient.OPTION_PAYMENT.equals(option.getOptionName())) {
						option.setOptionValue(paymentInfoValue);
						options.set(i, option);
						setAdater(options);
						break;
					}
				}
			}
		}
	}

	protected String getInitial(String fullName) {
		String inital = " ";
		try {
			String[] name = StringUtils.split(fullName, " ");
			inital = "" + fullName.charAt(0) + name[1].charAt(0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return inital;
	}

	/**
	 * @param optionNameIconMapping a map of user account optionItems and their icons
	 * @return List<PCOption>
	 * <p/>
	 * TODO: we probably don't need PCOption object as we're using a LinkedHashMap to keep a mapping between an icon and option name
	 * but if we will be displaying more item we might need PCOption
	 */
	public List<PCOption> makeOption(Map<String, Integer> optionNameIconMapping) {
		PCMainTabActivity activity = ((PCMainTabActivity) getActivity());
		PCSpecialUser appUser = activity.getAppUser();
		if (options != null && options.size() > 0) {
			options.clear();
			if (adapter != null) {
				adapter.notifyDataSetChanged();
			}
		}
		for (Map.Entry<String, Integer> entry : optionNameIconMapping.entrySet()) {
			PCOption option = new PCOption();
			String key = entry.getKey();
			if (appUser != null) {
				if (PCPesabusClient.OPTION_MOBILE_NUMBER.equals(key)) {
					option.setOptionValue(appUser.getPhoneNumber());
				} else if (PCPesabusClient.OPTION_PAYMENT.equals(key)) {
					if (StringUtils.isEmpty(appUser.getMaskedCardNumber()) || StringUtils.isEmpty(appUser.getCurrentUserAccount())) {
						option.setOptionValue("Add Card");
					} else {
						option.setOptionValue(appUser.getCurrentUserAccount());
					}
				} else if (PCPesabusClient.OPTION_CALLING_CREDIT.equals(key)) {
					DecimalFormat df2 = new DecimalFormat(".##");
					String balanceValue = appUser.getCallingBalance() == 0.0 ? callingBalance : df2.format(appUser.getCallingBalance());
					option.setOptionValue(balanceValue);
				}
				option.setOptionName(entry.getKey());
				option.setOptionIconName(entry.getValue());
				options.add(option);
			}
		}
		return options;
	}

	public class PCOption {

		String optionName;
		Integer optionIconName;
		String optionValue = "";


		public PCOption() {

		}

		public PCOption(String optionName, Integer optionIconName) {
			this.optionName = optionName;
			this.optionIconName = optionIconName;
		}

		public String getOptionValue() {
			return optionValue;
		}

		public void setOptionValue(String optionValue) {
			this.optionValue = optionValue;
		}

		public Integer getOptionIconName() {
			return optionIconName;
		}

		public void setOptionIconName(Integer optionIconName) {
			this.optionIconName = optionIconName;
		}

		public String getOptionName() {
			return optionName;
		}

		public void setOptionName(String optionName) {
			this.optionName = optionName;
		}

	}

	/**
	 * User account optionItems adapter
	 */
	private class PCOptionsAdapter extends BaseAdapter {
		private PCBaseActivity context = null;
		private LayoutInflater inflater = null;
		private TextView paymentCardField;

		public PCOptionsAdapter(PCBaseActivity context, List<PCOption> pcOptions) {
			this.context = context;
			options = pcOptions;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		/**
		 * How many items are in the data set represented by this Adapter.
		 *
		 * @return Count of items.
		 */
		@Override
		public int getCount() {
			return options.size();
		}

		/**
		 * Get the data item associated with the specified position in the data set.
		 *
		 * @param position Position of the item whose data we want within the adapter's
		 *                 data set.
		 * @return The data at the specified position.
		 */
		@Override
		public Object getItem(int position) {
			return options.get(position);
		}

		/**
		 * Get the row id associated with the specified position in the list.
		 *
		 * @param position The position of the item within the adapter's data set whose row id we want.
		 * @return The id of the item at the specified position.
		 */
		@Override
		public long getItemId(int position) {
			return 0;
		}

		/**
		 * Get a View that displays the data at the specified position in the data set. You can either
		 * create a View manually or inflate it from an XML layout file. When the View is inflated, the
		 * parent View (GridView, ListView...) will apply default layout parameters unless you use
		 * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
		 * to specify a root view and to prevent attachment to the root.
		 *
		 * @param position    The position of the item within the adapter's data set of the item whose view
		 *                    we want.
		 * @param convertView The old view to reuse, if possible. Note: You should check that this view
		 *                    is non-null and of an appropriate type before using. If it is not possible to convert
		 *                    this view to display the correct data, this method can create a new view.
		 *                    Heterogeneous lists can specify their number of view types, so that this View is
		 *                    always of the right type (see {@link #getViewTypeCount()} and
		 *                    {@link #getItemViewType(int)}).
		 * @param parent      The parent that this view will eventually be attached to
		 * @return A View corresponding to the data at the specified position.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			if (rowView == null) {
				rowView = inflater.inflate(R.layout.pc_line_item_option, parent, false);
			}
			final TextView option_name = (TextView) rowView.findViewById(R.id.option_name);
			ImageView optionIcon = (ImageView) rowView.findViewById(R.id.option_icon);
			TextView option_value = (TextView) rowView.findViewById(R.id.option_value);
			final PCOption option = (PCOption) getItem(position);
			option_name.setText(option.getOptionName());
			option_value.setText(option.getOptionValue());
			optionIcon.setImageResource(option.getOptionIconName());

			rowView.setBackgroundColor(getResources().getColor(R.color.sectionBackground));

			if (PCPesabusClient.OPTION_PAYMENT.equals(option_name.getText())) {
				this.paymentCardField = option_value;
			}

			rowView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TextView textView = (TextView) v.findViewById(R.id.option_name);
					String optionName = textView.getText().toString();
					String url = "";

					if (optionName != null) {
						if (optionName == PCPesabusClient.OPTION_LOGOUT) {
							onLogoutAction(null);
						} else if (optionName == PCPesabusClient.OPTION_PAYMENT) {
							//flag to detect if we're calling
							((PCMainTabActivity) getActivity()).isCallFromUserAccountFragment = true;
							PCCardDialogFragment dialogFragment = new PCCardDialogFragment();
							dialogFragment.setCalling_fragment(PCCardDialogFragment.Calling_Fragment.USER_ACCOUNT);
							FragmentManager fm = getActivity().getSupportFragmentManager();
							dialogFragment.show(fm, PCPesabusClient.CARD_DIALOG);
						} else if (optionName == PCPesabusClient.OPTION_MOBILE_NUMBER || optionName == PCPesabusClient.OPTION_CALLING_CREDIT) {
							//TODO: need to figure out what to do if user clicks mobile
						} else if (optionName == PCPesabusClient.OPTION_REFER_FRIENDS) {
							/**
							 * used to show screen to show screen to recommend friends
							 */
							Intent intent = new Intent(getActivity(), PCRecommendFriendsActivity.class);
							intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((PCMainTabActivity) getActivity()).getAppUser());
							startActivity(intent);

						} else if (optionName == PCPesabusClient.OPTION_TAKE_PHOTO_ID) {
							Intent intent = new Intent(getActivity(), PCTakePhotoIdActivity.class);
							intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((PCMainTabActivity) getActivity()).getAppUser());
							startActivity(intent);
						} else if (optionName == PCPesabusClient.OPTION_CONTACT_US) {

							PCHelpContactUsFragment frag = new PCHelpContactUsFragment();
							frag.setTransaction(null);
							FragmentManager fm = context.getSupportFragmentManager();
							frag.show(fm, "Contact us Dialog fragment");
						} else if (optionName == PCPesabusClient.OPTION_PRIVACY || optionName == PCPesabusClient.OPTION_TERMS_OF_SERVICES) {
							Intent intent = new Intent(getActivity(), PCAccountDetailsActivity.class);
							intent.putExtra(PCPesabusClient.OPTION, optionName);
							intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((PCMainTabActivity) getActivity()).getAppUser());
							startActivity(intent);
						}
					}
				}
			});
			return rowView;
		}
	}

}
