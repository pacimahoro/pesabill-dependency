/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */


package com.pesachoice.billpay.fragments;

import android.view.View;

import com.pesachoice.billpay.model.PCData;

import java.util.Collection;

/**
 * used to load necessary data required to setup transanction pcOnPhoneContactLoad
 * Created by desire.aheza on 2/12/2016.
 */
public interface PConRequiredDataLoaded {

    public void onDataLoadStart(View view);
    public void onDataLoadFinished(PCData pcData);
    public void onCollectionDataLoadFinished(Collection<? extends PCData> pcData);
}

