package com.pesachoice.billpay.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pesachoice.billpay.activities.R;

/**
 *
 * A simple {@link Fragment} subclass.
 */
public class PCFragmentCallingCardHelp extends Fragment {


    public PCFragmentCallingCardHelp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        return inflater.inflate(R.layout.pc_fragment_calling_card_help, container, false);
    }

}
