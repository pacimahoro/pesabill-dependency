/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCTransactionStatus;

/**
 * Handles showing a failed transaction.
 * It seems we need to provide more information about failed transaction.
 * We need to give details on how the refund process goes. Maybe have a link to our FAQ page once it's done.
 * But more important have the phone number to call us or email us.
 *
 * @author Pacifique Mahoro
 */
public class PCFailedTransactionFragment extends Fragment implements View.OnClickListener {

	private final static String CLAZZZ = PCCheckoutFragment.class.getName();
	private PCData transaction ;

	public void setTransaction(PCData transaction) {
		this.transaction = transaction;
	}

	public PCData getTransaction() {
		return this.transaction;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.pc_fragment_failed_transaction, container, false);

		TextView errorText = (TextView) view.findViewById(R.id.error_message);
		if (this.transaction != null) {
			String errorMessage = this.transaction.getErrorMessage();
			Log.d(CLAZZZ, errorMessage);
			if (errorText != null && errorMessage != null) {
				errorMessage = this.getFailureReason();
				errorText.setText(errorMessage);
			}
		}

		TextView refundText = (TextView) view.findViewById(R.id.refund);
		TextView statusText = (TextView) view.findViewById(R.id.status);
		Button contact_us_for_support = (Button) view.findViewById(R.id.contact_us_for_support);

		if (statusText != null && transaction instanceof PCTransaction) {
			PCTransactionStatus status = ((PCTransaction) transaction).getTransactionStatus();
			if (status == null) {
				status = PCTransactionStatus.FAILURE;
			}

			statusText.setText(status.toString());
			if (status == PCTransactionStatus.IN_PROGRESS || status == PCTransactionStatus.SUCCESS) {
				statusText.setTextColor(getResources().getColor(R.color.colorPrimary));
				refundText.setVisibility(View.GONE);
			} else {
				statusText.setTextColor(getResources().getColor(R.color.colorSecondary));
			}

		}

		Button done = (Button) view.findViewById(R.id.continue_button);
		if (done != null) {
			done.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onDoneClicked(v);
				}
			});
		}

		if (contact_us_for_support != null) {
			contact_us_for_support.setOnClickListener(this);
		}

		return view;
	}

	protected String getFailureReason() {
		String reason = "";
		String part1 = "Unable to complete transaction. Reason:";
		String part2 = "Please contact us at" + getResources().getString(R.string.contact_us_phone_no_dash)  + ", or send email to" + getResources().getString(R.string.contact_us_email);

		if (this.transaction != null) {
			reason = this.transaction.getErrorMessage();
			if (reason != null) {
				reason = reason.replaceAll(part1, "");
				reason = reason.replaceAll(part2, part2);
			}
		}

		return reason;
	}

	public void onDoneClicked(View v) {
		PCMainTabActivity mainTabActivity = (PCMainTabActivity) getActivity();
		mainTabActivity.goBackToProductsGridView();
	}

	public void onCallClicked(View v) {
		String number = getActivity().getResources().getString(R.string.contact_us_phone_no_dash);
		number = "tel:" + number;
		Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
		startActivity(callIntent);
	}

	public void onEmailClicked(View v) {
		String email = getActivity().getResources().getString(R.string.contact_us_email);
		Long transactionId = null;
		String subject;
		PCTransaction trans = (PCTransaction) this.transaction;

		if (trans != null) {
			transactionId = trans.getTransactionId();
		}

		if (transactionId != null) {
			subject = "Failed Transaction " + transactionId;
		} else {
			subject = "Inquiry on Failed Transaction";
		}

		String body = "I tried a transaction using PesaChoice App and it failed because " + this.getFailureReason() + " \n\nPlease let me know what I need to do to proceed.";

		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", email, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body);
		startActivity(Intent.createChooser(emailIntent, "Send email..."));
	}

	@Override
	public void onClick(View v) {
		if (v != null) {
			if (v.getId() == R.id.contact_us_for_support) {
				PCHelpContactUsFragment frag = new PCHelpContactUsFragment();
				frag.setTransaction(this.getTransaction());
				FragmentManager fm = this.getActivity().getSupportFragmentManager();
				frag.show(fm, "Contact us Dialog fragment");
			}
		}
	}
}
