package com.pesachoice.billpay.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.springframework.util.StringUtils;

import java.text.DecimalFormat;

/**
 * Shows a transaction confirmation dialog
 *
 * @author Pacifique Mahoro
 */
public class PCConfirmationDialogFragment extends DialogFragment implements PCTransactionableFragment {
	private final static String CLAZZZ = PCConfirmationDialogFragment.class.getName();
	private PCBillPaymentData transactionData = new PCBillPaymentData();
	private PCPesabusClient.PCServiceType serviceType;
	private static final DecimalFormat df2 = new DecimalFormat("#.##");


	public PCConfirmationDialogFragment() {
	}

	public void setTransactionData(PCBillPaymentData transactionData) {
		this.transactionData = transactionData;
	}

	public PCBillPaymentData getTransactionData() {
		return this.transactionData;
	}

	public void setServiceType(PCPesabusClient.PCServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public PCPesabusClient.PCServiceType getServiceType() {
		return this.serviceType;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = null;
		if (serviceType == PCPesabusClient.PCServiceType.CALLING_REFIL || serviceType == PCPesabusClient.PCServiceType.TICKETS) {
			view = inflater.inflate(R.layout.pc_fragment_confirmation_kivutel, null);
		} else {
			view = inflater.inflate(R.layout.pc_fragment_confirmation, null);
		}

		this.setViewInfo(view);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getResources().getString(R.string.confirmation_title))
				.setView(view)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						submitTransaction();

					}
				})
				.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Log.d(CLAZZZ, "Clicked on cancel on Alert box");
					}
				});

		return builder.create();
	}


	public void setViewInfo(View view) {
		PCMainTabActivity activity = (PCMainTabActivity) getActivity();
		if (view == null || activity == null || this.getTransactionData() == null) {
			return;
		}
		PCCountryProfile countryProfile = activity.getCountryProfile();
		PCServiceInfo serviceInfo = this.getTransactionData().retrieveServiceInfo();
		PCSpecialUser sender = this.getTransactionData().getSender();

		TextView usdText = (TextView) view.findViewById(R.id.usd_amount);
		TextView feeText = (TextView) view.findViewById(R.id.fee_amount);
		TextView totalText = (TextView) view.findViewById(R.id.total_amount);
		TextView pymentTypeView = (TextView) view.findViewById(R.id.card_number);
		TextView maskedCardNumber = (TextView) view.findViewById(R.id.maskedCardNumber);
		LinearLayout discountView = (LinearLayout) view.findViewById(R.id.discount_view);
		String usd, fee, total, local;
		String currencySymbl = activity.getCurrencySymbol();
		Double payment = 0.0, totalPayment = 0.0;
		PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();
		if (paymentInfo != null && PCBillPaymentData.PCPaymentType.MOBILE_MONEY == paymentInfo.getPaymentType()) {
			PCCurrency currency = transactionData.getBaseCurrency();
			if (currency != null) {
				payment = serviceInfo.getAmount();
				totalPayment = serviceInfo.getTotalAmount();
			}
		} else {
			payment = serviceInfo.getAmount();
			totalPayment = serviceInfo.getTotalAmount();
		}

		// For aesthetics, instead of USD use $
		if ("USD".equalsIgnoreCase(currencySymbl)) {
			currencySymbl = "$";
		}

		usd = currencySymbl + df2.format(payment);
		fee = currencySymbl + df2.format(serviceInfo.getFees());
		total = currencySymbl + df2.format(totalPayment);

		local = df2.format(serviceInfo.getTotalLocalCurrency());

		if (this.getTransactionData() != null) {

			Double discount = this.getTransactionData().getAppliedDiscount();
			Double totalAmount = serviceInfo.getTotalAmount();
			if (discount != null && discount >= 0 && !StringUtils.isEmpty(this.getTransactionData().getPromoCode())) {
				//show discount view
				discountView.setVisibility(View.VISIBLE);
				TextView usdAmountDiscount = (TextView) view.findViewById(R.id.usd_amount_discount);
				String currencySmb = "USD";
				if (sender != null && sender.getBaseCurrency() != null) {
					currencySmb = sender.getBaseCurrency().getCurrencySymbol();
				}
				usdAmountDiscount.setText(this.getTransactionData().getAppliedDiscount() + " " + currencySmb);
				//calculate total value including discount
				total = currencySmb + String.valueOf(getValueAfterDiscount(totalAmount, discount));
			}
		}

		if (countryProfile != null) {
			local = countryProfile.getCurrencySymbol() + " " + local;
		}

		String paymentTypeText = "";
		//TODO: fixme
		if (sender != null && PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS != this.getTransactionData().getPaymentProcessingType()) {
			if (transactionData.retrieveServiceInfo() != null) {
				paymentInfo = transactionData.getPaymentInfo();
				PCBillPaymentData.PCPaymentType paymentType = paymentInfo != null ? paymentInfo.getPaymentType() : null;
				activity.getAppUser().setPaymentType(paymentType);
				if (paymentType != null) {
					paymentTypeText = paymentType.getDisplayName();



				}
			}
		} else if (PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS == this.getTransactionData().getPaymentProcessingType()) {
			paymentTypeText = this.getTransactionData().getReceiver().getMaskedCardNumber();
		}

		usdText.setText(usd);
		//Setup transaction value
		if (serviceType != PCPesabusClient.PCServiceType.CALLING_REFIL && serviceType != PCPesabusClient.PCServiceType.TICKETS) {
			TextView localText = (TextView) view.findViewById(R.id.local_amount);
			localText.setText(local);
		}
		feeText.setText(fee);
		totalText.setText(total);
		pymentTypeView.setText(paymentTypeText);

		if((paymentInfo.getDefaultSource().getCardDetails()) != null ) {
			maskedCardNumber.setText(paymentInfo.getDefaultSource().getCardDetails().getMaskedLocalCard());
		}else {
			maskedCardNumber.setText(paymentInfo.getMaskedCardNumber());
		}
		TextView amountTitleText = (TextView) view.findViewById(R.id.usd_amount_title);
		if (serviceType == PCPesabusClient.PCServiceType.TICKETS && amountTitleText != null) {
			String title = "Amount";
			amountTitleText.setText(title);
		}
	}

	/**
	 * method to calculate discount value
	 *
	 * @param totalUsd total bill price
	 * @param discount total discound
	 * @return value after discound
	 */
	public double getValueAfterDiscount(Double totalUsd, Double discount) {
		return totalUsd.doubleValue() - discount.doubleValue();
	}

	public void submitTransaction() {
		PCMainTabActivity activity = (PCMainTabActivity) getActivity();
		if (activity != null) {
			activity.saveTransaction(this);
		}
	}
}
