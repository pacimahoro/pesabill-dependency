/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCReferralPromotionController;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingPaymentData;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCCardInfo;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCPrice;
import com.pesachoice.billpay.model.PCPromotion;
import com.pesachoice.billpay.model.PCRequestReferralPromotion;
import com.pesachoice.billpay.model.PCServiceFee;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCServiceTypePaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTicketServiceInfo;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCCurrencyExchangeRateUtil;
import com.pesachoice.billpay.utils.PCCurrencyUtil;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCSpinner;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Handles show products grid that a user can choose from.
 *
 * @author Pacifique Mahoro
 * @author Desire AHEZA
 */

public class PCCheckoutFragment extends Fragment implements View.OnClickListener, PCAsyncListener {
    private final static String clazzz = PCCheckoutFragment.class.getName();
    protected static final int CHECKOUT_VERIFY_USER_REQUEST = 701;
    protected static final String PAY_WITH_MOBILE_MONEY = "Pay with MTN,TIGO,AIRTEL Money";
    private static final String REPLACE_PAYMENT_CARD = "Replace Payment Card";
    private static final String ADD_PAYMENT_CARD = "Add Payment Card";
    protected PCBillPaymentData transactionData;
    protected PCSpinner cardNumberText;
    private TextView ticketPrices;
    protected EditText usdAmountText;
    protected EditText localAmountText;
    private EditText promotionalCodeText;
    private EditText mobileMoneyAccount;
    protected TextView senderCurrency;
    protected TextView receiverCurrency;
    protected PCCountryProfile countryProfile;
    protected PCMainTabActivity activity;
    protected int selectedPaymentType = 0;
    public boolean amRepeatingTrans = false;
    private ArrayAdapter<String> spinnerArrayAdapter;
    protected ProgressDialog progressDialog;
    protected TextView selectManyPackages;
    private PCCurrency senderCurr;
    protected TextView viewServiceFees;
    protected ArrayList<String> paymentOptions;
    private PCPesabusClient.PCServiceType currentServiceType;
    private Map<String, String> countryMoneySymbol = new HashMap<>();
    protected PCServiceInfo serviceInfo = null;
    // TODO: Get rid of this serviceType and instead use the transactionData
    protected PCPesabusClient.PCServiceType serviceType;
    private boolean keepChangingText = true;
    private PCAddAddressFragment frag;
    protected Double receiverExch = null;
    protected PCServiceTypePaymentInfo pcServiceTypePaymentInfo = null;
    private boolean isEvent;
    private TextView currencySymbl;
    private PCCustomerMetadata cardDetails;
    protected String userCurrency;
    private boolean isCardSet;
    private PCCardInfo cardInfo;
    private PCServiceFeeDetailsFragment serviceFeeDetailsFragment;

    public PCCheckoutFragment() {
    }

    public void setCardSet(boolean cardSet) {
        isCardSet = cardSet;
    }

    public void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    public PCPesabusClient.PCServiceType getServiceType() {
        return serviceType;
    }

    public void setTransactionData(PCBillPaymentData transactionData) {
        this.transactionData = transactionData;
    }

    public PCBillPaymentData getTransactionData() {
        return this.transactionData;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setRetainInstance(true);
        isCardSet = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_checkout, container, false);

        try {
            promotionalCodeText = (EditText) view.findViewById(R.id.promotion_code);
            activity = (PCMainTabActivity) getActivity();
            countryProfile = activity.getCountryProfile();
            //calculate the exchange rate
            try {
                if (activity != null) {
                    Log.e("eROOT:", "ACTIVITY");
                    senderCurr = activity.getAppUser().getBaseCurrency();

                    if (senderCurr != null) {
                        Log.e("eROOT:", countryProfile.getCountry());
                        if (senderCurr.getCountrySymbol().equalsIgnoreCase("USD") && countryProfile.getCountry().equalsIgnoreCase("zimbabwe")) {
                            countryProfile.setSelectedCountryExchangeRate(1.0);
                        } else {
                            receiverExch = PCCurrencyUtil.calculateExchangeRate(senderCurr, countryProfile.getCountry());
                            //Set the current Exchange Rate in CountryProfile
                            countryProfile.setSelectedCountryExchangeRate(receiverExch);

                        }
                    }
                }

            } catch (PCGenericError pcGenericError) {
                pcGenericError.printStackTrace();
            }
            this.setViewInfo(view);
            if (serviceType != PCPesabusClient.PCServiceType.TICKETS && serviceType != PCPesabusClient.PCServiceType.CALLING_REFIL) {
                setTextWatcherForAmount();
            }
            this.attachClickListeners(view);
            if (amRepeatingTrans) {
                setAmountOfRepeatedTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void setAmountOfRepeatedTransaction() {
        if (transactionData != null) {
            PCServiceInfo serviceInfo = transactionData.retrieveServiceInfo();
            if (serviceInfo != null) {
                double transAmount = serviceInfo.getAmount();
                if (usdAmountText != null) {
                    usdAmountText.setText(transAmount + "");
                }
                if (localAmountText != null) {
                    DecimalFormat format = new DecimalFormat("#.00");
                    localAmountText.setText(format.format(transAmount * receiverExch));
                }
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentActivity activity = getActivity();
        if (activity instanceof PCMainTabActivity) {
            PCMainTabActivity mainTabActivity = (PCMainTabActivity) activity;
            if (mainTabActivity.getCardInfo() != null) {
                cardDetails = new PCCustomerMetadata();
                cardDetails.setCardDetails(mainTabActivity.getCardInfo());
                PCPaymentInfo paymentInfo = new PCPaymentInfo();
                if (cardDetails != null) {
                    //Use card details set for transaction process.
                    paymentInfo.setDefaultSource(cardDetails);
                    paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
                    this.transactionData.setPaymentInfo(paymentInfo);
                    //Flag for skipping select option card validation
                    String localCardText = mainTabActivity.getCardInfo().getMaskedLocalCard();
                    if (!StringUtils.isEmpty(localCardText) && setPaymentOptions(localCardText)) {

                        this.attachPaymentsOptions(cardNumberText, paymentOptions);
                    }
                    isCardSet = true;
                }
            }
        }
        this.updatePaymentCardInfo(getView());
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    public void setViewInfo(View view) {
        try {
            PCBillPaymentData transaction = this.getTransactionData();
            if (view != null && transaction != null) {
                TextView serviceText = (TextView) view.findViewById(R.id.service_summary);
                TextView accountText = (TextView) view.findViewById(R.id.account_summary);
                TextView account_number_text = (TextView) view.findViewById(R.id.account_number_text);
                TextView receiverText = (TextView) view.findViewById(R.id.receiver_summary);
                senderCurrency = (TextView) view.findViewById(R.id.sender_currency);
                receiverCurrency = (TextView) view.findViewById(R.id.receiver_currency);
                selectManyPackages = (TextView) view.findViewById(R.id.select_many_packages);
                viewServiceFees = (TextView) view.findViewById(R.id.open_pricing_help);
                cardNumberText = (PCSpinner) view.findViewById(R.id.card_number);
                List<PCServiceFee> serviceFees = getServiceFees(((PCMainTabActivity) getActivity()).getAppUser());
                //show or hide the link to services fees fragment
                if (activity != null && serviceFees != null && serviceFees.size() > 0) {
                    viewServiceFees.setOnClickListener(this);
                } else {
                    viewServiceFees.setVisibility(View.GONE);
                }

                TextView exchangeRateText = null;

                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    exchangeRateText = (TextView) view.findViewById(R.id.textView_exchange_rate);
                    usdAmountText = (EditText) view.findViewById(R.id.usd_amount);
                    localAmountText = (EditText) view.findViewById(R.id.local_amount);
                }

                String service = PCTransactionFragmentFactory.descriptionForService(this.serviceType);
                String receiver = null;

                PCSpecialUser receiverObj = transaction.getReceiver();

                if (receiverObj != null) {
                    receiver = receiverObj.getFullName() == "null null" ? receiverObj.getPhoneNumber() : receiverObj.getFullName();
                }

                if (serviceInfo == null) {
                    serviceInfo = transaction.retrieveServiceInfo();
                }

                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    PCOperator op;
                    if (this.transactionData.getOperator() != null && this.transactionData.getOperator().getName() != null) {
                        Log.i("TransctionName", this.transactionData.getOperator().getName());
                    }
                    if (countryProfile != null) {

                        PCSpecialUser sender = transaction.getSender();

                        for (PCOperator pc : countryProfile.getOperators()) {
                            Log.i("OperatorName", pc.getName());

                            // we don't need to do currency exchange if user sending money from to the same country,
                            // if sender is using mobile money or if buying calling card.
                            if (PCPesabusClient.PCServiceType.CALLING_REFIL == serviceType
                                    || PCCurrencyExchangeRateUtil.isSenderReceiverInSameCountry(sender, receiverObj)) {
                                LinearLayout exchange_rate_details = (LinearLayout) view.findViewById(R.id.exchange_rate_details);
                                exchange_rate_details.setVisibility(View.GONE);
                                ImageView icon_exchange = (ImageView) view.findViewById(R.id.icon_exchange);
                                icon_exchange.setVisibility(View.GONE);
                                localAmountText.setVisibility(View.GONE);
                                receiverCurrency.setVisibility(View.GONE);
                                if (sender != null && sender.getBaseCurrency() != null) {
                                    senderCurrency.setText(sender.getBaseCurrency().getCurrencySymbol() + " ");
                                }
                            } else if (sender.getBaseCurrency().getCurrencySymbol().equalsIgnoreCase("USD") && countryProfile.getCountry().equalsIgnoreCase("zimbabwe")) {
                                LinearLayout exchange_rate_details = (LinearLayout) view.findViewById(R.id.exchange_rate_details);
                                exchange_rate_details.setVisibility(View.GONE);
                                ImageView icon_exchange = (ImageView) view.findViewById(R.id.icon_exchange);
                                icon_exchange.setVisibility(View.GONE);
                                localAmountText.setVisibility(View.GONE);
                                receiverCurrency.setVisibility(View.GONE);
                                senderCurrency.setText(sender.getBaseCurrency().getCurrencySymbol() + " ");
                            } else if (exchangeRateText != null) {
                                setExchangeRateViews(exchangeRateText);
                            }
                            PCOperator operator = this.transactionData.getOperator();
                            if (operator != null && operator.getName() != null) {
                                if (pc.getName().trim().equalsIgnoreCase(operator.getName().trim())) {
                                    final List<PCPrice> prices = pc.getPrices();
                                    if (prices != null && prices.size() > 1) {
                                        // instead of preselecting item number 1 in the price list, let's show the user different packages
                                        // then have them select what they want
                                        selectManyPackages.setVisibility(View.VISIBLE);
                                        //showListOfPackages(prices);
                                        double val = prices.get(0).getPrice();

                                        int i = 0;
                                        for (PCPrice prc : prices) {
                                            String symbl = prc.getCurrencySymbol();
                                            if (StringUtils.isEmpty(symbl)) {
                                                symbl = getCurrencySymbol();
                                            }
                                            prc.setCurrencySymbol(symbl);
                                            prices.set(i, prc);
                                            i++;
                                        }
                                        //set values in local and receiver views
                                        setLocalAndRecieverAmountView(val);
                                        selectManyPackages.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                        usdAmountText.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } else if (serviceType == PCPesabusClient.PCServiceType.TICKETS) {
                    isEvent = true;
                    LinearLayout paymentLayout = (LinearLayout) view.findViewById(R.id.pricing_layout);
                    LinearLayout ticketsPaymentLayout = (LinearLayout) view.findViewById(R.id.pricing_layout_ticket);
                    paymentLayout.setVisibility(View.GONE);
                    ticketsPaymentLayout.setVisibility(View.VISIBLE);

                    //make list of price
                    final List<PCPrice> prices = new ArrayList<>();

                    String currencySymbol = getCurrencySymbol();
                    for (int i = 1; i < 4; i++) {
                        PCPrice price = new PCPrice();
                        price.setPrice(i * serviceInfo.getAmount());
                        price.setCurrencySymbol(currencySymbol);
                        price.setDescription(i > 1 ? i + " Tickets" : i + " Ticket");
                        prices.add(price);
                    }
                    ticketPrices = (TextView) view.findViewById(R.id.ticket_prices);
                    currencySymbl = (TextView) view.findViewById(R.id.currency_symbol);
                    currencySymbl.setText(currencySymbol);
                    double tcktPrc = prices.get(0).getPrice();
                    ticketPrices.setText("" + tcktPrc);
                    ticketPrices.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showListOfPackages(prices);

                        }
                    });

                }
                if (PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS == transaction.getPaymentProcessingType()) {
                    usdAmountText.setText("" + serviceInfo.getUsd());
                }

                if (accountText != null) {
                    if (serviceInfo != null && serviceInfo.getAccountNumber() != null) {
                        accountText.setText(serviceInfo.getAccountNumber());
                    } else {
                        account_number_text.setVisibility(View.GONE);
                        accountText.setVisibility(View.GONE);
                    }
                }
                if (serviceText != null && service != null) {
                    serviceText.setText(service);
                }
                if (receiverText != null && receiver != null) {
                    receiverText.setText(receiver);
                }

                this.updatePaymentCardInfo(view);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isCountryUs() {
        return activity != null && activity.isCountryUS();
    }

    protected String getCurrencySymbol() {
        String currencySymbol = "$";

        if (!isCountryUs()) {
            if (serviceInfo instanceof PCTicketServiceInfo) {
                currencySymbol = ((PCTicketServiceInfo) serviceInfo).getCurrencySymbl();
            } else if (activity.getAppUser() != null) {
                PCCurrency currency = activity.getAppUser().getBaseCurrency();
                if (currency != null) {
                    currencySymbol = currency.getCurrencySymbol();
                }
            }
        }

        if (StringUtils.isEmpty(currencySymbol)) {
            currencySymbol = "$";
        }

        return currencySymbol;
    }

    protected void setExchangeRateViews(TextView exchangeRateText) {
        Log.w(clazzz, " setExchangeRateViews called: " + exchangeRateText);
        if (senderCurr != null && senderCurrency != null && countryProfile != null) {
            senderCurrency.setText(senderCurr.getCurrencySymbol());
            //setup currency Symbol
            if (receiverCurrency != null) {
                receiverCurrency.setText(countryProfile.getCurrencySymbol());
            }
            if (receiverExch != null && exchangeRateText != null) {
                exchangeRateText.setText(" 1 " + senderCurr.getCurrencySymbol() + " = " + receiverExch + " " + countryProfile.getCurrencySymbol());
            }
        }
    }

    private void setTextWatcherForAmount() {
        usdAmountText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {

                    if (keepChangingText) {
                        keepChangingText = false;
                        String value = usdAmountText.getText().toString();
                        if (!StringUtils.isEmpty(value)) {
                            localAmountText.setText(getLocalAmout(value));
                        } else if (StringUtils.isEmpty(value)) {
                            localAmountText.setText("");
                        }
                    } else {
                        keepChangingText = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        usdAmountText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String value = usdAmountText.getText().toString();
                    value = value.replaceAll(",", "");
                    usdAmountText.setText(value);
                }
            }
        });

        localAmountText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String value = localAmountText.getText().toString();
                    value = value.replaceAll("\\D+", "");
                    localAmountText.setText(value);
                }
            }
        });

        localAmountText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (keepChangingText) {
                        keepChangingText = false;
                        String value = localAmountText.getText().toString();
                        if (!StringUtils.isEmpty(value)) {
                            String formatted = getFormattedTransAmount(value);
                            usdAmountText.setText(formatted);
                        } else if (StringUtils.isEmpty(value)) {
                            usdAmountText.setText("");
                        }
                    } else {
                        keepChangingText = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @NonNull
    private NumberFormat getNumberFormat(int value) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setMaximumFractionDigits(value);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) formatter).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) formatter).setDecimalFormatSymbols(decimalFormatSymbols);
        return formatter;
    }


    private String getFormattedTransAmount(String val) {
        int scale = activity.isCountryUS() ? 2 : 0;
        BigDecimal amount = (new BigDecimal(val)).divide(new BigDecimal(receiverExch), scale, RoundingMode.HALF_DOWN);
        return amount.toString();
    }

    /**
     * round to two decimal
     *
     * @param value
     * @return
     */
    public static double round(double value) {

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void updatePaymentCardInfo(View view) {
        if (view == null) {
            return;
        }

        PCSpecialUser user;
        PCBillPaymentData transaction = this.getTransactionData();
        if (transaction != null && PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS == transaction.getPaymentProcessingType()) {
            user = this.getTransactionData().getReceiver();
        } else {
            user = ((PCMainTabActivity) getActivity()).getAppUser();
        }
        if (isCardSet) {
            cardNumberText.setSelection(1);
        } else {
            Log.e("setCardNumber", "Card Number Text");
            setCardNumber(user);
        }

    }

    /**
     * Set card number method changed from void method to boolean for tests purpose.
     *
     * @param user the user
     * @return the card number
     */
    public boolean setCardNumber(PCSpecialUser user) {
        boolean msg = false;
        if (cardNumberText == null && getView() != null) {
            cardNumberText = (PCSpinner) getView().findViewById(R.id.card_number);

        }

        /**
         * Fixing repeat transaction
         *
         */
        if (user != null && user.getMaskedCardNumber() != null) {

            /**
             * check if the user has a credit/debit card that they can use to refil their cards
             * not that we not supposed to use the card that is bundled to {@link PCUser} object, we have to
             * verify if the user has a debit/credit card that they can use to refil by {@code PCCallingServiceInfo.maskedCardNumber} of {@link PCCallingServiceInfo}
             *
             */

            String cardText = null;
            if (transactionData != null && transactionData.getPaymentProcessingType() == PCActivity.PCPaymentProcessingType.CALLING) {
                PCCallingServiceInfo serviceInfo = ((PCMainTabActivity) getActivity()).getCallingServiceInfo();
                if (serviceInfo != null && !StringUtils.isEmpty(serviceInfo.getMaskedCardNumber())) {
                    cardText = serviceInfo.getMaskedCardNumber();
                }
            }

            if (StringUtils.isEmpty(cardText)) {
                cardText = user.getMaskedCardNumber();
            }

            //set payments options
            if (setPaymentOptions(cardText) && cardNumberText != null) {
                cardNumberText.setSelection(1);
                if (transactionData != null && user != null) {
                    PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();
                    // Get the paymentInfo from appUser.
                    if (paymentInfo == null) {
                        paymentInfo = user.getPaymentInfo();
                    }
                    if (paymentInfo != null) {
                        paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
                    }
                }
            }

            cardNumberText.invalidate();
            msg = true;
        }

        return msg;
    }

    private void openAddAddressFragment() {

        if (frag == null || (frag != null && frag.getServiceType() == null)) {
            frag = new PCAddAddressFragment();
            frag.setTransactionData(this.getTransactionData());
            frag.setServiceType(this.getServiceType());
            FragmentManager fm = this.getActivity().getSupportFragmentManager();
            frag.show(fm, "Transaction Confirmation Dialog");
        }

    }

    protected void attachClickListeners(View view) {
        if (view != null) {

            Button submitButton = (Button) view.findViewById(R.id.continue_button);
            attachPaymentsOptions(cardNumberText, paymentOptions);
            cardNumberText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.w("PCCheckoutFragment : ", "Card Number Item selected: " + position + " id: " + id);
                    selectedPaymentType = position;
                    if (paymentOptions != null) {
                        String selectedOption = paymentOptions.get(position);

                        PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();
                        PCSpecialUser user = (PCSpecialUser) ((PCMainTabActivity) getActivity()).getAppUser();
                        userCurrency = user.getBaseCurrency() != null ? user.getBaseCurrency().getCountrySymbol() : null;
                        if (paymentInfo == null) {
                            paymentInfo = user != null && user.getPaymentInfo() != null ? user.getPaymentInfo() : new PCPaymentInfo();
                        }
                        transactionData.setPaymentInfo(paymentInfo);
                        //open dialog to input MM account Number
                        if (PAY_WITH_MOBILE_MONEY.equalsIgnoreCase(selectedOption)) {
                            showDialogToInputMobileMoneyAccount();
                            paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
                            transactionData.setPaymentInfo(paymentInfo);
                        } else if (REPLACE_PAYMENT_CARD.equalsIgnoreCase(selectedOption) || ADD_PAYMENT_CARD.equalsIgnoreCase(selectedOption)) {
                            paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
                            showCardDialog();
                        } else if (!StringUtils.isEmpty(selectedOption)) {
                            if (selectedOption.equalsIgnoreCase(user.getMaskedCardNumber())) {
                                paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
                            } else if (selectedOption.equalsIgnoreCase(activity.getAccountInfo(PCBillPaymentData.PCPaymentType.MOBILE_MONEY))) {
                                paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if (submitButton != null) {
                submitButton.setOnClickListener(this);
            }
        }
    }

    protected void showDialogToInputMobileMoneyAccount() {
        //BEING USED TO TRACK THE PAYMENT TYPE
        if (transactionData.getPaymentInfo() != null) {
            transactionData.getPaymentInfo().setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
        }
        //show dialog to input Mobile money account number
        createAndShowDialog();
    }

    private void createAndShowDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(activity);
        dialog.setTitle(PAY_WITH_MOBILE_MONEY);
        dialog.setContentView(R.layout.pc_user_mobile_money_dialog);
        mobileMoneyAccount = (EditText) dialog.findViewById(R.id.mobile_money_account);
        if (activity != null) {
            try {
                populateMobileMoneyDialog(mobileMoneyAccount, activity);
            } catch (PCGenericError err) {

            }
        }

        Button dialogButton = (Button) dialog.findViewById(R.id.button_submit_mm_account);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isEmpty(mobileMoneyAccount.getText().toString())) {
                    mobileMoneyAccount.setError("enter mobile money account number");
                } else {
                    int i = 0;
                    PCSpecialUser specialUser = activity.getAppUser();
                    String mmAccount = mobileMoneyAccount.getText().toString();
                    for (String paymentopt : paymentOptions) {
                        if (PAY_WITH_MOBILE_MONEY.equalsIgnoreCase(paymentopt)) {
                            paymentOptions.set(i, mmAccount);
                            specialUser.setHasMobileMoneyAccount(true);
                            break;
                        }
                        i++;
                    }
                    PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();
                    if (paymentInfo != null) {
                        paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
                    }
                    if (activity != null) {

                        if (specialUser != null) {
                            mmAccount = PCGeneralUtils.cleanPhoneString(mmAccount);
                            if (StringUtils.isEmpty(mmAccount) && !PCGeneralUtils.isNumeric(mmAccount)) {
                                mobileMoneyAccount.setError("Mobile Money Account is Invalid");

                            } else {
                                activity.setMobMoneyAccountToUser(mmAccount);
                                if (activity.pcUserAccountFragment != null) {
                                    activity.pcUserAccountFragment.setPaymentInfoNumber(mmAccount);
                                }
                            }
                        }
                    }
                    attachPaymentsOptions(cardNumberText, paymentOptions);
                    cardNumberText.setSelection(i);

                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }

    // To be overridden is necessary
    public void populateMobileMoneyDialog(EditText mobileMoneyAccount, PCMainTabActivity activity) throws PCGenericError {
        if (activity != null && mobileMoneyAccount != null) {
            mobileMoneyAccount.setText(activity.getAccountInfo(PCBillPaymentData.PCPaymentType.MOBILE_MONEY));
        }
    }

    public void attachPaymentsOptions(Spinner providerSpinner, ArrayList<String> paymentOptions) {
        Log.e("PCCheckoutFragment: ", "Entry of attachPaymentsOptions");
        Log.e("ProviderSpinner => ", "" + providerSpinner);
        Log.e("CardNumberText: ", "" + cardNumberText);
        if (providerSpinner != null && paymentOptions != null && paymentOptions.size() > 0) {
            Log.e("PCCheckoutFragment", paymentOptions.toString());
            String[] operatorNames = paymentOptions.toArray(new String[paymentOptions.size()]);
            spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.pc_line_item_spinner, operatorNames);
            providerSpinner.setAdapter(spinnerArrayAdapter);
            if (selectedPaymentType >= 0) {
                cardNumberText.setSelection(selectedPaymentType, false);
            }
            spinnerArrayAdapter.notifyDataSetChanged();
        }
    }

    protected boolean setPaymentOptions(String maskedPaymentCard) {
        boolean paymentSet = false;
        paymentOptions = new ArrayList<>();
        paymentOptions.add("Select Payment option");
        PCSpecialUser appUser = ((PCMainTabActivity) activity).getAppUser();
        Boolean isMobileMoneyAllowed = false;
        if (appUser != null) {
            String country = appUser.getCountry();
            // For now the option to pay with mobile money will be for Rwanda or Uganda or Authorize test accounts.
            isMobileMoneyAllowed = "UGANDA".equalsIgnoreCase(country) || "RWANDA".equalsIgnoreCase(country) || "RW".equalsIgnoreCase(country) ||
                    "UG".equalsIgnoreCase(country) || isAuthorizedTestAccount();
        }

        if (!StringUtils.isEmpty(maskedPaymentCard)) {
            paymentOptions.add(maskedPaymentCard);
            if (isMobileMoneyAllowed) {
                paymentOptions.add(PAY_WITH_MOBILE_MONEY);
            }
            paymentOptions.add(REPLACE_PAYMENT_CARD);
            paymentSet = true;

        } else {
            paymentOptions.add(ADD_PAYMENT_CARD);
            if (isMobileMoneyAllowed) {
                paymentOptions.add(PAY_WITH_MOBILE_MONEY);
            }
        }
        return paymentSet;
    }

    @Override
    public void onClick(View v) {
        View view = getView();
        if (view != null) {
            Button submitButton = (Button) view.findViewById(R.id.continue_button);

            //check if we want to add a refil credit card
            if (transactionData != null && transactionData.getPaymentProcessingType() == PCActivity.PCPaymentProcessingType.CALLING) {
                ((PCMainTabActivity) getActivity()).isAddingCallingCreditCard = true;

            } else {
                ((PCMainTabActivity) getActivity()).isAddingCallingCreditCard = false;
            }

            if (v.getId() == R.id.open_pricing_help) {
                showServiceFeeInfoFragment();
            }

            if (v == submitButton) {

                this.onSubmit();

            }
        }
    }

    private void showPayWithMobileMoneyAccountBecausePaymentIsLessThan5Dollar() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setTitle("Please use your Mobile money");
        builder1.setMessage("Use your Mobile money account. Your payment is less than 5$, which is the minimum allowed to pay using payment card");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void showCardDialog() {
        PCMainTabActivity activity = (PCMainTabActivity) getActivity();
        // NOTE: Make sure if we set the callingServiceInfo if this is calling.
        // FIXME: This is not the best way to do it but it will fix the bug we have now.
        // A better approach would be to make sure the main activity has a reference to the current transaction object
        if (transactionData instanceof PCCallingPaymentData) {
            activity.setCallingServiceInfo(((PCCallingPaymentData) transactionData).getCallingServiceInfo());
        }
        //show add card fragment
        showCardFragment();
    }

    public void showCardFragment() {
        PCCardPaymentFragment frag = new PCCardPaymentFragment();
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.grid_container_country, frag)
                .addToBackStack(null)
                .commit();

    }

    /**
     * add unique ID to transaction
     */
    private void setUpUniqueId() {
        try {
            if (activity != null) {
                String uniqueId = activity.getUserTrackingKey();
                transactionData.setUniqueID(uniqueId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    protected void onSubmit() {

        try {
            this.setAndValidateInputs();
            this.dismissKeyboard();
            activity = ((PCMainTabActivity) getActivity());
            PCSpecialUser logedInUser = activity.getAppUser();
            if (logedInUser != null && logedInUser.isAuthenticated() && transactionData != null) {

                //set transaction currency
                transactionData.setBaseCurrency(logedInUser.getBaseCurrency());
                //defaultSource { cardDetails {}}
                //setup user tracking key on this transaction
                setUpUniqueId();

                //TODO: Make call to submit promotional code
                if (promotionalCodeText != null && !TextUtils.isEmpty(promotionalCodeText.getText().toString().trim())) {
                    PCReferralPromotionController promotionController = (PCReferralPromotionController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.REFERRAL_PROMOTION_CONTROLLER, this);
                    currentServiceType = PCPesabusClient.PCServiceType.VERIFY_PROMOTION_CODE;
                    if (promotionController != null) {
                        promotionController.setServiceType(currentServiceType);
                        PCRequestReferralPromotion request = new PCRequestReferralPromotion();
                        if (!StringUtils.isEmpty(logedInUser.getEmail())) {
                            request.setEmail(logedInUser.getEmail());
                        } else {
                            request.setEmail(logedInUser.getPhoneNumber());
                        }

                        if (promotionalCodeText != null) {
                            request.setPromoCode("" + promotionalCodeText.getText());
                        }
                        request.setCountry(senderCurr.getCountry());
                        request.setTokenId(logedInUser.getTokenId());
                        request.setAmount(this.getInputAmount().toString());
                        request.setCurrency(senderCurr);
                        promotionController.setRequest(request);
                        promotionController.execute(request);
                    }
                } else {
                    transactionData.setPromoCode("");
                    transactionData.setAppliedDiscount(0.0);
                    showConfirmationDialog();
                }
            } else {
                Intent intent = new Intent(this.getActivity(), PCLoginActivity.class);
                intent.putExtra("PCCHECOUT", "PCCHEOUT");
                startActivityForResult(intent, CHECKOUT_VERIFY_USER_REQUEST);
            }


        } catch (Throwable exc) {
            exc.printStackTrace();
            Log.e(clazzz, "Could not handle Checkout process because [" + exc.getMessage() + "]");
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            PCGenericError error = new PCGenericError();
            if (exc instanceof PCGenericError) {
                error = (PCGenericError) exc;
                error.setNeedToGoBack(false);
            } else {
                error.setMessage(exc.getMessage());
            }
            ((PCMainTabActivity) getActivity()).presentError(error, "Error during checkout");
        }
    }

    public void showConfirmationDialog() {
        //set selected payments
        PCBillPaymentData.PCPaymentType paymentType = this.transactionData.getPaymentInfo() != null ?
                this.transactionData.getPaymentInfo().getPaymentType() :
                PCBillPaymentData.PCPaymentType.PAYMENT_CARD;

        PCConfirmationDialogFragment dialogFragment = new PCConfirmationDialogFragment();
        dialogFragment.setTransactionData(this.getTransactionData());
        dialogFragment.setServiceType(this.getServiceType());
        FragmentManager fm = getActivity().getSupportFragmentManager();
        dialogFragment.show(fm, "Transaction Confirmation Dialog");
    }

    private void showServiceFeeInfoFragment() {
        //set selected payments
        PCBillPaymentData.PCPaymentType paymentType = this.transactionData.getPaymentInfo() != null ?
                this.transactionData.getPaymentInfo().getPaymentType() :
                PCBillPaymentData.PCPaymentType.PAYMENT_CARD;

        if (activity != null) {
            PCServiceFeeDetailsFragment frag = new PCServiceFeeDetailsFragment();
            frag.setSelectedPaymentType(paymentType);
            String processingType = transactionData.getPaymentProcessingType().toString();
            frag.setSelectedServiceType(processingType);
            PCSpecialUser user = activity.getAppUser();
            PCOperatorRequest companyRequest = activity.getPcOperatorRequest(countryProfile.getCountry());
            if (companyRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
                frag.setCurrency("RWF");
                frag.setServiceFees(countryProfile.getServiceFees());
            } else {
                frag.setCurrency(user.getBaseCurrency().getCurrencySymbol());
                frag.setServiceFees(getServiceFees(user));
            }
            FragmentManager fm = getActivity().getSupportFragmentManager();
            if (serviceType.equals(PCPesabusClient.PCServiceType.AGENT_TRANSACTION)) {
                fm.beginTransaction()
                        .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_calling_card_help))
                        .addToBackStack(null)
                        .commit();
            } else {
                fm.beginTransaction()
                        .replace(R.id.grid_container_country, frag, Integer.toString(R.layout.pc_fragment_calling_card_help))
                        .addToBackStack(null)
                        .commit();

            }
        }
    }


    public void dismissKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = getView();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }


    public void setAndValidateInputs() throws PCGenericError {
        View view = getView();
        PCGenericError error;

        if (view != null && transactionData != null) {
            Double usdAmount = this.getInputAmount();
            PCCurrencyUtil.AmountInput amountInput = new PCCurrencyUtil().new AmountInput();
            amountInput.setValue(usdAmount);
            String serviceType = PCData.getActionType(transactionData.getPaymentProcessingType());
            PCBillPaymentData.PCPaymentType paymentType = transactionData.getPaymentInfo() != null ? transactionData.getPaymentInfo().getPaymentType() : null;
            amountInput.setServiceType(serviceType);
            amountInput.setPaymentType(paymentType);

            PCSpecialUser sender = ((PCMainTabActivity) getActivity()).getAppUser();

            List<PCServiceFee> serviceFees = getServiceFees(sender);

            pcServiceTypePaymentInfo = PCCurrencyUtil.determineServiceFee(amountInput, serviceFees);
            // Set more variables.
            pcServiceTypePaymentInfo.setCurrencySymbol(senderCurr.getCurrencySymbol());
            pcServiceTypePaymentInfo.setServiceTypeName(serviceType);
            pcServiceTypePaymentInfo.setExchangeRate(countryProfile.getExchangeRate());
            pcServiceTypePaymentInfo.setReceiverCurrencySymbl(countryProfile.getCurrencySymbol());
            amountInput.setServiceTypePaymentInfo(pcServiceTypePaymentInfo);

            amountInput.setBaseCurrency(senderCurr);
            amountInput.setAuthorizedTestAccount(isAuthorizedTestAccount());
            // Validate input
            PCCurrencyUtil.validateAmount(amountInput);

            if (!isCardSet && (cardNumberText == null || "Select Payment option".equalsIgnoreCase(cardNumberText.getSelectedItem().toString())
                    || ADD_PAYMENT_CARD.equalsIgnoreCase(cardNumberText.getSelectedItem().toString())
                    || "Change payment option".equalsIgnoreCase(cardNumberText.getSelectedItem().toString()))) {
                error = new PCGenericError();
                error.setMessage("Please Select Payment Option");
                throw error;
            }

            this.populateTransaction(usdAmount);

            if (sender != null) {
                String selectedOption = cardNumberText.getSelectedItem().toString();
                String card = sender.getMaskedCardNumber();
                //set payment info
                setTransactionPaymentInfo(paymentType, sender, card);

                this.transactionData.setMaskedCardNumber(card);

                serviceInfo.setMaskedCardNumber(card);
            }
        }
    }

    private void setIfItemInReceiverCountry(String serviceType) {
        if (PCData.ActionType.TICKETS.toString().equals(serviceType) && pcServiceTypePaymentInfo != null) {

        }
    }

    protected void setTransactionPaymentInfo(PCBillPaymentData.PCPaymentType pcPaymentType, PCSpecialUser sender, String card) {
        PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();
        if (paymentInfo == null) {
            paymentInfo = new PCPaymentInfo();
        }
        PCCustomerMetadata customerMetadata = new PCCustomerMetadata();
        if (paymentInfo != null && paymentInfo.getDefaultSource() != null) {
            customerMetadata = paymentInfo.getDefaultSource();
        }
        customerMetadata.setEmail(sender.getEmail());
        if (pcPaymentType != null && pcPaymentType.getPaymentType().equalsIgnoreCase(PCBillPaymentData.PCPaymentType.MOBILE_MONEY.toString())) {
            String mmAccount = mobileMoneyAccount != null ? mobileMoneyAccount.getText().toString() : null;
            customerMetadata.setPhoneNumber(mmAccount);
        } else if (pcPaymentType != null && pcPaymentType.getPaymentType().equalsIgnoreCase(PCBillPaymentData.PCPaymentType.PAYMENT_CARD.toString())) {
            customerMetadata.setMaskedCardNumber(card);
        }
        paymentInfo.setDefaultSource(customerMetadata);
        transactionData.setPaymentInfo(paymentInfo);
    }

    @Nullable
    public List<PCServiceFee> getServiceFees(PCSpecialUser sender) {
        List<PCServiceFee> serviceFees;
        // NOTE: when the customer doing the transaction is in the same country as the recipient
        // we will charge/use local fees. Local fees are returned in the country profile for each country
        // However, if the recipient and the sender are in different country, we will use our cross border fees.
        if (PCCurrencyExchangeRateUtil.isSenderReceiverInSameCountry(sender, transactionData.getReceiver())) {
            serviceFees = countryProfile.getServiceFees();
        } else {
            PCCurrency currency = sender != null ? sender.getBaseCurrency() : null;
            serviceFees = currency != null ? currency.getCrossBorderFees() : null;
        }
        return serviceFees;
    }

    public boolean isAuthorizedTestAccount() {
        PCMainTabActivity activity = (PCMainTabActivity) getActivity();
        Boolean isAuthorizedTestAccount = false;
        if (activity != null) {
            PCSpecialUser appUser = activity.getAppUser();
            PCPingResults pingData = ((PCMainTabActivity) getActivity()).getApiKey();
            List<String> authorizedTestAccounts = (pingData != null && pingData.getAuthorizedTestEmails() != null) ? pingData.getAuthorizedTestEmails() : null;
            isAuthorizedTestAccount = false;
            if (authorizedTestAccounts != null) {
                for (String account : authorizedTestAccounts) {
                    if (account != null && account.equalsIgnoreCase(appUser.getEmail())) {
                        isAuthorizedTestAccount = true;
                        break;
                    }
                }
            }
        }
        return isAuthorizedTestAccount;
    }

    protected void populateTransaction(Double amount) {
        PCServiceInfo serviceInfo = this.transactionData.retrieveServiceInfo();
        if (countryProfile != null) {
            Double localAmount = countryProfile.convertToLocalUsingCalculatedExchangeRate(amount);
            Double fee = null;
            if (!PCActivity.PCPaymentProcessingType.CALLING.equals(transactionData.getPaymentProcessingType())) {
                if (pcServiceTypePaymentInfo != null) {
                    BigDecimal feeBig = pcServiceTypePaymentInfo.getServiceFee();
                    if (feeBig != null) {
                        fee = feeBig.doubleValue();
                    }
                }

                if (fee == null || fee == 0.0) {
                    fee = countryProfile.computeFeeAmount(amount);
                }

            } else {
                //keep fees as zero when you're buying calling credit.
                fee = 0.0;
            }
            if (!localAmount.isNaN()) {

                //TODO: discuss with the team before release
                try {
                    localAmount = Double.valueOf(localAmountText.getText().toString().trim());
                } catch (Exception e) {

                }
                if (serviceInfo != null) {
                    Log.e("Localll:", "LocalAmountttt");
                    serviceInfo.setTotalLocalCurrency(localAmount);
                    //transactionData.setTotalLocalCurrency(localAmount);
                }
            }
            if (!fee.isNaN()) {
                serviceInfo.setFees(fee);
            }

            //set the currency object to support local transactions
            PCSpecialUser user = activity.getAppUser();
            if (user != null) {
                PCCurrency currency = user.getBaseCurrency();
                if (currency == null) {
                    currency = new PCCurrency();
                }
                if (StringUtils.isEmpty(currency.getCountry())) {
                    currency.setCountry(user.getCountry());
                }
                Log.i(clazzz, "Exchange Rate: " + receiverExch);
                currency.setExchangeRate("" + receiverExch);
                this.transactionData.setBaseCurrency(currency);
                serviceInfo.setAmount(amount);
                serviceInfo.setTotalAmount(amount + fee);
                //set transaction usd amount
                if (currency != null && "USD".equalsIgnoreCase(currency.getCurrencySymbol())) {
                    serviceInfo.setUsd(getUsdAmount(amount));
                    double totalAmount = amount + fee;
                    serviceInfo.setTotalUsd(getUsdAmount(totalAmount));
                    //this.transactionData.setTotalUsd(getUsdAmount(totalAmount));
                }
            }
        }
    }

    protected Double getUsdAmount(double amount) {
        double usdAmount = 0.0;
        if (countryProfile != null) {
            double usdExch = countryProfile.getExchangeRate();
            usdAmount = activity.isCountryUS() ? amount : amount / usdExch;
            DecimalFormat format = new DecimalFormat("#.00");
            usdAmount = Double.valueOf(format.format(usdAmount));
        }
        return usdAmount;
    }

    protected Double getInputAmount() throws PCGenericError {
        Double usdAmount = null;
        PCGenericError error = new PCGenericError();
        String currencySymbol = "USD";
        if (senderCurr != null && !StringUtils.isEmpty(senderCurr.getCurrencySymbol())) {
            currencySymbol = senderCurr.getCurrencySymbol();
        }

        try {
            if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                if (usdAmountText == null || usdAmountText.getText().toString().length() == 0) {
                    error.setMessage("Please enter the amount (" + currencySymbol + ") for this transaction");
                    throw error;
                }
                usdAmount = getAmountInDouble(usdAmountText.getText().toString());
            } else {
                String amount = ticketPrices.getText().toString().trim();
                usdAmount = getAmountInDouble(amount);
            }
        } catch (Exception exc) {
            if (exc instanceof PCGenericError) {
                throw exc;
            }
            error.setMessage("Error when parsing amount");
            throw error;
        }

        return usdAmount;
    }

    private double getAmountInDouble(String amount) throws PCGenericError {
        PCGenericError error = new PCGenericError();
        Double trAmount = null;
        try {
            NumberFormat numberFormat = NumberFormat.getNumberInstance();
            Number transAmount = numberFormat.parse(amount);
            trAmount = transAmount.doubleValue();
        } catch (Exception e) {
            error.setMessage("Error in parsing amount");
            throw error;
        }
        return trAmount;
    }

    private String formartPriceInDollar(double priceInLocal) {
        int scale = activity.isCountryUS() ? 2 : 0;
        BigDecimal amount = new BigDecimal(priceInLocal * 100).divide(new BigDecimal(countryProfile.getExchangeRate() * 100), scale, RoundingMode.HALF_DOWN);
        return amount.toString();
    }

    protected void showListOfPackages(List<PCPrice> prices) {
        activity = (PCMainTabActivity) getActivity();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setTitle("Select Package");
        final PackageAdapter arrayAdapter = new PackageAdapter(activity, R.layout.pc_two_row_layout, prices);

        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PCPrice price = (PCPrice) arrayAdapter.getItem(which);
                        //TODO: need to set quantity of purchased tickets here
                        if (serviceType == PCPesabusClient.PCServiceType.TICKETS) {
                            isEvent = true;
                            String currencySymbol = getCurrencySymbol();
                            currencySymbl.setText(currencySymbol);
                            ticketPrices.setText("" + price.getPrice());
                            String ticketPrice = ticketPrices.getText().toString().trim();
                            double selectedPrice = Double.parseDouble(ticketPrice);
                            double initialPrice = arrayAdapter.getItem(0).getPrice();
                            int quantity = (int) Math.round(selectedPrice / initialPrice);
                            ((PCTicketServiceInfo) getTransactionData().retrieveServiceInfo()).setQuantity(quantity);
                        } else {
                            double val = price.getPrice();
                            setLocalAndRecieverAmountView(val);
                        }
                        //set the id of the selected price in the service info
                        getTransactionData().retrieveServiceInfo().setId(price.getId());
                        dialog.dismiss();

                    }
                });
        builderSingle.show();
    }

    protected void setLocalAndRecieverAmountView(double val) {
        if (usdAmountText != null) {
            usdAmountText.setText(isCountryUs() ? formartPriceInDollar(val) : val + "");
        }

    }

    private String getLocalAmout(String val) {
        BigDecimal amount = (new BigDecimal(val).multiply(new BigDecimal(receiverExch)));
        return amount.setScale(0, RoundingMode.HALF_DOWN).toString();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHECKOUT_VERIFY_USER_REQUEST:
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    //make sure that the logged is user is the one who will make the transanction
                    if (data != null) {
                        Bundle bundle = data.getExtras();
                        if (bundle != null) {
                            {
                                Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
                                if (objBundle != null) {
                                    PCSpecialUser currentUser = (PCSpecialUser) objBundle;
                                    ((PCMainTabActivity) getActivity()).setAppUser(currentUser);
                                    this.transactionData.setSender(currentUser);
                                    setCardNumber(currentUser);
                                } else {
                                    ((PCMainTabActivity) getActivity()).logoutTheUser();
                                }
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskStarted() {
        if (PCPesabusClient.PCServiceType.VERIFY_PROMOTION_CODE.equals(currentServiceType)) {
            progressDialog = ProgressDialog.show(this.getActivity(), "Validating promotion code", "Promotion Code", true);
        }

    }

    @Override
    public void onTaskCompleted(PCData data) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (data != null && data.getErrorMessage() != null && data.getErrorMessage().length() > 0 && activity != null) {

            PCGenericError error = new PCGenericError();
            error.setMessage(data.getErrorMessage());
            activity.presentError(error, "Error");
            return;
        } else if (data != null) {
            //set appliedDiscount and promocode in the transaction
            if (transactionData != null) {
                PCPromotion promo = (PCPromotion) data;
                transactionData.setPromoCode(promo.getPromoCode());
                transactionData.setAppliedDiscount(promo.getAppliedDiscount());
            }
            showConfirmationDialog();
        }

    }

    public class PackageAdapter extends ArrayAdapter<PCPrice> {
        Context mContext;
        List<PCPrice> prices;

        public PackageAdapter(Context context, int textViewResourceId, List<PCPrice> prices) {
            super(context, textViewResourceId, prices);
            this.mContext = context;
            this.prices = prices;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            // assign the view we are converting to a local variable
            View v = convertView;

            // first check to see if the view is null. if so, we have to inflate it.
            // to inflate it basically means to render, or show, the view.
            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.pc_two_row_layout, null);
            }

            PCPrice i = prices.get(position);

            if (i != null) {
                TextView package_description = (TextView) v.findViewById(R.id.package_description);
                TextView package_price = (TextView) v.findViewById(R.id.package_price);
                package_description.setText(i.getDescription());

                String currencySymbol = i.getCurrencySymbol();
                if (StringUtils.isEmpty(currencySymbol) || isCountryUs()) {
                    currencySymbol = "$";
                }
                package_price.setText(currencySymbol + " " + ((serviceType != PCPesabusClient.PCServiceType.TICKETS) && (isCountryUs()) ? formartPriceInDollar(i.getPrice()) : i.getPrice()));
            }
            return v;
        }
    }

}
