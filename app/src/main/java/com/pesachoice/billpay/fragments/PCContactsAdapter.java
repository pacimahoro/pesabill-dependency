package com.pesachoice.billpay.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCContactInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter of list of contacts
 * Created by desire.aheza on 1/17/2016.
 */
public class PCContactsAdapter extends ArrayAdapter<PCContactInfo> {
    private Context context;
    private List<PCContactInfo> contactInfos;
    private List<PCContactInfo> contactInfosOriginal;
    private LayoutInflater inflater = null;
    private ContactFilter filter;
    private SparseBooleanArray mSelectedItemsIds;
    // declare the builder object once..withBorder(4)
    private TextDrawable.IBuilder builder = TextDrawable.builder()
            .beginConfig()
            .useFont(Typeface.DEFAULT)
            .fontSize(30)
            .toUpperCase()
            .endConfig()
            .round();

    public PCContactsAdapter(Context context, int textViewResourceId, List<PCContactInfo> contactInfos) {
        super(context,textViewResourceId,contactInfos);
        this.context = context;
        this.contactInfos = contactInfos;
        this.mSelectedItemsIds = new SparseBooleanArray();
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void add(PCContactInfo object)
    {
        if (contactInfosOriginal != null)
            contactInfosOriginal.add(object);
        else
            contactInfos.add(object);
    }

    @Override
    public void remove(PCContactInfo object)
    {
        if (contactInfos != null)
            contactInfosOriginal.remove(object);
        else
            contactInfos.remove(object);
        notifyDataSetChanged();
    }
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    @Override
    public int getCount()
    {
        return contactInfos.size();
    }

    @Override
    public PCContactInfo getItem(int position)
    {
        return contactInfos.get(position);
    }

    @Override
    public int getPosition(PCContactInfo item)
    {
        return contactInfos.indexOf(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder viewHolder;
        if (convertView == null) {
            // inflate the layout
            convertView = inflater.inflate(R.layout.pc_contact_listitem,parent,false);
            // set up the ViewHolder
            viewHolder = new ItemViewHolder();
            viewHolder.contactName = (TextView) convertView.findViewById(R.id.textView_contact_name);
            viewHolder.contactPhoneNumber = (TextView) convertView.findViewById(R.id.textView_contact_number);
            viewHolder.contactPhoto = (ImageView) convertView.findViewById(R.id.imageView_contact_photo);
            // store the holder with the view.
            convertView.setTag(viewHolder);
        }
        else
        {
            // just avoided calling findViewById() on resource everytime by using the viewHolder
            viewHolder = (ItemViewHolder) convertView.getTag();
        }
        PCContactInfo contactInfo = (PCContactInfo)contactInfos.get(position);
        String contactName = contactInfo.getDisplayName();
        viewHolder.contactName.setText(contactName);
        viewHolder.contactPhoneNumber.setText(contactInfo.getPhoneNumber());
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color1 = generator.getRandomColor();

        // reuse the builder specs to create multiple drawables
        TextDrawable ic1 = builder.build((contactName.length() >= 2 ?contactName.substring(0, 2):contactName.substring(0, 1)), color1);

        viewHolder.contactPhoto.setImageDrawable(ic1);

        convertView.setTag(R.id.textView_contact_name,contactInfo);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity appActivity = (AppCompatActivity)context;
                Intent intent = new Intent();
                intent.putExtra(PCPesachoiceConstant.CONTACT_RESULTS,(PCContactInfo) v.getTag(R.id.textView_contact_name));
                appActivity.setResult(appActivity.RESULT_OK, intent);
                appActivity.finish();

            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {

        if (filter == null){
            filter  = new ContactFilter();
        }
        return filter;
    }

    private class ContactFilter extends Filter{

        @Override
        public CharSequence convertResultToString(Object resultValue) {

            if (resultValue instanceof PCContactInfo){
                PCContactInfo u = (PCContactInfo) resultValue;
                return u.getDisplayName();
            } else {
                return super.convertResultToString(resultValue);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            try{

                if (contactInfosOriginal == null) {
                    contactInfosOriginal = new ArrayList<PCContactInfo>(contactInfos);
                }
                ArrayList<PCContactInfo> filteredValues;
                if (constraint == null || constraint.length() <= 0) {
                    filteredValues= new ArrayList<PCContactInfo>(contactInfosOriginal);
                }
                else {
                    filteredValues = new ArrayList<PCContactInfo>();
                    int count = contactInfosOriginal.size();
                    final String prefixString = constraint.toString().toLowerCase();
                    for (int i = 0; i < count; i++) {
                        PCContactInfo item = contactInfosOriginal.get(i);
                        if (item.getDisplayName().toLowerCase().contains(prefixString) ||item.getPhoneNumber().contains(prefixString)) {
                            filteredValues.add(item);
                        }
                    }
                }
                results.values = filteredValues;
                results.count = filteredValues.size();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            contactInfos = (ArrayList<PCContactInfo>)results.values;
            if (results.count > 0) {

                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
    static class ItemViewHolder{
        TextView contactName;
        TextView contactPhoneNumber;
        ImageView contactPhoto;
    }
}

  