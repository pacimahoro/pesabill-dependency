/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.fragments;

import android.support.v4.app.Fragment;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCBillPaymentData;

/**
 * Interface reserved for any transaction component.
 *
 * @author Pacifique Mahoro
 */
public interface PCTransactionableFragment<T extends Fragment> {

    /**
     * Getter for transaction object
     * @return PCBillPaymentData
     */
    public PCBillPaymentData getTransactionData();


    /**
     * Setter for a transaction object
     * @param transaction
     *              The transaction object to set.
     */
    public void setTransactionData(PCBillPaymentData transaction);


    /**
     * Getter for service type
     * @return PCServiceType
     */
    public PCPesabusClient.PCServiceType getServiceType();

    /**
     * Setter for service type.
     * @param serviceType
     * 				The type of service needed.
     */
    public void setServiceType(PCPesabusClient.PCServiceType serviceType);

}
