package com.pesachoice.billpay.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCUser;

import org.w3c.dom.Text;

import java.util.List;

/**
 *Handle account related payments details
 *
 * @Author Desire Aheza
 * @Modified emmy
 */
public class PCAccountPayementsFragment extends Fragment implements View.OnClickListener ,AdapterView.OnItemClickListener {
    private PCAccountDetailsActivity activity = new PCAccountDetailsActivity();
    private ListView cards;
    public PCAccountPayementsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pc_fragment_payments,container,false);
        TextView addNewCard = (TextView) rootView.findViewById(R.id.textView_add_debit_card);

        // If we already have a card attached, treat this as replacing an existing card,
        // otherwise we are adding a new card.
        activity = (PCAccountDetailsActivity)getActivity();
        PCUser appUser = null;
        Intent intent = activity.getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
                appUser = (PCUser) objBundle;
            }
        }
        if (appUser != null && appUser.getMaskedCardNumber() != null &&
                appUser.getMaskedCardNumber().length() > 0) {
            cards = (ListView) rootView.findViewById(R.id.listView_payment_cards);
            cards.setOnItemClickListener(this);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,R.layout.pc_line_item_layout,R.id.textViewParent,new String[]{appUser.getMaskedCardNumber()});
            cards.setAdapter(adapter);
        }
        else {
            addNewCard.setVisibility(View.VISIBLE);
            //add new card
            addNewCard.setOnClickListener(this);
        }

        return rootView;
    }

    @Override
    public void onClick(View v) {

        if (null !=v && v.getId() == R.id.textView_add_debit_card ) {
            openCardPaymentFragment();
        }

    }

    private void openCardPaymentFragment() {

        PCCardPaymentFragment addCardFragment = new PCCardPaymentFragment();
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        // Replace the current pcOnPhoneContactLoad with add card pcOnPhoneContactLoad,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, addCardFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    /**
     * this is called when a user want to replace his card
     * @param parent
     *          listview
     * @param view
     *          item view
     * @param position
     *          position of the card
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != view && view.findViewById(R.id.textViewParent) != null){
            openCardPaymentFragment();
        }
    }
}
