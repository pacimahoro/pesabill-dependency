/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCAgentWalletTransaction;
import com.pesachoice.billpay.model.PCAirtimeTransaction;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingTransaction;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCElectricityServiceInfo;
import com.pesachoice.billpay.model.PCElectricityTransaction;
import com.pesachoice.billpay.model.PCInternetTransaction;
import com.pesachoice.billpay.model.PCMoneyTransferTransaction;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCProductPaymentTranscation;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTVTransaction;
import com.pesachoice.billpay.model.PCTicketPaymentTransaction;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCTransactionStatus;
import com.pesachoice.billpay.model.PCTuitionTransaction;
import com.pesachoice.billpay.model.PCWaterTransaction;

import org.springframework.util.StringUtils;

import java.text.DecimalFormat;

/**
 * Handles showing the receipt of a successful transaction.
 *
 * @author Pacifique Mahoro
 */
public class PCReceiptFragment extends Fragment {
	private final static String CLAZZZ = PCCheckoutFragment.class.getName();
	private PCTransaction transaction = new PCTransaction();
	private static DecimalFormat df2 = new DecimalFormat(".##");

	public void setTransaction(PCTransaction transaction) {
		this.transaction = transaction;
	}

	public PCTransaction getTransaction() {
		return this.transaction;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		if (this.transaction != null && (this.transaction instanceof PCProductPaymentTranscation)) {
			view = inflater.inflate(R.layout.pc_fragment_receipt_less_fields, container, false);
		} else {
			view = inflater.inflate(R.layout.pc_fragment_receipt, container, false);
		}
		this.setViewInfo(view);
		return view;
	}

	private void setViewInfo(View view) {

		//to show meter number code
		TextView meter_serial_code = null;
		TextView meter_serial_code_label = null;
		String meterSerialCode = null;
		PCMainTabActivity activity = (PCMainTabActivity) getActivity();

		if (view == null || activity == null || this.getTransaction() == null) {
			return;
		}
		PCCountryProfile countryProfile = activity.getCountryProfile();
		final PCOperatorRequest operatorRequest = activity.getPcOperatorRequest(countryProfile.getCountry());
		PCSpecialUser sender = this.transaction.getSender();
		PCSpecialUser receiver = this.transaction.getReceiver();

		PCServiceInfo serviceInfo = null;
		PCPesabusClient.PCServiceType serviceType = null;
		String product_name_text = "";
		if (this.transaction instanceof PCAirtimeTransaction) {
			serviceInfo = ((PCAirtimeTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCElectricityTransaction) {
			serviceInfo = ((PCElectricityTransaction) this.transaction).getServiceInfo();
			meter_serial_code = (TextView) view.findViewById(R.id.meter_serial_code);
			meter_serial_code_label = (TextView) view.findViewById(R.id.meter_serial_code_text);
			if (serviceInfo != null) {
				meterSerialCode = ((PCElectricityServiceInfo) serviceInfo).getMeterSerialCode();
				if (meter_serial_code != null && meter_serial_code_label != null && !StringUtils.isEmpty(meterSerialCode)) {
					meter_serial_code.setVisibility(View.VISIBLE);
					meter_serial_code_label.setVisibility(View.VISIBLE);
					meter_serial_code.setText(meterSerialCode);
				}
			}

		} else if (this.transaction instanceof PCInternetTransaction) {
			serviceInfo = ((PCInternetTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCTVTransaction) {
			serviceInfo = ((PCTVTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCCallingTransaction) {
			serviceInfo = ((PCCallingTransaction) this.transaction).getServiceInfo();
			serviceType = PCPesabusClient.PCServiceType.CALLING_REFIL;
		} else if (this.transaction instanceof PCTuitionTransaction) {
			serviceInfo = ((PCTuitionTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCWaterTransaction) {
			serviceInfo = ((PCWaterTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCMoneyTransferTransaction) {
			serviceInfo = ((PCMoneyTransferTransaction) this.transaction).getServiceInfo();
		} else if (this.transaction instanceof PCProductPaymentTranscation) {
			serviceInfo = ((PCProductPaymentTranscation) this.transaction).getServiceInfo();
			serviceType = PCPesabusClient.PCServiceType.SELL;
			product_name_text = ((PCProductPaymentTranscation) this.transaction).getServiceInfo().getProductDescription();
		} else if (this.transaction instanceof PCTicketPaymentTransaction) {
			serviceInfo = ((PCTicketPaymentTransaction) this.transaction).getServiceInfo();
			serviceType = PCPesabusClient.PCServiceType.EVENTS;
		} else if(this.transaction instanceof PCAgentWalletTransaction) {
			serviceInfo = ((PCAgentWalletTransaction) this.transaction).getAgentServiceInfo();
			serviceType = PCPesabusClient.PCServiceType.AGENT_TRANSACTION;
			product_name_text = ((PCAgentWalletTransaction) this.transaction).getAgentServiceInfo().getProductDescription();

		}
		//initialize common views
		TextView amountText = (TextView) view.findViewById(R.id.usd_amount);
		TextView bill_amount_text_title = (TextView) view.findViewById(R.id.bill_amount_text_title);
		TextView totalAmountText = (TextView) view.findViewById(R.id.total_amount);
		TextView cardNumber = (TextView) view.findViewById(R.id.card_number);
		TextView receiverNameText = (TextView) view.findViewById(R.id.receiver);
		Button done = (Button) view.findViewById(R.id.continue_button);

		//card to be charged
		String maskedCardNumber = "";

		//set contact number
		if (receiver != null) {
			String name = receiver.getFirstName() + " " + receiver.getLastName();
			String phoneNumber = receiver.getPhoneNumber();
			if (!"null null".equals(name)) {
				receiverNameText.setText(name);
			} else if (!StringUtils.isEmpty(phoneNumber)) {
				receiverNameText.setText(phoneNumber);
			}
			maskedCardNumber = receiver.getMaskedCardNumber();
		}
		if (serviceInfo != null) {
			String amount, total, currencySymb = activity.getCurrencySymbol();
			//set text for common fields
			if(operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
				amount = "RWF" + df2.format(serviceInfo.getAmount());
				total =  "RWF" + df2.format(serviceInfo.getTotalAmount());
			} else {
				amount = currencySymb + df2.format(serviceInfo.getAmount());
				total = currencySymb + df2.format(serviceInfo.getTotalAmount());

			}
			amountText.setText(amount);
			totalAmountText.setText(total);

		}

		if (this.transaction instanceof PCTicketPaymentTransaction) {
			bill_amount_text_title.setText("TICKET PRICE");
		}

		//handler submit button
		done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onDoneClicked(v);
			}
		});

		//Check if need to show product sell receipt
		if (this.transaction instanceof PCProductPaymentTranscation) {
			TextView product_name = (TextView) view.findViewById(R.id.product_name);
			cardNumber.setText(maskedCardNumber);
			product_name.setText(product_name_text);
		} else {

			TextView localText = (TextView) view.findViewById(R.id.local_amount);
			TextView accountNameText = (TextView) view.findViewById(R.id.account_number);
			TextView account_number_text = (TextView) view.findViewById(R.id.account_number_text);
			TextView contactPhoneText = (TextView) view.findViewById(R.id.contact_phone);

			if (serviceInfo != null) {

				String local = df2.format(serviceInfo.getTotalLocalCurrency());

				if (countryProfile != null) {
					if(operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
						local = "RWF" + " " + local;
					} else {
						local = countryProfile.getCurrencySymbol() + " " + local;
					}
				}
				PCTransactionStatus transactionStatus = serviceInfo.getTransactionStatus();
				if (transactionStatus != null && PCTransactionStatus.IN_PROGRESS.toString().equalsIgnoreCase(transactionStatus.toString())) {
					TextView receipt_title = (TextView) view.findViewById(R.id.receipt_title);
					receipt_title.setText("payment " + transactionStatus.toString());
				}

				if (sender != null) {
					//TODO: service info not returning the setting the paymentType
					PCBillPaymentData.PCPaymentType paymentType = activity.getAppUser().getPaymentType();//serviceInfo.getPaymentType();//sender.getMaskedCardNumber();
					if (paymentType != null) {
						maskedCardNumber = paymentType.toString();
					}
				}
				if (receiver != null) {
					contactPhoneText.setText(receiver.getPhoneNumber());
				}


				if (serviceType == null) {
					localText.setText(local);
				} else {
					LinearLayout local_amount_layout = (LinearLayout) view.findViewById(R.id.local_amount_layout);
					local_amount_layout.setVisibility(View.GONE);
				}

				cardNumber.setText(maskedCardNumber);
				String accountNumber = serviceInfo.getAccountNumber();
				if (StringUtils.isEmpty(accountNumber)) {
					accountNameText.setText(accountNumber);
				} else {
					account_number_text.setVisibility(View.INVISIBLE);
					accountNameText.setVisibility(View.INVISIBLE);
				}
			}
		}
	}

	public void onDoneClicked(View v) {
		PCMainTabActivity mainTabActivity = (PCMainTabActivity) getActivity();
		mainTabActivity.goBackToProductsGridView();
	}
}
