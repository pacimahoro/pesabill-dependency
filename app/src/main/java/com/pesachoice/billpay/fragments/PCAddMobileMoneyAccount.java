package com.pesachoice.billpay.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;


/**
 * Fragment being used to collect users mobile money account
 * A simple {@link Fragment} subclass.
 */
public class PCAddMobileMoneyAccount extends Fragment {

	private EditText mobileMoneyAccount;
	private PCMainTabActivity activity = new PCMainTabActivity();

	public PCAddMobileMoneyAccount() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.pc_add_mobile_money_account, container, false);
		activity = (PCMainTabActivity) getActivity();
		setUpView(view);
		return view;
	}

	private void setUpView(View view) {

		PCSpecialUser user;
		if (activity != null) {
			user = activity.getAppUser();
			// custom dialog
			mobileMoneyAccount = (EditText) view.findViewById(R.id.mobile_money_account);
			try {
				InputMethodManager inputManager =
						(InputMethodManager) activity.
								getSystemService(activity.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(
						activity.getCurrentFocus().getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (user != null) {
				mobileMoneyAccount.setText(user.getPhoneNumber());
			}
			Button buttonSubmitMobileMoneyAccount = (Button) view.findViewById(R.id.button_submit_mm_account);
			buttonSubmitMobileMoneyAccount.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String account = mobileMoneyAccount.getText().toString();

				/*
					detect what account is trying to use. check if it matches the current number user has verified with Pesachoice
                 */

					account = PCGeneralUtils.cleanPhoneString(account);
					if (StringUtils.isEmpty(account) && !PCGeneralUtils.isNumeric(account)) {
						mobileMoneyAccount.setError("Mobile Money Account is Invalid");

					} else {
						activity.setMobMoneyAccountToUser(account);
						if (activity.pcUserAccountFragment != null) {
							activity.pcUserAccountFragment.setPaymentInfoNumber(account);
						}
						activity.onBackPressed();
					}

				}
			});
		}
	}
}