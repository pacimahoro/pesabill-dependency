package com.pesachoice.billpay.fragments;

import com.pesachoice.billpay.model.PCBillingAddress;

/**
 * Interface for required card payment info
 *
 * @author Pacifique Mahoro
 */
public interface PCCardPayment {

    public String getCardNumber();

    public String getCvv();

    public String getExpMonth();

    public String getExpYear();

    public PCBillingAddress getBillingAddress();

    public String getCardType();
}
