package com.pesachoice.billpay.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCSendSMSViaPesaBusController;
import com.pesachoice.billpay.model.PCCustomerServiceInfo;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCSendSMSRequest;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import org.springframework.util.StringUtils;

import java.text.MessageFormat;

/**
 * used to show options to contact pesachoise support team
 */
public class PCHelpContactUsFragment extends DialogFragment implements View.OnClickListener, PCAsyncListener {

	private PCData transaction ;
	private AlertDialog dialog;
	private PCMainTabActivity activity;
	private PCCustomerServiceInfo customerServiceInfo;
	private ProgressDialog progressDialog;
	private EditText messageUsContent;
	private LinearLayout messageUsView;
	private final int MY_PERMISSIONS_REQUEST_MAKE_CALL = 1007;
	private final String clazzz = PCHelpContactUsFragment.class.getName();
	String number = null;

	private final static String EMAIL_SUPPORT_TEMPLATE_ERROR = "I tried a transaction using PesaChoice App and it failed {0}" +
			"\n\nPlease let me know what I need to do to proceed.";

	public void setTransaction(PCData transaction) {
		this.transaction = transaction;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		activity = (PCMainTabActivity) this.getActivity();

		if (activity != null) {
			customerServiceInfo = activity.getCustomerServiceInfo();
		}

		View view = inflater.inflate(R.layout.pc_help_contact_us_fragment, null);
		TextView call_us = (TextView) view.findViewById(R.id.call_us);
		messageUsContent = (EditText) view.findViewById(R.id.message_us_content);
		messageUsView = (LinearLayout) view.findViewById(R.id.message_us_view);
		Button sendMessage = (Button) view.findViewById(R.id.send_message);
		TextView message_us = (TextView) view.findViewById(R.id.message_us);
		TextView email_us = (TextView) view.findViewById(R.id.email_us);
		TextView chat_us = (TextView) view.findViewById(R.id.chat_us);
		//set listeners
		sendMessage.setOnClickListener(this);
		call_us.setOnClickListener(this);
		message_us.setOnClickListener(this);
		email_us.setOnClickListener(this);
		chat_us.setOnClickListener(this);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getResources().getString(R.string.contact_us_title))
				.setView(view);
		dialog = builder.create();
		return dialog;
	}

	@Override
	public void onClick(View view) {
		if (view != null) {
			if (view.getId() == R.id.call_us) {
				dismiss();
				onCallClicked();
			} else if (view.getId() == R.id.message_us) {
				messageUsView.setVisibility(View.VISIBLE);
			} else if (view.getId() == R.id.email_us) {
				dismiss();
				onEmailClicked();
			} else if (view.getId() == R.id.send_message) {
				sendMessage();
			} else if (view.getId() == R.id.chat_us) {
				Toast.makeText(activity, "Option is not yet supported", Toast.LENGTH_LONG).show();
			}
		}
	}

	public void onCallClicked() {
		if (customerServiceInfo != null) {
			if ((!"USA".equalsIgnoreCase(customerServiceInfo.getCountry()) ||
					!"United States".equalsIgnoreCase(customerServiceInfo.getCountry()))) {
				//number = customerServiceInfo.getPhoneNumber();
				number = "+" + getActivity().getResources().getString(R.string.contact_us_phone_no_dash);
			}
		}
		number = "tel:" + number;
		requestCallingPermission(Manifest.permission.CALL_PHONE);
	}

	private void makeCall() {
		if (!StringUtils.isEmpty(number)) {
			Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
			startActivity(callIntent);
		}
	}

	protected void requestCallingPermission(String needIntent) {
		// Here, thisActivity is the current activity
		if (ContextCompat.checkSelfPermission(getActivity(),
				needIntent)
				!= PackageManager.PERMISSION_GRANTED) {


			ActivityCompat.requestPermissions(getActivity(),
					new String[]{needIntent},
					MY_PERMISSIONS_REQUEST_MAKE_CALL);

		} else {
			//make call
			makeCall();
		}
	}

	public void onEmailClicked() {
		String email = getActivity().getResources().getString(R.string.contact_us_email);

		if (customerServiceInfo != null && !StringUtils.isEmpty(customerServiceInfo.getEmail())) {
			email = customerServiceInfo.getEmail();
		}

		Long transactionId = null;
		String subject;
		PCTransaction trans = (PCTransaction) this.transaction;

		if (trans != null) {
			transactionId = trans.getTransactionId();
		}

		if (transactionId != null) {
			subject = "Failed Transaction " + transactionId;
		} else {
			subject = "Inquiry on Failed Transaction";
		}

		String body = MessageFormat.format(EMAIL_SUPPORT_TEMPLATE_ERROR, this.getFailureReason());
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", email, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body);
		startActivity(Intent.createChooser(emailIntent, "Send email..."));
	}

	private String getFailureReason() {
		String reason = "";
		String part1 = "Unable to complete transaction. Reason:";
		String part2 = "Please contact us at (405) 835-6196";

		if (this.transaction != null) {
			reason = this.transaction.getErrorMessage();
			if (reason != null) {
				reason = reason.replaceAll(part1, "");
				reason = reason.replaceAll(part2, "");
			}
		}

		reason = StringUtils.isEmpty(reason) ? "." : " because: \n" + reason;
		return reason;
	}

	private void sendMessage() {
		try {
			String message = "";
			if (messageUsContent != null && activity != null) {
				message = messageUsContent.getText().toString();
				if (StringUtils.isEmpty(message)) {
					messageUsContent.setError("Message is required");
				} else {
					if (dialog != null) {
						dialog.dismiss();
					}
					PCSendSMSRequest request = new PCSendSMSRequest();
					if (customerServiceInfo != null) {
						request.setPhoneNumbers(new String[]{PCGeneralUtils.cleanPhoneString(customerServiceInfo.getPhoneNumber())});
					}
					request.setMessage(message);
					//set the user tracking id
					request.setPhoneId(activity.getUserTrackingKey());
					PCSendSMSViaPesaBusController sendSMSViaPesaBusController = (PCSendSMSViaPesaBusController) PCControllerFactory.constructController
							(PCControllerFactory.PCControllerType.SEND_MESSAGE, this);
					if (sendSMSViaPesaBusController != null) {
						sendSMSViaPesaBusController.setActivity(activity);
						sendSMSViaPesaBusController.setServiceType(PCPesabusClient.PCServiceType.SEND_MESSAGE);
						sendSMSViaPesaBusController.execute(request);
					}
				}
			}
		} catch (Throwable exc) {
			Log.e(clazzz, "Could not handle sending sms process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError)exc;
				error.setNeedToGoBack(false);
			}
			else
				error.setMessage(exc.getMessage());
			activity.presentError(error, "Sending SMS Error");
		}
	}

	@Override
	public void onTaskStarted() {
		progressDialog = ProgressDialog.show(this.getActivity(), "Sending", "Sending message", true);
	}

	@Override
	public void onTaskCompleted(PCData pcData) {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		if (pcData != null && !pcData.isSuccess()) {
			Toast.makeText(activity, pcData.getErrorMessage(), Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(activity, "Message Sent", Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_MAKE_CALL: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					makeCall();

				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}
}
