package com.pesachoice.billpay.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.util.StringUtils;


/**
 * Shows a card payment dialog
 *
 * @author Pacifique Mahoro
 */
public class PCCardDialogFragment extends DialogFragment {
	private final static String CLAZZZ = PCCardDialogFragment.class.getName();
	private EditText mobileMoneyAccount;
	private PCMainTabActivity activity = new PCMainTabActivity();

	public PCCardDialogFragment() {

	}

	private Calling_Fragment calling_fragment;

	public enum Calling_Fragment {CHECKOUT, USER_ACCOUNT}

	;

	public Calling_Fragment getCalling_fragment() {
		return calling_fragment;
	}

	public void setCalling_fragment(Calling_Fragment calling_fragment) {
		this.calling_fragment = calling_fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		return view;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		activity = (PCMainTabActivity) getActivity();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(getResources().getString(R.string.checkout_card_attachment_title))
				.setItems(this.getCardPaymentItems(), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {//open add payment card fragment
							showCardFragment(dialog);
						} else if (which == 1) {//Open add mobile Money account Dialog
							createAndShowDialog();
						}
					}
				});

		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Log.d(CLAZZZ, "Clicked on OK on Alert box");
			}
		});

		return builder.create();
	}

	private String[] getCardPaymentItems() {
		if (activity != null) {
			PCUser appUser = activity.getAppUser();
			String action = getResources().getString(R.string.checkout_card_add_card);
			String mobileMoney = "Use mobile money";
			//to handle adding cards from Calling transaction pcOnPhoneContactLoad
			if (activity.isAddingCallingCreditCard) {
				PCCallingServiceInfo serviceInfo = ((PCMainTabActivity) getActivity()).getCallingServiceInfo();
				if (serviceInfo != null && !StringUtils.isEmpty(serviceInfo.getMaskedCardNumber())) {
					String current = "Use card ending - " + serviceInfo.getMaskedCardNumber();
					action = getResources().getString(R.string.checkout_card_replace_card);

					if (activity.isCallFromUserAccountFragment) {
						return activity.isCountryUS() ? new String[]{action} : new String[]{action, mobileMoney};
					}
					return new String[]{action, current};
				}

			} else if (appUser != null && !StringUtils.isEmpty(appUser.getMaskedCardNumber())) {//handler other user cases
				String current = "Use card ending - " + appUser.getMaskedCardNumber();
				action = getResources().getString(R.string.checkout_card_replace_card);

				if (activity.isCallFromUserAccountFragment) {
					return activity.isCountryUS() ? new String[]{action} : new String[]{action, mobileMoney};
				}
				return new String[]{action, current};
			} else {
				return activity.isCountryUS() ? new String[]{action} : new String[]{action, mobileMoney};
			}
		}
		return new String[0];
	}

	public void showCardFragment(DialogInterface d) {
		PCCardPaymentFragment frag = new PCCardPaymentFragment();
		PCMainTabActivity activity = (PCMainTabActivity) getActivity();
		// TODO: is this check necessary? If so, it looks like it should a property of the BaseActivity and then each activity can set it appropriately.
		if (activity != null && activity.isCallFromUserAccountFragment) {
			FragmentManager fm = getFragmentManager();
			fm.beginTransaction()
					.replace(R.id.grid_container, frag)
					.addToBackStack(null)
					.commit();
			//to scoll back to tab one
			activity.movePrevious(2);
		} else {
			Intent intent = new Intent(getActivity(), PCAccountDetailsActivity.class);
			intent.putExtra(PCPesabusClient.OPTION, PCPesabusClient.OPTION_PAYMENT);
			intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, ((PCMainTabActivity) getActivity()).getAppUser());
			startActivity(intent);
		}
	}

	private void createAndShowDialog() {

		PCAddMobileMoneyAccount frag = new PCAddMobileMoneyAccount();
		Integer id = R.id.grid_container;
		if (Calling_Fragment.USER_ACCOUNT.equals(this.getCalling_fragment())) {
			id = R.id.pc_fragment_user_account;
		} else if (Calling_Fragment.CHECKOUT.equals(this.getCalling_fragment())) {
			//TODO: keep the assigned value!!!
		}
		FragmentManager fm = getFragmentManager();
		fm.beginTransaction()
				.replace(id, frag)
				.addToBackStack(null)
				.commit();
	}
}
