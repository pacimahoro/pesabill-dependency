/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.view.View;

import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCData;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles show products grid that a user can choose from.
 *
 * @author Desire Aheza
 */
public class PCSupportedCountriesFragment extends PCMainTabsFragment {

    public PCSupportedCountriesFragment() {

    }
    @Override
    public void onDataLoadStart(View view) {
        super.onDataLoadStart(view);
        this.onDataLoadFinished(null);
    }

    @Override
    public void onDataLoadFinished(PCData pcData) {
        super.onDataLoadFinished(pcData);
        this.setupMainView(null);
        resizeGridViewToEnableScrolling();
    }

    @Override
    public void setupMainView(PCCountryProfile countryProfile) {
        //setup countries list
        List<String> countryNames = new ArrayList<>();
        countryNames.add("Nigeria");
        countryNames.add("Rwanda");
        countryNames.add("Uganda");
        countryNames.add("Kenya");
        List<Integer> images = new ArrayList<>();

        //setup icon
        images.add(R.drawable.icn_nigeria);
        images.add(R.drawable.icn_rwanda);
        images.add(R.drawable.icn_uganda);
        images.add(R.drawable.icn_kenya);
        if (getActivity() != null) {
            adapter = new PCCountryAndProductAdapter(getActivity(), countryNames, images);
            adapter.setTypeOfDatatToAdapt(TypeOfDataToAdapt.COUNTRIES);
            mainFragmentLayout.setAdapter(adapter);
        }
    }
}
