package com.pesachoice.billpay.fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCReferralPromotionController;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCAgentServiceInfo;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferServiceInfo;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCPrice;
import com.pesachoice.billpay.model.PCProductServiceInfo;
import com.pesachoice.billpay.model.PCRequestReferralPromotion;
import com.pesachoice.billpay.model.PCServiceFee;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCServiceInfoFactory;
import com.pesachoice.billpay.model.PCServiceTypePaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCCurrencyExchangeRateUtil;
import com.pesachoice.billpay.utils.PCCurrencyUtil;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCSpinner;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emmy on 18/03/2018.
 */

public class PCAgentCheckoutFragment extends PCCheckoutFragment implements PCTransactionComponent {
    private final static String CLAZZZ = PCAgentCheckoutFragment.class.getName();
    public final static String TRANSACTION_ERROR_TITLE = "Transaction Error";
    private EditText accountText;
    private EditText accountNumberText;
    private EditText durationText;
    private PCSpinner durationTypeSpinner;
    private PCCurrency senderCurr;
    private static final String PAY_WITH_CASH = "Pay With Cash";
    private PCCurrency currency;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_agent_checkout, container, false);
        try {
            activity = (PCMainTabActivity) getActivity();
            countryProfile = activity.getCountryProfile();
            //calculate the exchange rate
            Log.e("country :", "Loadind");
            senderCurr = activity.getAppUser().getBaseCurrency();
            if (senderCurr != null) {


            } else {
                if (countryProfile.getCountry().equalsIgnoreCase("Rwanda")) {
                    List<PCCurrency> curr = countryProfile.getCurrencies();
                    for (PCCurrency listCurr : curr) {

                        if (listCurr.getCountry().equalsIgnoreCase("Rwanda")) {
                            senderCurr = listCurr;
                            Log.e("Contriessss;", listCurr.getCountry());
                            Log.e("ExchangeRate;", listCurr.getExchangeRate());
                            //Set the current Exchange Rate in CountryProfile
                            countryProfile.setSelectedCountryExchangeRate(1.0);
                            Log.e("country :", "finishhhLoadind");
                            break;
                        }
                    }
                }
            }
            this.setViewInfo(view);
            this.attachClickListeners(view);
            if (amRepeatingTrans) {
                setAmountOfRepeatedTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void setViewInfo(View view) {
        try {
            PCBillPaymentData transaction = this.getTransactionData();
            if (view != null && transaction != null) {
                TextView serviceText = (TextView) view.findViewById(R.id.service_summary);
                accountText = (EditText) view.findViewById(R.id.customer_summary);
                accountNumberText = (EditText) view.findViewById(R.id.cutomer_phone);
                durationText = (EditText) view.findViewById(R.id.duration_number);
                durationTypeSpinner = (PCSpinner) view.findViewById(R.id.duration_type);
                durationTypeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                cardNumberText = (PCSpinner) view.findViewById(R.id.card_number);
                senderCurrency = (TextView) view.findViewById(R.id.sender_currency);
                receiverCurrency = (TextView) view.findViewById(R.id.receiver_currency);
                selectManyPackages = (TextView) view.findViewById(R.id.select_many_packages);
                viewServiceFees = (TextView) view.findViewById(R.id.open_pricing_help);
                accountNumberText.setText("+" + countryProfile.getCountryCode());
                List<PCServiceFee> serviceFees = countryProfile.getServiceFees();
                //List<String> serviceFees = new ArrayList<>();
                //show or hide the link to services fees fragment
                if (activity != null && serviceFees != null && serviceFees.size() > 0) {
                    viewServiceFees.setOnClickListener(this);
                } else {
                    viewServiceFees.setVisibility(View.GONE);
                }
                TextView exchangeRateText = null;
                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    exchangeRateText = (TextView) view.findViewById(R.id.textView_exchange_rate);
                    usdAmountText = (EditText) view.findViewById(R.id.usd_amount);
                    localAmountText = (EditText) view.findViewById(R.id.local_amount);
                }
                String service = PCTransactionFragmentFactory.descriptionForService(this.serviceType);
                String receiver = null;

                PCSpecialUser receiverObj = transaction.getReceiver();

                if (receiverObj != null) {
                    receiver = receiverObj.getFullName() == "null null" ? receiverObj.getPhoneNumber() : receiverObj.getFullName();
                }

                if (serviceInfo == null) {
                    serviceInfo = transaction.retrieveServiceInfo();
                }

                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    PCOperator op;
                    if (this.transactionData.getOperator() != null && this.transactionData.getOperator().getName() != null) {
                        Log.i("TransctionName", this.transactionData.getOperator().getName());
                    }
                    if (countryProfile != null) {
                        PCSpecialUser sender = transaction.getSender();
                        for (PCOperator pc : countryProfile.getOperators()) {
                            Log.i("OperatorName", pc.getName());
                            // we don't need to do currency exchange if user sending money from to the same country,
                            // if sender is using mobile money or if buying calling card.
                            if (PCPesabusClient.PCServiceType.CALLING_REFIL == serviceType
                                    || countryProfile.getCountry().equalsIgnoreCase("Rwanda")) {
                                LinearLayout exchange_rate_details = (LinearLayout) view.findViewById(R.id.exchange_rate_details);
                                exchange_rate_details.setVisibility(View.GONE);
                                ImageView icon_exchange = (ImageView) view.findViewById(R.id.icon_exchange);
                                icon_exchange.setVisibility(View.GONE);
                                localAmountText.setVisibility(View.GONE);
                                receiverCurrency.setVisibility(View.GONE);
                                senderCurrency.setText(countryProfile.getCurrencySymbol() + " ");

                            } else if (exchangeRateText != null) {
                                setExchangeRateViews(exchangeRateText);
                            }
                            PCOperator operator = this.transactionData.getOperator();
                            if (operator != null && operator.getName() != null) {
                                if (pc.getName().trim().equalsIgnoreCase(operator.getName().trim())) {
                                    final List<PCPrice> prices = pc.getPrices();
                                    if (prices != null && prices.size() > 1) {
                                        // instead of preselecting item number 1 in the price list, let's show the user different packages
                                        // then have them select what they want
                                        selectManyPackages.setVisibility(View.VISIBLE);
                                        //showListOfPackages(prices);
                                        double val = prices.get(0).getPrice();

                                        int i = 0;
                                        for (PCPrice prc : prices) {
                                            String symbl = prc.getCurrencySymbol();
                                            if (StringUtils.isEmpty(symbl)) {
                                                symbl = getCurrencySymbol();
                                            }
                                            prc.setCurrencySymbol(symbl);
                                            prices.set(i, prc);
                                            i++;
                                        }

                                        //set values in local and receiver views
                                        setLocalAndRecieverAmountView(val);

                                        selectManyPackages.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                        usdAmountText.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }


            Log.e("Update pay", "payment Info");
            this.updatePaymentCardInfo(view);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePaymentCardInfo(View view) {
        if (view == null) {
            return;
        }

       /* PCSpecialUser user;
        PCBillPaymentData transaction = this.getTransactionData();
        if (transaction != null && PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS == transaction.getPaymentProcessingType()) {
            user = this.getTransactionData().getReceiver();
        }else {
            user = ((PCMainTabActivity) getActivity()).getAppUser();
        }
        if(isCardSet) {
            cardNumberText.setSelection(1);
        } else {
            Log.e("setCardNumber","Card Number Text");
            setCardNumber(user);
        }*/
        setPaymentOptions(null);
        attachPaymentsOptions(cardNumberText, paymentOptions);

    }

    @Override
    protected boolean setPaymentOptions(String maskedPaymentCard) {
        boolean paymentSet = false;
        paymentOptions = new ArrayList<>();
        paymentOptions.add("Select Payment option");
        Boolean isMobileMoneyAllowed = true;

        if (isMobileMoneyAllowed) {
            paymentOptions.add(PAY_WITH_MOBILE_MONEY);
        }
        paymentOptions.add(PAY_WITH_CASH);
        paymentSet = true;


        return paymentSet;
    }


    @Override
    public void getAndSetReceiverInfo(View v) throws PCGenericError {
        if (getActivity() instanceof PCMainTabActivity) {
            PCSpecialUser appUser = ((PCMainTabActivity) getActivity()).getAppUser();
            transactionData.setSender(appUser);

            PCGenericError error;
            View view = getView();
            if (view == null) {
                // TODO: what should we do here?
                return;
            }

            String fullName = StringUtils.capitalize(accountText.getText().toString());

            String phoneNumber = accountNumberText.getText().toString();
            phoneNumber = PCGeneralUtils.cleanPhoneString(phoneNumber);

            PCMainTabActivity activity = ((PCMainTabActivity) getActivity());
            String countryCode = null;
            if (activity != null) {
                PCCountryProfile countryProfile = activity.getCountryProfile();
                if (countryProfile != null) {
                    countryCode = countryProfile.getCountryCode();
                }
            }

            // Default to UG country code.
            if (countryCode == null) {
                countryCode = "256";
            }

            if (fullName.length() == 0) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("Customer full Name is required");
                throw error;
            }
            if (phoneNumber.length() == 0) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("Customer Phone Number is required");
                throw error;
            }
            if ((phoneNumber.length() < 12 || phoneNumber.length() > 12) && !("1".equals(phoneNumber.substring(0, 1)) && phoneNumber.length() == 11)) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("A valid receiver number should have 9 digits excluding +" + countryCode + ". Please check the number again");
                throw error;
            }

            String firstName, lastName = "";
            String[] parts = fullName.split(" ");
            firstName = parts[0];
            for (int i = 1; i < parts.length; i++) {
                lastName += parts[i] + " ";
            }
            lastName = lastName.trim();
            PCSpecialUser receiver = new PCSpecialUser();
            Log.e("Receiver:", "ReceiverrInfooo");
            if (receiver != null) {
                receiver.setFullName(fullName);
                receiver.setFirstName(firstName);
                receiver.setLastName(lastName);
                receiver.setPhoneNumber(phoneNumber);
                receiver.setCountry("Rwanda");
                Log.e("Receiver:", "ReceiverrInfooo");
            }
            PCBillPaymentData transaction = this.getTransactionData();
            transaction.setReceiver(receiver);
        }
    }

    @Override
    public void setContactInfo(PCContactInfo contactInfo) {
        // TODO: Not yet implemented
        Log.w(CLAZZZ, "setContactInfo: method called but not yet implemented");
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        // NOTE: Initialize the service info for the very 1st time
        if (transactionData instanceof PCAgentWalletData) {
            if (transactionData.retrieveServiceInfo() == null) {
                PCAgentWalletData transaction = (PCAgentWalletData) this.getTransactionData();
                PCProductServiceInfo serviceInfo = (PCProductServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
                serviceInfo.setCompanyCode("EmbraceSolar");
                serviceInfo.setProductDescription(durationText.getText().toString() + "  " + durationTypeSpinner.getSelectedItem());
                String countrySendingTo = ((PCMainTabActivity) getActivity()).countrySendingTo;
                if (StringUtils.isEmpty(countrySendingTo)) {
                    countrySendingTo = "Rwanda";
                }
                serviceInfo.setReceiverCountry(countrySendingTo);
                //set the account number to be the receiver's phone number
                transaction.setProductServiceInfo(serviceInfo);
                this.setServiceInfo(serviceInfo);
            }
        }
    }

    @Override
    public void getOperatorInfo(View v) throws PCGenericError {
        PCOperator op = new PCOperator();
        transactionData.setOperator(op);

        // FIXME: Get this from the company id.
        op.setName("EmbraceSolar");

        PCGenericError error;
        String durationTime = durationText.getText().toString();
        if (durationTime.length() == 0) {
            error = new PCGenericError();
            error.setTitle("Validation Error");
            error.setMessage("Top up time is missing. Minimum: 1");
            throw error;
        }
        op.setDurationTime(durationTime);
        String durationType = durationTypeSpinner.getSelectedItem() != null ? (String) durationTypeSpinner.getSelectedItem() : (String) durationTypeSpinner.getItemAtPosition(0);
        op.setDurationType(durationType);
        op.setCountry("Rwanda");
    }

    @Override
    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity) getActivity();

        if (activity != null) {
            activity.presentError(error, TRANSACTION_ERROR_TITLE);
        }
    }

    @Override
    public void validateInputs(View v) throws PCGenericError {
        this.getAndSetReceiverInfo(v);
        this.getServiceInfo(v);
    }

    @Override
    public void setAndValidateInputs() throws PCGenericError {
        View view = getView();
        PCGenericError error;
        // Validate necessary fields.
        validateInputs(view);

        if (view != null && transactionData != null) {
            Double usdAmount = this.getInputAmount();
            PCCurrencyUtil.AmountInput amountInput = new PCCurrencyUtil().new AmountInput();
            amountInput.setValue(usdAmount);
            String serviceType = PCData.getActionType(transactionData.getPaymentProcessingType());
            PCBillPaymentData.PCPaymentType paymentType = transactionData.getPaymentInfo() != null ? transactionData.getPaymentInfo().getPaymentType() : null;
            amountInput.setServiceType(serviceType);
            amountInput.setPaymentType(paymentType);
            PCSpecialUser sender = ((PCMainTabActivity) getActivity()).getAppUser();
            List<PCServiceFee> serviceFees = getServiceFees(sender);
            PCServiceTypePaymentInfo pcServiceTypePaymentInfo = PCCurrencyUtil.determineServiceFee(amountInput, serviceFees);
            // Set more variables.
            if (pcServiceTypePaymentInfo != null) {
                pcServiceTypePaymentInfo.setCurrencySymbol(countryProfile.getCurrencySymbol());
                pcServiceTypePaymentInfo.setServiceTypeName(serviceType);
                pcServiceTypePaymentInfo.setExchangeRate(countryProfile.getExchangeRate());
                pcServiceTypePaymentInfo.setReceiverCurrencySymbl(countryProfile.getCurrencySymbol());
                amountInput.setServiceTypePaymentInfo(pcServiceTypePaymentInfo);
            }

            List<PCCurrency> curr = countryProfile.getCurrencies();
            for (PCCurrency listCurr : curr) {
                if (listCurr.getCountry().equalsIgnoreCase("Rwanda"))
                    senderCurr = listCurr;
                amountInput.setBaseCurrency(senderCurr);
                break;

            }
            amountInput.setAuthorizedTestAccount(isAuthorizedTestAccount());

            // Validate input
            PCCurrencyUtil.validateAmount(amountInput);
            if ("Select Payment option".equalsIgnoreCase(cardNumberText.getSelectedItem().toString())) {
                error = new PCGenericError();
                error.setMessage("Please Select Payment Option");
                throw error;
            }

            this.populateTransaction(usdAmount);

            if (sender != null) {
                String selectedOption = cardNumberText.getSelectedItem().toString();
                String card = null; // sender.getMaskedCardNumber();
                //set payment info
                setTransactionPaymentInfo(paymentType, sender, card);

//                this.transactionData.setMaskedCardNumber(card);
//                serviceInfo.setMaskedCardNumber(card);
            }
        }
    }

/*    @Override
    protected void showServiceFeeInfoFragment() {
        //set selected payments
        if (activity != null) {
            PCServiceFeeDetailsFragment frag = new PCServiceFeeDetailsFragment();
            frag.setSelectedPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
            String processingType = transactionData.getPaymentProcessingType().toString();
            frag.setSelectedServiceType(processingType);
            PCSpecialUser user = activity.getAppUser();
            frag.setCurrency("RWF");
            Log.e("ServiceFee;","Service Feee");
            if(user != null) {
                Log.e("ServiceFee;","Service FeeedotError");
                frag.setServiceFees(getServiceFees(user));
            }

            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.beginTransaction()
                    .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_calling_card_help))
                    .addToBackStack(null)
                    .commit();
        }
    }*/


/*
    @Override
    protected void populateTransaction(Double amount) {
        if (transactionData instanceof PCAgentWalletData) {
            if (transactionData.retrieveServiceInfo() == null) {
                PCAgentWalletData transaction = (PCAgentWalletData) this.getTransactionData();
                PCProductServiceInfo serviceInfo = (PCProductServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
                Double localAmount = countryProfile.convertToLocalUsingCalculatedExchangeRate(amount);
                Double fee = null;
                if (!PCActivity.PCPaymentProcessingType.CALLING.equals(transactionData.getPaymentProcessingType())) {
                    if (pcServiceTypePaymentInfo != null) {
                        BigDecimal feeBig = pcServiceTypePaymentInfo.getServiceFee();
                        if (feeBig != null) {
                            fee = feeBig.doubleValue();
                        }
                    }

                    if (fee == null || fee == 0.0) {
                        fee = countryProfile.computeFeeAmount(amount);
                    }

                } else {
                    //keep fees as zero when you're buying calling credit.
                    fee = 0.0;
                }
                if (!localAmount.isNaN()) {

                    //TODO: discuss with the team before release
                    try {
                        localAmount = Double.valueOf(localAmountText.getText().toString().trim());
                    } catch (Exception e) {

                    }
                    if(serviceInfo != null) {
                        Log.e("Localll:","LocalAmountttt");
                        serviceInfo.setTotalLocalCurrency(localAmount);
                        transactionData.setTotalLocalCurrency(localAmount);
                    }
                }
                if (!fee.isNaN()) {
                    serviceInfo.setFees(fee);
                }

                if (countryProfile != null) {
                    //keep fees as zero when you're buying calling credit.
                    //set the currency object to support local transactions
                    List<PCCurrency> curr = countryProfile.getCurrencies();
                    for (PCCurrency listCurr : curr) {
                        if (listCurr.getCountry().equalsIgnoreCase("Rwanda"))
                            senderCurr = listCurr;
                        Log.i("Log", "Exchange Rate: " + 1.0);
                        this.transactionData.setBaseCurrency(senderCurr);
                        //senderCurr.setExchangeRate();
                        senderCurr.setCountry(senderCurr.getCountry());
                        serviceInfo.setFees(0.0);
                        serviceInfo.setAmount(amount);
                        serviceInfo.setTotalAmount(amount + 0.0);
                        break;

                        //set transaction usd amount
                    }
                }
                transaction.setProductServiceInfo(serviceInfo);
                this.setServiceInfo(serviceInfo);
            }
        }
    }*/

    @Override
    protected void onSubmit() {
        try {
            this.setAndValidateInputs();
            this.dismissKeyboard();
            activity = ((PCMainTabActivity) getActivity());
            PCSpecialUser logedInUser = activity.getAppUser();
            if (logedInUser != null && logedInUser.isAuthenticated() && transactionData != null) {
                //set transaction currency
                transactionData.setBaseCurrency(logedInUser.getBaseCurrency());
                //defaultSource { cardDetails {}}
                //setup user tracking key on this transaction
                if (countryProfile != null) {
                    transactionData.setActionType("AGENT_TRANSACTION");
                    PCAgentWalletData transaction = (PCAgentWalletData) this.getTransactionData();
                    transaction.setAgentId("agent@embracesolar.com");
                    transaction.setAgentTokenId(logedInUser.getTokenId());
                    transaction.setAgentPhoneNumber("250785044685");
                    transaction.setCountry(countryProfile.getCountry());
                    transaction.setCompanyId("EmbraceSolar");
                    this.setTransactionData(transaction);
                }
                //TODO: Make call to submit promotional code
                showConfirmationDialog();

            } else {
                Intent intent = new Intent(this.getActivity(), PCLoginActivity.class);
                intent.putExtra("PCCHECOUT", "PCCHEOUT");
                startActivityForResult(intent, CHECKOUT_VERIFY_USER_REQUEST);
            }


        } catch (Throwable exc) {
            exc.printStackTrace();
            Log.e(CLAZZZ, "Could not handle Checkout process because [" + exc.getMessage() + "]");
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            PCGenericError error = new PCGenericError();
            if (exc instanceof PCGenericError) {
                error = (PCGenericError) exc;
                error.setNeedToGoBack(false);
            } else {
                error.setMessage(exc.getMessage());
            }
            ((PCMainTabActivity) getActivity()).presentError(error, "Error during checkout");
        }
    }


    @Override
    protected void attachClickListeners(View view) {
        if (view != null) {
            Button submitButton = (Button) view.findViewById(R.id.continue_button);
            attachPaymentsOptions(cardNumberText, paymentOptions);
            cardNumberText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.w("PCCheckoutFragment : ", "Card Number Item selected: " + position + " id: " + id);
                    selectedPaymentType = position;
                    if (paymentOptions != null) {
                        String selectedOption = paymentOptions.get(position);
                        PCPaymentInfo paymentInfo = transactionData.getPaymentInfo();

                        if (PAY_WITH_CASH.equalsIgnoreCase(selectedOption)) {
                            Log.e(CLAZZZ, "WE ARE PAYING WITH CASH");
                        } else if (!StringUtils.isEmpty(selectedOption)) {
                            if (selectedOption.equalsIgnoreCase(PAY_WITH_CASH)) {
                                Log.e(CLAZZZ, "WE ARE PAYING WITH CASH");
                            } else if (paymentInfo == null) {
                                PCSpecialUser user = (PCSpecialUser) ((PCMainTabActivity) getActivity()).getAppUser();
                                userCurrency = user.getBaseCurrency() != null ? user.getBaseCurrency().getCountrySymbol() : null;
                                paymentInfo = user != null && user.getPaymentInfo() != null ? user.getPaymentInfo() : new PCPaymentInfo();
                            }
                            transactionData.setPaymentInfo(paymentInfo);
                            //open dialog to input MM account Number
                            if (PAY_WITH_MOBILE_MONEY.equalsIgnoreCase(selectedOption)) {
                                showDialogToInputMobileMoneyAccount();
                                paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
                                transactionData.setPaymentInfo(paymentInfo);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if (submitButton != null) {
                submitButton.setOnClickListener(this);
            }
        }
    }


    public void populateMobileMoneyDialog(EditText mobileMoneyAccount, PCMainTabActivity activity) throws PCGenericError {
        if (activity != null && mobileMoneyAccount != null) {
            getAndSetReceiverInfo(getView());
            if (transactionData.getReceiver() != null) {
                mobileMoneyAccount.setText(transactionData.getReceiver().getPhoneNumber());
            }
        }
    }

}
