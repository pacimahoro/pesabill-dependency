package com.pesachoice.billpay.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.PCSplashActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCUser;


import org.springframework.util.StringUtils;

import java.text.DecimalFormat;

/**
 * Shows update app dialog
 *
 * @author Desire Aheza
 */
public class PCUpdateAppDialogFragment extends DialogFragment implements View.OnClickListener {

    private String appVersionCode;
    private AlertDialog dialog;

    public PCUpdateAppDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = null;
        view = inflater.inflate(R.layout.pc_fragment_update_pesachoice, null);
        Button cancel_update = (Button) view.findViewById(R.id.cancel_update);
        Button update_pesachoice = (Button) view.findViewById(R.id.update_pesachoice);
        TextView updateMessage = (TextView) view.findViewById(R.id.update_pesachoice_message);

        String versionName = getCurrentAppVersionName();
        if (!StringUtils.isEmpty(versionName)) {
            updateMessage.setText("Update " + versionName +" is available for download. By downloading this new version, you will get the latest features, improvements, and bug fixes of PesaChoice");
        } else {
            updateMessage.setText("An updated version is available to download. By Downloading the latest version, you will get the latest features, improvements, and bug fixes of Pesachoice");
        }

        this.setViewInfo(view);
        cancel_update.setOnClickListener(this);
        update_pesachoice.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.update_app_title))
                .setView(view);

        dialog = builder.create();
        return dialog;
    }


    public void setViewInfo(View view) {

    }

    public String getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(String appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    @Override
    public void onClick(View v) {
        if (v != null && v.getId() == R.id.cancel_update) {
            if (dialog != null)
                dialog.dismiss();
            ((PCSplashActivity)getActivity()).startTheApp();
        }
        else if (v != null && v.getId() == R.id.update_pesachoice) {
            final String appPackageName = (getActivity()).getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private String getCurrentAppVersionName() {
        String versionName = "";
        try {
            FragmentActivity activity = this.getActivity();
            if(activity != null) {
                PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
                versionName = packageInfo.versionName;
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

}
