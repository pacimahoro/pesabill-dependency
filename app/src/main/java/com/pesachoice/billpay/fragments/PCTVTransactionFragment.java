/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCServiceInfoFactory;
import com.pesachoice.billpay.model.PCTVServiceInfo;
import com.pesachoice.billpay.model.PCTVSubcriptionPaymentData;

/**
 * Represents a pcOnPhoneContactLoad for TV subscription service
 * @author Pacifique Mahoro
 */
public class PCTVTransactionFragment extends PCTransactionFragment {
    public PCTVTransactionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_transaction, container, false);

        // Set these fields we will need them to set the selected contact info.
        fullNameText = (EditText)view.findViewById(R.id.full_name);
        phoneText = (EditText)view.findViewById(R.id.mobile);
        setPhoneTextPrefix(view);
        Button continueBtn = (Button)view.findViewById(R.id.continue_button);
        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }
        this.setProviderListSpinner(view);
        this.setAddContactsButton(view);

        return view;
    }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.TV;
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        View view = getView();
        if (view == null) { return; }

        PCGenericError error;
        EditText accountText = (EditText)view.findViewById(R.id.account_number);
        if (accountText != null) {
            String accountNumber = accountText.getText().toString();
            if (accountNumber.length() == 0) {
                error = new PCGenericError();
                error.setMessage("Account Number is required");
                throw error;
            }

            PCTVSubcriptionPaymentData transaction = (PCTVSubcriptionPaymentData)this.getTransactionData();
            PCTVServiceInfo serviceInfo = (PCTVServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
            serviceInfo.setReceiverCountry(((PCMainTabActivity)getActivity()).countrySendingTo);
            serviceInfo.setAccountNumber(accountNumber);
            transaction.setTvServiceInfo(serviceInfo);
        }
    }

}

