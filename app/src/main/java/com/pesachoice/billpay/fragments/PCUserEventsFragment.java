/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCCustomViewFinderScannerActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCEventsController;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBaseVerifyEventRequest;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCEvent;
import com.pesachoice.billpay.model.PCEventsProfile;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTicketDetails;
import com.pesachoice.billpay.model.PCTicketPaymentData;
import com.pesachoice.billpay.model.PCTicketServiceInfo;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.model.PCVerifyEventRequest;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import org.springframework.util.StringUtils;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Handles a view that show a list of events that are nearby the user.
 *
 * @author Desire AHEZA
 */
public class PCUserEventsFragment extends PCMainTabsFragment implements PCAsyncListener {

	private View rootView = null;
	private PCUserEventsFragment pcUserEventsFragment;
	private PCEventsAdapter adapter;
	private TextView emptyView;
	private Button scan_event_qr;
	private PCMainTabActivity activity = new PCMainTabActivity();
	private PCEventsProfile pcEventsProfile = null;
	private boolean isVerifyingEvent;
	private boolean userTicketConfirmation = false;
	private final String CLAZZ = PCUserEventsFragment.class.getName();
	private String ticketNumber = null;

	public PCUserEventsFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.pc_fragment_user_events, container, false);
		pcUserEventsFragment = this;
		activity = (PCMainTabActivity) getActivity();
		this.onDataLoadStart(rootView);
		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				String jsonString = data.getStringExtra("result");
				String countryWithEvents = activity.countrySendingTo;
				//make request object
				final PCVerifyEventRequest eventRequest = new PCVerifyEventRequest();

				PCSpecialUser currentUser = activity.getAppUser();
				//set device id
				eventRequest.setDeviceId(activity.getApiKey().getDeviceId());
				//set organizer details
				eventRequest.setOrganizer(currentUser);
				ObjectMapper mapper = new ObjectMapper();
				PCTicketDetails pcTicketDetails = null;
				try {
					pcTicketDetails = mapper.readValue(jsonString, PCTicketDetails.class);

					if (pcTicketDetails != null && !StringUtils.isEmpty(pcTicketDetails.getTicketNumber())) {
						boolean confirmingTicket = true;

						final PCBaseVerifyEventRequest qrEventRequest = makeVerifyEventRequest(pcTicketDetails.getTicketNumber(), confirmingTicket);
						isVerifyingEvent = true;
						if (confirmingTicket) {
							userTicketConfirmation = true;
						}
						eventRequest(qrEventRequest);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					Toast.makeText(activity, "scan the right QR", Toast.LENGTH_LONG).show();
				} catch (Throwable exc) {
					Log.e(CLAZZ, "Could not handle getting user Events process because [" + exc.getMessage() + "]");
					if (progressBar != null) {
						progressBar.setVisibility(View.GONE);
					}
					PCGenericError error = new PCGenericError();
					if (exc instanceof PCGenericError) {
						error = (PCGenericError) exc;
						error.setNeedToGoBack(false);
					} else {
						error.setMessage(exc.getMessage());
					}
					activity.presentError(error, "Error While getting userEvents");
				}

			}

		}

	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	/**
	 * making request to get user events
	 */
	private void makeRequest() {

		PCMainTabActivity activity = (PCMainTabActivity) this.getActivity();
		try {
			if (activity != null) {
				final PCUser userData = activity.getAppUser();
				String countryWithEvents = activity.countrySendingTo;
				PCEventsController eventsController =
						(PCEventsController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.USER_EVENTS_CONTROLLER, this);
				if (eventsController != null) {
					//make request object
					final PCRequest eventRequest = new PCRequest();
					eventRequest.setCountry(countryWithEvents);
					eventRequest.setEmail(userData.getEmail());

					//send Request
					eventsController.setActivity(activity);
					eventsController.setServiceType(PCPesabusClient.PCServiceType.EVENTS);
					eventsController.execute(eventRequest);
				}
			}
		} catch (Throwable exc) {
			Log.e("PCUserEventsFragment", "Could not handle getting events process because [" + exc.getMessage() + "]");
			if (progressBar != null) {
				progressBar.setVisibility(View.GONE);
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError) exc;
				error.setNeedToGoBack(false);
			} else {
				error.setMessage(exc.getMessage());
			}
			activity.presentError(error, "Error While Getting Events");
		}
	}

	public void setEmptyView() {
		if (progressBar != null) {
			progressBar.setVisibility(View.GONE);
		}
		emptyView = new TextView(getContext());
		emptyView.setLayoutParams(
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		emptyView.setText("No Events found around!");
		emptyView.setGravity(Gravity.CENTER_HORIZONTAL);
		emptyView.setPadding(20, 60, 20, 20);
		emptyView.setTextSize(18);
		emptyView.setVisibility(View.VISIBLE);
		emptyView.setBackgroundColor(getResources().getColor(R.color.sectionBackground));
		emptyView.setTextColor(getResources().getColor(R.color.colorPrimary));
		((ViewGroup) mainFragmentLayout.getParent()).addView(emptyView);
		mainFragmentLayout.setEmptyView(emptyView);

		//disabling click listern on this empty view
		emptyView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//DO NOTHING!!!
				Log.d("Blocking clicking", "BLOACKING CLICKING");
			}
		});
	}

	public void setAdapter(PCEventsProfile data) {

		if (data != null && data.getEvents() != null) {
			Log.d("Number of events", "" + data.getEvents().size());
			List<PCEvent> ary = data.getEvents();
			if (activity != null && ary != null && ary.size() > 0) {
				initializeAdapter(ary);
			} else {
				setEmptyView();
			}
		} else {
			setEmptyView();
		}
	}

	private void createAndShowDialog(PCEventsProfile pcEventsProfile) {

		try {
			if (pcEventsProfile != null) {
				// custom dialog
				final Dialog dialog = new Dialog(activity);
				dialog.setContentView(R.layout.pc_ticket_verified_dialog);

				//resize the dialog
				setWidthTODialog(dialog);

				final PCEvent event = pcEventsProfile.getTicket();

				Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
				// if OK button is clicked, call pesabus to let them know that user just entered the event
				dialogButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						try {
							String eventTicketNumber = event.getTicketNumber();
							if (StringUtils.isEmpty(eventTicketNumber) && pcUserEventsFragment != null) {
								eventTicketNumber = pcUserEventsFragment.ticketNumber;
							}
							boolean confirmingTicket = true;
							verifyOrConfirmTicket(eventTicketNumber, confirmingTicket, dialog);
						} catch (Throwable exc) {
							handleTicketVerificationException(exc, dialog);
						}
					}
				});

				dialog.show();
			}
		} catch (Exception e) {

		}
	}

	private void setWidthTODialog(Dialog dialog) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;

		if (dialog != null) {
			dialog.getWindow().setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT);
		}
	}

	private void createAndShowEnterTicketNumberDialog() {
		// custom dialog
		final Dialog dialog = new Dialog(activity);
		dialog.setContentView(R.layout.pc_enter_ticket_nbr_dialog);

		setWidthTODialog(dialog);

		final EditText ticketNbr = (EditText) dialog.findViewById(R.id.ticket_nbr);

		Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					ticketNumber = ticketNbr.getText().toString();
					boolean confirmingTicket = false;
					verifyOrConfirmTicket(ticketNumber, confirmingTicket, dialog);
				} catch (Throwable exc) {
					handleTicketVerificationException(exc, dialog);
				}
			}
		});

		dialog.show();

	}

	private void handleTicketVerificationException(Throwable exc, Dialog dialog) {
		dialog.dismiss();
		Log.e(CLAZZ, "Could not handle verifying user event process because [" + exc.getMessage() + "]");
		if (progressBar != null) {
			progressBar.setVisibility(View.GONE);
		}
		PCGenericError error = new PCGenericError();
		if (exc instanceof PCGenericError) {
			error = (PCGenericError) exc;
			error.setNeedToGoBack(false);
		} else {
			error.setMessage(exc.getMessage());
		}
		if (activity != null) {
			activity.presentError(error, "Error While Verifying user event");
		}
	}

	private void verifyOrConfirmTicket(String ticketNumber, boolean confirmingTicket, Dialog dialog) throws PCGenericError {
		final PCBaseVerifyEventRequest eventRequest = makeVerifyEventRequest(ticketNumber, confirmingTicket);
		isVerifyingEvent = true;
		if (confirmingTicket) {
			userTicketConfirmation = true;
		}
		eventRequest(eventRequest);
		dialog.dismiss();
	}

	@NonNull
	private PCBaseVerifyEventRequest makeVerifyEventRequest(String ticketNbr, boolean confirmingTicket) {
		//make request object
		final PCBaseVerifyEventRequest eventRequest = new PCBaseVerifyEventRequest();
		PCSpecialUser currentUser = activity.getAppUser();
		//set device id
		eventRequest.setDeviceId(activity.getApiKey().getDeviceId());
		eventRequest.setUser(currentUser);
		//set organizer details
		eventRequest.setOrganizer(currentUser);
		eventRequest.setTicketNumber(ticketNbr);
		eventRequest.setEmail(currentUser.getEmail());
		eventRequest.setConfirmTicketUsed(confirmingTicket);
		return eventRequest;
	}

	private void eventRequest(PCBaseVerifyEventRequest eventRequest) throws PCGenericError {
		PCEventsController eventsController = (PCEventsController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.USER_EVENTS_CONTROLLER, pcUserEventsFragment);

		if (eventsController != null) {
			//send Request
			eventsController.setActivity(activity);
			eventsController.setServiceType(PCPesabusClient.PCServiceType.EVENTS);
			eventsController.execute(eventRequest);

		}
	}

	private void initializeAdapter(List<PCEvent> ary) {
		adapter = new PCEventsAdapter(ary, activity);

		if (mainFragmentLayout != null) {
			mainFragmentLayout.setAdapter(adapter);
		}

		if (emptyView != null && ary != null && ary.size() > 0) {
			emptyView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onDataLoadStart(View view) {
		super.onDataLoadStart(view);

		if (pcEventsProfile != null) {
			setUpLayoutFinishProgressBar();
			setAdapter(pcEventsProfile);
		} else {
			makeRequest();
		}
	}

	@Override
	public void onTaskStarted() {

		if (progressBar == null) {
			progressBar = (ProgressBar) getView().findViewById(R.id.progressBar);
		}
		if (progressBar != null) {
			progressBar.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onTaskCompleted(PCData pcData) {
		this.setUpLayoutFinishProgressBar();
		try {
			if (pcData instanceof PCEventsProfile) {

				pcEventsProfile = (PCEventsProfile) pcData;

				if (isVerifyingEvent) {
					PCEvent event = pcEventsProfile.getTicket();

					boolean ticketNbrNotVerified = event != null && event.isSuccess() != null && !event.isSuccess();

					if ((pcData != null && (pcData.isSuccess() == null || (pcData.isSuccess() != null && !pcData.isSuccess())) || ticketNbrNotVerified)) {
						if (ticketNbrNotVerified) {
							pcData.setErrorMessage(event.getDescription());
						}
						PCGenericError error = new PCGenericError();
						error.setMessage(pcData.getErrorMessage());
						activity.presentError(error, "Ticket Verification Error");
					} else {
						createAndShowDialog(pcEventsProfile);

					}
				} else {
					//use data from the service response to populate events list
					setAdapter(pcEventsProfile);
				}
			}
		} catch (Exception e) {
			PCGenericError error = new PCGenericError();
			error.setMessage(e.getMessage());
			activity.presentError(error, "Ticket Verification Error");
		}
	}

	/**
	 * Adapter to use when showing events to the user
	 */
	public class PCEventsAdapter extends BaseAdapter {
		private List<PCEvent> events = new ArrayList<>();
		private Context context;
		private LayoutInflater inflater = null;


		public PCEventsAdapter(List<PCEvent> events, Context context) {
			super();
			this.events = events;
			this.context = context;

		}


		@Override
		public int getCount() {
			return events.size();
		}

		@Override
		public Object getItem(int position) {
			return events.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ListItemHolder listItemHolder;

			final PCEvent event = (PCEvent) getItem(position);

			if (convertView == null) {
				//inflate the layout
				this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.pc_event_list_item_layout, parent, false);

				//setup the viewholder
				listItemHolder = new ListItemHolder();
				listItemHolder.event_profile_image = (ImageView) convertView.findViewById(R.id.event_profile_image);
				listItemHolder.event_ticket_price = (TextView) convertView.findViewById(R.id.event_ticket_price);
				listItemHolder.event_date = (TextView) convertView.findViewById(R.id.event_date);
				listItemHolder.event_name = (TextView) convertView.findViewById(R.id.event_name);
				listItemHolder.event_description = (TextView) convertView.findViewById(R.id.event_description);
				listItemHolder.progress = (ProgressBar) convertView.findViewById(R.id.progress);
				//this is to provide UI for event organizers to receive ent ids
				if (canCurrentUserScanThisEvent(event)) {
					listItemHolder.orginizer_layout = (LinearLayout) convertView.findViewById(R.id.orginizer_layout);
					listItemHolder.orginizer_layout.setVisibility(View.VISIBLE);
					listItemHolder.enter_ticket_nbr = (TextView) convertView.findViewById(R.id.enter_ticket_nbr);
					listItemHolder.scan_event_qr = (TextView) convertView.findViewById(R.id.scan_event_qr);

					listItemHolder.scan_event_qr.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(activity, PCCustomViewFinderScannerActivity.class);
							if (pcUserEventsFragment != null) {
								pcUserEventsFragment.startActivityForResult(intent, 1);
							}
						}
					});

					listItemHolder.enter_ticket_nbr.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							createAndShowEnterTicketNumberDialog();
						}
					});
				}

				listItemHolder.itemView = convertView;
				//cache the view holder
				convertView.setTag(listItemHolder);
			} else {
				listItemHolder = (ListItemHolder) convertView.getTag();
			}

			DecimalFormat formatter = new DecimalFormat("###,###,###.##");

			if (event != null) {
				//get event current details
				PCCurrency currency = event.getCurrency();

				String currencySymbol = getCurrencySymbl(currency);

				//show ticket price or Free
				String total = currencySymbol + " " + formatter.format(event.getPrice());
				if (event.getPrice() == 0) {
					total = "Free";
				}

				listItemHolder.event_ticket_price.setText(total);
			}
			//handle clicks on the event ticket price view
			listItemHolder.event_ticket_price.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (event != null) {
						openEventCheckOut(event);
					}
				}
			});
			listItemHolder.itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (event != null) {
						openEventCheckOut(event);
					}
				}
			});

			if (null != event.getProfilePic()) {
				loadAndSetEventImage(listItemHolder.event_profile_image, listItemHolder.progress, event);
			} else {
				listItemHolder.event_profile_image.setImageResource(R.drawable.pesa_logo_cropped);
				listItemHolder.event_profile_image.setBackgroundResource(R.color.colorPrimary);
				listItemHolder.progress.setVisibility(View.GONE);
			}
			listItemHolder.event_date.setText(event.getDate());
			listItemHolder.event_description.setText(event.getDescription());
			listItemHolder.event_name.setText(event.getName());

			return convertView;
		}

		private String getCurrencySymbl(PCCurrency currency) {
			String currencySymbol = "$";

			if (currency != null) {
				currencySymbol = currency.getCurrencySymbol();
			}
			return currencySymbol;
		}

		private boolean canCurrentUserScanThisEvent(PCEvent event) {
			boolean isEventOrginizer = false;
			if (event != null) {
				PCUser user = activity.getAppUser();

				List<String> clearedVerifiers = event.getClearedVerifiers();

				if (clearedVerifiers != null) {
					for (String clearedVerifier : clearedVerifiers) {

						if ((!StringUtils.isEmpty(clearedVerifier) && clearedVerifier.equalsIgnoreCase(user.getEmail()))
								|| (!StringUtils.isEmpty(clearedVerifier) && clearedVerifier.equalsIgnoreCase(user.getPhoneNumber()))) {
							//set button to scan event QR because this the organizer!
							isEventOrginizer = true;
							break;
						}
					}
				}
			}

			return isEventOrginizer;
		}

		private void loadAndSetEventImage(final ImageView imageView, final ProgressBar progressBar, PCEvent event) {
			if (event == null || event.getProfilePic() == null) {
				return;
			}

			String profilePic = event.getProfilePic();

			// Leave the following URL for backwards compatibility
			// The new default is to use the firebase storage as the repo
			// where the images will be stored.
			if ("ug_independence_la".equalsIgnoreCase(profilePic)) {
				imageView.setImageResource(R.drawable.ug_independence_ca);
				progressBar.setVisibility(View.GONE);
			} else if ("rw_nyball_2017".equalsIgnoreCase(profilePic)) {
				imageView.setImageResource(R.drawable.rw_nyball_2017);
				progressBar.setVisibility(View.GONE);
			} else {
				// Load from Firebase storage
				// The image will be saved with the following format:
				profilePic = profilePic.replaceFirst("firebase:", "");

				// Create a storage reference from our app
				FirebaseStorage storage = FirebaseStorage.getInstance();
				StorageReference storageRef = storage.getReference();
				String rootPath = PCGeneralUtils.isPROD() ? "events/" : "test/events/";
				StorageReference storageReference = storageRef.child(rootPath + profilePic);
				// Load the image using Glide
				Glide.with(context)
						.using(new FirebaseImageLoader())
						.load(storageReference)
						.listener(new RequestListener<StorageReference, GlideDrawable>() {
							@Override
							public boolean onException(Exception e, StorageReference model, Target<GlideDrawable> target, boolean isFirstResource) {
								if (progressBar != null) {
									progressBar.setVisibility(View.GONE);
								}
								return false;
							}

							@Override
							public boolean onResourceReady(GlideDrawable resource, StorageReference model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
								if (progressBar != null) {
									progressBar.setVisibility(View.GONE);
								}
								return false;
							}
						})
						.into(imageView);
			}
		}

		/**
		 * used to open event checkout fragment
		 *
		 * @param event
		 */
		private void openEventCheckOut(PCEvent event) {

			//check if user buying tickets for themselves or gift for other users.
			checkIfUserPurchasingGiftTicket(event);

			PCPesabusClient.PCServiceType serviceType = PCPesabusClient.PCServiceType.TICKETS;
			PCTicketServiceInfo ticketServiceInfo = new PCTicketServiceInfo();
			ticketServiceInfo.setName(event.getName());
			double ticketPrice = event.getPrice();
			PCCurrency currency = event.getCurrency();

			ticketServiceInfo.setAmount(ticketPrice);
			ticketServiceInfo.setId(event.getId());
			if (currency != null) {
				ticketServiceInfo.setReceiverCountry(currency.getCountry());
				ticketServiceInfo.setCurrencySymbl(getCurrencySymbl(currency));
			}

			checkOut(ticketServiceInfo);
		}

		private void checkIfUserPurchasingGiftTicket(PCEvent event) {

		}

		public boolean checkOut(PCTicketServiceInfo pcTicketServiceInfo) {
			boolean result = false;
			PCCheckoutFragment frag = new PCCheckoutFragment();
			PCTicketPaymentData transaction = new PCTicketPaymentData();
			PCMainTabActivity activity = (PCMainTabActivity) getActivity();
			//Set the sender
			if (activity != null && transaction != null) {
				transaction.setSender(activity.getAppUser());
				transaction.setReceiver(activity.getAppUser());
			}
			if (activity != null && activity.getApiKey() != null) {
				transaction.setId(activity.getApiKey().getId());
			}
			transaction.setActionType(PCData.ActionType.TICKETS.name());
			transaction.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.EVENT_TICKETS);
			transaction.setTicketServiceInfo(pcTicketServiceInfo);
			frag.setTransactionData(transaction);
			frag.setServiceType(PCPesabusClient.PCServiceType.TICKETS);
			 if(activity == null) {
				 return result;
			 } else {
				 FragmentManager fm = activity.getSupportFragmentManager();
				 fm.beginTransaction()
						 .replace(R.id.list_of_events, frag, Integer.toString(R.layout.pc_fragment_checkout))
						 .addToBackStack(null);
				 result = true;
			 }
			return result;
		}


	}

	//view holder to cache our listitem layout
	static class ListItemHolder {
		View itemView;
		ImageView event_profile_image;
		TextView event_ticket_price;
		TextView event_date;
		TextView event_description;
		TextView event_name;
		TextView scan_event_qr;
		TextView enter_ticket_nbr;
		ProgressBar progress;
		LinearLayout orginizer_layout;
	}

}