package com.pesachoice.billpay.fragments;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.utils.PCTicketsUtils;
//import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Hold views to hold inputs required to buy ticket
 * A simple {@link Fragment} subclass.
 */
public class PCMyEventTicketFragment extends Fragment {


    public PCMyEventTicketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pc_event_ticket_fragment, container, false);
        ListView eventsQRsListView = (ListView) view.findViewById(R.id.listview_events_qr);
        PCAccountDetailsActivity accountDetailsActivity = (PCAccountDetailsActivity)this.getActivity();
        //get user event tickets
        List<String> eventsQRs = PCTicketsUtils.getEventsQRImages(accountDetailsActivity);
        PCQRadapter pcqRadapter = new PCQRadapter(accountDetailsActivity,R.layout.pc_event_ticket_fragment,eventsQRs);
        eventsQRsListView.setAdapter(pcqRadapter);
        return view;
    }

    class PCQRadapter extends ArrayAdapter<String> {
        private Context context;
        private LayoutInflater inflater = null;
        private List<String> qrFilesPaths;

        public PCQRadapter(Context context, int textViewResourceId, List<String> qrFilesPaths) {
            super(context, textViewResourceId, qrFilesPaths);
            this.context = context;
            this.qrFilesPaths = qrFilesPaths;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ItemViewHolder viewHolder;
            if (convertView == null) {
                // inflate the layout
                convertView = inflater.inflate(R.layout.pc_event_qr_item, parent, false);
                // set up the ViewHolder
                viewHolder = new ItemViewHolder();

                viewHolder.eventName = (TextView) convertView.findViewById(R.id.event_name);
                viewHolder.eventQRImage = (ImageView) convertView.findViewById(R.id.event_qr);
                // store the holder with the view.
                convertView.setTag(viewHolder);
            } else {
                // just avoided calling findViewById() on resource everytime by using the viewHolder
                viewHolder = (ItemViewHolder) convertView.getTag();
            }
            String eventQRPath = (String) qrFilesPaths.get(position);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(eventQRPath, options);
            viewHolder.eventQRImage.setImageBitmap(bitmap);
            return convertView;
        }

    }

    static class ItemViewHolder {
        TextView eventName;
        ImageView eventQRImage;
    }

}

