package com.pesachoice.billpay.fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCPrice;
import com.pesachoice.billpay.model.PCProductServiceInfo;
import com.pesachoice.billpay.model.PCServiceFee;
import com.pesachoice.billpay.model.PCServiceInfoFactory;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCSpinner;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by emmy on 03/04/2018.
 */

public class PCAgentInsuranceCheckout extends PCAgentCheckoutFragment implements PCTransactionComponent {
    private EditText accountText;
    private EditText accountNumberText;
    private EditText durationText;
    private PCSpinner durationTypeSpinner;
    private PCCurrency senderCurr;
    private static final String PAY_WITH_CASH = "Pay With Cash";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pc_fragment_agent_insurance_checkout, container, false);
        try {
            activity = (PCMainTabActivity) getActivity();
            countryProfile = activity.getCountryProfile();
            //calculate the exchange rate
            Log.e("country :","Loadind");
            senderCurr = activity.getAppUser().getBaseCurrency();
            if(senderCurr !=null) {


            } else {
                if(countryProfile.getCountry().equalsIgnoreCase("Rwanda")) {
                    List<PCCurrency> curr = countryProfile.getCurrencies();
                    for(PCCurrency listCurr : curr) {

                        if(listCurr.getCountry().equalsIgnoreCase("Rwanda")) {
                            senderCurr = listCurr;
                            Log.e("Contriessss;",listCurr.getCountry());
                            Log.e("ExchangeRate;",listCurr.getExchangeRate());
                            //Set the current Exchange Rate in CountryProfile
                            countryProfile.setSelectedCountryExchangeRate(1.0);
                            Log.e("country :","finishhhLoadind");
                            break;
                        }

                    }


                }
            }

            this.setViewInfo(view);
            this.attachClickListeners(view);
            if (amRepeatingTrans) {
                setAmountOfRepeatedTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void setViewInfo(View view) {
        try {
            PCBillPaymentData transaction = this.getTransactionData();
            if (view != null && transaction != null) {
                TextView serviceText = (TextView) view.findViewById(R.id.service_summary);
                accountText = (EditText) view.findViewById(R.id.customer_summary);
                accountNumberText = (EditText) view.findViewById(R.id.cutomer_phone);
                durationText = (EditText) view.findViewById(R.id.duration_number);
                durationTypeSpinner = (PCSpinner) view.findViewById(R.id.duration_type);
                durationTypeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                cardNumberText = (PCSpinner) view.findViewById(R.id.card_number);
                senderCurrency = (TextView) view.findViewById(R.id.sender_currency);
                receiverCurrency = (TextView) view.findViewById(R.id.receiver_currency);
                selectManyPackages = (TextView) view.findViewById(R.id.select_many_packages);
                viewServiceFees = (TextView) view.findViewById(R.id.open_pricing_help);
                accountNumberText.setText("+" + countryProfile.getCountryCode());
                List<PCServiceFee> serviceFees = countryProfile.getServiceFees();
                //List<String> serviceFees = new ArrayList<>();
                //show or hide the link to services fees fragment
                if (activity != null && serviceFees != null && serviceFees.size() > 0) {
                    viewServiceFees.setOnClickListener(this);
                } else {
                    viewServiceFees.setVisibility(View.GONE);
                }
                TextView exchangeRateText = null;
                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    exchangeRateText = (TextView) view.findViewById(R.id.textView_exchange_rate);
                    usdAmountText = (EditText) view.findViewById(R.id.usd_amount);
                    localAmountText = (EditText) view.findViewById(R.id.local_amount);
                }
                String service = PCTransactionFragmentFactory.descriptionForService(this.serviceType);
                String receiver = null;

                PCSpecialUser receiverObj = transaction.getReceiver();

                if (receiverObj != null) {
                    receiver = receiverObj.getFullName() == "null null" ? receiverObj.getPhoneNumber() : receiverObj.getFullName();
                }

                if (serviceInfo == null) {
                    serviceInfo = transaction.retrieveServiceInfo();
                }

                if (serviceType != PCPesabusClient.PCServiceType.TICKETS) {
                    PCOperator op;
                    if (this.transactionData.getOperator() != null && this.transactionData.getOperator().getName() != null) {
                        Log.i("TransctionName", this.transactionData.getOperator().getName());
                    }
                    if (countryProfile != null) {
                        PCSpecialUser sender = transaction.getSender();
                        for (PCOperator pc : countryProfile.getOperators()) {
                            Log.i("OperatorName", pc.getName());
                            // we don't need to do currency exchange if user sending money from to the same country,
                            // if sender is using mobile money or if buying calling card.
                            if (PCPesabusClient.PCServiceType.CALLING_REFIL == serviceType
                                    || countryProfile.getCountry().equalsIgnoreCase("Rwanda")) {
                                LinearLayout exchange_rate_details = (LinearLayout) view.findViewById(R.id.exchange_rate_details);
                                exchange_rate_details.setVisibility(View.GONE);
                                ImageView icon_exchange = (ImageView) view.findViewById(R.id.icon_exchange);
                                icon_exchange.setVisibility(View.GONE);
                                localAmountText.setVisibility(View.GONE);
                                receiverCurrency.setVisibility(View.GONE);
                                senderCurrency.setText(countryProfile.getCurrencySymbol() + " ");

                            } else if (exchangeRateText != null) {
                                setExchangeRateViews(exchangeRateText);
                            }
                            PCOperator operator = this.transactionData.getOperator();
                            if (operator != null && operator.getName() != null) {
                                if (pc.getName().trim().equalsIgnoreCase(operator.getName().trim())) {
                                    final List<PCPrice> prices = pc.getPrices();
                                    if (prices != null && prices.size() > 1) {
                                        // instead of preselecting item number 1 in the price list, let's show the user different packages
                                        // then have them select what they want
                                        selectManyPackages.setVisibility(View.VISIBLE);
                                        //showListOfPackages(prices);
                                        double val = prices.get(0).getPrice();

                                        int i = 0;
                                        for (PCPrice prc : prices) {
                                            String symbl = prc.getCurrencySymbol();
                                            if (StringUtils.isEmpty(symbl)) {
                                                symbl = getCurrencySymbol();
                                            }
                                            prc.setCurrencySymbol(symbl);
                                            prices.set(i, prc);
                                            i++;
                                        }

                                        //set values in local and receiver views
                                        setLocalAndRecieverAmountView(val);

                                        selectManyPackages.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                        usdAmountText.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showListOfPackages(prices);
                                            }
                                        });
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }


            Log.e("Update pay", "payment Info");
            this.updatePaymentCardInfo(view);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAndSetReceiverInfo(View v) throws PCGenericError {
        if (getActivity() instanceof PCMainTabActivity) {
            PCSpecialUser appUser = ((PCMainTabActivity) getActivity()).getAppUser();
            transactionData.setSender(appUser);

            PCGenericError error;
            View view = getView();
            if (view == null) {
                // TODO: what should we do here?
                return;
            }

            String fullName = StringUtils.capitalize(accountText.getText().toString());

            String phoneNumber = accountNumberText.getText().toString();
            phoneNumber = PCGeneralUtils.cleanPhoneString(phoneNumber);

            PCMainTabActivity activity = ((PCMainTabActivity) getActivity());
            String countryCode = null;
            if (activity != null) {
                PCCountryProfile countryProfile = activity.getCountryProfile();
                if (countryProfile != null) {
                    countryCode = countryProfile.getCountryCode();
                }
            }

            // Default to UG country code.
            if (countryCode == null) {
                countryCode = "256";
            }

            if (fullName.length() == 0) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("Customer full Name is required");
                throw error;
            }
            if (phoneNumber.length() == 0) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("Customer Phone Number is required");
                throw error;
            }
            if ((phoneNumber.length() < 12 || phoneNumber.length() > 12) && !("1".equals(phoneNumber.substring(0, 1)) && phoneNumber.length() == 11)) {
                error = new PCGenericError();
                error.setTitle("Validation Error");
                error.setMessage("A valid receiver number should have 9 digits excluding +" + countryCode + ". Please check the number again");
                throw error;
            }

            String firstName, lastName = "";
            String[] parts = fullName.split(" ");
            firstName = parts[0];
            for (int i = 1; i < parts.length; i++) {
                lastName += parts[i] + " ";
            }
            lastName = lastName.trim();
            PCSpecialUser receiver = new PCSpecialUser();
            Log.e("Receiver:","ReceiverrInfooo");
            if(receiver != null) {
                receiver.setFullName(fullName);
                receiver.setFirstName(firstName);
                receiver.setLastName(lastName);
                receiver.setPhoneNumber(phoneNumber);
                receiver.setCountry("Rwanda");
                Log.e("Receiver:","ReceiverrInfooo");
            }
            PCBillPaymentData transaction = this.getTransactionData();
            transaction.setReceiver(receiver);
        }
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        // NOTE: Initialize the service info for the very 1st time
        if (transactionData instanceof PCAgentWalletData) {
            if (transactionData.retrieveServiceInfo() == null) {
                PCAgentWalletData transaction = (PCAgentWalletData) this.getTransactionData();
                PCProductServiceInfo serviceInfo = (PCProductServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
                serviceInfo.setCompanyCode("EmbraceSolar");
                serviceInfo.setProductDescription(durationText.getText().toString() + "  " + durationTypeSpinner.getSelectedItem());
                String countrySendingTo = ((PCMainTabActivity)getActivity()).countrySendingTo;
                if (StringUtils.isEmpty(countrySendingTo)) {
                    countrySendingTo = "Rwanda";
                }
                serviceInfo.setReceiverCountry(countrySendingTo);
                //set the account number to be the receiver's phone number
                transaction.setProductServiceInfo(serviceInfo);
                this.setServiceInfo(serviceInfo);
            }
        }
    }


}
