/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPAYTuitionPaymentData;
import com.pesachoice.billpay.model.PCTuitionServiceInfo;
import com.pesachoice.billpay.model.PCServiceInfoFactory;

/**
 * this fragment has views to hold inputs required to make Tuition Transaction
 * A simple {@link Fragment} subclass.
 */
public class PCPayTuitionTransFragment extends PCTransactionFragment {


    public PCPayTuitionTransFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        View view = inflater.inflate(R.layout.pc_fragment_pay_tuition_trans, container, false);
        fullNameText = (EditText) view.findViewById(R.id.full_name);
        phoneText = (EditText) view.findViewById(R.id.mobile);
        setPhoneTextPrefix(view);
        Button continueBtn = (Button) view.findViewById(R.id.continue_button);

        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }
        this.setProviderListSpinner(view);
        this.setAddContactsButton(view);
        return view;
    }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.TUITION;
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        View view = getView();
        if (view == null) { return; }

        PCGenericError error;
        EditText accountText = (EditText) view.findViewById(R.id.account_number);
        EditText schoolName = (EditText) view.findViewById(R.id.school_name);
        EditText semesterName = (EditText) view.findViewById(R.id.section_name);
        EditText facultyName = (EditText) view.findViewById(R.id.faculty_name);

        if (accountText != null && fullNameText != null && phoneText != null && schoolName != null && semesterName != null && facultyName != null ) {//&& majorName!=null
            String accountNumber = getStringValue(accountText);

            if (fullNameText.length() == 0) {
                popupError("Full");
            }
            if (phoneText.length() == 0 || phoneText.length() >= 3){
                popupError("Phone");
            }
            if (schoolName.length() == 0) {
                popupError("School");
            }

            if (facultyName.length() == 0) {
                popupError("Faculty");
            }
            if (accountNumber.length() == 0) {
                popupError("Registration Number is required");
            }
            PCPAYTuitionPaymentData transaction = (PCPAYTuitionPaymentData)this.getTransactionData();

            PCTuitionServiceInfo serviceInfo = (PCTuitionServiceInfo) PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
            serviceInfo.setReceiverCountry(((PCMainTabActivity)getActivity()).countrySendingTo);
            serviceInfo.setAccountNumber(accountNumber);
            serviceInfo.setSchoolName(getStringValue(schoolName));
            serviceInfo.setFaculty(getStringValue(facultyName));
            serviceInfo.setStudentName(getStringValue(fullNameText));
            serviceInfo.setSection(getStringValue(semesterName));
            transaction.setTuitionServiceInfo(serviceInfo);
        }

    }

    @NonNull
    private String getStringValue(EditText editText) {
        return editText.getText().toString();
    }

    private void popupError( String errorMessage) throws PCGenericError {
        PCGenericError error;
        error = new PCGenericError();
        error.setMessage(errorMessage+" name is required");
        throw error;
    }
}
