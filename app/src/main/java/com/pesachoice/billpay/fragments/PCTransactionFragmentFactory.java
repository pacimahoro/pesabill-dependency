/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.support.v4.app.Fragment;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCAirtimePaymentData;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingPaymentData;
import com.pesachoice.billpay.model.PCElectricityPaymentData;
import com.pesachoice.billpay.model.PCInternetPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCPAYTuitionPaymentData;
import com.pesachoice.billpay.model.PCProductPaymentData;
import com.pesachoice.billpay.model.PCTVSubcriptionPaymentData;
import com.pesachoice.billpay.model.PCTicketPaymentData;
import com.pesachoice.billpay.model.PCWaterPaymentData;

/**
 * Used to create the appropriate transaction pcOnPhoneContactLoad based on the type
 * of service that the user selected
 *
 * @author Pacifique Mahoro
 */
public class PCTransactionFragmentFactory {

    public static final String AIRTIME = "airtime";
    public static final String ELECTRICITY = "electricity";
    public static final String TV = "tv";
    public static final String INTERNET = "internet";
    public static final String TUITION = "tuition";
    public static final String WATER = "water";
    public static final String CALL_SERVICES="Calling Card";
    public static final String MONEY_TRANSFER="Money_Transfer";
    public static final String INSURANCE="insurance";
    public static final String AGENT_TRANSACTION="Agent Transaction";
    public static final String MONEY_TRANSFER_ALIAS="Money Transfer";
    public static final String SELL="sell";
    public static final String EVENTS="events";
    public static final String PAY_EVENTS="Pay Events";
    public static final String NEW_AIRTIMESERVICEINFO="airtimeServiceInfo";
    public static final String NEW_CALLINGSERVICEINFO="callingServiceInfo";
    public static final String NEW_ELECTRICITYSERVICEINFO="electricityServiceInfo";
    public static final String NEW_INTERNETSERVICEINFO="internetServiceInfo";
    public static final String NEW_MONEYTRANSFERSERVICEINFO="moneyTransferServiceInfo";
    public static final String NEW_PRODUCTSERVICEINFO="productServiceInfo";
    public static final String NEW_TICKETSERVICEINFO="ticketServiceInfo";
    public static final String NEW_TUITIONSERVICEINFO="tuitionServiceInfo";
    public static final String NEW_TVSERVICEINFO="tvServiceInfo";
    public static final String NEW_WATERSERVICEINFO="waterServiceInfo";
    public static final String NEW_AGENTSERVICEINFO="agentServiceInfo";

    public static PCPesabusClient.PCServiceType getServiceType(String strType) {
        if (strType == null || strType.length() == 0) {
            return null;
        }

        PCPesabusClient.PCServiceType type = null;
        if (AIRTIME.equalsIgnoreCase(strType) || NEW_AIRTIMESERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.SEND_AIRTIME;
        }
        else if (ELECTRICITY.equalsIgnoreCase(strType) || NEW_ELECTRICITYSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.PAY_ELECTRICITY;
        }
        else if (TV.equalsIgnoreCase(strType) || NEW_TVSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.PAY_TV;
        }
        else if (INTERNET.equalsIgnoreCase(strType) || NEW_INTERNETSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.PAY_INTERNET;
        }
        else if (TUITION.equalsIgnoreCase(strType) || NEW_TUITIONSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.PAY_TUITION;
        }
        else if (WATER.equalsIgnoreCase(strType) || NEW_WATERSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.PAY_WATER;
        }
        else if (CALL_SERVICES.equalsIgnoreCase(strType) || NEW_CALLINGSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.CALLING_REFIL;
        }
        else if (MONEY_TRANSFER.equalsIgnoreCase(strType) || MONEY_TRANSFER_ALIAS.equalsIgnoreCase(strType) || NEW_MONEYTRANSFERSERVICEINFO.equalsIgnoreCase(strType)){
            type = PCPesabusClient.PCServiceType.MONEY_TRANSFER;
        }
        else if ( SELL.equalsIgnoreCase(strType) || NEW_PRODUCTSERVICEINFO.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.SELL;
        }
        else if (EVENTS.equalsIgnoreCase(strType)) {
            type = PCPesabusClient.PCServiceType.EVENTS;
        }
        else if (PAY_EVENTS.equalsIgnoreCase(strType ) || NEW_TICKETSERVICEINFO.equalsIgnoreCase(strType )) {
            type = PCPesabusClient.PCServiceType.TICKETS;
        } else if (AGENT_TRANSACTION.equalsIgnoreCase(strType ) || NEW_AGENTSERVICEINFO.equalsIgnoreCase(strType) ) {
            type = PCPesabusClient.PCServiceType.AGENT_TRANSACTION;
        }else if (INSURANCE.equalsIgnoreCase(strType ) ) {
            type = PCPesabusClient.PCServiceType.INSURANCE;
        }


        return type;
    }

    public static PCTransactionComponent<? extends Fragment>
    constructTransactionFragment(PCPesabusClient.PCServiceType serviceType) {
        PCTransactionComponent fragment = null;
        PCBillPaymentData transactionData;
        if (serviceType != null) {
            switch (serviceType) {
                case SEND_AIRTIME:
                    transactionData = new PCAirtimePaymentData();
                    // TODO: Need to set the sender here.
                    fragment = new PCAirtimeTransactionFragment();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.AIR_TIME);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.SEND_AIRTIME);
                    break;
                case MONEY_TRANSFER:
                case MONEY_TRANSFER_ALIAS:
                    transactionData = new PCMoneyTransferPaymentData();
                    fragment = new PCMoneyTransferTransactionFragment();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.MONEY_TRANSFER);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.MONEY_TRANSFER);
                    break;
                case CALLING_REFIL:
                    transactionData = new PCCallingPaymentData();
                    fragment = new PCAirtimeTransactionFragment();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.CALLING);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.CALLING_REFIL);
                    break;
                case PAY_ELECTRICITY:
                    transactionData = new PCElectricityPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL);
                    fragment = new PCElectricityTransactionFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.PAY_ELECTRICITY);
                    break;
                case PAY_INTERNET:
                    transactionData = new PCInternetPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.INTERNET_BILL);
                    fragment = new PCInternetTransactionFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.PAY_INTERNET);
                    break;
                case PAY_TV:
                    transactionData = new PCTVSubcriptionPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.TV_BILL);
                    fragment = new PCTVTransactionFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.PAY_TV);
                    break;
                case PAY_TUITION:
                    transactionData = new PCPAYTuitionPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.TUITION);
                    fragment= new PCPayTuitionTransFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.PAY_TUITION);
                    break;
                case PAY_WATER:
                    transactionData = new PCWaterPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.WATER);
                    fragment= new PCWaterTransactionFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.PAY_WATER);
                    break;
                case SELL:
                    transactionData = new PCProductPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS);
                    fragment = new PCProductPaymentFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.SELL);
                    break;
                case TICKETS:
                    transactionData = new PCTicketPaymentData();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.EVENT_TICKETS);
                    fragment = new PCProductPaymentFragment();
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.EVENTS);
                    break;
                case PAYMENT:
                    transactionData = new PCMoneyTransferPaymentData();
                    fragment = new PCMoneyTransferTransactionFragment();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.MONEY_TRANSFER);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.MONEY_TRANSFER);
                    break;
                case AGENT_TRANSACTION:
                    transactionData = new PCAgentWalletData();
                    fragment = new PCAgentCheckoutFragment();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.AGENT_TRANSACTION);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.AGENT_TRANSACTION);
                    break;
                case INSURANCE:
                    transactionData = new PCAgentWalletData();
                    fragment = new PCAgentInsuranceCheckout();
                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.AGENT_TRANSACTION);
                    fragment.setTransactionData(transactionData);
                    fragment.setServiceType(PCPesabusClient.PCServiceType.INSURANCE);
                    break;
                default:
                    break;

            }
        }
        return fragment;
    }

    public static String descriptionForService(PCPesabusClient.PCServiceType serviceType) {
        String desc = null;
        if (serviceType != null) {
            switch (serviceType) {
                case SEND_AIRTIME:
                    desc = "Pay Airtime";
                    break;
                case PAY_ELECTRICITY:
                    desc = "Pay Electricity";
                    break;
                case PAY_INTERNET:
                    desc = "Pay Internet";
                    break;
                case PAY_TV:
                    desc = "Pay TV";
                    break;
                case PAY_TUITION:
                    desc="Pay Tuition";
                    break;
                case CALLING_REFIL:
                    desc="Calling Card";
                    break;
                case PAY_WATER:
                    desc="Pay Water";
                    break;
                case MONEY_TRANSFER:
                    desc="Money Transfer";
                    break;
                case PAYMENT:
                    desc="Money Transfer";
                    break;
                case SELL:
                    desc="Sell Product";
                    break;
                case TICKETS:
                    desc="Buy Tickets";
                    break;
                case AGENT_TRANSACTION:
                    desc="Agent Transaction";
                    break;
                case INSURANCE:
                    desc="Agent Transaction";
                    break;
                default:
                    break;

            }
        }

        return desc;
    }

    public static PCBillPaymentData
    getBillPaymentData(String serviceType){
        PCBillPaymentData billPaymentData=null;
        switch (serviceType) {
            case AIRTIME:
            case NEW_AIRTIMESERVICEINFO:
                billPaymentData = new PCAirtimePaymentData();
                break;
            case ELECTRICITY:
            case NEW_ELECTRICITYSERVICEINFO:
                billPaymentData = new PCElectricityPaymentData();
                break;
            case INTERNET:
            case NEW_INTERNETSERVICEINFO:
                billPaymentData = new PCInternetPaymentData();
                break;
            case TV:
            case NEW_TVSERVICEINFO:
                billPaymentData = new PCTVSubcriptionPaymentData();
                break;
            case MONEY_TRANSFER:
            case NEW_MONEYTRANSFERSERVICEINFO:
                billPaymentData = new PCMoneyTransferPaymentData();
                break;
            case SELL:
            case NEW_PRODUCTSERVICEINFO:
                billPaymentData = new PCProductPaymentData();
                break;
            case EVENTS:
            case NEW_TICKETSERVICEINFO:
                billPaymentData = new PCTicketPaymentData();
                break;
            case WATER:
            case NEW_WATERSERVICEINFO:
                billPaymentData = new PCWaterPaymentData();
                break;
            case TUITION:
            case NEW_TUITIONSERVICEINFO:
                billPaymentData = new PCPAYTuitionPaymentData();
                break;
            case CALL_SERVICES:
            case NEW_CALLINGSERVICEINFO:
                billPaymentData = new PCCallingPaymentData();
                break;
            case AGENT_TRANSACTION:
            case NEW_AGENTSERVICEINFO:
                billPaymentData = new PCAgentWalletData();
                break;
            default:
                break;
        }
        return billPaymentData;
    }
}
