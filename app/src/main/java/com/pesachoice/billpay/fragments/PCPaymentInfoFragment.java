package com.pesachoice.billpay.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.pesachoice.billpay.activities.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PCPaymentInfoFragment extends DialogFragment {


    public PCPaymentInfoFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.pc_fragment_payment_info, null);
        LinearLayout add_serviceFees = (LinearLayout) view.findViewById(R.id.add_serviceFees);
        this.setViewInfo(add_serviceFees);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Payment Info").setView(view);


        return builder.create();
    }

    public void setViewInfo(LinearLayout view) {
        String[] column = { "Service Type", "Payment Type", "Fee", "Min", "Max" };
        String[] row = { "1", "2", "3", "4",
                };
        int rl = row.length; int cl=column.length;

        ScrollView sv = new ScrollView(this.getActivity());
        TableLayout tableLayout = createTableLayout(row, column,rl, cl);

        HorizontalScrollView hsv = new HorizontalScrollView(this.getActivity());
        view.addView(tableLayout);
        view.invalidate();
    }


    private TableLayout createTableLayout(String [] rv, String [] cv,int rowCount, int columnCount) {
        // 1) Create a tableLayout and its params
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams();
        TableLayout tableLayout = new TableLayout(this.getActivity());
        tableLayout.setLayoutParams(new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // 2) create tableRow params
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams();
        tableRowParams.setMargins(1, 1, 1, 1);
        tableRowParams.weight = 1;

        for (int i = 0; i < rowCount; i++) {
            // 3) create tableRow
            TableRow tableRow = new TableRow(this.getActivity());
            tableRow.setLayoutParams((new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)));
            tableRow.setBackgroundColor(Color.BLACK);

            for (int j= 0; j < columnCount; j++) {
                // 4) create textView
                TextView textView = new TextView(this.getActivity());
                textView.setBackgroundColor(Color.WHITE);
                textView.setGravity(Gravity.CENTER);

                String s1 = Integer.toString(i);
                String s2 = Integer.toString(j);
                String s3 = s1 + s2;
                int id = Integer.parseInt(s3);
               //TODO: DUMMY DATA: to be removed before release to prod
                if (i == 0 && j == 0) {
                    textView.setText("***");
                } else if (i == 0 ){

                    textView.setText(cv[j-1]);
                }else if (j == 0){

                    textView.setText(rv[i-1]);
                }else {
                    textView.setText(""+id);
                }

                // 5) add textView to tableRow
                tableRow.addView(textView, tableRowParams);
            }

            // 6) add tableRow to tableLayout
            tableLayout.addView(tableRow, tableLayoutParams);
        }

        return tableLayout;
    }

}
