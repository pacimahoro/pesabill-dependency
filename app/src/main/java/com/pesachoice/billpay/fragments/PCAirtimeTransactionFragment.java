/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCAirtimePaymentData;
import com.pesachoice.billpay.model.PCAirtimeServiceInfo;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingPaymentData;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCServiceInfoFactory;

import org.w3c.dom.Text;

/**
 * Represents a pcOnPhoneContactLoad for airtime service
 * @author Pacifique Mahoro
 * @author Desire AHEZA
 */
public class PCAirtimeTransactionFragment extends PCTransactionFragment {

    private final static String CLAZZZ =  PCAirtimeTransactionFragment.class.getName();

    public PCAirtimeTransactionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pc_fragment_transaction_airtime, container, false);

        fullNameText = (EditText) view.findViewById(R.id.full_name);
        phoneText = (EditText) view.findViewById(R.id.mobile);
        setPhoneTextPrefix(view);
        if (PCPesabusClient.PCServiceType.CALLING_REFIL.equals(this.getServiceType())) {
            LinearLayout show_link_to_help = (LinearLayout) view.findViewById(R.id.show_link_to_help);
            show_link_to_help.setVisibility(View.VISIBLE);
            TextView open_calling_help = (TextView) view.findViewById(R.id.open_calling_help);
            open_calling_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    PCFragmentCallingCardHelp frag = new PCFragmentCallingCardHelp();
                    fm.beginTransaction()
                            .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_calling_card_help))
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
        Button continueBtn = (Button)view.findViewById(R.id.continue_button);
        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }

        this.setAddContactsButton(view);
        // set Provider List
        this.setProviderListSpinner(view);

        return view;
    }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.AIRTIME;
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {

        PCBillPaymentData trans = this.getTransactionData();
        if (trans != null && trans.getPaymentProcessingType() != null) {
            if (PCActivity.PCPaymentProcessingType.CALLING.equals(trans.getPaymentProcessingType())) {
                PCCallingPaymentData transaction = (PCCallingPaymentData)this.getTransactionData();
                PCCallingServiceInfo serviceInfo = (PCCallingServiceInfo)PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
                serviceInfo.setAccountNumber(transaction.getReceiver().getPhoneNumber());
                transaction.setCallingServiceInfo(serviceInfo);
            }
            else if (PCActivity.PCPaymentProcessingType.AIR_TIME.equals(trans.getPaymentProcessingType())) {
                PCAirtimePaymentData transaction = (PCAirtimePaymentData)this.getTransactionData();
                PCAirtimeServiceInfo serviceInfo = (PCAirtimeServiceInfo)PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
                serviceInfo.setReceiverCountry(((PCMainTabActivity)getActivity()).countrySendingTo);
                serviceInfo.setAccountNumber(transaction.getReceiver().getPhoneNumber());
                transaction.setAirtimeServiceInfo(serviceInfo);
            }
        }
    }

    @Override
    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity)getActivity();

        if (activity != null) {
            activity.presentError(error, TRANSACTION_ERROR_TITLE);
        }
    }

}
