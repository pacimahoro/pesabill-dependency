/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCountryProfileController;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCAirtimePaymentData;
import com.pesachoice.billpay.model.PCAirtimeServiceInfo;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingPaymentData;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferServiceInfo;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCServiceInfoFactory;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCCountryItem;
import com.pesachoice.billpay.utils.PCFontManager;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCSpinner;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Represents a pcOnPhoneContactLoad for airtime service
 * @author Pacifique Mahoro
 * @author Desire AHEZA
 */
public class PCMoneyTransferTransactionFragment extends PCTransactionFragment implements PConRequiredDataLoaded  {
    private final static String CLAZZZ =  PCMoneyTransferTransactionFragment.class.getName();
    private ArrayList<PCCountryItem> countryList;
    private  CountryArrayAdapter adapter;
    public String countrySendingTo;
    private PCSupportRepeatTransFragment repeatTransactionFragment = null;
    public PCPesabusClient.PCServiceType currentServiceType;
    private PCMainTabsFragment.TypeOfDataToAdapt typeOfDatatToAdapt;
    private Context context;
    private ProgressDialog progressDialog;
    private PConRequiredDataLoaded dataloadedFrag;


    public PCMoneyTransferTransactionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.pc_fragment_transaction_money_transfer, container, false);
         fullNameText = (EditText) view.findViewById(R.id.full_name);
         phoneText = (EditText) view.findViewById(R.id.mobile);
         initList();
         PCSpinner spinnnerCountries = (PCSpinner) view.findViewById(R.id.country_flag);
         spinnnerCountries.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
         adapter = new CountryArrayAdapter(this.getContext(),countryList);
         spinnnerCountries.setAdapter(adapter);
         dataloadedFrag = this;
         spinnnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
              @Override
              public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                  PCCountryItem countryClicked = (PCCountryItem) adapterView.getItemAtPosition(i);
                  countrySendingTo = countryClicked.getCountryName();
                  Log.e("Country :" ,countrySendingTo);
                  if(getActivity() instanceof  PCMainTabActivity) {
                      PCMainTabActivity mainTabActivity = (PCMainTabActivity) getActivity();
                      mainTabActivity.makeCallToGetCountryProfile(countrySendingTo,dataloadedFrag,null);
                      dataloadedFrag.onDataLoadStart(view);

                  }

              }

              @Override
              public void onNothingSelected(AdapterView<?> adapterView) {
                  PCCountryItem countryItem = (PCCountryItem) adapterView.getSelectedItem();
                 String countrySendingTo = countryItem.getCountryName();
                  //TextView country = (TextView) view.findViewById(R.id.country_picked);
                  //country.setText(countrySendingTo);
              }
          });
        setPhoneTextPrefix(view);
        Button continueBtn = (Button) view.findViewById(R.id.continue_button);
        if (continueBtn != null) {
            continueBtn.setOnClickListener(this);
        }
        this.setAddContactsButton(view);
        return view;
    }


    private void initList() {
        countryList = new ArrayList<>();
        if(getActivity() instanceof  PCMainTabActivity) {
            PCMainTabActivity mainTabActivity = (PCMainTabActivity) getActivity();
            PCOperatorRequest operatorUser = mainTabActivity.getPcOperatorRequest(countrySendingTo);
            if (operatorUser.getCompanyId().equalsIgnoreCase("virunga")) {
                countryList.add(new PCCountryItem("Kenya", R.drawable.icn_kenya));
                countryList.add(new PCCountryItem("Rwanda",R.drawable.icn_rwanda));
                 countryList.add(new PCCountryItem("Uganda",R.drawable.icn_uganda));
            } else {
                countryList.add(new PCCountryItem("Zimbabwe", R.drawable.icn_zimbabwe));
            }

        }

     }

    @Override
    public PCData.ActionType getActionType() {
        return PCData.ActionType.MONEY_TRANSFER;
    }

    @Override
    public void getServiceInfo(View v) throws PCGenericError {
        PCBillPaymentData trans = this.getTransactionData();
        if (trans != null && trans.getPaymentProcessingType() != null) {
            PCMoneyTransferPaymentData transaction = (PCMoneyTransferPaymentData)this.getTransactionData();
            PCMoneyTransferServiceInfo serviceInfo = (PCMoneyTransferServiceInfo)PCServiceInfoFactory.constructServiceInfo(this.getServiceType());
            serviceInfo.setReceiverCountry(((PCMainTabActivity)getActivity()).countrySendingTo);
            //set the account number to be the receiver's phone number
            serviceInfo.setAccountNumber(transaction.getReceiver().getPhoneNumber());
            transaction.setMoneyTransferServiceInfo(serviceInfo);
        }
    }


    @Override
    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity)getActivity();

        if (activity != null) {
            activity.presentError(error, TRANSACTION_ERROR_TITLE);
        }
    }

    @Override
    public void onDataLoadStart(View view) {
        //progressDialog = new ProgressDialog();
        progressDialog = ProgressDialog.show(this.getContext(), "Loading country information","", true);
    }

    @Override
    public void onDataLoadFinished(PCData pcData) {
        Log.e(CLAZZZ, "DataLoad");
        if (progressDialog != null) {
            if (phoneText.getText() == null) {
                phoneText.setHint("+" + ((PCMainTabActivity) getActivity()).getCountryProfile().getCountryCode() + " 751-231-230");
            } else {
                phoneText.setText("");
                phoneText.setHint("+" + ((PCMainTabActivity) getActivity()).getCountryProfile().getCountryCode() + " 751-231-230");
            }
            progressDialog.dismiss();

        }

    }

    @Override
    public void onCollectionDataLoadFinished(Collection<? extends PCData> pcData) {
        Log.e(CLAZZZ,"OnCollectionData");

    }

    private class CountryArrayAdapter extends ArrayAdapter {
        private PCMainTabsFragment.TypeOfDataToAdapt typeOfDatatToAdapt;
        private Context context;
        private PCMainTabsFragment frag = new PCActivityDetailFragment();

         public CountryArrayAdapter (Context context, ArrayList<PCCountryItem> countryList) {
             super(context,0,countryList);

         }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        public View initView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.pc_country_flag_spinner, parent, false);
            }

            CircleImageView imageView = (CircleImageView) convertView.findViewById(R.id.country_picker_image);
            TextView countryText = (TextView) convertView.findViewById(R.id.country_picked);
            PCCountryItem countryItem = (PCCountryItem) getItem(position);
            if(countryItem != null) {
                imageView.setImageResource(countryItem.getCountryFlag());
                countryText.setText(countryItem.getCountryName());
            }
            return convertView;
        }
    }
}
