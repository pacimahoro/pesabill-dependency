/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCAgentWalletTransaction;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCSpecialUser;

import java.util.List;

/**
 * this adapter will be used to adapter countries and products we support in each country
 * Created by desire.aheza on 4/10/2016.
 */
public class PCCountryAndProductAdapter extends BaseAdapter {
    private Context context;
    private List<String> productNames;
    private List<Integer> productImages;
    private LayoutInflater inflater = null;
    private PCMainTabsFragment.TypeOfDataToAdapt typeOfDatatToAdapt;

    // TODO: Fix or rename this "frag", we should avoid initializing things here
    private PCMainTabsFragment frag = new PCActivityDetailFragment();

    public PCCountryAndProductAdapter(Context c, List<String> productNames, List<Integer> productImages) {
        this.context = c;
        this.productNames = productNames;
        this.productImages = productImages;

        if (context != null) {
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }


    public void setTypeOfDatatToAdapt(PCMainTabsFragment.TypeOfDataToAdapt typeOfDatatToAdapt) {
        this.typeOfDatatToAdapt = typeOfDatatToAdapt;
    }

    @Override
    public int getCount() {
        return this.productNames.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.pc_fragment_products_item, null);
        TextView tv = (TextView) rowView.findViewById(R.id.textView1);
        ImageView img = (ImageView) rowView.findViewById(R.id.imageView1);

        if (position < this.productNames.size()) {
            String productName = this.productNames.get(position);
            tv.setText(productName.contains("_") ? productName.replace("_", " ") : productName);
        }
        if (position < this.productImages.size()) {
            img.setImageResource(this.productImages.get(position));
        }
        Log.e("Item Selected","selectedItemName");
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    TextView textView = (TextView) v.findViewById(R.id.textView1);
                    String selectedItemName = textView.getText().toString();
                    Log.e("Item Selected","adapt");
                    if (typeOfDatatToAdapt == PCMainTabsFragment.TypeOfDataToAdapt.PRODUCTS) {
                        Log.e("Item Selected","adapt");
                        PCPesabusClient.PCServiceType serviceType = PCTransactionFragmentFactory.getServiceType(selectedItemName);

                        if (serviceType != null) {
                            Log.e("Item Selected",serviceType.toString());
                            if (serviceType == PCPesabusClient.PCServiceType.EVENTS) {
                                PCMainTabActivity mainTabActivity = (PCMainTabActivity) context;

                                if (mainTabActivity != null) {
                                    PCUserEventsFragment frag = new PCUserEventsFragment();
                                    FragmentManager fm = ((PCMainTabActivity) context).getSupportFragmentManager();
                                    fm.beginTransaction()
                                            .replace(R.id.grid_container, frag)
                                            .addToBackStack(null)
                                            .commit();
                                }
                            } else {
                               /* PCTransactionComponent<? extends Fragment> frag = PCTransactionFragmentFactory.constructTransactionFragment(serviceType);
                                FragmentManager fm = ((PCMainTabActivity) context).getSupportFragmentManager();
                                fm.beginTransaction()
                                        .replace(R.id.grid_container_country, (Fragment) frag, Integer.toString(R.layout.pc_fragment_checkout))
                                        .addToBackStack(null)
                                        .commit();
                                //TODO: this is really concerning need to discuss with PACI The reason he it this way
                                if (serviceType == PCPesabusClient.PCServiceType.SELL) {
                                    PCMainTabActivity mainTabActivity = (PCMainTabActivity)context;
                                    if (mainTabActivity != null) {
                                        mainTabActivity.retrieveMerchantsdProfile(mainTabActivity.countrySendingTo);
                                    }
                                }*/
                                Log.e("Item Selected",selectedItemName);
                                if (selectedItemName.equalsIgnoreCase("INSURANCE")) {
                                    Log.e("Item Selected",selectedItemName);
                                    PCTransactionComponent<? extends Fragment> frag = PCTransactionFragmentFactory.constructTransactionFragment(serviceType);
                                    FragmentManager fm = ((PCMainTabActivity) context).getSupportFragmentManager();
                                    PCAgentWalletData transactionData = new PCAgentWalletData();
                                    transactionData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.AGENT_TRANSACTION);
                                    frag.setTransactionData(transactionData);
                                    Log.e("Item Selected","after payment process");
                                    frag.setServiceType(PCPesabusClient.PCServiceType.INSURANCE);
                                    fm.beginTransaction()
                                            .replace(R.id.grid_container, (Fragment) frag, Integer.toString(R.layout.pc_fragment_agent_insurance_checkout))
                                            .addToBackStack(null)
                                            .commit();
                                } else {
                                    Log.e("Item Selected","after if condition");
                                    buildAndPresentCheckout(serviceType);
                                }
                            }

                        }
                    } else if (typeOfDatatToAdapt == PCMainTabsFragment.TypeOfDataToAdapt.COUNTRIES) {
                        if (frag == null)
                            frag = new PCProductsFragment();
                        else if (!selectedItemName.equalsIgnoreCase(frag.selectedCountry))
                            frag = new PCProductsFragment();
                        frag.setSelectedCountry(selectedItemName);
                        FragmentManager fm = ((PCMainTabActivity) context).getSupportFragmentManager();
                        fm.beginTransaction()
                                .replace(R.id.grid_container, (Fragment) frag, selectedItemName)
                                .addToBackStack(null)
                                .commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return rowView;
    }

    private void buildAndPresentCheckout(PCPesabusClient.PCServiceType serviceType) {
        if (context instanceof PCMainTabActivity) {
            PCMainTabActivity mainTabActivity = (PCMainTabActivity) context;
            PCSpecialUser appUser = mainTabActivity.getAppUser();
            boolean isAgent = appUser != null && (appUser.isAgent() || appUser.isThirdParty());
            if (isAgent) {
                serviceType = PCPesabusClient.PCServiceType.AGENT_TRANSACTION;
            }

            PCTransactionComponent<? extends Fragment> frag = PCTransactionFragmentFactory.constructTransactionFragment(serviceType);

            FragmentManager fm = ((PCMainTabActivity) context).getSupportFragmentManager();
            fm.beginTransaction()
                    .replace(R.id.grid_container, (Fragment) frag, serviceType.name())
                    .addToBackStack(null)
                    .commit();
        }
    }
}
