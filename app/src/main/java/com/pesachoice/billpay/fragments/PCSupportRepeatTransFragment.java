package com.pesachoice.billpay.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTicketServiceInfo;
import com.pesachoice.billpay.model.PCTransaction;

import org.springframework.util.StringUtils;

import java.util.List;


/**
 * this is used to support repeat transaction in pesabill.
 * we will have to extend this class everytime we want repeat transaction features
 */
public abstract class PCSupportRepeatTransFragment extends PCMainTabsFragment implements PCAsyncListener {

    private static final String CLAZZZ = PCSupportRepeatTransFragment.class.getName();
    protected PCTransaction transaction = new PCTransaction();
    protected PCCheckoutFragment frag = null;
    private PCAgentCheckoutFragment agentFrag = null;
    protected PCServiceInfo pcServiceInfo = new PCTicketServiceInfo();
    protected PCMainTabActivity activity = null;
    protected ProgressDialog progressDialog = null;
    protected View repeatTranscation = null;
    private PCPesabusClient.PCServiceType serviceType = null;
    protected boolean hasNotSetRepeatTransactionListener = true;
    protected PCSupportRepeatTransFragment repeatTransFragment;
    private PCBillPaymentData pcBillPaymentData;
    private PCServiceInfo serviceInfo;
    private PCSpecialUser receiver;

    public PCSupportRepeatTransFragment() {
        // Required empty public constructor
    }

    public void setPcServiceInfo(PCServiceInfo serviceInfo) {
        this.pcServiceInfo = serviceInfo;
    }

    public void setTransaction(PCTransaction transaction) {
        this.transaction = transaction;
    }

    public PCTransaction getTransaction() {
        return this.transaction;
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskCompleted(PCData data) {

    }

    private void checkOut() {
        FragmentManager fm = activity.getSupportFragmentManager();
        if (activity instanceof PCMainTabActivity) {
            PCOperatorRequest op = activity.getPcOperatorRequest("Rwanda");
            if (op != null && op.getCompanyId().equalsIgnoreCase("embracesolar")) {
                frag = new PCAgentCheckoutFragment();
                frag.amRepeatingTrans = true;
                fm.beginTransaction()
                        .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_agent_checkout))
                        .addToBackStack(null)
                        .commit();
                //to scoll back to tab one

            } else {
                frag.amRepeatingTrans = true;
                fm.beginTransaction()
                        .replace(R.id.grid_container_country, frag, Integer.toString(R.layout.pc_fragment_checkout))
                        .addToBackStack(null)
                        .commit();
            }
        }
        activity.movePrevious(1);
        if (repeatTransFragment instanceof PCActivityDetailFragment) {
            //close the open fragment i.e. activity opened to go back to the list of activities
            activity.getSupportFragmentManager().beginTransaction().remove(this).commit();
        }

    }

    protected void prepareCheckout() {
        try {
            activity = (PCMainTabActivity) getActivity();
            if (activity != null && transaction != null) {
                final PCOperatorRequest operatorRequest = activity.getPcOperatorRequest(activity.getCountryProfile().getCountry());
                if (operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
                    frag = new PCAgentCheckoutFragment();
                    serviceType = PCTransactionFragmentFactory.getServiceType("agentServiceInfo");
                    transaction.setSender(activity.getAppUser());
                    transaction.setActionType(transaction.getActionType());
                    frag.setServiceType(serviceType);
                    pcBillPaymentData = PCTransactionFragmentFactory.getBillPaymentData("agentServiceInfo");
                    pcBillPaymentData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.getProcessingType("agentServiceInfo"));
                    pcBillPaymentData.setServiceInfo(transaction.getServiceInfo());
                    serviceInfo = pcBillPaymentData.getServiceInfo();
                    receiver = transaction.getReceiver();
                    if (serviceInfo != null) {
                        List<PCCurrency> currency = activity.getCountryProfile().getCurrencies();
                        for (PCCurrency listCurr : currency) {
                            if(listCurr.getCountry().equalsIgnoreCase("Rwanda")) {
                                    serviceInfo.setAmount(listCurr.getAmount());
                                    serviceInfo.setTotalAmount(listCurr.getTotalAmount());

                                if (receiver != null)
                                    receiver.setCountry(serviceInfo.getReceiverCountry());

                                   break;
                            }
                        }
                    }

                } else {
                    frag = new PCCheckoutFragment();
                    serviceType = PCTransactionFragmentFactory.getServiceType(transaction.getServiceType());
                    transaction.setSender(activity.getAppUser());
                    transaction.setActionType(transaction.getActionType());
                    frag.setServiceType(serviceType);
                    pcBillPaymentData = PCTransactionFragmentFactory.getBillPaymentData(transaction.getServiceType());
                    pcBillPaymentData.setPaymentProcessingType(PCActivity.PCPaymentProcessingType.getProcessingType(transaction.getServiceType()));
                    pcBillPaymentData.setServiceInfo(transaction.getServiceInfo());
                    serviceInfo = pcBillPaymentData.retrieveServiceInfo();
                    receiver = transaction.getReceiver();
                    if (transaction.getBaseCurrency() != null && serviceInfo != null) {
                        PCCurrency currency = transaction.getBaseCurrency();
                        if (currency.getTotalAmount() > 0 || currency.getAmount() > 0) {
                            serviceInfo.setAmount(currency.getAmount());
                            serviceInfo.setTotalAmount(currency.getTotalAmount());
                        }
                        if (receiver != null)
                            receiver.setCountry(serviceInfo.getReceiverCountry());
                    }

                }

                if (activity.getApiKey() != null) {
                    transaction.setId(activity.getApiKey().getId());
                }
                //TODO: need to fix this bug and also ask Odilon to modify the serviceType String
                // NOTE: We need to set the amount and totalAmount when a transaction was done especially in a different currency.
                pcBillPaymentData.setReceiver(receiver);
                pcBillPaymentData.setSender(((PCMainTabActivity) this.getActivity()).getAppUser());
                pcBillPaymentData.setOperator(this.getTransaction().getOperator());
                frag.setTransactionData(pcBillPaymentData);
                this.pcServiceInfo = serviceInfo;
                frag.setServiceInfo(this.pcServiceInfo);
            }
        } catch (Exception e) {
            String errorMessage = "Error while trying to repeat a transaction";
            presentError(e, errorMessage);
            e.printStackTrace();
        }
    }

    protected void presentError(Exception e, String errorMessage) {
        Log.e(CLAZZZ, errorMessage + "[" + e.getMessage() + "]");
        PCGenericError error = new PCGenericError();
        error.setMessage(errorMessage);
        this.presentError(error, "Error", CLAZZZ);
    }

    /**
     * Initiate checkout boolean.
     *
     * @return the boolean changed from protected void for retry transaction unit test
     */
    public boolean initiateCheckout() {
        boolean repeatResult = false;
        //String country = transaction.getServiceInfo().getReceiverCountry();
        String country = "";
        if (activity != null) {
            /**
             * use current country profile if we got the profile of the same transaction
             */
            final PCOperatorRequest operatorRequest = activity.getPcOperatorRequest(activity.getCountryProfile().getCountry());
            if (operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
                country = transaction.getProductServiceInfo().getReceiverCountry();
            } else {
                country = transaction.getServiceInfo().getReceiverCountry();
            }
            if (PCPesabusClient.PCServiceType.CALLING_REFIL == serviceType || (activity.getCountryProfile() != null && country.equalsIgnoreCase(activity.getCountryProfile().getCountry()))) {
                checkOut();
                repeatResult = true;
            } else if (!StringUtils.isEmpty(country)) {
                progressDialog = ProgressDialog.show(activity, "Repeat Transaction", "loading...", true);
                //open checkout to repeat the same transaction
                activity.makeCallToGetCountryProfile(country, null, repeatTransFragment);
                repeatResult = true;

            }
        }
        return repeatResult;
    }

    public void onCountryProfileDataLoadFinished() {
        dismissProgressBar();
        this.checkOut();
    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    protected void presentError(PCGenericError error, String title, final String CLAZZZ) {
        String message = error.getMessage();
        if (activity != null) {
            if (activity.isFinishing() || message == null) {
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setTitle(title);

            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Log.d(CLAZZZ, "Clicked on OK on Alert box");
                }
            });

            Dialog alert = builder.create();
            alert.show();
        }
    }

}
