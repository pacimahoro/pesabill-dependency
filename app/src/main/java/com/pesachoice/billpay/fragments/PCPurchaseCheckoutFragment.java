package com.pesachoice.billpay.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCCardInfo;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCProductPaymentData;
import com.pesachoice.billpay.model.PCProductServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCKeychainWrapper;


import org.springframework.util.StringUtils;


/**
 * this fragment has views to hold inputs and methods required to make a purchase transaction
 * A simple {@link Fragment} subclass.
 */
public class PCPurchaseCheckoutFragment extends Fragment  implements View.OnClickListener,PCCardPayment, PCTransactionableFragment {

    private PCPesabusClient.PCServiceType serviceType;
    private PCBillPaymentData transactionData = new PCBillPaymentData();

    private Button continueButton;
    private ImageView scanCard;
    private EditText cardNumber;
    private EditText customerPhoneNumber;
    private EditText cvc;
    private Spinner monthSpinner;
    private Spinner cardType;
    private Spinner yearSpinner;
    private static int MY_SCAN_REQUEST_CODE = 100;


    public PCPurchaseCheckoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pc_phone_number_and_card_fragment, container, false);
        this.continueButton = (Button) view.findViewById(R.id.continue_button);
        this.scanCard = (ImageView) view.findViewById(R.id.scan_card);
        continueButton.setText("CHARGE $" + this.transactionData.retrieveServiceInfo().getUsd());

        //set onClick on the button
        continueButton.setOnClickListener(this);
        scanCard.setOnClickListener(this);

        //initialize views
        this.cardNumber = (EditText) view.findViewById(R.id.number);
        this.customerPhoneNumber = (EditText) view.findViewById(R.id.customer_phone_number);
        this.customerPhoneNumber.requestFocus();
        this.cardNumber.addTextChangedListener(new PaymentCardNumberFormatWatcher());
        this.cvc = (EditText) view.findViewById(R.id.cvc);
        this.monthSpinner = (Spinner) view.findViewById(R.id.expMonth);
        this.yearSpinner = (Spinner) view.findViewById(R.id.expYear);

        return view;
    }

    public void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public PCPesabusClient.PCServiceType getServiceType() {
        return serviceType;
    }

    public void setTransactionData(PCBillPaymentData transactionData) {
        this.transactionData = transactionData;
    }

    public void setSpinText(Spinner spin, String text)
    {
        for (int i= 0; i < spin.getAdapter().getCount(); i++)
        {
            if (spin.getAdapter().getItem(i).toString().contains(text))
            {
                spin.setSelection(i);
            }
        }

    }
    private boolean validateInput() {
        boolean inputValid = false;
        if (customerPhoneNumber != null && StringUtils.isEmpty(customerPhoneNumber.getText().toString())) {
            customerPhoneNumber.setError("Phone number is required");
        }
        else if (customerPhoneNumber != null && !TextUtils.isDigitsOnly(customerPhoneNumber.getText().toString())) {
            customerPhoneNumber.setError("Invalid Phone number");
        }
        else if (cardNumber != null && StringUtils.isEmpty(cardNumber.getText().toString())) {
            cardNumber.setError("Card number is required");
        }
        else if (monthSpinner != null && monthSpinner.getSelectedItem() != null && "month".equalsIgnoreCase(monthSpinner.getSelectedItem().toString())) {
            View selectedView = monthSpinner.getSelectedView();
            if (selectedView != null && selectedView instanceof TextView) {
                TextView selectedTextView = (TextView) selectedView;
                selectedTextView.setError("");
                selectedTextView.setTextColor(Color.RED);//just to highlight that this is an error
                selectedTextView.setText("select month");//changes the selected item text to this
            }
        }
        else if(yearSpinner != null && yearSpinner.getSelectedItem() != null&& "year".equalsIgnoreCase(yearSpinner.getSelectedItem().toString())) {
            View selectedView = yearSpinner.getSelectedView();
            if (selectedView != null && selectedView instanceof TextView) {
                TextView selectedTextView = (TextView) selectedView;
                selectedTextView.setError("");
                selectedTextView.setTextColor(Color.RED);//just to highlight that this is an error
                selectedTextView.setText("select year");//changes the selected item text to this
            }
        }
        else if (cvc != null && StringUtils.isEmpty(cvc.getText().toString())) {
            cvc.setError("cvc number is required");
        }
        else {
            inputValid = true;
        }
        return inputValid;
    }

    @Override
    public String getCardNumber() {
        String input = this.cardNumber.getText().toString().trim();
        input = input.replace(" ", "");
        return this.getEncryptedData(input);
    }



    @Override
    public String getCvv() {
        return this.getEncryptedData(this.cvc.getText().toString().trim());
    }

    @Override
    public String getExpMonth() {
        return this.getEncryptedData(getValue(this.monthSpinner));
    }

    @Override
    public String getExpYear() {
        return this.getEncryptedData((getValue(this.yearSpinner)));
    }

    @Override
    public PCBillingAddress getBillingAddress() {
        return null;
    }

    @Override
    public String getCardType() {
        return this.getEncryptedData(getValue(this.cardType));
    }

    public void buildRequestAndShowSummary(View v) {

        PCProductPaymentData transaction = (PCProductPaymentData)this.transactionData;
        PCMainTabActivity activity = (PCMainTabActivity)getActivity();
        //set deviceId
        transaction.setDeviceId(activity.getApiKey().getDeviceId());
        //set Receiver
        PCSpecialUser receiver = new PCSpecialUser();
        receiver.setPhoneNumber(customerPhoneNumber.getText().toString());
        receiver.setMaskedCardNumber(makeReceiverCardMasked(cardNumber.getText().toString().trim()));
        transaction.setReceiver(receiver);

        PCSpecialUser sender = ((PCMainTabActivity) getActivity()).getAppUser();
        transaction.setSender(sender);

        //setup service info
        PCProductServiceInfo serviceInfo = transaction.getProductServiceInfo();
        // TODO: We need to get this from the country profile. It's not hard-coded.
        serviceInfo.setCompanyCode("1");

        //cardInfo
        PCCardInfo cardInfo = new PCCardInfo();
        cardInfo.setNumber(getCardNumber());
        cardInfo.setMonthOfExpiration(getExpMonth());
        cardInfo.setYearOfExpiration(getExpYear());
        cardInfo.setCvv(getCvv());
        serviceInfo.setCard(cardInfo);
        transaction.setProductServiceInfo(serviceInfo);
        this.transactionData = transaction;
    }

    /**
     * Encrypt card data
     * @param data
     *          card data item
     * @return encrypted card data item
     *
     */
    private String getEncryptedData(String data) {
        String cipher = null;
        PCPingResults results = null;
        if (getActivity() instanceof PCMainTabActivity) {
            results = ((PCMainTabActivity)getActivity()).getApiKey();
        }

        String apiKey = results != null ?results.getApiKey() : null;
        //TODO: need to refactor code to handle this case much better
        cipher = PCKeychainWrapper.securedAESEncryption(data, apiKey, null);
        return cipher;
    }

    private String getValue(Spinner spinner) {
        String v = spinner.getSelectedItem().toString();
        if (spinner == this.monthSpinner) {
            int m;
            try {
                m = Integer.parseInt(v);
                if (m < 10) {
                    v = "0" + m;
                }
            } catch (NumberFormatException exc) {
            }
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        if (v != null) {
            if (v.getId() == R.id.continue_button) {
                //make sure that all input box are filed
                if (validateInput()) {
                    buildRequestAndShowSummary(v);
                    submitTransaction();
                }
            }
            else if (v.getId() == R.id
                    .scan_card){
                onScanPress(v);
            }
        }
    }

    private String makeReceiverCardMasked(String text){

        String str = text.length() <= 4 ? text : text.substring(text.length() - 4);
        return "XXXX"+str;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {

        }
    }

    /**
     * used to scan credit/debit card
     * @param v
     */
    public void onScanPress(View v) {
    }

    /**
     * Formats the watched EditText to a credit card number
     */
    public static class PaymentCardNumberFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove all spacing char
            int pos = 0;
            while (true) {
                if (pos >= s.length()) break;
                if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                    s.delete(pos, pos + 1);
                } else {
                    pos++;
                }
            }

            // Insert char where needed.
            pos = 4;
            while (true) {
                if (pos >= s.length()) break;
                final char c = s.charAt(pos);
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space);
                }
                pos += 5;
            }

        }
    }

    private void showConfirmationDialog() {
        PCConfirmationDialogFragment dialogFragment = new PCConfirmationDialogFragment();
        dialogFragment.setTransactionData(this.transactionData);
        dialogFragment.setServiceType(this.getServiceType());
        FragmentManager fm = getActivity().getSupportFragmentManager();
        dialogFragment.show(fm, "Transaction Confirmation Dialog");
    }

    public void presentError(PCGenericError error) {
        PCBaseActivity activity = (PCBaseActivity)getActivity();

        if (error != null && activity != null) {
            String title = error.getTitle();
            if (title == null) {
                title = "Transaction Error";
            }
            activity.presentError(error, title);
        }
    }

    public void submitTransaction() {
        PCMainTabActivity activity = (PCMainTabActivity)getActivity();
        if (activity != null) {
            activity.saveTransaction(this);
        }
    }

    private void openCheckOut() {
        PCCheckoutFragment frag = new PCCheckoutFragment();
        PCMainTabActivity activity = (PCMainTabActivity)getActivity();

        //Set the sender
        if (activity != null && this.transactionData != null) {
            this.transactionData.setSender(activity.getAppUser());
        }

        frag.setTransactionData(this.transactionData);
        frag.setServiceType(this.getServiceType());

        FragmentManager fm = this.getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.grid_container, frag, Integer.toString(R.layout.pc_fragment_checkout))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public PCBillPaymentData getTransactionData() {
        return this.transactionData;
    }
}