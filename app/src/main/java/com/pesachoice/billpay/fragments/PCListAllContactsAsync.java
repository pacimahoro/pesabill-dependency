package com.pesachoice.billpay.fragments;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.pesachoice.billpay.activities.PCOnPhoneContactLoad;
import com.pesachoice.billpay.model.PCContactInfo;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;


/**
 * Laoding user contact async
 * Created by desire.aheza on 1/17/2016.
 */
public class PCListAllContactsAsync extends AsyncTask<Void, Void, ArrayList<PCContactInfo>> {

	private PCOnPhoneContactLoad pcOnPhoneContactLoad;

	@Override
	protected void onPreExecute() {
		pcOnPhoneContactLoad.onContactLoadStarted();
	}

	public PCListAllContactsAsync(PCOnPhoneContactLoad pcOnPhoneContactLoad) {
		this.pcOnPhoneContactLoad = pcOnPhoneContactLoad;
	}

	@Override
	protected ArrayList<PCContactInfo> doInBackground(Void... params) {
		LinkedHashMap<String, PCContactInfo> contactMap = new LinkedHashMap<>();

		try {
			ContentResolver resolver = null;
			if (pcOnPhoneContactLoad instanceof AppCompatActivity) {
				resolver = ((AppCompatActivity) pcOnPhoneContactLoad).getContentResolver();
			} else if (pcOnPhoneContactLoad instanceof Fragment) {
				resolver = ((Fragment) pcOnPhoneContactLoad).getActivity().getContentResolver();
			}
			String photoPath = " ";
			Cursor c = resolver.query(
					ContactsContract.Data.CONTENT_URI,
					null,
					ContactsContract.Data.HAS_PHONE_NUMBER + "!=0 AND (" + ContactsContract.Data.MIMETYPE + "=? OR " + ContactsContract.Data.MIMETYPE + "=?)",
					new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE},
					ContactsContract.Data.CONTACT_ID);
			while (c.moveToNext()) {
				long id = c.getLong(c.getColumnIndex(ContactsContract.Data.CONTACT_ID));
				String name = c.getString(c.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
				String data1 = c.getString(c.getColumnIndex(ContactsContract.Data.DATA1));
				if (!StringUtils.isEmpty(data1) && !StringUtils.isEmpty(name)) {

					if (!contactMap.containsKey(data1)) {
						contactMap.put(data1, new PCContactInfo(data1, name, photoPath));
					}
				}
			}

		} catch (Exception e) {
			//TODO:need to figure out a best to handle this exception
			e.printStackTrace();
		}
		return new ArrayList<PCContactInfo>(contactMap.values());
	}

	@Override
	protected void onPostExecute(ArrayList<PCContactInfo> pcContactInfos) {
		this.pcOnPhoneContactLoad.onContactLoadFinished(pcContactInfos);
	}
}