package com.pesachoice.billpay.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.utils.PCExpandableHeightGridView;

/**
 * this fragment has views to hold inputs and methods required to make/receive phone calls using pesaibll
 * A simple {@link Fragment} subclass.
 */
public class PCPhoneFragment extends PCMainTabsFragment  {

    private PCExpandableHeightGridView gridView;
    private ListView listView;

    public PCPhoneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        View rootView = inflater.inflate(R.layout.pc_phone_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView_activity);
        addGridViewAsListViewHeader();
        setAdapters();
        return rootView;
    }

    /**
     * set header for listview (it's a gridview)
     */

    private void addGridViewAsListViewHeader() {
        View header = (View) getActivity().getLayoutInflater().inflate(R.layout.pc_header_phone, null);
        listView.addHeaderView(header);
        gridView = (PCExpandableHeightGridView) header.findViewById(R.id.gridview_header);
        // set expand to show all gridview rows
        gridView.setExpanded(true);
    }

    /**
     * set adapters for list view and gridview
     */
    private void setAdapters() {

        String[] gridItemList = new String[]{"Odilon","Pacifique","Desire","Shamila","Kaka","Tresor","Mark"};
        String[] listItems = new String[]{"Item 1","Item 2","Item 3","Item 4","Item 5","Item 6","Item 7","Item 8","Item 9","Item 10","Item 11"};
        // set listview and gridview adapters
        PCCustomGridCallingAdapter gridViewAdapter = new PCCustomGridCallingAdapter(getActivity(), R.layout.pc_fragment_products_item, R.id.textViewParent, gridItemList);
        PCCustomCallingAdapter listViewAdapter = new PCCustomCallingAdapter(getActivity(), R.layout.pc_line_item_layout, R.id.textViewParent, listItems);
        gridView.setAdapter(gridViewAdapter);
        listView.setAdapter(listViewAdapter);

    }

    public class PCCustomCallingAdapter extends ArrayAdapter<String> {

        private Context context;

        public PCCustomCallingAdapter(Context activity, int resource, int textViewResourceId, String[] texts) {
            super(activity, resource, textViewResourceId, texts);
            this.context = activity;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            // inflate layout from xml
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // If holder not exist then locate all view from UI file.
            if (convertView == null) {
                // inflate UI from XML file
                convertView = inflater.inflate(R.layout.pc_line_item_layout, parent, false);
                // get all UI view
                holder = new ViewHolder(convertView);
                // set tag for holder
                convertView.setTag(holder);
            } else {
                // if holder created, get tag from view
                holder = (ViewHolder) convertView.getTag();
            }

            holder.item.setText(getItem(position));

            return convertView;
        }

        private class ViewHolder {
            private TextView item;

            public ViewHolder(View v) {
                item = (TextView)v.findViewById(R.id.textViewParent);
            }
        }
    }

    public class PCCustomGridCallingAdapter extends ArrayAdapter<String> {

        private Context context;
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // declare the builder object once.
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();

        public PCCustomGridCallingAdapter(Context activity, int resource, int textViewResourceId, String[] texts) {
            super(activity, resource, textViewResourceId, texts);
            this.context = activity;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            // inflate layout from xml
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // If holder not exist then locate all view from UI file.
            if (convertView == null) {
                // inflate UI from XML file
                convertView = inflater.inflate(R.layout.pc_fragment_products_item, parent, false);
                // get all UI view
                holder = new ViewHolder(convertView);
                // set tag for holder
                convertView.setTag(holder);
            } else {
                // if holder created, get tag from view
                holder = (ViewHolder) convertView.getTag();
            }
            String gridItem = getItem(position);
            holder.item.setText(gridItem);
            String ch = gridItem.substring(0,1);
            holder.image.setImageDrawable(builder.build(ch,generator.getColor(ch)));
            return convertView;
        }

        private class ViewHolder {
            private TextView item;
            private ImageView image;

            public ViewHolder(View v) {
                item = (TextView) v.findViewById(R.id.textView1);
                image = (ImageView) v.findViewById(R.id.imageView1);
            }
        }
    }
}
