package com.pesachoice.billpay.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCKeychainWrapper;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//import io.card.payment.CardIOActivity;
//import io.card.payment.CreditCard;

/**
 * Contains card payment attachment form
 *
 * @author Pacifique Mahoro
 */
public class PCCardPaymentFragment extends Fragment implements PCCardPayment, View.OnClickListener {
	private Button saveButton;
	private EditText cardNumber;
	private Spinner card_type;
	private EditText cvc;
	private Spinner monthSpinner;
	private Spinner yearSpinner;
	private String streetAddressText;
	private String cityNameText;
	private String zipCodeText;
	private String stateNameText;
	private String countryNameText;
	private EditText streetAddress;
	private EditText cityName;
	private EditText stateName;
	private EditText zipCode;
	private EditText countryName;
	private LinearLayout addressSection;
	private boolean addressFormHasError = true;
	private View view;
	private PCBillingAddress pcBillingAddress = new PCBillingAddress();
	private PCBaseActivity activity;
	private static final int MY_SCAN_REQUEST_CODE = 100;
	private PCSpecialUser appUser;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.pc_fragment_cardpayment, container, false);

		PCBaseActivity act = (PCBaseActivity) this.getActivity();
		if (act != null) {
			if (this.getActivity() instanceof PCMainTabActivity) {
				this.activity = (PCMainTabActivity) this.getActivity();
			} else if (this.getActivity() instanceof PCAccountDetailsActivity) {
				this.activity = (PCAccountDetailsActivity) this.getActivity();
			}
		}

		this.saveButton = (Button) view.findViewById(R.id.save);
		//	this.scanCard = (ImageView) view.findViewById(R.id.scan_card);
		appUser = activity.getAppUser();
		String currencySymbl = appUser.getBaseCurrency().getCurrencySymbol();

		if(activity != null && activity.getAppUser() !=null){
			if(currencySymbl.equals("RWF")){
				saveButton.setText("Set card info");
			}
		}
		//set onClick on the button
		saveButton.setOnClickListener(this);

		//initialize views
		this.cardNumber = (EditText) view.findViewById(R.id.number);
		this.cardNumber.addTextChangedListener(new PaymentCardNumberFormatWatcher());
		this.cvc = (EditText) view.findViewById(R.id.cvc);
		this.monthSpinner = (Spinner) view.findViewById(R.id.expMonth);
		this.yearSpinner = (Spinner) view.findViewById(R.id.expYear);
		this.card_type =  (Spinner) view.findViewById(R.id.card_type);

		if (act != null) {

			PCSpecialUser pcSpecialUser = act.getAppUser();

			if (pcSpecialUser != null && act.isCountryUsaOrCaByPhoneNum(pcSpecialUser.getPhoneNumber())) {

				this.addressSection = (LinearLayout) view.findViewById(R.id.address_section);
				if (addressSection != null) {
					addressSection.setVisibility(View.VISIBLE);
				}
				this.streetAddress = (EditText) view.findViewById(R.id.street_address);
				this.cityName = (EditText) view.findViewById(R.id.city_name);
				this.stateName = (EditText) view.findViewById(R.id.state_name);
				this.zipCode = (EditText) view.findViewById(R.id.zip_code_number);
				this.countryName = (EditText) view.findViewById(R.id.country_name);
				//check if u have address already and populate it
				populateAddress();
			}
		}
		return view;
	}

	public void setSpinText(Spinner spin, String text) {
		for (int i = 0; i < spin.getAdapter().getCount(); i++) {
			if (spin.getAdapter().getItem(i).toString().contains(text)) {
				spin.setSelection(i);
			}
		}

	}

	private void populateAddress() {
		if (activity != null && activity.getAppUser() != null) {
			pcBillingAddress = activity.getAppUser().getBillingAddress();
			if (pcBillingAddress != null) {
				this.streetAddress.setText(pcBillingAddress.getStreet());
				this.cityName.setText(pcBillingAddress.getCity());
				this.stateName.setText(pcBillingAddress.getState());
				this.zipCode.setText(pcBillingAddress.getZip());
				this.countryName.setText(pcBillingAddress.getCountry());
			}
		}
	}

	public PCBaseActivity getCurrentActivity() {
		return activity instanceof PCMainTabActivity ? (PCMainTabActivity) activity : (PCAccountDetailsActivity) activity;
	}

	/**
	 * Validate inputs and then set error if input box doesn't have text
	 */
	private boolean validateAddressInput() {

		//TODO: need to discuss with Team if we really need to throw a dialog box in case of error or if we can use built in error displaying
		if (view == null) {
			// TODO: what should we do here?
			return true;
		}
		//TODO: currently we're only validating US address.
		if (activity != null && !activity.isCountryUS()) {
			return true;
		}

		streetAddressText = getInputInEditText(streetAddress);
		cityNameText = getInputInEditText(cityName);
		stateNameText = getInputInEditText(stateName);
		zipCodeText = getInputInEditText(zipCode);
		countryNameText = getInputInEditText(countryName);

		if (StringUtils.isEmpty(streetAddressText)) {
			addressFormHasError = true;
			streetAddress.setError("Street Address is required");
		} else if (StringUtils.isEmpty(cityNameText)) {
			addressFormHasError = true;
			cityName.setError("City name is required");

		} else if (StringUtils.isEmpty(zipCodeText)) {
			addressFormHasError = true;
			zipCode.setError("Zip Code is required");

		} else if (StringUtils.isEmpty(stateNameText)) {
			addressFormHasError = true;
			stateName.setError("State is required");
		} else if (StringUtils.isEmpty(countryNameText)) {
			addressFormHasError = true;
			countryName.setError("Country is required");
		} else {
			addressFormHasError = false;
		}

		return addressFormHasError;
	}

	private void updateBillingAddress() {
		pcBillingAddress.setCountry(countryNameText);
		pcBillingAddress.setCity(cityNameText);
		pcBillingAddress.setState(stateNameText);
		pcBillingAddress.setStreet(streetAddressText);
		pcBillingAddress.setZip(zipCodeText);
	}

	private String getInputInEditText(EditText editText) {
		return editText != null ? editText.getText().toString() : null;
	}

	private boolean validateInput() {
		boolean inputValid = false;
		if (cardNumber != null && StringUtils.isEmpty(cardNumber.getText().toString())) {
			cardNumber.setError("Card number is required");
		} else if (monthSpinner != null && monthSpinner.getSelectedItem() != null && "month".equalsIgnoreCase(monthSpinner.getSelectedItem().toString())) {
			View selectedView = monthSpinner.getSelectedView();
			if (selectedView != null && selectedView instanceof TextView) {
				TextView selectedTextView = (TextView) selectedView;
				selectedTextView.setError("");
				selectedTextView.setTextColor(Color.RED);//just to highlight that this is an error
				selectedTextView.setText("select month");//changes the selected item text to this
			}
		} else if (yearSpinner != null && yearSpinner.getSelectedItem() != null && "year".equalsIgnoreCase(yearSpinner.getSelectedItem().toString())) {
			View selectedView = yearSpinner.getSelectedView();
			if (selectedView != null && selectedView instanceof TextView) {
				TextView selectedTextView = (TextView) selectedView;
				selectedTextView.setError("");
				selectedTextView.setTextColor(Color.RED);//just to highlight that this is an error
				selectedTextView.setText("select year");//changes the selected item text to this
			}
		} else if (cvc != null && StringUtils.isEmpty(cvc.getText().toString())) {
			cvc.setError("cvc number is required");
		} else {
			inputValid = true;
		}
		return inputValid;
	}

	@Override
	public String getCardNumber() {
		String input = this.cardNumber.getText().toString().trim();
		input = input.replace(" ", "");
		PCMainTabActivity parentActivity = (PCMainTabActivity) getActivity();

		/** Extract last four digits from the card number account.
		 * 	This will mimic usual maskedCard number and will be used for previewing card on confirmation dialog, sort making sure
		 * 	that the card is set.
		 */
		/**
		 * TODO: Missing check card length.
		 * This is intended to prevent user to fill provide incomplete card number.
		 */
		parentActivity.setMaskedLocalCard(input.substring(12));
		//Proceed with encryption
		return this.getEncryptedData(input);
	}



	@Override
	public String getCardType() {
		/** Remove the encryption for card type for Local cards to pass
		 */
		return getValue(this.card_type);
	}

	@Override
	public String getCvv() {
		/** Remove the encryption for Cvv for Local cards to pass
		 */
		return this.cvc.getText().toString().trim();
	}

	@Override
	public String getExpMonth() {
		/** Remove the encryption for exp month for Local cards to pass
		 */
		return getValue(this.monthSpinner);
	}

	@Override
	public String getExpYear() {
		/** Remove the encryption for exp year for Local cards to pass
		 */
		return getValue(this.yearSpinner);
	}
	@Override
	public PCBillingAddress getBillingAddress() {
		return pcBillingAddress;
	}

	public void saveForm(View v) {
		PCBaseActivity parentActivity = (PCBaseActivity) getActivity();
		if (parentActivity != null) {
			parentActivity.saveNewCreditCard(this);
		}

	}

	public void holdCardDetails(View v){
		PCBaseActivity parentActivity = (PCBaseActivity) getActivity();
		if(parentActivity != null){
			parentActivity.holdLocalCardInfo(this);
		}
	}
	/**
	 * Encrypt card data
	 *
	 * @param data card data item
	 * @return encrypted card data item
	 */
	private String getEncryptedData(String data) {
		String cipher = null;
		PCPingResults results = null;
		if (getActivity() instanceof PCMainTabActivity) {
			results = ((PCMainTabActivity) getActivity()).getApiKey();
		} else if (getActivity() instanceof PCAccountDetailsActivity) {
			results = ((PCAccountDetailsActivity) getActivity()).getApiKey();
		}
		String apiKey = results != null ? results.getApiKey() : null;
		//TODO: need to refactor code to handle this case much better
		cipher = PCKeychainWrapper.securedAESEncryption(data, apiKey, null);
		return cipher;
	}

	private String getValue(Spinner spinner) {
		String v = spinner.getSelectedItem().toString();
		if (spinner == this.monthSpinner) {
			int m;
			try {
				m = Integer.parseInt(v);
				if (m < 10) {
					v = "0" + m;
				}
			} catch (NumberFormatException exc) {
			}
		}
		return v;
	}

	@Override
	public void onClick(View v) {
		if (v != null) {
			if (v.getId() == R.id.save) {
				//make sure that all input box are filed, and also we are only showing address if country is US
				if (validateInput()) {
					boolean hasAddressError = false;
					//validate input address only if current user is in USA
					if (activity.isCountryUS() && addressSection != null && activity != null) {
						if (!validateAndUpdateAddress()) {
							hasAddressError = true;
						}
					}
					if (!hasAddressError) {
						String userCurrency = appUser != null ? appUser.getBaseCurrency().getCurrencySymbol(): "";
						if(userCurrency.equalsIgnoreCase("RWF")){
							holdCardDetails(v);
						}else{
							saveForm(v);
						}

						//set transactionData and don't save the card.
					}
				}
			}
		}
	}

	private boolean validateAndUpdateAddress() {
		boolean hasError = true;
		//update address if country is US
		if (validateAddressInput()) {
			hasError = false;
			updateBillingAddress();
		}

		return hasError;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == MY_SCAN_REQUEST_CODE) {

//			}
		}
	}

	/**
	 * used to scan credit/debit card
	 *
	 * @param v
	 */
	public void onScanPress(View v) {

	}

	/**
	 * Formats the watched EditText to a credit card number
	 */
	public static class PaymentCardNumberFormatWatcher implements TextWatcher {

		// Change this to what you want... ' ', '-' etc..
		private static final char space = ' ';

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			// Remove all spacing char
			int pos = 0;
			while (true) {
				if (pos >= s.length()) {
					break;
				}
				if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
					s.delete(pos, pos + 1);
				} else {
					pos++;
				}
			}

			// Insert char where needed.
			pos = 4;
			while (true) {
				if (pos >= s.length()) {
					break;
				}
				final char c = s.charAt(pos);
				// Only if its a digit where there should be a space we insert a space
				if ("0123456789".indexOf(c) >= 0) {
					s.insert(pos, "" + space);
				}
				pos += 5;
			}

		}
	}

}
