/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pesachoice.billpay.activities.PCAccountDetailsActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCFAQData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This pcOnPhoneContactLoad is used to display a list of FAQs related to Pesachoice app
 * @author desire.aheza
 */
public class PCProductFAQFragment extends Fragment {

    private ExpandableListAdapter faqAdapter;
    private PCAccountDetailsActivity activity = new PCAccountDetailsActivity();

    public PCProductFAQFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this pcOnPhoneContactLoad
        View view= inflater.inflate(R.layout.pc_fragment_pcproduct_faq, container, false);
        activity = (PCAccountDetailsActivity)getActivity();
        setupContactUsWidget(view);

        ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.expandableList);
        List<PCFAQData> faqsAndAnswers = getFaqsAndAnswers();
        faqAdapter = new FaqsAdapter(this.getActivity(), faqsAndAnswers);
        expandableListView.setAdapter(faqAdapter);
        return view;
    }

    private void setupContactUsWidget(View view) {
        LinearLayout callUs = (LinearLayout) view.findViewById(R.id.call_us);
        LinearLayout emailUs = (LinearLayout) view.findViewById(R.id.email_us);
        ImageView emailUsIcon = (ImageView) emailUs.findViewById(R.id.option_icon);
        TextView emailUsText = (TextView) emailUs.findViewById(R.id.option_name);
        ImageView callUsIcon =(ImageView)callUs.findViewById(R.id.option_icon);
        TextView callUsText = (TextView) callUs.findViewById(R.id.option_name);
        emailUsIcon.setImageResource(R.drawable.icn_email_us);
        callUsIcon.setImageResource(R.drawable.icn_call_us);
        emailUsText.setText("Email us");
        callUsText.setText("Call Us");
        callUs.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + PCPesachoiceConstant.PESACHOICE_PHONE_NUMBER));
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+"+ getResources().getString(R.string.contact_us_phone_no_dash)));
                if(intent.resolveActivity(activity.getPackageManager()) != null)
                    startActivity(intent);
            }
        });

        emailUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                //intent.putExtra(Intent.EXTRA_EMAIL, new String[]{PCPesachoiceConstant.PESACHOICE_EMAIL});
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{getResources().getString(R.string.contact_us_email_info)});
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                if (intent.resolveActivity(activity.getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * prepare a map of FAQs and responses
     * @return a map of Faqs and their corresponding answers
     */
    protected List<PCFAQData> getFaqsAndAnswers(){
        BufferedReader reader = null;
        StringBuffer buffer = new StringBuffer();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getActivity().getAssets().open("pc_product_faq.json")));

            // do reading, loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
               buffer.append(mLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonToFAQList(buffer.toString());
    }


    protected List<PCFAQData> jsonToFAQList(String faqs) {
        List<PCFAQData> faqsList = new ArrayList<>();
        try {
           JSONObject jObject = new JSONObject(faqs);
           JSONArray items = (JSONArray)jObject.get("items");

           for (int i = 0; i < items.length(); i++) {
               JSONObject obj = items.getJSONObject(i);
               PCFAQData qa = new PCFAQData();
               qa.setQuestion(obj.getString("question"));
               qa.setAnswer(obj.getString("answer"));

               faqsList.add(qa);
           }
        }catch (JSONException e){
           e.printStackTrace();
        }

        return faqsList;
    }
    /**
     * adapter to add faqs in listview
     */
    private class FaqsAdapter extends BaseExpandableListAdapter {

        List<PCFAQData> faqs = new ArrayList<>();
        Context context;

        public FaqsAdapter(Context context,List<PCFAQData> faqs) {
            this.context = context;
            this.faqs = faqs;
        }


        @Override
        public int getGroupCount() {
            return faqs.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        @Override
        public Object getGroup(int groupPosition) {
            PCFAQData faq = faqs.get(groupPosition);
            return faq.getQuestion();
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            PCFAQData faq = faqs.get(groupPosition);
            return faq.getAnswer();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.pc_line_item_layout, null);
            }
            String textGroup = (String) getGroup(groupPosition);
            TextView textViewGroup = (TextView) convertView
                    .findViewById(R.id.textViewParent);
            textViewGroup.setText(textGroup);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.pc_line_item_faq_answer_layout, null);
            }

            TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewChild);
            String text = (String) getChild(groupPosition, childPosition);
            textViewItem.setText(text);
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }
}
