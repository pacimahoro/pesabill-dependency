/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCGenericError;

/**
 * Interface reserved for any transaction pcOnPhoneContactLoad component.
 *
 * @author Pacifique Mahoro
 */
public interface PCTransactionComponent<T extends Fragment> extends PCTransactionableFragment {
    /**
     *
     * @param contactInfo
     *              receiver contact information
     */
    public void setContactInfo(PCContactInfo contactInfo);


    /**
     * Present error, it can be a validation error or any other.
     * @param error
     *              The error to be shown.
     */
    public void presentError(PCGenericError error);

    /**
     * Validates required fragments field and throws an exception if it fails.
     * @param v
     *          View that contains Data to be shown.
     * @throws PCGenericError
     *              Error thrown when validation fails.
     */
    public void validateInputs(View v) throws PCGenericError;


    /**
     * Logic to get the receiver info from the view.
     * All the pcOnPhoneContactLoad have a receiver info section
     *
     * @param v
     * @throws PCGenericError
     *              Error in case some required fields are missing or fail validation.
     */
    public void getAndSetReceiverInfo(View v) throws PCGenericError;


    public void getOperatorInfo(View v) throws PCGenericError;


    public void getServiceInfo(View v) throws PCGenericError;
}
