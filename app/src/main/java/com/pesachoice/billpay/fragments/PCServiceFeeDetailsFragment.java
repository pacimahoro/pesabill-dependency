package com.pesachoice.billpay.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.activities.helpers.PCListViewUtility;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCServiceFee;


import java.util.ArrayList;
import java.util.List;

/**
 * this fragment has views to hold inputs and methods required to show details of selected service fees
 * A simple {@link Fragment} subclass.
 */
public class PCServiceFeeDetailsFragment extends Fragment {

    private ListView listView;
    private Spinner paymentTypes;
    private String currency;
    private List<PCServiceFee> serviceFeesPaymentCard = new ArrayList<>();
    private List<PCServiceFee> serviceFeesPaymentCash = new ArrayList<>();
    private List<PCServiceFee> serviceFees = serviceFeesPaymentCard;
    private List<PCServiceFee> serviceFeesMobileMoney = new ArrayList<>();
    private String selectedServiceType;
    private ServiceFeesAdapter adapter;
    private PCBillPaymentData.PCPaymentType selectedPaymentType;

    public PCServiceFeeDetailsFragment() {
        // Required empty public constructor
    }

    public void setSelectedServiceType(String selectedServiceType) {
        this.selectedServiceType = selectedServiceType.toUpperCase();
    }

    public PCBillPaymentData.PCPaymentType getSelectedPaymentType() {
        return selectedPaymentType;
    }

    public void setSelectedPaymentType(PCBillPaymentData.PCPaymentType selectedPaymentType) {
        this.selectedPaymentType = selectedPaymentType;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setServiceFees(List<PCServiceFee> serviceFees) {
        for (PCServiceFee serviceFee : serviceFees) {
            List<String> services = serviceFee.getServices();
            Boolean containsService = false;
            boolean hasAllServicesType = false;
            if (services != null && selectedServiceType != null) {
                for (String service : services) {
                    if (selectedServiceType.equalsIgnoreCase(service) ||
                            selectedServiceType.replaceAll(" ","_").equalsIgnoreCase(service)) {
                        containsService = true;
                        break;
                    }
                    else if (PCBillPaymentData.ActionType.ALL_SERVICES.toString().equalsIgnoreCase(service)) {
                        hasAllServicesType = true;
                        break;
                    }
                }
                if (containsService) {
                    addServiceFeeItem(serviceFee);
                }
                else if (hasAllServicesType) {
                    addServiceFeeItem(serviceFee);
                }
            }
        }
    }

    private void addServiceFeeItem(PCServiceFee serviceFee) {
        PCBillPaymentData.PCPaymentType currentServicePaymentType = serviceFee.getPaymentType();
        if (currentServicePaymentType != null) {
            String pymentType = currentServicePaymentType.getPaymentType();
            if (pymentType.equals(PCBillPaymentData.PCPaymentType.PAYMENT_CARD.toString())) {
                serviceFeesPaymentCard.add(serviceFee);
                //serviceFeesPaymentCard.add(serviceFee);
            } else if (pymentType.equals(PCBillPaymentData.PCPaymentType.MOBILE_MONEY.toString())) {
                serviceFeesMobileMoney.add(serviceFee);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pc_service_fee_details_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.service_fees);
        paymentTypes = (Spinner) view.findViewById(R.id.payment_types);
        refreshAdapter();
        paymentTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    serviceFees = serviceFeesPaymentCard;
                }
                if (position == 1) {
                    serviceFees = serviceFeesMobileMoney;
                }
                refreshAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void refreshAdapter() {
        adapter = new ServiceFeesAdapter(this.getActivity(), serviceFees);
        listView.setAdapter(adapter);
        PCListViewUtility.setListViewHeightBasedOnChildren(listView);
        adapter.notifyDataSetChanged();
    }

    // add items into spinner dynamically
    private void addItemsOnSpinner2() {

    }

    private class ServiceFeesAdapter extends ArrayAdapter<PCServiceFee> {

        public ServiceFeesAdapter(Context context, List<PCServiceFee> objects) {
            super(context, 0, objects);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PCServiceFee serviceFeeData = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.pc_service_fee_layout, parent, false);
            }
            // Lookup view for data population
            TextView serviceFee = (TextView) convertView.findViewById(R.id.service_fee_amount);
            TextView maxAmount = (TextView) convertView.findViewById(R.id.max_amount);
            TextView minAmount = (TextView) convertView.findViewById(R.id.min_amount);
            // Populate the data into the template view using the data object
            serviceFee.setText(serviceFeeData.getFeeAmount().doubleValue()+" "+currency);
            maxAmount.setText(serviceFeeData.getMaxAmount().doubleValue()+" "+currency);
            minAmount.setText(serviceFeeData.getMinAmount().doubleValue()+" "+currency);
            // Return the completed view to render on screen
            return convertView;
        }
    }
}



