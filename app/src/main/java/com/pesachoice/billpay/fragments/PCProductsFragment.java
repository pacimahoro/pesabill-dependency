
/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMoneyTransferTransaction;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Handles show products grid that a user can choose from.
 *
 * @author Pacifique Mahoro
 * @author Desire Aheza
 */
public class PCProductsFragment extends PCMainTabsFragment {

    public PCProductsFragment() {

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

   /* public PCData.ActionType getActionType() {
        return PCData.ActionType.MONEY_TRANSFER;
    }*/

    @Override
    public void onDataLoadStart(View view) {
        super.onDataLoadStart(view);
        if (getActivity() instanceof PCMainTabActivity) {
            PCMainTabActivity activity = (PCMainTabActivity) getActivity();
            PCSpecialUser user = activity.getAppUser();
            if (user.isAgent() || user.isThirdParty()) {
                selectedCountry = "Rwanda";
            } else {
                selectedCountry = getSelectedCountry();
            }
            if (pcCountryProfile != null && selectedCountry != null && this.selectedCountry.equalsIgnoreCase(pcCountryProfile.getCountry())) {
                setUpLayoutFinishProgressBar();
                final PCOperatorRequest operatorRequest = activity.getPcOperatorRequest(selectedCountry);
                if (operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
                    setupMainView(pcCountryProfile);
                } else {

                }
                resizeGridViewToEnableScrolling();
            } else {
                ((PCMainTabActivity) this.getActivity()).makeCallToGetCountryProfile(this.selectedCountry, this, null);
            }

        }
    }

    @Override
    public void onDataLoadFinished(PCData pcData) {
        try {
            super.onDataLoadFinished(pcData);
            if (pcData instanceof PCCountryProfile) {
                pcCountryProfile = (PCCountryProfile) pcData;
                if (getActivity() instanceof PCMainTabActivity) {
                    PCMainTabActivity activity = (PCMainTabActivity) getActivity();
                    final PCOperatorRequest operatorRequest = activity.getPcOperatorRequest(selectedCountry);
                    if (operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
                        setupMainView(pcCountryProfile);
                    } else {

                    }
                    resizeGridViewToEnableScrolling();

                }
            }
        } catch (Exception e) {
            //TODO need to handle this better
            e.printStackTrace();
        }

    }


    @Override
    public void onCollectionDataLoadFinished(Collection<? extends PCData> pcData) {
        setUpLayoutFinishProgressBar();
    }

    @Override
    public void setupMainView(PCCountryProfile pcCtryProfile) {
        try {
            List<String> products = new ArrayList<>();
            if (pcCtryProfile != null & pcCtryProfile.getProducts() != null) {
                products.addAll(pcCtryProfile.getProducts());
                List<Integer> images = makeProductIcons(products);
                if (getActivity() != null) {
                    adapter = new PCCountryAndProductAdapter(getActivity(), products, images);
                    adapter.setTypeOfDatatToAdapt(TypeOfDataToAdapt.PRODUCTS);
                    mainFragmentLayout.setAdapter(adapter);
                }
            } else {
                TextView emptyView = new TextView(this.getActivity());
                emptyView.setText("No Products to Show");
                mainFragmentLayout.setEmptyView(emptyView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openRefilTransanctionFrag() {
        PCPesabusClient.PCServiceType serviceType = PCPesabusClient.PCServiceType.CALLING_REFIL;// PCTransactionFragmentFactory.getServiceType("Buy Credit");

        if (serviceType != null) {
            PCTransactionComponent<? extends Fragment> frag = PCTransactionFragmentFactory.constructTransactionFragment(serviceType);
            FragmentManager fm = ((PCMainTabActivity) getActivity()).getSupportFragmentManager();
            fm.beginTransaction()
                    .replace(R.id.grid_container, (Fragment) frag, serviceType.name())
                    .addToBackStack(null)
                    .commit();
        }
    }

    private List<Integer> makeProductIcons(List<String> productNames) {
        List<Integer> productIcons = new ArrayList<>();
        PCMainTabActivity act = (PCMainTabActivity) getActivity();


        try {
            for (int i = 0; i < productNames.size(); i++) {
                String prodName = productNames.get(i).trim().toLowerCase();
                //TODO:need to find a best way to handle this for a long term stability
                if (PCTransactionFragmentFactory.CALL_SERVICES.equalsIgnoreCase(prodName)) {
                    prodName = "callingcard";
                }
                if (PCTransactionFragmentFactory.MONEY_TRANSFER.equalsIgnoreCase(prodName)) {
                    prodName = "agent_transaction";
                }
                if (act != null && act.getResources() != null) {
                    String identifier = "icn_product_" + prodName;
                    Integer prodIcn = act.getResources().getIdentifier(identifier, "drawable", act.getPackageName());
                    if (prodIcn > 0) {
                        productIcons.add(prodIcn);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Safe guard
            PCMainTabActivity activity = (PCMainTabActivity) getActivity();
            if (activity != null) {
                activity.straightLogout();
            }
        }

        return productIcons;
    }

}

