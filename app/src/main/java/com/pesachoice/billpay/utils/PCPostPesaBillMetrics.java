package com.pesachoice.billpay.utils;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.springframework.util.StringUtils;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class being used to send events (purchase,login, card attachement...) to our dashboard
 * Created by desire.aheza on 7/11/2017.
 */

public class PCPostPesaBillMetrics {
	private static final String TAG = PCPostPesaBillMetrics.class.getName();
	private static final int CHECKOUT_PARMS_COUNT = 6;

	public enum CUSTOM_EVENT_TAG {
		COUNTRY("Country"),
		STATUS("Status");

		private String name;

		CUSTOM_EVENT_TAG(String s) {
			name = s;
		}

		public String getName() {
			return name;
		}
	}

	public enum EVENT_TYPE {
		PAYMENT_CARD("Payment Card"),
		SUCCESS("Success"),
		FAILED("Failed");

		public String name;

		EVENT_TYPE(String s) {
			name = s;
		}

		public String getName() {
			return name;
		}
	}

	public static boolean postPurchaseMetric(PCBaseActivity activity, PCBillPaymentData billPaymentData, boolean success) {
		Map<String, Object> eventParams = new HashMap<>();
		boolean eventSent = false;
		try {
			if (billPaymentData != null) {
				PCServiceInfo serviceInfo = billPaymentData.getServiceInfo();
				if (serviceInfo == null) {
					serviceInfo = billPaymentData.retrieveServiceInfo();
				}
				if (serviceInfo != null) {
					PCSpecialUser sender = billPaymentData.getSender();
					String type = null;
					PCActivity.PCPaymentProcessingType processingType = billPaymentData.getPaymentProcessingType();
					if (processingType != null) {
						type = processingType.toString();
					}
					eventParams.put("purchaseType", type);
					String country = "";
					String currencySymbol = "";

					if (sender != null) {
						country = sender.getCountry();
						PCCurrency currency = sender.getBaseCurrency();
						if (StringUtils.isEmpty(country)) {
							if (currency != null) {
								country = currency.getCountry();
							}
						}
						if (currency != null) {
							currencySymbol = currency.getCurrencySymbol();
						}
					}

					eventParams.put("country", country);
					eventParams.put("currencySymbol", currencySymbol);

					String serviceType = serviceInfo.getActionType();
					double totalPayment = serviceInfo.getTotalAmount();
					String paymentType = null;
					PCPaymentInfo paymentInfo = billPaymentData.getPaymentInfo();
					if (paymentInfo != null) {
						PCBillPaymentData.PCPaymentType pcPaymentType = paymentInfo.getPaymentType();
						if (pcPaymentType != null) {
							paymentType = pcPaymentType.getPaymentType();
						}
					}

					eventParams.put("serviceType", serviceType);
					eventParams.put("totalPayment", totalPayment);
					eventParams.put("paymentType", paymentType);

					if (eventParams.size() == CHECKOUT_PARMS_COUNT && !containsNullEmptyOrZero(eventParams)) {
						logMetric(activity, success, type, country, currencySymbol, serviceType, totalPayment, paymentType);
						eventSent = true;
					}
				}
			}
		} catch (IllegalStateException e) {
			Log.d(TAG, "Exception when sending Checkout event", e);
			throw e;
		} catch (Exception e) {
			Log.d(TAG, "Exception when sending Checkout event", e);
		}
		return eventSent;
	}

	private static void logMetric(PCBaseActivity activity, boolean success, String type, String country, String currencySymbol, String serviceType, double totalPayment, String paymentType) {
		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.PRICE, String.valueOf(totalPayment));
		bundle.putString(FirebaseAnalytics.Param.CURRENCY, currencySymbol);
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, type);
		bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, serviceType);
		bundle.putString(FirebaseAnalytics.Param.LOCATION, country);
		bundle.putString("PAYMENT_TYPE", paymentType);

		bundle.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(success));

		if (activity != null) {
			FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);

			if (mFirebaseAnalytics != null) {
				mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);
			}
		}

	}

	/**
	 * @param country where login from
	 * @param method  using phone number or email
	 */
	public static boolean postLoginMetric(PCBaseActivity activity, String country, String method, boolean success) {
		boolean eventSent = !StringUtils.isEmpty(country) && !StringUtils.isEmpty(method);

		try {

			Bundle bundle = new Bundle();
			if (!StringUtils.isEmpty(country)) {
				bundle.putString(FirebaseAnalytics.Param.LOCATION, country);
			}
			bundle.putString(FirebaseAnalytics.Param.MEDIUM, method);
			bundle.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(success));

			if (activity != null) {
				FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
				if (mFirebaseAnalytics != null) {
					mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
				}
			}
			eventSent = true;

		} catch (IllegalStateException e) {
			Log.d(TAG, "Exception while sending Login event", e);
		} catch (Exception e) {
			eventSent = false;
			Log.d(TAG, "Exception while sending Login event", e);
		}

		return eventSent;
	}

	/**
	 * @param country where sign up from
	 * @param method  using android phone
	 */
	public static boolean postSignUpMetric(PCBaseActivity activity, String country, String method, boolean success) {
		boolean eventSent = !StringUtils.isEmpty(country) && !StringUtils.isEmpty(method);

		try {

			Bundle bundle = new Bundle();
			if (!StringUtils.isEmpty(country)) {
				bundle.putString(FirebaseAnalytics.Param.LOCATION, country);
			}
			bundle.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, method);
			bundle.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(success));

			if (activity != null) {
				FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
				if (mFirebaseAnalytics != null) {
					mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
				}
			}
			eventSent = true;

		} catch (IllegalStateException e) {
			Log.d(TAG, "Exception while sending Sign up event", e);
		} catch (Exception e) {
			eventSent = false;
			Log.d(TAG, "Exception while sending Sign up event", e);
		}

		return eventSent;
	}

	public static boolean postCardAttachmentEvent(PCBaseActivity activity, String country, String method, boolean result) {
		boolean eventSent = !StringUtils.isEmpty(country) && !StringUtils.isEmpty(method);

		try {
			Bundle bundle = new Bundle();
			if (!StringUtils.isEmpty(country)) {
				bundle.putString(FirebaseAnalytics.Param.LOCATION, country);
			}
			bundle.putString(FirebaseAnalytics.Param.MEDIUM, method);
			bundle.putString(FirebaseAnalytics.Param.VALUE, String.valueOf(result));

			if (activity != null) {
				FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
				if (mFirebaseAnalytics != null) {
					mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, bundle);
				}
			}
			eventSent = true;
		} catch (IllegalStateException e) {
			Log.d(TAG, "Exception while sending Card attachment event", e);
		} catch (Exception e) {
			eventSent = false;
			Log.d(TAG, "Exception while sending Login event", e);
		}

		return eventSent;
	}

	/**
	 * @param eventName  metric name to dashboard
	 * @param attributes custom attributes
	 */
	public static boolean postCustomEvent(String eventName, AbstractMap.SimpleEntry<String,String>... attributes) {

		boolean eventSent = false;

		try {
			eventSent = !StringUtils.isEmpty(eventName) && attributes != null && attributes.length > 0;

			if (eventSent) {

			}
		} catch (IllegalStateException e) {
			Log.d(TAG, "Exception while sending Custom event: " + eventName, e);
			throw e;
		} catch (Exception e) {
			eventSent = false;
			Log.d(TAG, "Exception while sending Custom event: " + eventName, e);
		}

		return eventSent;
	}

	private static boolean containsNullEmptyOrZero(Map<String, Object> eventParams) {
		return eventParams.containsValue(null) || eventParams.containsValue("") || eventParams.containsValue(0.0);
	}

}
