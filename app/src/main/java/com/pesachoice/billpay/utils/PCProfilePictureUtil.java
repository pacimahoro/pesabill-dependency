package com.pesachoice.billpay.utils;


import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.activities.helpers.PCPhotoUpload;
import com.pesachoice.billpay.business.PCPesachoiceConstant;

import org.springframework.util.StringUtils;



/**
 * attach profile picture to our database
 * Created by desire.aheza on 2/4/2017.
 */

public class PCProfilePictureUtil {

    public static final String storage_bucket = PCPesachoiceConstant.STORAGE_ID;
    private static StorageReference storageRef;
    private static Uri downloadUrl;
    private static PCPhotoUpload activity;
    public static final String main_forlder = PCPesachoiceConstant.MAIN_FOLDER;
    private PCProfilePictureUtil profile;
    public static void initializeBucket(PCPhotoUpload pcPhotoUploadActivity){

        try{
            activity=pcPhotoUploadActivity;
            FirebaseStorage storage = FirebaseStorage.getInstance();
            if(storage!=null){
                // Create a storage reference from our app
                storageRef=storage.getReferenceFromUrl("gs://"+storage_bucket);
                if(storageRef!=null){
                    //get reference to our storage directory
                    storageRef.child(main_forlder);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean uploadPhoto(String photoName, byte[] imageToUpload){

        if(storageRef!=null && !StringUtils.isEmpty(photoName)&& imageToUpload!=null){

            // Create a reference to 'pesachoice_ids/photoName'
            StorageReference pesaProfilePicRef = storageRef.child(PCPesachoiceConstant.MAIN_FOLDER+"/"+photoName);
            UploadTask uploadTask = pesaProfilePicRef.putBytes(imageToUpload);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    downloadUrl = taskSnapshot.getDownloadUrl();
                    if(activity!=null){
                        activity.onFinishedPhotoUploading(downloadUrl);
                    }
                }
            });
        }
        return downloadUrl==null?false:true;
    }


}
