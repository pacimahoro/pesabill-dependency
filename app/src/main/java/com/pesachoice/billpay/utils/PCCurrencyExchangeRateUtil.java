package com.pesachoice.billpay.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Class that holds utils methods to deal with currency,exchange
 * Created by desire.aheza on 2/8/2017.
 */

public class PCCurrencyExchangeRateUtil {

    private static PCBaseActivity activity = null;
    private final static String TAG = PCCurrencyExchangeRateUtil.class.getName();

    //get currency and exchange rate to the receiving country
    public static Map<String,Double> getCurrencyAndXchangeRate(PCBaseActivity act) {
        activity = act;
        Map<String,Double> map = new HashMap<>();
        if (activity != null && activity.getAppUser() != null) {
            PCSpecialUser currentUser = activity.getAppUser();

            String countryCodeValue;

            if (currentUser != null && !StringUtils.isEmpty(currentUser.getCountry())) {
                //get exchange rate based on sender country and receiver country
                countryCodeValue = currentUser.getCountry();
            }
            else {
                //TODO: discuss with paci "call Twilio or use telephone manager"
                countryCodeValue = getCountryCode();

            }
            Log.d(TAG,getCurrencySymbol(countryCodeValue));
        }
        return map;
    }

    /**
     * using android telephony manager get country code
     * @return
     */
    private static String getCountryCode() {
        String countryCodeValue;
        TelephonyManager tm = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        countryCodeValue = tm.getNetworkCountryIso();
        return countryCodeValue;
    }

    /**
     * given sender and receiver determine if they're in the same country
     * @param sender
     * @param receiver
     * @return
     */
    public static boolean isSenderReceiverInSameCountry(PCBaseActivity act, PCUser sender, PCUser receiver){
        boolean theyReInSameCountry = false;
        activity = act;
        try{
            if (sender != null && receiver != null) {
                String senderCountry = sender.getCountry();
                String receverCountry = receiver.getCountry();

                if (StringUtils.isEmpty(senderCountry)) {
                    //TODO:discuss with Paci
                    senderCountry = getCountry(getCountryCode());
                }

                theyReInSameCountry = receverCountry.equalsIgnoreCase(senderCountry);
            }

        } catch (Exception e) {
            showError(e,"Error Checking if sender and Receiver are in the same country");
        }

        return theyReInSameCountry;
    }

    /**
     * Is sender and receiver in same country boolean if it return false it will convert it in the receiver currency.
     *
     * @param sender   the sender
     * @param receiver the receiver
     * @return the boolean
     */
        public static boolean isSenderReceiverInSameCountry(PCSpecialUser sender, PCSpecialUser receiver) {
            boolean theyReInSameCountry = false;
            try {
                if (sender != null && receiver != null) {
                    PCCurrency senderCountryCurrency = sender.getBaseCurrency();
                    String receiverCountry = receiver.getCountry();

                    if (senderCountryCurrency != null && !StringUtils.isEmpty(senderCountryCurrency.getCountry())
                            && !StringUtils.isEmpty(receiverCountry)) {
                        theyReInSameCountry = receiverCountry.equalsIgnoreCase(senderCountryCurrency.getCountry());

                    }
                }

            } catch (Exception e) {
                showError(e,"Error Checking if sender and Receiver are in the same country");
            }

            return theyReInSameCountry;
        }


    public static String getCountry(PCBaseActivity activ) {
        activity = activ;
        return getCountry(getCountryCode());
    }

    private static String getCountry(String countryCode) {

        try {
            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray) parser.parse(new BufferedReader(
                    new InputStreamReader( activity.getAssets().open("country.json"))));

            for (Object o : array)
            {
                JSONObject country = (JSONObject) o;
                String country_name = (String) country.get("name");
                String country_code = (String) country.get("code");

                if (!StringUtils.isEmpty(country_code)&& country_code.equalsIgnoreCase(countryCode))
                    return country_name;
            }


        } catch ( ParseException | IOException e) {
            e.printStackTrace();
            showError(e,"Error getting the country name");
        }
        return null;
    }

    public static Map<Currency, Locale> getCurrencyLocaleMap() {
        Map<Currency, Locale> currencyLocaleMap = new HashMap<Currency, Locale>();
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                Currency currency = Currency.getInstance(locale);
                currencyLocaleMap.put(currency, locale);
            } catch (Exception e) {
                showError(e,"Error getting Local Currency");
            }
        }
        return currencyLocaleMap;
    }

    private static void showError(Exception e, String errorMessage) {
        Log.e(PCCurrencyExchangeRateUtil.class.getName(), errorMessage+"[" + e.getMessage() + "]");
        PCGenericError error = new PCGenericError();
        error.setMessage(errorMessage);
        presentError(error, "Error");
    }

    public static String getCurrencySymbol(String currencyCode) {
        String currencySymbol = null;

        if (!StringUtils.isEmpty(currencyCode)) {

            Locale currencyLocale = null;
            Map<Currency, Locale> currencyLocaleMap = null;
            Currency currency = null;
            try {
                currency = Currency.getInstance(currencyCode);
                currencyLocaleMap = getCurrencyLocaleMap();
                currencyLocale = currencyLocaleMap.get(currency);
            } catch (Exception e) {
                Log.e(TAG,"No symbol is there for currencyCode="
                        + currencyCode,e);
            }
            if (currency != null && currencyLocale != null) {
                currencySymbol = currency.getSymbol(currencyLocaleMap
                        .get(currency));
            } else {
                currencySymbol = currencyCode;
            }
        }
        return currencySymbol;
    }


    public static void presentError(PCGenericError error, String title) {
        String message = error.getMessage();
        if (activity != null ) {

            if (activity.isFinishing() || message == null)
                return;
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(message)
                    .setTitle(title);

            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //finish the activity
                    activity.finish();
                    Log.d(PCCurrencyExchangeRateUtil.class.getName(), "Clicked on OK on Alert box");
                }
            });

            Dialog alert = builder.create();
            alert.show();
        }
    }
}

