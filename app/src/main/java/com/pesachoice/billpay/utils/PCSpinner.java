package com.pesachoice.billpay.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * customer spinner view, to allow us select the same item twice
 * Created by desire.aheza on 3/18/2017.
 */

public class PCSpinner extends Spinner {


    public PCSpinner(Context context) {
        super(context);
    }

    public PCSpinner(Context context, int mode) {
        super(context, mode);
    }

    public PCSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PCSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PCSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    @Override
    public void setSelection(int position, boolean animate) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position, animate);
        if (sameSelected) {
            try{
                // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
                getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
            }catch(Exception e){

            }
        }
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected) {
            try{
                // Spinner does not call the OnItemSelectedListener if the same item is selected, so do it manually now
                getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
            }catch(Exception e){

            }
        }
    }
}
