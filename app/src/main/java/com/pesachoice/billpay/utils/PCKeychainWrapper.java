/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.utils;

import com.pesachoice.billpay.business.PCPesachoiceConstant;

import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Encryption based methods
 * Created by pacifique on 11/2/15.
 * Updated by Desire Aheza on 05/02/2016
 */
public class  PCKeychainWrapper {

    private final static String SALT = "FvTivqTqZXsgLLx1v3P8TGRyVHaSOB1pvfm02wvGadj7RLHV8GrfxaZ84oGA8RsKdNRpxdAojXYg9iAj";

    public static String securedSHA256DigestHash(String input, String userName) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(userName);
        strBuilder.append(input);
        strBuilder.append(SALT);

        return computeSHA256DigestForString(strBuilder.toString());
    }

    public static String securedAESEncryption(String plainText, String input,String plainTextAnotherArgs){
        String result = null;
        try{
            if(!StringUtils.isEmpty(input)){
                StringBuilder buildClear=new StringBuilder();
                if(StringUtils.isEmpty(plainTextAnotherArgs))
                    buildClear.append(input).append(plainText);
                else
                    buildClear.append(plainText).append(input).append(plainTextAnotherArgs);
                byte[] rawKey = input.getBytes("UTF-8");
                byte[] clear = buildClear.toString().getBytes("UTF-8");
                byte[] res = encrypt(rawKey, clear);
                result = bytesToHex(res);
            }
            else{
                throw new Exception(PCPesachoiceConstant.FAILED_IN_AES_ENCRIPTION);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    public static String computeSHA256DigestForString(String secret){
        final MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] bytes = secret.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            String sig = bytesToHex(digest.digest());
            return sig;
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e){
            throw new RuntimeException("Cannot calculate signature");
        }
    }

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
