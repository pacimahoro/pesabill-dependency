package com.pesachoice.billpay.utils;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.activities.R;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;

import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * use to support the integration of PUSH notification
 * Created by desire.aheza on 1/17/2017.
 */
public class PCPushNotificationSupport {

    private static String SUBSCRIBEKEY=null;
    private static PubNub pubNub;

    //get a pubNub to allow us to receive pushed messages
    public static synchronized PubNub initialize(PCBaseActivity activity){

        if(pubNub == null){
            PNConfiguration pnConfiguration = new PNConfiguration();
            if(StringUtils.isEmpty(SUBSCRIBEKEY)){
                getKey(activity);
            }
            pnConfiguration.setSubscribeKey(SUBSCRIBEKEY);
            pnConfiguration.setSecure(false);
            pubNub=new PubNub(pnConfiguration);

            //subscribe to the channel "pesachoice_channel"
            pubNub.subscribe()
                    .channels(Arrays.asList("pesachoice_channel"))
                    .execute();
        }

        return pubNub;
    }

    /**
     * get key required to receive pushed messages
     * @param activity
     */
    private static void getKey(PCBaseActivity activity){
        if(activity != null && activity.getResources()!= null ){
            SUBSCRIBEKEY = activity.getResources().getString(R.string.pubnub_subscriber_key);
        }
    }

}
