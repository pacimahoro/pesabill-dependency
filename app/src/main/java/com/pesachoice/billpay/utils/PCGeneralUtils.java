/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.utils;



import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * Used general and shared utility functionality
 *
 * @author Odilon Senyana
 */
public class PCGeneralUtils {

	/**
	 * Checks a valid email
	 *
	 * @param target email to validate
	 * @return <b>boolean</b>
	 * <b>{@Code true}</b> if the email is valid and <b>{@code false}</b> otherwise
	 */
	public final static boolean isValidEmail(String target) {
		boolean result = false;

			if (EMAIL_ADDRESS.matcher(target).matches()) {

				result = true;

			} else {
				result = false;
			}

		return result;
	}


	public static final Pattern EMAIL_ADDRESS
			= Pattern.compile(
			"[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
					"\\@" +
					"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
					"(" +
					"\\." +
					"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
					")+"
	);

	/**
	 * Provide a way to tell whether we are in PROD or Test.
	 * There is a probably a better way but until, we will use this workaround
	 * The logic below is derived from whether or not we are pointing to the production URL
	 * @return true or false whether we are in production or test environment.
	 */
	public final static boolean isPROD(){
		return !(PCPesabusClient.URL.startsWith("https://dev") || PCPesabusClient.URL.startsWith("http://dev"));
	}


	/**
	 * Checks a valid phone number, given a particular country. this method takes into consideration that a phone number
	 * has to contain only numbers, and also with digits between 6 and 15
	 *
	 * @param phone   Phone number to validate

	 * @return <b>boolean</b>
	 * <b>{@Code true}</b> if the phone number is valid and <b>{@code false}</b> otherwise
	 */
	public  static boolean isValidPhone(String phone, String country) {
		// TODO: Do something more robust.

        // Make sure it's digit only
        boolean isDigitOnly = phone.matches("[0-9]+");

        // Check against the minimum phone length for a valid number.
		return isDigitOnly && phone.length() > 6;
	}

	/**
	 * Clean the phone number string of all empty spaces and + signs.
	 *
	 * @param phone       Phone number to clean string
	 * @param countryCode Country code that the number belongs to.
	 * @return <b>String</b>
	 * represents the clean phone number.
	 */
	public final static String cleanPhoneString(String phone, String... countryCode) {
		if (phone == null || phone.length() == 0) {
			return phone;
		} else

		// Strip of all empty or any other special character spaces;
		return phone.replaceAll("[^0-9]", "");
	}

	/**
	 * Format a phone number given its country code
	 *
	 * @param phone       Phone number to format
	 * @param countryCode Country code that the number belongs to
	 * @return <b>String</b>
	 * represents the formatted phone number.
	 */
	public static final String formatPhoneNumber(String phone, String countryCode) {
		if (countryCode == null || phone == null) {
			return null;
		}

		if (phone.startsWith(countryCode)) {
			return phone.replaceFirst(countryCode, "+" + countryCode + " ");
		}

		return phone;
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static boolean isCountryUS(String ctry) {
		return "USA".equalsIgnoreCase(ctry) || "US".equalsIgnoreCase(ctry)
				|| "United States Of America".equalsIgnoreCase(ctry) || "United States".equalsIgnoreCase(ctry);
	}

	public static String createTrackingUserKey(PCSpecialUser userResponse, String apiKey) {
		StringBuilder keyComb = new StringBuilder();
		String firstName = userResponse.getFirstName();
		String lastName = userResponse.getFirstName();
		String phoneNumber = userResponse.getPhoneNumber();

		keyComb.append(StringUtils.isEmpty(firstName) ? "" : firstName);
		keyComb.append(StringUtils.isEmpty(lastName) ? "" : lastName);
		keyComb.append(StringUtils.isEmpty(phoneNumber) ? "" : phoneNumber);

		return PCKeychainWrapper.securedAESEncryption(keyComb.toString(), apiKey, "");
	}
}
