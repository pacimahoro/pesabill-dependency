package com.pesachoice.billpay.utils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSimplifiedUser;
import com.pesachoice.billpay.model.PCTicketDetails;
import com.pesachoice.billpay.model.PCTicketServiceInfo;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * Generating QR code for a ticket and other util methods for event ticket
 * Created by desire.aheza on 12/3/2016.
 */
public class PCTicketsUtils {
    private static String TAG=PCTicketsUtils.class.getName();
    /**
     * used to save purchased ticket information
     * @param activity
     * @param ticketTranscation
     */
    public static void saveTicketInformation(PCMainTabActivity activity,PCTransaction ticketTranscation){

        PCTicketServiceInfo serviceInfo= (PCTicketServiceInfo) ticketTranscation.getServiceInfo();
        if(serviceInfo!=null){
            String eventStringDetails=getJsonString(serviceInfo);
            Bitmap eventQRImage=encodeAsBitMap(eventStringDetails);
            storeImage(eventQRImage,activity,serviceInfo.getName().replaceAll("\\s+"," "));
        }

    }

    public static Bitmap getTicketQRInformation(PCTransaction ticketTransaction){
        Bitmap eventQRImage = null;

        if (ticketTransaction != null) {
            PCServiceInfo serviceInfo = ticketTransaction.getServiceInfo();
            if (serviceInfo instanceof PCTicketServiceInfo) {
                PCTicketServiceInfo ticketServiceInfo = (PCTicketServiceInfo)serviceInfo;
                PCUser receiver = ticketTransaction.getReceiver();
                PCTicketDetails pcTicketDetails = new PCTicketDetails();
                pcTicketDetails.setEventName(ticketServiceInfo.getName());
                pcTicketDetails.setPrice(ticketServiceInfo.getTotalLocalCurrency());
                pcTicketDetails.setTicketNumber(String.valueOf(ticketTransaction.getTransactionId()));

                if (receiver != null) {
                    pcTicketDetails.setEmail(receiver.getEmail());
                    pcTicketDetails.setUserName(receiver.getFirstName() + " " + receiver.getLastName());
                }

                // TODO: Maybe add the currency and more details to the ticketDetails
                String eventStringDetails = getJsonString(pcTicketDetails);
                eventQRImage = encodeAsBitMap(eventStringDetails);
            }
        }

        return eventQRImage;
    }

    private static File getOrCreateImageFolder(AppCompatActivity activity){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + activity.getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        return mediaStorageDir;
    }

    private static Bitmap encodeAsBitMap(String content){
        QRCodeWriter writer = new QRCodeWriter();
        Bitmap bmp=null;
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 200, 200);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return bmp;
    }

    private static String getJsonString(Object obj) {
        String jsonString = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            jsonString = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }

    private static File createQRFile(File mediaStorageDir,String fileName ){
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName=fileName+"MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    /**
     * store image on external SD file
     * @param image
     * @param activity
     * @param imageName
     */
    private static void storeImage(Bitmap image,AppCompatActivity activity,String imageName) {
        //get image directory
        File mediaStorageDir = getOrCreateImageFolder(activity);
        //get image file name
        File pictureFile=createQRFile(mediaStorageDir,imageName);
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");
            return;
        }
        try {
            //save image
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    public static List<String> getEventsQRImages(AppCompatActivity activity){
        List<String> qrFilesPath=new ArrayList<>();
        //get image directory
        File mediaStorageDir = getOrCreateImageFolder(activity);
        File[] eventesQRImages=mediaStorageDir.listFiles();
        for(File f:eventesQRImages){
            qrFilesPath.add(f.getAbsolutePath());
        }
        return qrFilesPath;
    }
}
