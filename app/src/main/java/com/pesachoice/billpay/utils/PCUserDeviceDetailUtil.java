package com.pesachoice.billpay.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.model.PCUserDeviceDetails;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 *
 * Class that holds utils methods for extracting user device details
 * Created by desire.aheza on 7/15/2017.
 */

public class PCUserDeviceDetailUtil {
	/**
	 * Convert byte array to hex string
	 * @param bytes
	 * @return
	 */


	/**
	 * Get utf8 byte array.
	 * @param str
	 * @return  array of NULL if error was found
	 */


	/**
	 * Load UTF8withBOM or any ansi text file.
	 * @param filename
	 * @return
	 * @throws java.io.IOException
	 */



	/**
	 * Returns MAC address of the given interface name.
	 * @param interfaceName eth0, wlan0 or NULL=use first interface
	 * @return  mac address or empty string
	 */
	private static String getMACAddress(String interfaceName) throws SocketException {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac == null) return "";
				StringBuilder buf = new StringBuilder();
				for (int idx=0; idx<mac.length; idx++)
					buf.append(String.format("%02X:", mac[idx]));
				if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
				return buf.toString();
			}
		} catch (Exception ex) {
			throw ex;
		}
		return "";

	}

	/**
	 * Get IP address from first non-localhost interface
	 * @param useIPv4  true=return ipv4, false=return ipv6
	 * @return  address or empty string
	 */
	private static String getIPAddress(boolean useIPv4) throws SocketException {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress();

						boolean isIPv4 = sAddr.indexOf(':')<0;

						if (useIPv4) {
							if (isIPv4)
								return sAddr;
						} else {
							if (!isIPv4) {
								int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
								return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
		return "";
	}

	public static PCUserDeviceDetails getUserDeviceDetails(final PCBaseActivity activity){
		PCUserDeviceDetails userDeviceDetails=new PCUserDeviceDetails();
		try{
			userDeviceDetails.setIpv4Address(getIPAddress(true));
			userDeviceDetails.setIpv6Address(getIPAddress(false));
			userDeviceDetails.setMacWlanAddress(getMACAddress("wlan0"));
			userDeviceDetails.setMacEthAddress(getMACAddress("eth0"));
			String osVersion = Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName();
			userDeviceDetails.setOsVersion(osVersion);
			userDeviceDetails.setPhoneModel(Build.MODEL+ " " + Build.VERSION.RELEASE);

			userDeviceDetails.setPhoneManufacture(Build.MANUFACTURER);
			userDeviceDetails.setCountry(getUserCountry(activity));
		}catch (Exception e){
			Log.d(PCUserDeviceDetailUtil.class.getName(),"Exception when getting user device details",e);
		}

		return userDeviceDetails;
	}

	/**
	 * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
	 * @param context Context reference to get the TelephonyManager instance from
	 * @return country code or null
	 */
	public static String getUserCountry(Context context) {
		try {
			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			final String simCountry = tm.getSimCountryIso();
			if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
				return simCountry.toLowerCase(Locale.US);
			}
			else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
				String networkCountry = tm.getNetworkCountryIso();
				if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
					return networkCountry.toLowerCase(Locale.US);
				}
			}
		}
		catch (Exception e) { }
		return null;
	}
}
