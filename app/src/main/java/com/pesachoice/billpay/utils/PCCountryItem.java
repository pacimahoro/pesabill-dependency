package com.pesachoice.billpay.utils;

/**
 * Created by emmy on 23/02/2018.
 */

public class PCCountryItem {

    private String countryName;
    private int countryFlag;

    public PCCountryItem(String countryName, int countryFlag) {
        this.countryName = countryName;
        this.countryFlag = countryFlag;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getCountryFlag() {
        return countryFlag;
    }
}
