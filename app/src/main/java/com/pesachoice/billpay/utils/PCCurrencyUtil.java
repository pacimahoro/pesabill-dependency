package com.pesachoice.billpay.utils;

import android.util.Log;

import com.neovisionaries.i18n.CountryCode;
import com.neovisionaries.i18n.CurrencyCode;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCServiceFee;
import com.pesachoice.billpay.model.PCServiceTypePaymentInfo;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class that holds utils methods to deal with currency
 * @author Pacifique Mahoro
 */

public class PCCurrencyUtil {
    private final static String clazzz = PCCurrencyUtil.class.getSimpleName();

    // i.e. $10 is the minimum allowed amount
    public static final String MAXIMUM_ERROR_WITH_VARIABLE = "{0} is the max amount using {1}.";
    public static final String MINIMUM_ERROR_WITH_VARIABLE = "{0} is the min amount using {1}.";
    public static final String MINIMUM_ERROR = "{0} is the minimum allowed amount";
    public static final String VALIDATION_ERROR = "Validation Error";
    public static final String VALIDATION_WARNING = "Validation Warning";
    public static final String VALIDATION_LOG = "Validation Log";
    public static final String MISSING_PAYMENT_AMOUNT = "Missing payment amount";
    public static final String USE_MOBILE_MONEY = "Pay w/ Mobile money instead";

    public static final List<String> SUPPORTS_MOBILE_MONEY;

    static {
        SUPPORTS_MOBILE_MONEY = new ArrayList<String>();
        SUPPORTS_MOBILE_MONEY.add("RWF");
        SUPPORTS_MOBILE_MONEY.add("UGX");
    }


    public enum ErrorType {
        INVALID_AMOUNT("Invalid Amount");

        private String message;

        ErrorType(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return this.message;
        }
    }

    public static double calculateExchangeRate(PCCurrency senderCurrency, String receiverCountry) throws PCGenericError {
        double exchangeRate = 0.0;
        if (senderCurrency != null && !StringUtils.isEmpty(receiverCountry)) {

            Map<String, Double> interCountryExchangeRates = senderCurrency.getInterCountryExchangeRates();

            if
                    (interCountryExchangeRates != null)
                for (String countryName : interCountryExchangeRates.keySet()) {
                    if (countryName.equalsIgnoreCase(receiverCountry)) {
                        exchangeRate = interCountryExchangeRates.get(countryName);
                        break;
                    }
                }
        }
        if (exchangeRate == 0.0) {
            PCGenericError validationError = new PCGenericError();
            validationError.setMessage("Error Calculating exchange rate");
            throw validationError;
        }

        return exchangeRate;
    }

    public static Boolean validateAmount(final AmountInput amountInput) throws PCGenericError {
        Boolean result = true;
        PCGenericError validationError;

        // Missing payment info
        if (amountInput == null) {
            validationError = new PCGenericError();
            validationError.setTitle(VALIDATION_ERROR);
            validationError.setMessage(MISSING_PAYMENT_AMOUNT);
            throw validationError;
        }

        // Missing amount
        if (amountInput.getValue().isNaN()) {
            validationError = new PCGenericError();
            validationError.setMessage(ErrorType.INVALID_AMOUNT.toString());
            throw validationError;
        }

        // When the base currency is missing, assume US, and payment is in Dollars;
        if (amountInput.getBaseCurrency() == null) {
            Log.i(clazzz, "Missing base currency. Will assume the US");
            PCCurrency usCurrency = new PCCurrency();
            usCurrency.setCurrencySymbol(CurrencyCode.USD.getCurrency().getCurrencyCode());
            usCurrency.setCountrySymbol(CountryCode.US.getName());
            amountInput.setBaseCurrency(usCurrency);
        }

        // Check the payment type.
        if (amountInput.getPaymentType() == null) {
            // In Rwanda and Uganda default to Mobile money if no payment type is specified.
            // Otherwise, Defaults to Card based.
            if (CountryCode.RW.getName().equalsIgnoreCase(amountInput.getBaseCurrency().getCountrySymbol()) ||
                    CountryCode.UG.getName().equalsIgnoreCase(amountInput.getBaseCurrency().getCountrySymbol())
                    ) {
                amountInput.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
            } else {
                amountInput.setPaymentType(PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
            }
        }

        if (amountInput.getBaseCurrency() != null) {
            Log.i(clazzz, "Will charge in the following currency: " + amountInput.getBaseCurrency().getCurrencySymbol());
        }

        if (amountInput.getServiceTypePaymentInfo() != null) {
            Log.i(clazzz, "payment type: " + amountInput.getPaymentType());
            Log.i(clazzz, "Minimum amount allowed: " + amountInput.getServiceTypePaymentInfo().getMinimumAllowedAmount());
            Log.i(clazzz, "Maximum amount allowed: " + amountInput.getServiceTypePaymentInfo().getMaximumAllowedAmount());
            Log.i(clazzz, "Entered amount: " + amountInput.getValue());

            result = isAmountAllowed(amountInput);
        }

        return result;
    }

    private static Boolean isAmountAllowed(AmountInput amountInput) throws PCGenericError {
        PCGenericError validationError = null;

        PCServiceTypePaymentInfo pcServiceTypePaymentInfo = amountInput.getServiceTypePaymentInfo();
        double val = amountInput.getValue();
        PCBillPaymentData.PCPaymentType paymentType = amountInput.getPaymentType();
        boolean isAuthorizedTestAccount = amountInput.isAuthorizedTestAccount();

        Log.i(clazzz, "About to check if the following amount is allowed: " + val);
        Log.i(clazzz, "AmountInput Info: " + amountInput);
        try {
            double maxValue = pcServiceTypePaymentInfo.getMaximumAllowedAmount().doubleValue();
            double minValue = pcServiceTypePaymentInfo.getMinimumAllowedAmount().doubleValue();

            String errorMessage;
            validationError = new PCGenericError();
            if ((val < minValue) && !isAuthorizedTestAccount) {
                String minValueStr = minValue + " " + pcServiceTypePaymentInfo.getCurrencySymbol();
                errorMessage = MessageFormat.format(PCCurrencyUtil.MINIMUM_ERROR_WITH_VARIABLE, minValueStr, paymentType.getDisplayName());
                validationError.setTitle(PCCurrencyUtil.ErrorType.INVALID_AMOUNT.toString());
                Boolean hasMobileMoney = false;

                for (String mCurrency : SUPPORTS_MOBILE_MONEY) {
                    if (mCurrency.equalsIgnoreCase(pcServiceTypePaymentInfo.getCurrencySymbol())) {
                        hasMobileMoney = true;
                        break;
                    }
                }

                if (paymentType == PCBillPaymentData.PCPaymentType.PAYMENT_CARD && hasMobileMoney) {
                    errorMessage += "\n" + USE_MOBILE_MONEY;
                }
                validationError.setMessage(errorMessage);
                throw validationError;
            } else if (maxValue != 0.0 && val > maxValue) {
                String minValueStr = maxValue + " " + pcServiceTypePaymentInfo.getCurrencySymbol();
                errorMessage = MessageFormat.format(PCCurrencyUtil.MAXIMUM_ERROR_WITH_VARIABLE, minValueStr, paymentType.getDisplayName());
                validationError.setTitle(PCCurrencyUtil.ErrorType.INVALID_AMOUNT.toString());
                validationError.setMessage(errorMessage);
                throw validationError;
            }
        } catch (Exception exc) {
            if (exc instanceof PCGenericError) {
                throw exc;
            }
            exc.printStackTrace();
            validationError = new PCGenericError();
            validationError.setMessage(VALIDATION_ERROR);
            throw validationError;
        }

        return true;
    }

    /**
     * Determines the exact service fee. Given amount, payment type, and service that a user is paying for.
     *
     * @param amountInput
     * @param serviceFees
     * @return
     */
    public static PCServiceTypePaymentInfo determineServiceFee(AmountInput amountInput, List<PCServiceFee> serviceFees) {
        PCServiceTypePaymentInfo serviceTypePaymentInfo = null;
        if (amountInput != null) {
            Double transAmount = amountInput.getValue();
            PCBillPaymentData.PCPaymentType paymentType = amountInput.getPaymentType();
            String serviceType = amountInput.getServiceType();

            BigDecimal val = new BigDecimal(transAmount);

            serviceTypePaymentInfo = new PCServiceTypePaymentInfo();
            serviceTypePaymentInfo.setMinimumAllowedAmount(new BigDecimal(0.0));
            serviceTypePaymentInfo.setMaximumAllowedAmount(new BigDecimal(0.0));

            if (serviceFees != null && serviceFees.size() > 0) {
                serviceTypePaymentInfo.setServiceFee(new BigDecimal(0.0));

                //TODO we need to discuss on this! this is not really efficient

                BigDecimal currentMax = new BigDecimal(0);
                BigDecimal currentMin = new BigDecimal(0);
                for (PCServiceFee pcServiceFee : serviceFees) {
                    List<String> pcServiceTypes = pcServiceFee.getServices();
                    if (paymentType == pcServiceFee.getPaymentType() && serviceType != null) {
                        Boolean containsType = false;
                        if (pcServiceTypes != null) {
                            for (String sType : pcServiceTypes) {
                                if (sType.equalsIgnoreCase(serviceType) ||
                                        PCBillPaymentData.ActionType.ALL_SERVICES.toString().equalsIgnoreCase(sType)) {
                                    containsType = true;
                                    break;
                                }
                            }
                        }

                        if (containsType) {
                            //set min max
                            BigDecimal maxAllowed = pcServiceFee.getMaxAmount();
                            BigDecimal minAllowed = pcServiceFee.getMinAmount();

                            // Initialize the min and max
                            if (maxAllowed != null && serviceTypePaymentInfo.getMaximumAllowedAmount().intValue() == 0) {
                                serviceTypePaymentInfo.setMaximumAllowedAmount(maxAllowed);
                            }
                            if (minAllowed != null && serviceTypePaymentInfo.getMinimumAllowedAmount().intValue() == 0) {
                                serviceTypePaymentInfo.setMinimumAllowedAmount(minAllowed);
                            }
                            if (pcServiceFee.getFeeAmount() != null && serviceTypePaymentInfo.getServiceFee() == null) {
                                serviceTypePaymentInfo.setServiceFee(pcServiceFee.getFeeAmount());
                            }

                            // Look for the perfect range.
                            if (maxAllowed != null && minAllowed != null && (maxAllowed.compareTo(val) >= 0) && (minAllowed.compareTo(val) < 0)) {
                                serviceTypePaymentInfo.setMaximumAllowedAmount(maxAllowed);
                                serviceTypePaymentInfo.setMinimumAllowedAmount(minAllowed);
                                serviceTypePaymentInfo.setServiceFee(pcServiceFee.getFeeAmount());
                                break;
                            }


                        }
                    }
                }
            }
        }

        return serviceTypePaymentInfo;
    }

    public final class AmountInput {
        private Double value;
        private PCServiceTypePaymentInfo serviceTypePaymentInfo;
        private PCBillPaymentData.PCPaymentType paymentType;
        private PCCurrency baseCurrency;
        private String serviceType;
        private boolean authorizedTestAccount;

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public PCServiceTypePaymentInfo getServiceTypePaymentInfo() {
            return serviceTypePaymentInfo;
        }

        public void setServiceTypePaymentInfo(PCServiceTypePaymentInfo serviceTypePaymentInfo) {
            this.serviceTypePaymentInfo = serviceTypePaymentInfo;
        }

        public PCBillPaymentData.PCPaymentType getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(PCBillPaymentData.PCPaymentType paymentType) {
            this.paymentType = paymentType;
        }

        public PCCurrency getBaseCurrency() {
            return baseCurrency;
        }

        public void setBaseCurrency(PCCurrency baseCurrency) {
            this.baseCurrency = baseCurrency;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public boolean isAuthorizedTestAccount() {
            return authorizedTestAccount;
        }

        public void setAuthorizedTestAccount(boolean authorizedTestAccount) {
            this.authorizedTestAccount = authorizedTestAccount;
        }
    }
}
