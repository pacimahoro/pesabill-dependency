/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;;
import android.widget.ImageView;
import android.widget.Toast;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCBaseController;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCountryProfileController;
import com.pesachoice.billpay.business.service.PCMerchantsProfileController;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.business.service.PCUserController;
import com.pesachoice.billpay.fragments.PCFailedTransactionFragment;
import com.pesachoice.billpay.fragments.PCMainTabsFragment;
import com.pesachoice.billpay.fragments.PCProductPaymentFragment;
import com.pesachoice.billpay.fragments.PCProductsFragment;
import com.pesachoice.billpay.fragments.PCReceiptFragment;
import com.pesachoice.billpay.fragments.PCSupportCallingFragment;
import com.pesachoice.billpay.fragments.PCSupportRepeatTransFragment;
import com.pesachoice.billpay.fragments.PCSupportedCountriesFragment;
import com.pesachoice.billpay.fragments.PCTransactionableFragment;
import com.pesachoice.billpay.fragments.PCUserAccountFragment;
import com.pesachoice.billpay.fragments.PCUserActivitiesFragment;
import com.pesachoice.billpay.fragments.PCViewPagerAdapter;
import com.pesachoice.billpay.fragments.PConRequiredDataLoaded;
import com.pesachoice.billpay.model.PCAgentWalletTransaction;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCCallingTransaction;
import com.pesachoice.billpay.model.PCCardInfo;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMerchantsProfile;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCTransactionStatus;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCPushNotificationSupport;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;
import com.pesachoice.billpay.utils.PCUserDeviceDetailUtil;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
//import com.twilio.client.Device;
import org.springframework.util.StringUtils;
import java.util.Queue;
import java.util.*;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.services.concurrency.AsyncTask;

/**
 * Handles the main acti vity that the user sees when they log into the app
 * It contains a tab controller for the main parts of the app (Bill Pay, Activity, Account)
 *
 * @author Pacifique Mahoro.
 */
public class PCMainTabActivity extends PCBaseActivity  {
	private final static String CLAZZZ = PCMainTabActivity.class.getName();
	private Toolbar toolbar;
	private TabLayout tabLayout;
	private ViewPager viewPager;
	public PCPesabusClient.PCServiceType currentServiceTypeAuth;
	private PCPesabusClient.PCServiceType currentServiceTypeGetReveivers;
	private PCCountryProfile countryProfile = null;
	public PCViewPagerAdapter adapter;
	private PConRequiredDataLoaded transFrag;
	private PConRequiredDataLoaded supportedCountryAndProductFragment;
	private ImageView loadInviteScreen;
	private PCBillPaymentData pcBillPaymentTransaction = new PCBillPaymentData();
	public static final int CONTACT_REQUEST_CODE = 1;
	public String countrySendingTo = "Kenya";
	private Bundle savedInstanceState;
	public PCUserAccountFragment pcUserAccountFragment = new PCUserAccountFragment();
	public boolean isCallFromUserAccountFragment = false;
	private PCCallingServiceInfo callingServiceInfo;
	public boolean isAddingCallingCreditCard = false;
	private List<PCUser> pcAllReceivers = null;
	private PCSupportRepeatTransFragment repeatTransactionFragment = null;
	private boolean gettingProfileForRepeatTranscation = false;
	private List<String> messageReceived = new ArrayList<>();
	private PCCardInfo cardInfo;
	private String maskedLocalCard;
	private PCMainTabActivity activity;


	public String getMaskedLocalCard() {
		return maskedLocalCard;
	}

	public void setMaskedLocalCard(String maskedLocalCard) {
		maskedLocalCard = "XXXX-XXXX-XXXX-" + maskedLocalCard;
		this.maskedLocalCard = maskedLocalCard;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(CLAZZZ, "onCreate of Main Tab Activity is called");
		setContentView(R.layout.pc_activity_main_tab);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		toolbar.showOverflowMenu();
		setSupportActionBar(toolbar);
		this.savedInstanceState = savedInstanceState;
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		viewPager.setOffscreenPageLimit(3);
		this.setupViewPager(viewPager);
		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		tabLayout.setupWithViewPager(viewPager);
		setupTabIcons();
		Intent intent = getIntent();
		if (intent != null) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
				this.appUser = (PCSpecialUser) objBundle;
				userAuntheticated();

			}
		}
	}

	public void userAuntheticated () {
		if (appUser != null) {
			boolean autoLogedIn = this.getIntent().getExtras().getBoolean(PCPesachoiceConstant.USER_AUTO_LOGGED_IN, false);

			if (autoLogedIn) {
				try {
					final PCRequest isAuthenticatedRequest = new PCRequest();
					isAuthenticatedRequest.setUser(appUser);
					isAuthenticatedRequest.setDetailed(true);
					isAuthenticatedRequest.setSpecial(true);
					this.currentServiceTypeAuth = PCPesabusClient.PCServiceType.IS_AUTHENTICATED;
					PCUserController userController = (PCUserController)
							PCControllerFactory.constructController(
									PCControllerFactory.PCControllerType.USER_CONTROLLER, this);
					if (userController != null) {
						userController.setActivity(this);
						userController.setServiceType(PCPesabusClient.PCServiceType.IS_AUTHENTICATED);
						userController.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, isAuthenticatedRequest);
					}
				} catch (Throwable exc) {
					Log.e(CLAZZZ, "Could not handle Authentication process because [" + exc.getMessage() + "]");
					if (progressDialog != null) {
						progressDialog.dismiss();
					}
					PCGenericError error = new PCGenericError();
					if(exc instanceof PCGenericError) {
						error = (PCGenericError)exc;
					}
					else
						error.setMessage(exc.getMessage());
					this.presentError(error, "Error While Authenticating User");
				}
			} else {
				generateCallingDetails();
			}
			//start notification module
			PubNub pubNub = PCPushNotificationSupport.initialize(this);
			pubNub.addListener(getPubNubListener());
		}
	}

	/*
	 * Receive intent for incoming call from Twilio Client Service
	 * Android will only call Activity.onNewIntent() if `android:launchMode` is set to `singleTop`.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	@NonNull
	protected SubscribeCallback getPubNubListener() {
		return new SubscribeCallback() {
			@Override

			public void status(PubNub pubnub, PNStatus status) {
				// the status object returned is always related to subscribe but could contain
				// information about subscribe, heartbeat, or errors
				// use the operationType to switch on different options
				switch (status.getOperation()) {
					// let's combine unsubscribe and subscribe handling for ease of use
					case PNSubscribeOperation:
					case PNUnsubscribeOperation:
						// note: subscribe statuses never have traditional
						// errors, they just have categories to represent the
						// different issues or successes that occur as part of subscribe
						switch (status.getCategory()) {
							case PNConnectedCategory:
								// this is expected for a subscribe, this means there is no error or issue whatsoever
							case PNReconnectedCategory:
								// this usually occurs if subscribe temporarily fails but reconnects. This means
								// there was an error but there is no longer any issue
							case PNDisconnectedCategory:
								// this is the expected category for an unsubscribe. This means there
								// was no error in unsubscribing from everything
							case PNUnexpectedDisconnectCategory:
								// this is usually an issue with the internet connection, this is an error, handle appropriately
							case PNAccessDeniedCategory:
								// this means that PAM does allow this client to subscribe to this
								// channel and channel group configuration. This is another explicit error
							default:
								// More errors can be directly specified by creating explicit cases for other
								// error categories of `PNStatusCategory` such as `PNTimeoutCategory` or `PNMalformedFilterExpressionCategory` or `PNDecryptionErrorCategory`
						}

					case PNHeartbeatOperation:
						// heartbeat operations can in fact have errors, so it is important to check first for an error.
						// For more information on how to configure heartbeat notifications through the status
						// PNObjectEventListener callback, consult <link to the PNCONFIGURATION heartbeart config>
						if (status.isError()) {
							// There was an error with the heartbeat operation, handle here
						} else {
							// heartbeat operation was successful
						}
					default: {
						// Encountered unknown status type
					}
				}
			}

			@Override
			public void message(PubNub pubnub, PNMessageResult message) {
				// handle incoming messages
				if (message != null && message.getMessage() != null) {
					showNotification(message.getMessage().toString());
				} else {
					showNotification("Error Decoding message");
				}
			}

			@Override
			public void presence(PubNub pubnub, PNPresenceEventResult presence) {
				// handle incoming presence data
			}
		};
	}

	private void showNotification(String message) {
		int notifyID = 1;
		Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.pesa_logo);
		messageReceived.add(message);
		int messageCount = messageReceived.size();
		String messageHint = messageCount > 1 ? "new notifications" : "new notification";

		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.pesa_logo)
						.setContentTitle("PesaChoice")
						.setAutoCancel(true)
						.setWhen(System.currentTimeMillis())
						.setContentText(messageCount + " " + messageHint);
// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, PCNotificationActivity.class);
		String[] msgs = messageReceived.toArray(new String[messageReceived.size()]);
		resultIntent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, msgs);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(PCMainTabActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
				stackBuilder.getPendingIntent(
						0,
						PendingIntent.FLAG_UPDATE_CURRENT
				);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// notifyID allows you to update the notification later on.
		mNotificationManager.notify(notifyID, mBuilder.build());
	}

	public void movePrevious(int pastItem) {
		//it doesn't matter if you're already in the first item
		viewPager.setCurrentItem(viewPager.getCurrentItem() - pastItem);
	}

	public void makeCallToGetCountryProfile(String countrySendingTo, PConRequiredDataLoaded  frag, PCSupportRepeatTransFragment activityDetailFragmt) {
		try {
			this.currentServiceType = PCPesabusClient.PCServiceType.COUNTRY_PROFILE;

			if (frag != null) {
				supportedCountryAndProductFragment = frag;
				gettingProfileForRepeatTranscation = false;
			} else if (activityDetailFragmt != null) {
				gettingProfileForRepeatTranscation = true;
				this.repeatTransactionFragment = activityDetailFragmt;
			}
			//get selected destination country
			this.countrySendingTo = countrySendingTo;
			//make request object
			final PCOperatorRequest operatorRequest = getPcOperatorRequest(countrySendingTo);
			//get country profile
			PCCountryProfileController countryProfileController = (PCCountryProfileController)
					PCControllerFactory.constructController(PCControllerFactory.PCControllerType.COUNTRY_PROFILE, this);
			if (countryProfileController != null) {
				countryProfileController.setActivity(this);
				//countryProfileController.setRequest(setCompanyId("virunga"));
				countryProfileController.setServiceType(PCPesabusClient.PCServiceType.COUNTRY_PROFILE);
				countryProfileController.execute(operatorRequest);
			}
		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle getting countryProfile process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}

			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError) exc;
			} else {
				error.setMessage(exc.getMessage());
			}
			if (this.repeatTransactionFragment != null) {
				this.repeatTransactionFragment.dismissProgressBar();
				error.setNeedToGoBack(false);
			}
			this.presentError(error, "Error Getting Country Profile");
		}
	}

	public void retrieveMerchantsdProfile(String countrySendingTo) {
		try {
			this.currentServiceType = PCPesabusClient.PCServiceType.MERCHANTS_PROFILE;

			PCMerchantsProfileController merchantsProfileController =
					(PCMerchantsProfileController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.MERCHANTS_PROFILE, this);

			//make request object
			final PCOperatorRequest operatorRequest = getPcOperatorRequest(countrySendingTo);
			if (merchantsProfileController != null) {
				merchantsProfileController.setActivity(this);
				merchantsProfileController.setServiceType(PCPesabusClient.PCServiceType.MERCHANTS_PROFILE);
				merchantsProfileController.execute(operatorRequest);
			}
		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle getting Merchants Profile process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError)exc;

			}
			else
				error.setMessage(exc.getMessage());
			this.presentError(error, "Error Getting Merchant Profile");
		}
	}

	public PCCallingServiceInfo getCallingServiceInfo() {
		return callingServiceInfo;
	}

	public void setCallingServiceInfo(PCCallingServiceInfo callingServiceInfo) {
		this.callingServiceInfo = callingServiceInfo;
	}

	@NonNull
	public PCOperatorRequest getPcOperatorRequest(String countrySendingTo) {
		// Get the country profile data, which will be shared throughout the application
		final PCOperatorRequest operatorRequest = new PCOperatorRequest();
		String companyId = getResources().getString(R.string.company_Id);
		operatorRequest.setCountry(countrySendingTo);
		operatorRequest.setEmail(this.appUser.getEmail());
		operatorRequest.setTokenId(this.appUser.getTokenId());
		operatorRequest.setPhoneNumber(this.getAppUser().getPhoneNumber());
		operatorRequest.setCompanyId(companyId);
		return operatorRequest;
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(CLAZZZ, "onPause of Main Tab Activity is called");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(CLAZZZ, "onResume of Main Tab Activity is called");
		Intent intent = getIntent();
		if (intent != null) {
			/*
			 * Determine if the receiving Intent has an extra for the incoming connection. If so,
             * remove it from the Intent to prevent handling it again next time the Activity is resumed
             */
		}
	}


	public PCCountryProfile getCountryProfile() {
		return this.countryProfile;

	}

	public void setupViewPager(ViewPager viewPager) {
		adapter = new PCViewPagerAdapter(getSupportFragmentManager());
		supportedCountryAndProductFragment = new PCSupportedCountriesFragment();
		if (savedInstanceState == null) {
			adapter.addFragment((PCMainTabsFragment) supportedCountryAndProductFragment, "Services");
			adapter.addFragment(new PCUserActivitiesFragment(), "Activity");
			pcUserAccountFragment = new PCUserAccountFragment();
			adapter.addFragment(pcUserAccountFragment, "Account");
			appUser = getLogedInUser();
			if (appUser != null && appUser.isCustomerRep()) {
				adapter.addFragment(new PCSupportCallingFragment(), "Support");
			}

		} else {
			Integer count = savedInstanceState.getInt("tabsCount");
			String[] titles = savedInstanceState.getStringArray("titles");
			for (int i = 0; i < count; i++) {
				adapter.addFragment(getFragment(i), titles[i]);
			}
		}
		viewPager.setAdapter(adapter);
	}

	private Fragment getFragment(int position) {
		return savedInstanceState == null ? adapter.getItem(position) : getSupportFragmentManager().findFragmentByTag(getFragmentTag(position));
	}

	private String getFragmentTag(int position) {
		return "android:switcher:" + R.id.viewpager + ":" + position;
	}

	public void setupTabIcons() {
		// FIXME: This needs to be configurable not hardcoded.
		tabLayout.getTabAt(0).setIcon(R.drawable.icn_tab_bill);
		tabLayout.getTabAt(1).setIcon(R.drawable.icn_tab_list);
		tabLayout.getTabAt(2).setIcon(R.drawable.icn_tab_account);
		if (appUser != null && appUser.isCustomerRep()) {
			tabLayout.getTabAt(3).setIcon(R.drawable.icn_tab_phone);
		}


	}


	@Override
	public void setAppUser(PCSpecialUser user) {
		super.setAppUser(user);
		refleshUserAccountFragment();
	}

	protected void refleshUserAccountFragment() {
		// FIXME: We should have this Hardcoded like this
		if (adapter != null && adapter.getCount() >= PCPesachoiceConstant.NUMBER_OF_FRAGMENT_IN_PCMAINTABACTIVITY - 1) {
			PCUserAccountFragment accountFragment = (PCUserAccountFragment) adapter.getItem(PCPesachoiceConstant.NUMBER_OF_FRAGMENT_IN_PCMAINTABACTIVITY - 1);
			android.support.v4.app.FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
			fragTransaction.detach(accountFragment);
			fragTransaction.attach(accountFragment);
			fragTransaction.commit();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		try {
			switch (requestCode) {

				case (CONTACT_REQUEST_CODE):
					if (resultCode == RESULT_OK) {
						PCContactInfo contactInfo = data.getParcelableExtra(PCPesachoiceConstant.CONTACT_RESULTS);
						if (contactInfo != null) {
							Toast.makeText(this, contactInfo.getDisplayName(), Toast.LENGTH_LONG).show();
						}
					}
					break;
				default:
					break;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void logoutTheUser() {
		Log.d(CLAZZZ, "About to logout the user.");
		logoutPCuser(appUser);
	}

	@Override
	public void onTaskStarted() {
		super.onTaskStarted();
		if (isAttachingCard()) {
			progressDialog = ProgressDialog.show(this, "Card Attachment", "Saving", true);
		}
		if (currentServiceType == PCPesabusClient.PCServiceType.MERCHANTS_PROFILE) {
			progressDialog = ProgressDialog.show(this, "", "Loading Merchants", true);
		}
		if (this.currentServiceType == PCPesabusClient.PCServiceType.USER_ACTIVITIES) {
			progressDialog = ProgressDialog.show(this, "User Activities", "Loading transaction history", true);
		}
		if (isCurrentServiceTypeTransaction()) {
			progressDialog = ProgressDialog.show(this, "Complete Transaction", "Processing", true);
		}
	}

	@Override
	public void onTaskCompleted(PCData data) {
		super.onTaskCompleted(data);
		if (data != null && data.getErrorMessage() != null && data.getErrorMessage().length() > 0) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}

			if (this.isCurrentServiceTypeTransaction()) {
				//post metrics transaction failed
				PCPostPesaBillMetrics.postPurchaseMetric(this, pcBillPaymentTransaction, false);
				//get service info of a given transaction
				PCServiceInfo serviceInfo = getServiceInfo((PCTransaction) data);
				// check if the transaction is still pending
				if (serviceInfo != null && serviceInfo.getTransactionStatus() == PCTransactionStatus.IN_PROGRESS) {
					//set the current transaction to PENDING
					pcBillPaymentTransaction.retrieveServiceInfo().setTransactionStatus(PCTransactionStatus.IN_PROGRESS);
					this.showTransactionReceipt((PCTransaction) data);
				} else {
					this.showFailedTransaction(data);
				}
			}
			// FIXME: Why do we need these checks???
			else if ((this.currentServiceType == PCPesabusClient.PCServiceType.LOGOUT) ||
					(this.currentServiceType == PCPesabusClient.PCServiceType.PING && data instanceof PCPingResults) ||
					(this.currentServiceType == PCPesabusClient.PCServiceType.COUNTRY_PROFILE && data instanceof PCCountryProfile) ||
					(this.currentServiceType == PCPesabusClient.PCServiceType.MERCHANTS_PROFILE && data instanceof PCMerchantsProfile) ||
					(this.currentServiceType == PCPesabusClient.PCServiceType.GET_CALLING_PROFILE && data instanceof PCCallingServiceInfo) ||
					(data instanceof PCUser && data.getActionType() != null && "AUTHORIZATION".equalsIgnoreCase(data.getActionType()))) {
				this.straightLogout();
			} else {
				if (isAttachingCard()) {
					// FIXME: Pass the correct method or type for this metric event
					PCPostPesaBillMetrics.postCardAttachmentEvent(this, getCountry(appUser), "", false);
				}
				PCGenericError error = new PCGenericError();
				error.setMessage(data.getErrorMessage());
				this.presentError(error, "Error");
			}
			return;
		}

		if (isAttachingCard()) {
			PCCustomerMetadata cardMeta = (PCCustomerMetadata) data;
			if (appUser != null && cardMeta != null) {
				appUser.setMaskedCardNumber(cardMeta.getMaskedCardNumber());
				setAccount(cardMeta.getMaskedCardNumber(), PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
			}

			if (cardMeta != null && callingServiceInfo != null) {
				callingServiceInfo.setMaskedCardNumber(cardMeta.getMaskedCardNumber());
			}

			// FIXME: Pass the correct method or type for this metric event
			PCPostPesaBillMetrics.postCardAttachmentEvent( this, getCountry(appUser), "" , true);
			Fragment frag = pcUserAccountFragment;
			updatePaymentInfo(frag, PCBillPaymentData.PCPaymentType.PAYMENT_CARD);
			getSupportFragmentManager().popBackStack();
		}
		if (this.currentServiceType == PCPesabusClient.PCServiceType.COUNTRY_PROFILE && data instanceof PCCountryProfile) {
			//hack to support calling into products
			List<String> products = ((PCCountryProfile) data).getProducts();
			this.countryProfile = (PCCountryProfile) data;
			String country = appUser.getCountry();
			if (appUser.getBaseCurrency() != null && !StringUtils.isEmpty(appUser.getBaseCurrency().getCountrySymbol())) {
				country = appUser.getBaseCurrency().getCountrySymbol();
			}

			//TODO: removing calling card -currently stoped supporting it.
			if (transFrag != null) {
				transFrag.onDataLoadFinished(data);
			}

			if (!gettingProfileForRepeatTranscation && supportedCountryAndProductFragment != null) {
				Log.e(CLAZZZ,"GettingProfile for RepeatTransaction");
				if (((Fragment) supportedCountryAndProductFragment).getActivity() == this) {
					supportedCountryAndProductFragment.onDataLoadFinished(data);
				}
			} else if (gettingProfileForRepeatTranscation && this.repeatTransactionFragment != null && ((PCCountryProfile) data).getProducts() != null) {
				this.repeatTransactionFragment.onCountryProfileDataLoadFinished();
			}
		}

		if (this.currentServiceType == PCPesabusClient.PCServiceType.MERCHANTS_PROFILE && data instanceof PCMerchantsProfile) {
			FragmentManager fm = getSupportFragmentManager();
			Fragment frag = fm.findFragmentByTag(PCPesabusClient.PCServiceType.SELL.name());
			if (frag != null && frag instanceof PCProductPaymentFragment) {
				((PCProductPaymentFragment) frag).updateMerchantsProfile((PCMerchantsProfile) data);
			}
		}

		if (this.isCurrentServiceTypeTransaction()) {
			if (data instanceof PCCallingTransaction && ((PCCallingTransaction) data).getReceiver() != null && this.appUser != null && ((PCCallingTransaction) data).getReceiver().getPhoneNumber().equals(this.appUser.getPhoneNumber())) {
				PCCallingTransaction transactionData = (PCCallingTransaction) data;
				updateCallingDetails(transactionData.getServiceInfo());
			}
			//set the current transaction to SUCCESS
			pcBillPaymentTransaction.retrieveServiceInfo().setTransactionStatus(PCTransactionStatus.SUCCESS);
			//post metrics transaction failed
			PCPostPesaBillMetrics.postPurchaseMetric(this, pcBillPaymentTransaction, true);
			this.showTransactionReceipt((PCTransaction) data);
		}

		if ((this.currentServiceTypeAuth == PCPesabusClient.PCServiceType.IS_AUTHENTICATED || this.currentServiceTypeAuth == PCPesabusClient.PCServiceType.USER_INFO) && data instanceof PCUser) {
			this.appUser = (PCSpecialUser) data;
			if (this.currentServiceTypeAuth == PCPesabusClient.PCServiceType.IS_AUTHENTICATED) {
				generateCallingDetails();
			}
		}

		if (data != null && this.currentServiceType == PCPesabusClient.PCServiceType.PING && data instanceof PCPingResults) {
			pingResults = (PCPingResults) data;
		}

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	private boolean isAttachingCard() {
		return currentServiceType == PCPesabusClient.PCServiceType.PAYMENT_PROFILE ||
				currentServiceType == PCPesabusClient.PCServiceType.REPLACE_CARD;
	}

	public void setMobMoneyAccountToUser(String account) {
		this.getAppUser().setHasMobileMoneyAccount(true);
		this.setAccount(account, PCBillPaymentData.PCPaymentType.MOBILE_MONEY);
	}

	public void setAccount(String account, PCBillPaymentData.PCPaymentType pymtType) {
		PCPaymentInfo paymentInfo = this.getAppUser().getPaymentInfo();
		PCCustomerMetadata metadata = null;
		if (paymentInfo != null) {
			metadata = paymentInfo.getDefaultSource();
			if (metadata == null) {
				metadata = new PCCustomerMetadata();
			}
			if (pymtType == PCBillPaymentData.PCPaymentType.PAYMENT_CARD) {
				metadata.setMaskedCardNumber(account);
			} else if (pymtType == PCBillPaymentData.PCPaymentType.MOBILE_MONEY) {
				metadata.setPhoneNumber(account);
			}
			this.getAppUser().setCurrentUserAccount(account);
			paymentInfo.setPaymentType(pymtType);
			ArrayList<PCCustomerMetadata> metadatas = new ArrayList<>();
			metadatas.add(metadata);
			paymentInfo.setMobileAccounts(metadatas);
			paymentInfo.setDefaultSource(metadata);
		}
		this.getAppUser().setPaymentInfo(paymentInfo);
		this.saveUserDetails(this.getAppUser(), true);
	}

	protected void generateCallingDetails() {
		PCCallingServiceInfo callingInfo = new PCCallingServiceInfo();
		if (this.appUser != null) {
			callingInfo.setCardId(this.appUser.getCallingCardId());
			callingInfo.setBalance(this.appUser.getCallingBalance());
			callingInfo.setMaskedCardNumber(this.appUser.getCallingPaymentCard());
			updateCallingDetails(callingInfo);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		refleshUserAccountFragment();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.e("ACTIVITY_DESTORIED", "DESTORIED");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		adapter = new PCViewPagerAdapter(getSupportFragmentManager());
		outState.putInt("tabsCount", adapter.getCount());
		outState.putStringArray("titles", adapter.getTitles().toArray(new String[0]));
	}

	private void updateCallingDetails(PCCallingServiceInfo data) {
		this.setCallingServiceInfo(data);
		if (pcUserAccountFragment != null) {
			pcUserAccountFragment.setCallingCreditDetails(callingServiceInfo);
		}
	}

	public void setTransactionFragment(PConRequiredDataLoaded frag) {
		this.transFrag = frag;
	}

	private boolean isCurrentServiceTypeTransaction() {
		PCPesabusClient.PCServiceType serviceType = this.currentServiceType;
		return serviceType == PCPesabusClient.PCServiceType.PAY_ELECTRICITY ||
				serviceType == PCPesabusClient.PCServiceType.SEND_AIRTIME ||
				serviceType == PCPesabusClient.PCServiceType.PAY_TV ||
				serviceType == PCPesabusClient.PCServiceType.CALLING_REFIL ||
				serviceType == PCPesabusClient.PCServiceType.PAY_INTERNET ||
				serviceType == PCPesabusClient.PCServiceType.PAY_TUITION ||
				serviceType == PCPesabusClient.PCServiceType.MONEY_TRANSFER ||
				serviceType == PCPesabusClient.PCServiceType.PAY_WATER ||
				serviceType == PCPesabusClient.PCServiceType.TICKETS ||
				serviceType == PCPesabusClient.PCServiceType.EVENTS ||
				serviceType == PCPesabusClient.PCServiceType.SELL ||
				serviceType == PCPesabusClient.PCServiceType.AGENT_TRANSACTION||
				serviceType == PCPesabusClient.PCServiceType.INSURANCE;
	}

	public void saveTransaction(PCTransactionableFragment fragment) {
		if (fragment == null) {
			return;
		}

		pcBillPaymentTransaction = fragment.getTransactionData();
		PCPesabusClient.PCServiceType serviceType = fragment.getServiceType();
		try {
			pcBillPaymentTransaction.getSender().setPcUserDeviceDetails(PCUserDeviceDetailUtil.getUserDeviceDetails(this));
			PCBaseController transactionController;
			final PCOperatorRequest operatorRequest = getPcOperatorRequest(countrySendingTo);
				if (operatorRequest.getCompanyId().equalsIgnoreCase("embracesolar")) {
					transactionController = (PCBaseController)
							PCControllerFactory.constructController(
									PCControllerFactory.PCControllerType.AGENT_TRANSACTION_CONTROLLER, this);
				} else {
					transactionController = (PCTransactionController)
							PCControllerFactory.constructController(
									PCControllerFactory.PCControllerType.TRANSACTION_CONTROLLER, this);
				}

				this.currentServiceType = serviceType;
				if (transactionController != null) {
					transactionController.setActivity(this);
					transactionController.setServiceType(serviceType);
					transactionController.execute(pcBillPaymentTransaction);

			}

		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle the process because [" + exc.getMessage() + "]");
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError)exc;
				error.setNeedToGoBack(false);
			}
			else
				error.setMessage(exc.getMessage());
			this.presentError(error, "Error ");
		}
	}

	public List<PCUser> getAllReceivers() {
		return pcAllReceivers;
	}


	/**
	 * Show transaction receipt method changed from void to boolean for helping to perform unit test.
	 *
	 * @param transaction the transaction
	 * @return the boolean
	 */
	public boolean showTransactionReceipt(PCTransaction transaction) {
		boolean msg = false;
		if (transaction == null) {
			return msg;
		}

		//build receipt
		PCReceiptFragment frag = new PCReceiptFragment();
		frag.setTransaction(transaction);
		//add the transanction to the list of activities
		if (pcBillPaymentTransaction != null) {
			transaction.setBaseCurrency(pcBillPaymentTransaction.getBaseCurrency());
			adapter = new PCViewPagerAdapter(getSupportFragmentManager());
			//check if we have discount and show it to the user
			Double discount = pcBillPaymentTransaction.getAppliedDiscount();
			if (discount != null && discount > 0) {
				pcBillPaymentTransaction.setTotalUsd(pcBillPaymentTransaction.getTotalUsd() - discount);

			} else {
				pcBillPaymentTransaction.setTotalUsd(pcBillPaymentTransaction.getTotalUsd());

			}
			//TODO:save ticket information

		}
		FragmentManager fm = getSupportFragmentManager();
		if (transaction instanceof PCAgentWalletTransaction) {
			fm.beginTransaction()
					.replace(R.id.grid_container, frag)
					.addToBackStack(null)
					.commit();
		} else {
			fm.beginTransaction()
					.replace(R.id.grid_container_country, frag)
					.addToBackStack(null)
					.commit();
		}
		msg = true;
		return msg;
	}

	protected void showFailedTransaction(PCData transaction) {
		if (transaction == null) {
			return;
		}

		PCFailedTransactionFragment frag = new PCFailedTransactionFragment();
		frag.setTransaction(transaction);
		FragmentManager fm = getSupportFragmentManager();

		if (transaction instanceof PCAgentWalletTransaction) {
			fm.beginTransaction()
					.replace(R.id.grid_container, frag)
					.addToBackStack(null)
					.commit();
		} else {
			fm.beginTransaction()
					.replace(R.id.grid_container_country, frag)
					.addToBackStack(null)
					.commit();

		}
	}

	public void goBackToProductsGridView() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		while (fragmentManager.getBackStackEntryCount() != 0) {
			fragmentManager.popBackStackImmediate();
		}
	}

	public PCCardInfo getCardInfo() {
		return cardInfo;
	}

	public void setCardInfo(PCCardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {
		try {
			PCUserActivitiesFragment activitiesFragment = (PCUserActivitiesFragment) adapter.getItem(1);
			if (data != null && data.size() > 0) {
				Object objTest = data.peek();
				if (currentServiceType == PCPesabusClient.PCServiceType.GET_ALL_RECEIVERS && objTest instanceof PCUser) {
					if (data != null) {
						Log.d("Receivers count ", "" + data.size());
						pcAllReceivers = new ArrayList<>((Queue<PCUser>) data);

					}
				} else {
					activitiesFragment.onCollectionDataLoadFinished(data);

				}
			} else if (data != null && data.size() == 0) {
				activitiesFragment.setEmptyView();
			}
		} catch (Throwable error) {
			if (error.getMessage() != null && error.getMessage().length() > 0) {
				Log.d(CLAZZZ, error.getMessage());
			}
		}
	}



}