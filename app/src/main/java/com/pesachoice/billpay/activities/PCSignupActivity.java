/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCSignupController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCKeychainWrapper;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;

import org.springframework.util.StringUtils;

import java.util.Queue;

/**
 * Class used for signing up new customers
 *
 * @author Odilon Senyana
 *
 */
public class PCSignupActivity extends PCBaseActivity
		implements AdapterView.OnItemSelectedListener, View.OnFocusChangeListener, TextView.OnEditorActionListener {

	private final static String CLAZZZ = PCSignupActivity.class.getName();
	private ProgressDialog progressDialog;
	private String selectedCountryName;
	private PCSignupActivity activity;
	private PCPesabusClient.PCServiceType currentServiceType;
	private String fullName, email, phone, password, firstName, lastName;
	private EditText firstNameEditText, lastNameEditText, phoneText, passwordText;
    private EditText emailText;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_activity_signup);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		setSupportActionBar(toolbar);
		phoneText = (EditText) findViewById(R.id.phone);
		countryPicker1 = (TextView) findViewById(R.id.countryPicker1);
		//set the country code by default based on the user's sim card or wifi they're connected
		selectedCountry = null;
		activity = this;
        String countryCodeValue = this.getUserCountryCode(this);
		selectedCountry = countryCodeValue != null ? countryCodeMap.get(countryCodeValue) : null;
		if (!StringUtils.isEmpty(selectedCountry)) {
			countryPicker1.setText(selectedCountry.substring(selectedCountry.indexOf("+")));
		} else {
			showCountriesCode(activity);
		}
		
		countryPicker1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showCountriesCode(activity);
			}
		});

		EditText phoneText = (EditText) findViewById(R.id.phone);
		phoneText.setOnFocusChangeListener(this);

		EditText passwordText = (EditText) findViewById(R.id.password);
		passwordText.setOnFocusChangeListener(this);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		//set privacy and tos links
		TextView tosAndPrivacyText = (TextView) findViewById(R.id.read_tos_and_privacy);
		tosAndPrivacyText.setMovementMethod(LinkMovementMethod.getInstance());
		// Listen for keyboard dismissal
		findViewById(R.id.signup_container).setOnClickListener(new DismissKeyboardListener(this));

        /*
         * TODO: incomplete
         */
		if (savedInstanceState == null) {
			//TODO: Missing implementation
		}
	}

	public void onCreateAccount(View view) {

		// Get input fields
		firstNameEditText = (EditText) findViewById(R.id.firstName);
		lastNameEditText = (EditText) findViewById(R.id.lastName);
		emailText = (EditText) findViewById(R.id.email);
		phoneText = (EditText) findViewById(R.id.phone);
		passwordText = (EditText) findViewById(R.id.password);

		firstName = firstNameEditText.getText().toString();
		lastName = lastNameEditText.getText().toString();
		email = emailText.getText().toString();
		email = trimUserInput(email);
		String cntrPicker1CountryCode = countryPicker1.getText().toString();
		cntrPicker1CountryCode = trimUserInput(cntrPicker1CountryCode);
		phone = cntrPicker1CountryCode + phoneText.getText().toString();
		/**
		 * Fixing regex pattern
		 *
		 */
		phone = phone.replaceAll("[^0-9]","");
		phone = trimUserInput(phone);
		password = passwordText.getText().toString();
		password = trimUserInput(password);

		ProgressDialog progress = null;

		Log.d(CLAZZZ, "FullName: " + fullName);
		Log.d(CLAZZZ, "email: " + email);
		Log.d(CLAZZZ, "phone: " + phone);

		try {
		    email = email != null ? email.trim() : "";
            phone = phone != null ? phone.trim() : "";

			//validate inputs before sign up
			if (this.validCustomerInfo(firstName, lastName, email, phone, password)) {
			    password = password.trim();

				// Encrypt the password
				password = PCKeychainWrapper.securedSHA256DigestHash(password, email);
				phone = PCGeneralUtils.cleanPhoneString(phone);

				//prepare user request to signup
				PCUser user = new PCUser();
				user.setEmail(email);
				user.setPwd(password.trim());
				user.setFirstName(firstName.trim());
				user.setLastName(lastName.trim());
				user.setPhoneNumber(phone.trim());

				//user country name, we're using '+' sign to separate both country code and country name
				if (!StringUtils.isEmpty(selectedCountry)) {
					String country = selectedCountry.substring(0, selectedCountry.indexOf('+')).trim();
					user.setCountry(country);
					selectedCountryName = country;
				}
                   
				fullName = StringUtils.capitalize(firstName + " " + lastName);
				user.setUserName(fullName);
				// make service call to sign up user by getting the appropriate controller
				PCSignupController signupController = (PCSignupController)
						PCControllerFactory.constructController(
								PCControllerFactory.PCControllerType.SIGN_UP_CONTROLLER, this);
				if (signupController != null) {
					signupController.setActivity(this);
					signupController.setServiceType(PCPesabusClient.PCServiceType.SIGN_UP);
					signupController.execute(user);
				}
			}
		} catch (PCGenericError error) {
			this.presentError(error, this.getResources().getString(R.string.sign_error_title));
		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle Signup process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if(exc instanceof PCGenericError) {
				error = (PCGenericError)exc;
			}
			else
				error.setMessage(exc.getMessage());
			presentError(error, "Error while Signing up");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	/**
	 * Validate customer user inputs
	 * @param firstName
	 *          customer firstname
	 * @param lastName
	 *          customer lastname
	 * @param email
	 *          customer email
	 * @param phone
	 *          customer phone number
	 * @param password
	 *          customer password
	 * @throws PCGenericError
	 */

	public boolean validCustomerInfo(String firstName, String lastName, String email, String phone, String password) throws PCGenericError  {
		boolean isFormValid = false;
		String countryCode = countryPicker1 != null ? (String) countryPicker1.getText() : "";

		// First check for any empty fields or in wrong format
		if (!PCGeneralUtils.isValidPhone(phone, countryCode)) {
			phoneText.setError("Invalid Phone Number");
			isFormValid = false;
		} else if (firstName == null || firstName.length() == 0) {
			firstNameEditText.setError("First name is required");
			//throw error;
			isFormValid = false;
		} else if (lastName == null || lastName.length() == 0) {
			lastNameEditText.setError("Last name is required");
			isFormValid = false;
		}
		// Is Email valid
		else if (!PCGeneralUtils.isValidEmail(email)) {
			emailText.setError("Invalid Email Address");
			isFormValid = false;
		} else if (password == null || password.length() == 0) {
			passwordText.setError("password is required");
			isFormValid = false;
		} else {
			isFormValid = true;
		}
		return isFormValid;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		EditText phoneText = (EditText) findViewById(R.id.phone);

		if (hasFocus && phoneText == v) {

		}
	}

	@Override
	public void presentError(PCGenericError error, String title) {
		String message = error.getMessage();
		if (isFinishing() || message == null) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setTitle(title);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Log.d(CLAZZZ, "Clicked on OK on Alert box");
			}
		});

		Dialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onEditorAction(TextView v, int keyCode, KeyEvent event) {
		if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
				(keyCode == KeyEvent.KEYCODE_ENTER)) {
			// hide virtual keyboard
			InputMethodManager imm = (InputMethodManager) this.getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			return true;
		}
		return false;
	}

	@Override
	public void onTaskStarted() {
		progressDialog = ProgressDialog.show(this, "Creating User Account", "Please wait while we create your account", true);
	}

	@Override
	public void onTaskCompleted(PCData data) {
		Boolean success = false;
		if (data != null && data instanceof PCUser) {

			final PCUser user = (PCUser) data;
			if (StringUtils.isEmpty(user.getCountry())) {
				user.setCountry(selectedCountryName);
			}
			if (user.getErrorMessage() != null && !"".equals(user.getErrorMessage())) { // service call failed
				Log.e(CLAZZZ, "An error occured: [" + user.getErrorMessage() + "]");
				PCGenericError error = new PCGenericError();
				error.setMessage(user.getErrorMessage());
				this.presentError(error, this.getResources().getString(R.string.sign_error_title));
			} else { // service call succeeded
              /*
               * TODO: handle success
               */
				Intent intent = new Intent(this, PCVerifyPhoneActivity.class);
				intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
				startActivityForResult(intent, VERIFIY_ACTIVITY_REQUEST_CODE);
				success = true;
			}
			PCPostPesaBillMetrics.postSignUpMetric(this, selectedCountryName, "ANDROID_PHONE", success);
		} else {
			// Some error occurred and response data is missinag
			Log.e(CLAZZZ, "Application level error");
            /*
             * TODO: missing implementation on what the user should see in this case
             */
			success = false;
			PCPostPesaBillMetrics.postSignUpMetric(this, selectedCountryName, "ANDROID_PHONE", success);
		}
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {
		//TODO
	}
}
