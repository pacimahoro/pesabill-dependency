package com.pesachoice.billpay.activities.helpers;

import android.net.Uri;

/**
 * Created by desire.aheza on 2/5/2017.
 */

public interface PCPhotoUpload {
    void onStartUploadingPhoto();
    void onFinishedPhotoUploading(Uri uri);
}
