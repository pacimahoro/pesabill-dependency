package com.pesachoice.billpay.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.PCUserErrable;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCReferralPromotionController;
import com.pesachoice.billpay.fragments.PCContactsAdapter;
import com.pesachoice.billpay.fragments.PCListAllContactsAsync;
import com.pesachoice.billpay.fragments.PCRecommendedFriendsFragment;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCReferralData;
import com.pesachoice.billpay.model.PCReferralRequest;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Activity to hold UI we're using to recommend friends
 */
public class PCRecommendFriendsActivity extends AppCompatActivity implements PCOnPhoneContactLoad, PCMultiSelectRecyclerViewAdapter.ViewHolder.ClickListener, PCUserErrable, PCAsyncListenerManyItem {

	private final static String clazzz = PCRecommendFriendsActivity.class.getName();
	//progress bar
	private ProgressBar mProgressBar;
	private PCContactsAdapter mContactAdapter;
	private ListView mContactsList;
	private RecyclerView mRecyclerView;
	private PCMultiSelectRecyclerViewAdapter mAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	private PCUser appUser = null;
	private final int MY_PERMISSIONS_REQUEST_CONTACT = 1007;
	private PCRecommendFriendsActivity activity;
	private ImageView sendReferral;
	private TextView emptyView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("PCRecommend.class", "started Executing");
		setContentView(R.layout.pc_activity_recommend_friends);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		setSupportActionBar(toolbar);
		sendReferral = (ImageView) findViewById(R.id.send_referral);
		Intent intent = getIntent();
		if (intent != null) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
				this.appUser = (PCDetailedUser) objBundle;
			}
			activity = this;
			mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
			sendReferral.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						if (mAdapter != null) {
							StringBuilder builder = new StringBuilder();
							//build referral request
							PCReferralRequest pcReferralRequest = new PCReferralRequest();
							pcReferralRequest.setUser(appUser);
							pcReferralRequest.setReferrals(mAdapter.getSelectedContacts());
							PCReferralPromotionController pcReferralPromotionController = (PCReferralPromotionController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.REFERRAL_PROMOTION_CONTROLLER, activity);
							if (pcReferralPromotionController != null) {
								pcReferralPromotionController.setServiceType(PCPesabusClient.PCServiceType.REFERRAL_USERS);
								pcReferralPromotionController.setRequest(pcReferralRequest);
								pcReferralPromotionController.execute(pcReferralRequest);
								mAdapter.clearSelection();
								enableDisableSendButton();
							}
						}
					} catch (Throwable exc) {
						Log.e(clazzz, "Could not handle the Friends Referral process because [" + exc.getMessage() + "]");
						if (mProgressBar != null) {
							mProgressBar.setVisibility(View.GONE);
						}
						PCGenericError error = new PCGenericError();
						if (exc instanceof PCGenericError) {
							error = (PCGenericError) exc;

						} else {
							error.setMessage(exc.getMessage());
						}
						presentError(error, "Friends Referral Error");
					}
				}
			});

			//setup recycler view
			mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
			emptyView = (TextView) findViewById(R.id.empty_view);
			mRecyclerView.setHasFixedSize(true);
			mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
			mRecyclerView.addItemDecoration(new PCDividerItemDecoration(this));
			requestCallingPermission(Manifest.permission.READ_CONTACTS);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_CONTACT: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					// permission was granted, yay! Do the
					// contacts-related task you need to do.
					loadContact();

				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.
					if (emptyView != null) {
						emptyView.setVisibility(View.VISIBLE);
					}
					if (mRecyclerView != null) {
						mRecyclerView.setVisibility(View.GONE);
					}
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}

	private void requestCallingPermission(String needIntent) {
		// Here, thisActivity is the current activity
		if (ContextCompat.checkSelfPermission(this,
				needIntent)
				!= PackageManager.PERMISSION_GRANTED) {


			ActivityCompat.requestPermissions(this,
					new String[]{needIntent},
					MY_PERMISSIONS_REQUEST_CONTACT);

		} else {
			//load contacts
			loadContact();
		}
	}

	private void loadContact() {
		mAdapter = new PCMultiSelectRecyclerViewAdapter(PCRecommendFriendsActivity.this, new ArrayList<PCContactInfo>(), this);
		mRecyclerView.setAdapter(mAdapter);
		//load contacts
		PCListAllContactsAsync listViewContactsLoader = new PCListAllContactsAsync(this);
		listViewContactsLoader.execute();
	}

	private void setTitleInRecommendActivity() {

	}

	@Override
	public void onContactLoadStarted() {
		//show progress bar we're loading data
		mProgressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onContactLoadFinished(List<PCContactInfo> contacts) {
		mProgressBar.setVisibility(View.GONE);
		mAdapter.addAll(contacts);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onItemClicked(int position) {
		//currently we only allow customers to invite 20 contacts at time
		if (mAdapter.getSelectedItemCount() <= 20) {
			toggleSelection(position);
		} else {
			Toast.makeText(this, "You can only invite 20 contacts at time", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onItemLongClicked(int position) {
		toggleSelection(position);
		return true;

	}

	private void toggleSelection(int position) {
		mAdapter.toggleSelection(position);
		//used to enable or disable the fab button according to the selected number of contacts
		enableDisableSendButton();
	}

	private void enableDisableSendButton() {
		if (mAdapter.getSelectedItemCount() != 0) {
			//enable fab button
			sendReferral.setImageResource(R.drawable.ic_action_send_referral);
			sendReferral.setClickable(true);
		} else {
			sendReferral.setImageResource(R.drawable.ic_action_send_referral_tranp);
			sendReferral.setClickable(false);
		}
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {

	}

	@Override
	public void onTaskStarted() {
		//show progress bar we're loading data
		mProgressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onTaskCompleted(PCData data) {
		if (mProgressBar != null) {
			mProgressBar.setVisibility(View.GONE);
		}

		if (data != null && data.getErrorMessage() != null && data.getErrorMessage().length() > 0) {

			PCGenericError error = new PCGenericError();
			error.setMessage(data.getErrorMessage());
			this.presentError(error, "Error in Referring new users service");
			return;
		} else if (data != null) {
			//set populate our dialog
			PCReferralData pcReferralData = (PCReferralData) data;
			PCRecommendedFriendsFragment dialogFragment = new PCRecommendedFriendsFragment();
			dialogFragment.setPcReferralData(pcReferralData);
			dialogFragment.setAllContacts(mAdapter.getItems());
			FragmentManager fm = this.getSupportFragmentManager();
			dialogFragment.show(fm, "Transaction Confirmation Dialog");
		}
	}

	@Override
	public void presentError(PCGenericError error, String title) {
		String message = error.getMessage();
		if (isFinishing() || message == null) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setTitle(title);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Log.d(clazzz, "Clicked on OK on Alert box");
			}
		});

		Dialog alert = builder.create();
		alert.show();
	}
}
