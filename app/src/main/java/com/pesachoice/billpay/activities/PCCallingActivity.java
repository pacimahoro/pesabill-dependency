package com.pesachoice.billpay.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * This activity will being invoked to open up credit card scanner
 */
public class PCCallingActivity extends AppCompatActivity {

    private static final String TAG = PCCallingActivity.class.getSimpleName();

    private static final int BarCodeScannerViewControllerUserCanceledErrorCode = 99991;

    String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uuid = getIntent().getStringExtra("uuid");
        new IntentIntegrator(this).setOrientationLocked(false).setCaptureActivity(PCCustomScannerActivity.class).initiateScan();
    }



    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            if (scanResult != null) {
                // handle scan result
                Log.i(TAG, "Text from Barcode Scanner: " + scanResult.getContents());
                getIntent().putExtra("data", scanResult.getContents());
                getIntent().putExtra("uuid", uuid);
            }
        }
        else if (resultCode == RESULT_CANCELED) {
            getIntent().putExtra("error", "User canceled");
            getIntent().putExtra("error_code", BarCodeScannerViewControllerUserCanceledErrorCode);
        }
        else
        {
            getIntent().putExtra("error", "ERROR");
            getIntent().putExtra("error_code", BarCodeScannerViewControllerUserCanceledErrorCode);
        }

        setResult(resultCode, this.getIntent());

        this.finish();
    }

}
