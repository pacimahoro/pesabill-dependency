/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.activities;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCVerifyController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCPreferenceHelper;

import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Queue;

/**
 * Used to handle new pcUser phone verification feature.
 *
 * @author Pacifique Mahoro
 */
public class PCVerifyPhoneActivity extends PCBaseActivity {
	private final static String CLAZZZ = PCVerifyPhoneActivity.class.getName();

	private PCUser pcUser = new PCUser();
	long verificationCode;
	protected ProgressDialog progressDialog;
	private EditText accecodeEdit, kenyaIdNumberText, kenyaDOBText;
	private String idNumberText, dobText, accessCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_activity_verify_phone);

		Intent intent = getIntent();
		PCUser user = (PCUser) intent.getExtras().get(PCPesachoiceConstant.USER_INTENT_EXTRA);
		this.setUser(user);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	private void setUser(PCUser user) {
		if (user == null || StringUtils.isEmpty(user.getFirstName()) || StringUtils.isEmpty(user.getLastName())) {
			return;
		}

		TextView nameText = (TextView) findViewById(R.id.name);
		EditText phoneText = (EditText) findViewById(R.id.phone);
		String initials = user.getFirstName().substring(0, 1) + user.getLastName().substring(0, 1);
		initials = initials.toUpperCase();
		nameText.setText(initials);
		phoneText.setText(user.getPhoneNumber());
		this.pcUser = user;

		Button doneBtn = (Button) findViewById(R.id.done);
		doneBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onVerifyPhoneAction(v);
			}
		});

		TextView enterCodeBtn = (TextView) findViewById(R.id.enterCode);
		enterCodeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				presentVerificationDialog(pcUser);
			}
		});
	}

	public void onVerifyPhoneAction(View view) {
		try {
			// Get input fields
			TextView phoneText = (TextView) findViewById(R.id.phone);

			String phone = phoneText.getText().toString();

			if (phone.length() == 0) {
				PCGenericError error = new PCGenericError();
				error.setMessage(this.getResources().getString(R.string.verify_phone_phone_error));

				this.presentError(error, "Error");
				return;
			}
			//set the new phone number if the pcUser has update it
			if (!phone.equals(pcUser.getPhoneNumber())) {
				pcUser.setPhoneNumber(phone);
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_PHONE_NUMBER, pcUser.getPhoneNumber());
			}

			PCRequest request = new PCRequest();
			request.setUser(pcUser);
			currentServiceType = PCPesabusClient.PCServiceType.VERIFY_PHONE;

			PCVerifyController controller = (PCVerifyController) PCControllerFactory.constructController(
					PCControllerFactory.PCControllerType.VERIFY_CONTROLLER, this);
			if (controller != null) {
				controller.setServiceType(currentServiceType);
				controller.setActivity(this);
				controller.setRequest(request);
				controller.execute(request);
			}
		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle UserVerification process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError)exc;
				error.setNeedToGoBack(false);
			}
			else
				error.setMessage(exc.getMessage());
			presentError(error, "Error while verifying user");
		}
	}

	public void presentVerificationDialog(PCData data) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(this.getResources().getString(R.string.verify_phone_complete_subtitle))
				.setTitle(this.getResources().getString(R.string.verify_phone_complete_title));

		String country = null;
		if (pcUser != null) {
			country = pcUser.getCountry();
		}

		// Get the layout inflater
		LayoutInflater inflater = this.getLayoutInflater();
		View view = inflater.inflate(R.layout.pc_activity_verify_phone_complete, null);

		toggleKenyaVerification(view, country);

		builder.setView(view);

		final PCVerifyPhoneActivity verifyActivity = this;

		builder.setPositiveButton(R.string.ok, null);

		builder.setNegativeButton(R.string.cancel, null);

		final Dialog alert = builder.create();
		alert.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(final DialogInterface dialog) {

				Button buttonOK = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
				Button buttonNO = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
				buttonOK.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						boolean hasNoError = validateInputsAndSetValue((Dialog) dialog);

						if (hasNoError) {
							Log.d(CLAZZZ, "accesscode: " + accessCode);

							verifyActivity.sendVerificationCode();
							dialog.dismiss();
						}
					}
				});
				buttonNO.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						//Dismiss once everything is OK.
						dialog.dismiss();
					}
				});
			}
		});
		alert.show();
	}

	public boolean validateInputsAndSetValue(Dialog dialog) {
		accecodeEdit = (EditText) dialog.findViewById(R.id.accessCode);
		EditText idNumber = (EditText) dialog.findViewById(R.id.idNumber);
		EditText dob = (EditText) dialog.findViewById(R.id.dob);

		boolean hasNoError = true;
		if (accecodeEdit != null && StringUtils.isEmpty(accecodeEdit.getText().toString())) {
			hasNoError = false;
			accecodeEdit.setError("access code is required");
		} else {
			accessCode = trimUserInput(accecodeEdit.getText().toString());

			try {
				verificationCode = NumberUtils.parseNumber(accessCode, Long.class);
			} catch (Exception e) {
				hasNoError = false;
				if (accecodeEdit != null) {
					accecodeEdit.setError("Invalid Verification Code");
				}
			}
		}
		if ("kenya".equalsIgnoreCase(pcUser.getCountry())) {
			if (idNumber != null && StringUtils.isEmpty(idNumber.getText().toString())) {
				idNumber.setError("ID is required");
				hasNoError = false;
			} else {
				idNumberText = idNumber.getText().toString();
			}
			if (dob != null && StringUtils.isEmpty(dob.getText().toString())) {
				dob.setError("Date of Birth is required");
				hasNoError = false;
			} else {
				dobText = dob.getText().toString();
			}
		}
		return hasNoError;
	}

	private void setUpAdditionalFieldsForKenya(View view) {
		if (view != null) {
			kenyaIdNumberText = (EditText) view.findViewById(R.id.idNumber);
			kenyaDOBText = (EditText) view.findViewById(R.id.dob);

			if (kenyaDOBText != null && kenyaIdNumberText != null) {

				kenyaIdNumberText.setVisibility(View.VISIBLE);
				kenyaDOBText.setVisibility(View.VISIBLE);

				//set up date picker
				final Calendar myCalendar = Calendar.getInstance();

				final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
										  int dayOfMonth) {
						// TODO Auto-generated method stub
						myCalendar.set(Calendar.YEAR, year);
						myCalendar.set(Calendar.MONTH, monthOfYear);
						myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
						updateLabel(kenyaDOBText, myCalendar);
					}

				};

				kenyaDOBText.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new DatePickerDialog(PCVerifyPhoneActivity.this, date, myCalendar
								.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
								myCalendar.get(Calendar.DAY_OF_MONTH)).show();
					}
				});
			}
		}
	}

	private void updateLabel(EditText editText, Calendar myCalendar) {
		try {
			String myFormat = "yyyy-MM-dd";
			SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
			if (editText != null & myCalendar != null) {
				editText.setText(sdf.format(myCalendar.getTime()));
				editText.setError(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void toggleKenyaVerification(View view, String countrySelected) {

		if ("Kenya".equalsIgnoreCase(countrySelected)) {//if kenya is selected show id and dob fields
			setUpAdditionalFieldsForKenya(view);
		} else {
			if (kenyaDOBText != null) {
				kenyaDOBText.setVisibility(View.GONE);
			}
			if (kenyaIdNumberText != null) {
				kenyaIdNumberText.setVisibility(View.GONE);
			}
		}

	}

	public void sendVerificationCode() {
		try {
			PCRequest request = getPcRequest();
			if (request == null) {
				return;
			}

			PCVerifyController controller = (PCVerifyController) PCControllerFactory.constructController(
					PCControllerFactory.PCControllerType.VERIFY_CONTROLLER, this);
			if (controller != null) {
				controller.setServiceType(currentServiceType);
				controller.setActivity(this);
				controller.setRequest(request);
				controller.execute(request);
			}
		} catch (Throwable exc) {
			Log.e(CLAZZZ, "Could not handle User verification process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if(exc instanceof PCGenericError){
				error = (PCGenericError)exc;
				error.setNeedToGoBack(false);
			}
			else
				error.setMessage(exc.getMessage());
			presentError(error, "Error Verifying User");
		}
	}

	@Nullable
	private PCRequest getPcRequest() {

		this.currentServiceType = PCPesabusClient.PCServiceType.COMPLETE_REGISTRATION;
		pcUser.setAccessCode(verificationCode);
		PCRequest request = new PCRequest();
		request.setUser(pcUser);
		request.setDetailed(true);
		request.setSpecial(true);
		if (pcUser != null && "Kenya".equalsIgnoreCase(pcUser.getCountry())) {
			request.setIdNumber(idNumberText);
			request.setDob(dobText);
		}
		return request;
	}

	private void showErrorInvalidVerificationCode(String message, String string) {
		PCGenericError error = new PCGenericError();
		error.setMessage(message);
		this.presentError(error, string);
	}

	@Override
	public void onTaskStarted() {
		super.onTaskStarted();
		if (currentServiceType == PCPesabusClient.PCServiceType.VERIFY_PHONE) {
			progressDialog = ProgressDialog.show(this,
					"Account Verification", "Please wait while we send you an SMS with a verification code", true);
		}
		if (currentServiceType == PCPesabusClient.PCServiceType.COMPLETE_REGISTRATION) {
			progressDialog = ProgressDialog.show(this,
					"Account Verification", "Please wait while we verify your code", true);
		}
	}

	@Override
	public void onTaskCompleted(PCData data) {
		super.onTaskCompleted(data);
		if (currentServiceType == PCPesabusClient.PCServiceType.VERIFY_PHONE) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}

			this.presentVerificationDialog(data);
		} else if (this.currentServiceType == PCPesabusClient.PCServiceType.COMPLETE_REGISTRATION) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			if (data != null && data instanceof PCSpecialUser) {
				if (data.getErrorMessage() != null && !"".equals(data.getErrorMessage())) {
					Log.e(CLAZZZ, "An error occured: [" + data.getErrorMessage() + "]");
					PCGenericError error = new PCGenericError();
					error.setMessage(data.getErrorMessage());
					this.presentError(error, this.getResources().getString(R.string.verify_phone_error_title));
				} else {
					PCSpecialUser userResponse = (PCSpecialUser) data;
					/*
					 * start pcUser tracking feature
					 */
					String userTrakingKey = PCGeneralUtils.createTrackingUserKey(userResponse, pingResults.getApiKey());
					this.saveUserTrackingKey(userTrakingKey);
					this.saveUserDetails(userResponse, true);
					Intent intent = new Intent(this, PCMainTabActivity.class);
					intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, userResponse);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			}
		}
	}

	/**
	 * This activity may be opened either from loginActivity or signupActivity
	 * As such, when the pcUser clicks on the up/back button,
	 * close this activity and present the activity that creates us.
	 *
	 * @param item MenuItem: menu item that was clicked.
	 * @return Boolean: whether or not it was handled.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == R.id.menu_help) {
            showHelpActivity();
            return true;
        }
        else if (item.getItemId() == R.id.menu_logout) {
            logoutPCuser(pcUser);
            return true;
        }

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {
		//TODO
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
