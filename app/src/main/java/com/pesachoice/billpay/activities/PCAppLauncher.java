/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCData;

import java.util.Queue;


/**
 * Class used for the either signing up or logging in. New customers use the signup functionality
 * and they are immediately directed to the sign up activity which in turn handles the registration process.
 * Existing customers are directed to login activity which handles login credentials
 * 
 * @author Odilon Senyana
 *
 */
public class PCAppLauncher extends PCBaseActivity {
	
	public final static String SOME_MESSAGE = "com.pesachoice.MESSAGE";
	public final static String clazzz = PCAppLauncher.class.getName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_activity_signup_login);
	}

	public void openLogin(View view) {
		Log.d(clazzz, "Open login Activity");

		Intent intent = new Intent(this, PCLoginActivity.class);
		startActivity(intent);
		finish();
	}

	public void openSignUp(View view) {
		Log.d(clazzz, "Open Sign Up Activity");
		Intent intent = new Intent(this, PCSignupActivity.class);
		startActivityForResult(intent, SIGNUP_ACTIVITY_REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SIGNUP_ACTIVITY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				Intent intent = new Intent(this, PCVerifyPhoneActivity.class);
				intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, data);
				startActivityForResult(intent, 3);
			}
		}
	}

	@Override
	public void onTaskCompleted(PCData data) {

	}

	@Override
	public void onTaskStarted() {

	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {

	}
}

