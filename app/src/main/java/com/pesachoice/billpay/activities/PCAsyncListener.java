/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.activities;

import com.pesachoice.billpay.model.PCData;

/**
 * @author Odilon Senyana
 */
public interface PCAsyncListener {

    /**
     * This needs is used by components that need to do something while waiting for results from
     * a particular service. Before going on with anyting else, the component needs to call this
     * method, then within it provide the implementation of what needs to be done prior to
     * service calls.
     */
    void onTaskStarted();

    /**
     * This used by components to do post-processing work after a call to a particular service.
     * Anyting that a component needs to do with results from the service is implemented in this
     * method.
     * @param data
     *          Service results
     */
    void onTaskCompleted(PCData data);
}
