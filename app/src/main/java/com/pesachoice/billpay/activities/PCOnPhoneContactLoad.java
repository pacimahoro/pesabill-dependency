package com.pesachoice.billpay.activities;

import com.pesachoice.billpay.model.PCContactInfo;

import java.util.List;

/**
 * used to to help in calling back back after loading contacts list using async task
 * Created by desire.aheza on 7/27/2016.
 */

public interface PCOnPhoneContactLoad {
    void onContactLoadStarted();
    void onContactLoadFinished(List<PCContactInfo> contacts);
}
