/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCKeychainWrapper;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;

import org.springframework.util.StringUtils;

import java.util.Queue;

/**
 * Used to build and handle the view for login feature.
 *
 * @author Odilon Senyana
 * @author Desire Aheza - modified this class to handle login using phone numbers
 * @author emmy
 */
public class PCLoginActivity extends PCBaseActivity implements View.OnClickListener {

    protected ProgressDialog progressDialog;
    protected String isFailedAuth = null;
    protected EditText passwordText;
    protected EditText phoneEditText;
    protected EditText userNameText;
    protected LinearLayout loginWithPhoneNumberLayout;
    protected Spinner loginTypes;
    private PCLoginActivity activity;
    private PCPesabusClient.PCServiceType currentServiceType;
    protected String loginMethod;
    private final static String CLAZZZ = PCLoginActivity.class.getName();
    private String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pc_activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeView();
        selectedCountry = countryCodeMap.get(this.getUserCountryCode(this));
        if (!StringUtils.isEmpty(selectedCountry)) {
            countryPicker1.setText(selectedCountry.substring(selectedCountry.indexOf("+")));
        } else {
            showCountriesCode(this);
        }
        countryPicker1.setOnClickListener(this);
        // IF the user was already logged in, get the info from cache.
        PCUser user = getLogedInUser();
        String userName = user.getEmail();
        userNameText.setText(userName);
        configureLoginTypeSelector();
        Intent intent = getIntent();
        //get flag to check if the user failed authentication
        if (intent != null && intent.getExtras() != null) {
            isFailedAuth = intent.getExtras().getString("PCCHECOUT");
        }
        // Listen for keyboard dismissal
        findViewById(R.id.login_container).setOnClickListener(new DismissKeyboardListener(this));
    }

    protected void initializeView() {
        countryPicker1 = (TextView) findViewById(R.id.countryPicker1);
        loginWithPhoneNumberLayout = (LinearLayout) findViewById(R.id.loging_with_phone_number_layout);
        loginTypes = (Spinner) findViewById(R.id.login_types);
        passwordText = (EditText) findViewById(R.id.password);
        phoneEditText = (EditText) findViewById(R.id.phone);
        userNameText = (EditText) findViewById(R.id.email);
    }

    protected void configureLoginTypeSelector() {
        if (loginTypes != null) {
            loginTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        userNameText.setVisibility(View.VISIBLE);
                        loginWithPhoneNumberLayout.setVisibility(View.GONE);
                    } else if (position == 1) {
                        userNameText.setVisibility(View.GONE);
                        loginWithPhoneNumberLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    /**
     * This is used as an action handler for whenever a user clicks on the login button.
     *
     * @param view
     */
    public void onLoginAction(View view) {

        String userName = null;
        if (View.VISIBLE == userNameText.getVisibility()) {
            userName = userNameText.getText().toString();
        } else if (View.VISIBLE == loginWithPhoneNumberLayout.getVisibility()) {
            //remove the plus(+) sign of country code
            String countryCode = countryPicker1.getText().toString().substring(1);
            //combine country code and phone number
            if (phoneEditText != null && !StringUtils.isEmpty(phoneEditText.getText().toString())) {
                userName = countryCode + phoneEditText.getText().toString();
            }
        }
        //remove the starting and ending spaces
        userName = trimUserInput(userName);
        String password = passwordText.getText().toString();
        try {

            if (pingResults != null) {
                this.validCustomerInfo(userName, password);
                // Set values: TODO ask the team if they're ok with this mechanism!
                if (TextUtils.isDigitsOnly(userName)) {
                    loginMethod = "PHONE NUMBER";
                    //encypt password
                    Log.e(CLAZZZ, "PSWD=" + password);
                    Log.e(CLAZZZ, "USERNAME=" + userName);
                    Log.e(CLAZZZ, "ApiKey=" + pingResults.getApiKey());
                    String pswd = PCKeychainWrapper.securedAESEncryption(userName, pingResults.getApiKey(), passwordText.getText().toString());
                    loginUser(userName, pswd, pingResults.getDeviceId());
                } else {
                    loginMethod = "EMAIL";
                    // Encrypt the password
                    password = PCKeychainWrapper.securedSHA256DigestHash(password, userName);
                    loginUser(userName, password, null);
                }
            } else {
                PCGenericError error = getPcGenericError("Error during login, please try again", "Login");
                throw error;
            }
        } catch (PCGenericError error) {
            this.presentError(error, this.getResources().getString(R.string.login_error_title));
        }
    }

    /**
     * This is used to user
     *
     * @param userName: username for the user
     * @param password: password
     * @param deviceId: device ID from Ping result
     */
    public void loginUser(String userName, String password, String deviceId) {
        try {
            final PCUser user = new PCUser();
            final PCRequest loginRequest = new PCRequest();
            String companyName = this.getResources().getString(R.string.company_Id);
            //get new updates that contain calling info
            loginRequest.setDetailed(true);
            loginRequest.setSpecial(true);
            loginRequest.setCompanyId(companyName);
            currentServiceType = PCPesabusClient.PCServiceType.LOGIN;
            if (PCGeneralUtils.isValidPhone(userName, "")) {
                loginRequest.setDeviceId(deviceId);
                user.setPhoneNumber(userName);
            } else {
                user.setEmail(userName);
            }
            user.setPwd(password);

            loginRequest.setUser(user);
            PCCredentialsController controller = (PCCredentialsController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, this);
            if (controller != null) {
                controller.setActivity(this);
                controller.setServiceType(PCPesabusClient.PCServiceType.LOGIN);
                controller.execute(loginRequest);
            }
        } catch (Throwable exc) {
            Log.e(CLAZZZ, "Could not handle the login process because [" + exc.getMessage() + "]");
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            PCGenericError error = new PCGenericError();
            if (exc instanceof PCGenericError) {
                error = (PCGenericError) exc;
                //error.setNeedToGoBack(false);
            } else {
                error.setMessage(exc.getMessage());
            }
            this.presentError(error, "Login Error");
        }
    }

    /**
     * This is used as an action handler for whenever a user clicks on the "forgot password" feature.
     * It creates the activity reserved to reset password and delegates all the work to it.
     *
     * @param view The UI view used to loging and or reset forgotten password
     */
    public void onForgotPasswordAction(View view) {
        Log.d(CLAZZZ, "Open password recovery activity");
        Intent intent = new Intent(this, PCPasswordRecoveryActivity.class);
        startActivityForResult(intent, PASSWORD_RECOVERY_ACTIVITY_REQUEST_CODE);
    }

    /**
     * method to validate login inputs
     *
     * @param username
     * @param password
     * @throws PCGenericError
     */
    public void validCustomerInfo(String username, String password) throws PCGenericError {

        PCGenericError error;
        if (userNameText != null && (View.VISIBLE == userNameText.getVisibility())) {
            showError(username, R.string.login_email_missing_username, "username");
        } else if (phoneEditText != null && (View.VISIBLE == loginWithPhoneNumberLayout.getVisibility())) {
            showError(username, R.string.login_missing_username, "username");
        }

        if (password == null || password.length() == 0) {
            error = getPcGenericError(this.getResources().getString(R.string.login_missing_password), "password");
            throw error;
        }
    }

    private void showError(String text, int id, String title) throws PCGenericError {
        PCGenericError error;
        if (text == null || text.length() == 0) {
            error = getPcGenericError(this.getResources().getString(id), title);
            throw error;
        }
    }

    protected PCGenericError getPcGenericError(String message, String field) {
        PCGenericError error = new PCGenericError();
        error.setMessage(message);
        error.setField(field);
        return error;
    }

    /**
     * used to set progress dialog message everytime a new service call is made
     */
    @Override
    public void onTaskStarted() {
        progressDialog = ProgressDialog.show(this, "User Login", "Please wait while logging process is going on", true);
    }

    /**
     * this method will be called when service all returns; we're expecting it to return:
     * PING REQUEST RESULT, LOGIN REQUEST RESULT
     * if anything goes wrong this method will extract an error message and pass it to
     * general method that will popup a dialog with error details.
     *
     * @param data
     */
    @Override
    public void onTaskCompleted(PCData data) {
        if (data != null && this.currentServiceType == PCPesabusClient.PCServiceType.LOGIN && data instanceof PCUser) {
            final PCSpecialUser user = (PCSpecialUser) data;
            String country = getCountry(user);

            boolean successLogin;
            if (user.getErrorMessage() != null && user.getErrorMessage().length() > 0) { // service call failed
                successLogin = false;
                Log.e(CLAZZZ, "An error occured: [" + user.getErrorMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(user.getErrorMessage());
                this.presentError(error, this.getResources().getString(R.string.login_error_title));
            } else { // service call succeeded
                successLogin = true;
                this.showMainController(user);
            }

            if (StringUtils.isEmpty(country)) {
                country = "UnknownCountry";
            }

            PCPostPesaBillMetrics.postLoginMetric(this, country, loginMethod, successLogin);
        } else {
            // Some error occurred and response data is missing
            Log.e(CLAZZZ, "Application level error");
            PCPostPesaBillMetrics.postLoginMetric(this, "", loginMethod, false);
            /*
             * TODO: missing implementation on what the user should see in this case
             */
            Log.e(CLAZZZ, "An error occured: [" + "while sign in" + "]");
            PCGenericError error = new PCGenericError();
            error.setMessage("Invalid username or password");
            this.presentError(error, this.getResources().getString(R.string.login_error_title));
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * Delegates main control of the application to the appropriate activity.
     * If the registration is complete then the main tab activity takes over.
     * However, if the registration is not complete, the user is taken to the
     * process of verifything the phone.
     *
     * @param user User data after registration
     */
    protected void showMainController(final PCSpecialUser user) {
        Log.d(CLAZZZ, "Open Main Activity");

        if (user == null) {
            return;
        }
        if (isFailedAuth == null) {
            this.saveUserDetails(user, true);

            if (user.isRegistrationComplete()) {
                Intent intent = createMainTabActivityIntent(user);
                startActivity(intent);
            } else if (user.isRegistrationComplete() || user.isAgent()) {
                Intent intent = createMainTabActivityIntent(user);
                startActivity(intent);
            } else {
                //cache user details
                Intent intent = new Intent(this, PCVerifyPhoneActivity.class);
                intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
                startActivity(intent);
                finish();
            }
        } else {
            Intent returnIntent = new Intent();
            this.saveUserDetails(user, true);
            returnIntent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
            setResult(AppCompatActivity.RESULT_OK, returnIntent);
            finish();
        }
    }

    /**
     * Create the main tab activity
     * Abstract this to its own method, this way derived classes can subclass this if needed.
     *
     * @param user the app user
     * @return an intent to be seen when the user is fully logged in.
     */
    protected Intent createMainTabActivityIntent(final PCSpecialUser user) {
        Intent intent = new Intent(this, PCMainTabActivity.class);
        intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    public PCPesabusClient.PCServiceType getCurrentServiceType() {
        return currentServiceType;
    }

    @Override
    public void onTaskCompleted(Queue<? extends PCData> data) {
        //TODO
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

        if (v != null) {
            if (v.getId() == R.id.countryPicker1) {
                showCountriesCode(this);
            }
        }
    }
}
