package com.pesachoice.billpay.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.pesachoice.billpay.model.PCData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Activity used to show Country lists and their country code, most likely this will be needed when you want the user to select a country
 * where they're located
 */
public class PCSelectCountryActivity extends PCBaseActivity {
    private  ListView listViewCountries;
    private  CountryArrayAdapter adapter;
    private Map<String,String> map = new HashMap<>();
    private List<String> countryNames;
    private Intent i;
    private EditText editTextSearchCountry;
    private static Comparator<String> ALPHABETICAL_ORDER = new Comparator<String>() {
        public int compare(String str1, String str2) {
            str1 = str1.substring(0,str1.indexOf('+'));
            str2 = str2.substring(0,str2.indexOf('+'));
            int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
            if (res == 0) {
                res = str1.compareTo(str2);
            }
            return res;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pc_activity_select_country);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setup toolbar
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        //setup the back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //set title
        TextView title = (TextView) toolbar.findViewById(R.id.textview_title);
        title.setText("Select Country");
        //set adapter
        listViewCountries = (ListView) findViewById(R.id.listView_countries);
        editTextSearchCountry = (EditText) findViewById(R.id.editText_search_country);

        editTextSearchCountry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    adapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Intent intent = getIntent();
        List<String> values = intent.getExtras().getStringArrayList("VALUES");
        List<String> keys = intent.getExtras().getStringArrayList("KEYS");
        ;
        if (values != null && keys != null && keys.size() == values.size()){
            int count=0;
            for (String key:keys) {
                map.put(key,values.get(count));
                count++;
            }
            countryNames = new ArrayList<String>(map.values());
            adapter=new CountryArrayAdapter(this,map);

        }
        TextView emptyView = (TextView) findViewById(R.id.emptyList);
        emptyView.setVisibility(View.VISIBLE);
        listViewCountries.setEmptyView(emptyView);
        //get intent data
        i = getIntent();
        listViewCountries.setAdapter(adapter);
        listViewCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) view.getTag(R.id.txt_country);
                i.putExtra("country", value);
                setResult(RESULT_OK, i);
                finish();
            }
        });

    }

    @Override
    public void onTaskCompleted(Queue<? extends PCData> data) {

    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskCompleted(PCData data) {

    }


    class CountryArrayAdapter extends BaseAdapter implements Filterable {
        Context context;
        private List<String> originalCountries = null;
        private List<String> filteredCountries = null;
        String[] countrySections;
        String[] sections;
        private LayoutInflater mInflater;
        private CountryFilter mFilter = new CountryFilter();
        public CountryArrayAdapter(Context context, Map<String,String> countries) {
            this.context = context;
            this.originalCountries = new ArrayList<>(countries.values());
            this.filteredCountries = new ArrayList<>(countries.values());
            this.mInflater = LayoutInflater.from(context);
            Collections.sort(originalCountries,ALPHABETICAL_ORDER);

            Collections.sort( filteredCountries,ALPHABETICAL_ORDER);
        }

        @Override
        public int getCount() {
            return filteredCountries.size();
        }

        @Override
        public Object getItem(int position) {
            return filteredCountries.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.pc_country_list_item_layout, null);
                // Creates a ViewHolder and store references to the two children views
                // we want to bind data to.
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.txt_country);
                // Bind the data efficiently with the holder.
                convertView.setTag(holder);
            } else {
                // Get the ViewHolder back to get fast access to the TextView
                // and the ImageView.
                holder = (ViewHolder) convertView.getTag();
            }

            // If weren't re-ordering this you could rely on what you set last time
            holder.text.setText(filteredCountries.get(position));
            convertView.setTag(R.id.txt_country,filteredCountries.get(position));
            return convertView;
        }

        @Override
        public Filter getFilter() {
            return mFilter;
        }

        class ViewHolder {
            TextView text;
        }

        class CountryFilter extends Filter{

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<String> list = originalCountries;

                int count = list.size();
                final ArrayList<String> nlist = new ArrayList<String>(count);

                String filterableString ;

                for (int i = 0; i < count; i++) {
                    filterableString = list.get(i);
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(filterableString);
                    }
                    //TODO: need to find a best way to handle this
                    if ("America".equalsIgnoreCase(filterString) || "USA".equalsIgnoreCase(filterString) || "United States of America".equalsIgnoreCase(filterString )){
                           if (!nlist.contains("United States"))
                                nlist.add("United States");
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                filteredCountries = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        }

    }


}
