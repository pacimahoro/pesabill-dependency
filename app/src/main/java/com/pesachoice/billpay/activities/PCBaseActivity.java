package com.pesachoice.billpay.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.appinvite.FirebaseAppInvite;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.PCUserErrable;
import com.pesachoice.billpay.business.service.PCCardController;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.fragments.PCCardPayment;
import com.pesachoice.billpay.fragments.PCCardPaymentFragment;
import com.pesachoice.billpay.fragments.PCMainTabsFragment;
import com.pesachoice.billpay.fragments.PCUserAccountFragment;
import com.pesachoice.billpay.model.PCAirtimeTransaction;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCCallingTransaction;
import com.pesachoice.billpay.model.PCCardInfo;
import com.pesachoice.billpay.model.PCCardServiceData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCCustomerServiceInfo;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCElectricityTransaction;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCInternetTransaction;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCServiceInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTVTransaction;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCTuitionTransaction;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.model.PCWaterTransaction;
import com.pesachoice.billpay.utils.PCGeneralUtils;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;
import com.pesachoice.billpay.utils.PCPreferenceHelper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static com.pesachoice.billpay.business.PCPesachoiceConstant.SHARED_PREF_USER_DEFAULT_TRACKING_KEY;
import static com.pesachoice.billpay.model.PCBillPaymentData.PCPaymentType.MOBILE_MONEY;

/**
 * Base Class for all our activities.
 * Created by Pacifique Mahoro on 11/2/15.
 * Modified by Desire Aheza on 4/10/16
 */
@SuppressWarnings("deprecation")
public abstract class PCBaseActivity extends AppCompatActivity implements PCUserErrable, PCAsyncListenerManyItem {
	private final static String clazzz = PCBaseActivity.class.getName();
	protected static final int LOGIN_ACTIVITY_REQUEST_CODE = 1;
	protected static final int SIGNUP_ACTIVITY_REQUEST_CODE = 2;
	protected static final int VERIFIY_ACTIVITY_REQUEST_CODE = 3;
	protected static final int PASSWORD_RECOVERY_ACTIVITY_REQUEST_CODE = 5;
	protected static final int PASSWORD_RESET_ACTIVITY_REQUEST_CODE = 6;
	private static final int REQUEST_INVITE = 0;
	protected boolean unsupportedCountry = false;
	protected Map<String, String> countryCodeMap;
	protected Map<String,String> countryFlagMap;
	protected static PCSpecialUser appUser;
	public PCPesabusClient.PCServiceType currentServiceType;
	protected Fragment currentFragment = new Fragment();
	protected static ProgressDialog progressDialog;
	protected static PCPingResults pingResults = new PCPingResults();
	protected TextView countryPicker1;
	protected ImageView countryPickerImage;
	protected String selectedCountry = "";
	private static final String TAG = PCBaseActivity.class.getSimpleName();
	private DataShare mdataShare;
	JSONParser parser = new JSONParser();



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FirebaseCrash.setCrashCollectionEnabled(true);
		countryCodeMap = new TreeMap<>();
		countryFlagMap = new TreeMap<>();
		makeCountriesCode();
		makeCountriesFlag();

		/**
		 * receiving invitation using firebase dynamic Links
		 */
		FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
				.addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
					@Override
					public void onSuccess(PendingDynamicLinkData data) {
						if (data == null) {
							Log.d(TAG, "getInvitation: no data");
							return;
						}

						// Get the deep link
						Uri deepLink = data.getLink();
						// Extract invite
						FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(data);
						if (invite != null) {
							String invitationId = invite.getInvitationId();
						}

						// Handle the deep link
						// [START_EXCLUDE]
						Log.d(TAG, "deepLink:" + deepLink);
						if (deepLink != null) {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setPackage(getPackageName());
							intent.setData(deepLink);
							startActivity(intent);
						}
						// [END_EXCLUDE]
					}
				})
				.addOnFailureListener(this, new OnFailureListener() {
					@Override
					public void onFailure(@NonNull Exception e) {
						Log.w(TAG, "getDynamicLink:onFailure", e);
					}
				});
	}

	public void hideKeyboard(View view) {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	@Override
	public void presentError(final PCGenericError error, String title) {
		String message = error.getMessage();
		if (isFinishing() || message == null) {
			return;
		}
		/**
		 * builder error notification to user action
		 */
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setTitle(title);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Log.d(clazzz, "Clicked on OK on Alert box");
				if (PCGenericError.ErrorType.NETWORK_CONNECTIVITY.equals(error.getErrorType()) && error.isNeedToGoBack()) {
					onBackPressed();
				}

			}
		});

		Dialog alert = builder.create();
		alert.show();

	}

	/**
	 * determine if the user is in USA.
	 */
	public boolean isCountryUS() {
		boolean isCountryUS = false;
		if (appUser != null) {
			PCCurrency currency = appUser.getBaseCurrency();
			if (currency != null) {
				String ctry = currency.getCountry();
				isCountryUS = PCGeneralUtils.isCountryUS(appUser.getCountry()) || PCGeneralUtils.isCountryUS(ctry);
			}
		}
		return isCountryUS;
	}

	/**
	 * currently using this function to detect if user is in canada or US, so that we can
	 * ask their address corresponding to credit card.
	 */
	public boolean isCountryUsaOrCaByPhoneNum(String phoneNumber) {
		return !StringUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("1") && phoneNumber.length() == 11;
	}

	public void setAppUser(PCSpecialUser user) {
		this.appUser = user;
	}

	public PCSpecialUser getAppUser() {
		return this.appUser;
	}

	public void saveUserTrackingKey(String key) {
		PCPreferenceHelper.saveToPrefs(this, SHARED_PREF_USER_DEFAULT_TRACKING_KEY, key);
	}

	/**
	 * check if unique was created if not create it and attached it to the transaction
	 *
	 * @return
	 */
	public String getUserTrackingKey() {
		String uniqueId = PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_DEFAULT_TRACKING_KEY, "");
		if (StringUtils.isEmpty(uniqueId) && getApiKey() != null) {
			uniqueId = PCGeneralUtils.createTrackingUserKey(getAppUser(), getApiKey().getApiKey());
			saveUserTrackingKey(uniqueId);
		}

		return uniqueId;
	}

	public void saveUserDetails(PCSpecialUser user, boolean isLogedIn) {

		if (user != null) {
			//save username and login status
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_EMAIL, user.getEmail());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_LOGGED_IN, isLogedIn);
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_TOKEN, user.getTokenId());//user.getPwd()
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_USERNAME, user.getUserName());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_PHONE_NUMBER, user.getPhoneNumber());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_MASKED_CREDIT_CARD, user.getMaskedCardNumber());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_IS_CUSTOMER_REP, user.isCustomerRep());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_VERIFIED_PHONE, user.isRegistrationComplete());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_LASTNAME, user.getLastName());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_FIRSTNAME, user.getFirstName());
			PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_DEFAULT_ACCOUNT, user.getCurrentUserAccount());
			//save user address
			if (user.getBillingAddress() != null && isCountryUS()) {
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_STREET_NAME, user.getBillingAddress().getStreet());
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_CITY, user.getTokenId());
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_ZIP_CODE, user.getUserName());
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_STATE, user.getPhoneNumber());
				PCPreferenceHelper.saveToPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_COUNTRY, user.getMaskedCardNumber());

			}
		}
	}

	protected void logoutPCuser(PCUser user) {

		if (user != null && !StringUtils.isEmpty(user.getEmail()) && !StringUtils.isEmpty(user.getTokenId())) {
			final PCRequest logoutRequest = new PCRequest();
			final PCUser userData = new PCUser();
			userData.setEmail(user.getEmail());
			userData.setTokenId(user.getTokenId());
			logoutRequest.setUser(userData);
			try {

				this.currentServiceType = PCPesabusClient.PCServiceType.LOGOUT;

				PCCredentialsController credentialsController = (PCCredentialsController)
						PCControllerFactory.constructController(
								PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, this);
				if (credentialsController != null) {
					credentialsController.setActivity(this);
					credentialsController.setServiceType(PCPesabusClient.PCServiceType.LOGOUT);
					credentialsController.execute(logoutRequest);
				}
			} catch (Throwable exc) {
				Log.e(clazzz, "Could not handle the logout process because [" + exc.getMessage() + "]");
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				PCGenericError error = new PCGenericError();
				if (exc instanceof PCGenericError) {
					error = (PCGenericError) exc;
				} else {
					error.setMessage(exc.getMessage());
				}
				this.presentError(error, "Error Logging out the user");
			}
		}

	}

	public PCSpecialUser getLogedInUser() {
		PCSpecialUser user = new PCSpecialUser();
		user.setPhoneNumber(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_PHONE_NUMBER, ""));
		user.setEmail(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_EMAIL, ""));
		user.setUserName(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_USERNAME, ""));
		user.setTokenId(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_TOKEN, ""));
		user.setLastName(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_LASTNAME, ""));
		user.setFirstName(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_FIRSTNAME, ""));
		user.setMaskedCardNumber(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_MASKED_CREDIT_CARD, ""));
		user.setCustomerRep(PCPreferenceHelper.getBooleanFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_IS_CUSTOMER_REP, false));
		user.setRegistrationComplete(PCPreferenceHelper.getBooleanFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_VERIFIED_PHONE, false));
		user.setCurrentUserAccount(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_DEFAULT_ACCOUNT, ""));
		PCBillingAddress address = new PCBillingAddress();
		if (isCountryUS()) {//currently we cares only about US addresses
			address.setStreet(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_STREET_NAME, ""));
			address.setCountry(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_COUNTRY, ""));
			address.setZip(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_ZIP_CODE, ""));
			address.setState(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_STATE, ""));
			address.setCity(PCPreferenceHelper.getFromPrefs(this, PCPesachoiceConstant.SHARED_PREF_USER_ADDRESS_CITY, ""));
			user.setBillingAddress(address);
		}
		return user;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	public PCPingResults getApiKey() {
		return pingResults;
	}

	/**
	 * used to get Local customer case service Info.
	 */
	public PCCustomerServiceInfo getCustomerServiceInfo() {
		PCCustomerServiceInfo customerServiceInfo = null;
		PCSpecialUser pcSpecialUser = this.getAppUser();
		if (pcSpecialUser != null && pingResults != null) {
			String currentUserPhoneNumb = pcSpecialUser.getPhoneNumber();
			String currentUserCountry = null;
			PCCurrency currency = pcSpecialUser.getBaseCurrency();
			if (currency != null) {
				currentUserCountry = currency.getCountry();
			}
			if (StringUtils.isEmpty(currentUserCountry)) {
				currentUserCountry = pcSpecialUser.getCountry();
			}

			List<PCCustomerServiceInfo> customerServiceInfoList = pingResults.getCustServiceContacts();
			if (customerServiceInfoList != null) {
				for (PCCustomerServiceInfo customerServiceInfo1 : customerServiceInfoList) {
					String country = customerServiceInfo1.getCountry();
					String phoneNumber = customerServiceInfo1.getPhoneNumber();

					if (!StringUtils.isEmpty(country) || !StringUtils.isEmpty(phoneNumber)) {

						if (country.equalsIgnoreCase(currentUserCountry) || country.equalsIgnoreCase(pcSpecialUser.getCountry())) {
							customerServiceInfo = customerServiceInfo1;
							break;
						}
					}
				}
				if (customerServiceInfo == null) {//GUESSING
					for (PCCustomerServiceInfo customerServiceInfo1 : customerServiceInfoList) {
						String phoneNumber = customerServiceInfo1.getPhoneNumber();
						if (currentUserPhoneNumb != null && currentUserPhoneNumb.length() > 4
								&& phoneNumber.length() > 4) {//TODO this need to be reviewed propery
							if (phoneNumber.substring(0, 3).equalsIgnoreCase(phoneNumber.substring(0, 3))) {
								customerServiceInfo = customerServiceInfo1;
								break;
							} else if (phoneNumber.substring(0, 2).equalsIgnoreCase(phoneNumber.substring(0, 2))) {
								customerServiceInfo = customerServiceInfo1;
								break;
							} else if (phoneNumber.substring(0, 1).equalsIgnoreCase(phoneNumber.substring(0, 1))) {
								customerServiceInfo = customerServiceInfo1;
								break;
							}
						}
					}
				}

			}
		}

		return customerServiceInfo;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		else if(item.getItemId() == R.id.menu_help) {
			showHelpActivity();
			return true;
		}
		else if (item.getItemId() == R.id.menu_invite_friends) {
			inviteFriendUsingFirebase ();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	protected void showHelpActivity() {
		Intent intent = new Intent(this, PCAccountDetailsActivity.class);
		intent.putExtra(PCPesabusClient.OPTION, PCPesabusClient.OPTION_HELP);
		startActivity(intent);
	}

	/**
	 * used to show send options
	 */
	private void inviteFriend() {

		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out Pesachoice app for your smartphone. Download it today from http://bit.ly/pesachoiceApp");
		sendIntent.setType("text/plain");
		Intent chooser = Intent.createChooser(sendIntent, "Invite friends");

		// Verify the intent will resolve to at least one activity
		if (sendIntent.resolveActivity(getPackageManager()) != null) {
			startActivity(chooser);
		}
	}

	/**
	 * This method invite friend to install pesachoice app using firebase
	 */
	public void inviteFriendUsingFirebase() {
		Intent sendIntent = new AppInviteInvitation.IntentBuilder("Invite Friends")
				.setMessage(getResources().getString(R.string.invite_message))
				.setDeepLink(Uri.parse(getResources().getString(R.string.link_Details)))
				.setCustomImage(Uri.parse(getResources().getString(R.string.google_image_url)))
				.setCallToActionText("Install!")
				.build();
		startActivityForResult(sendIntent, REQUEST_INVITE);
	}


	private void showMessage(String msg) {
		Context context = getApplicationContext();
		CharSequence text = msg;
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}


	/**
	 * used to show screen to show screen to recommend friends
	 */
	private void showRecommendFriendsScreen() {

		Intent intent = new Intent(this, PCRecommendFriendsActivity.class);
		intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, getAppUser());
		startActivity(intent);
	}

	public void showCountriesCode(PCBaseActivity activity) {
		//Intent to provide choose country screen
		Intent intent = null;
		if (activity instanceof PCSignupActivity) {
			intent = new Intent((PCSignupActivity) activity, PCSelectCountryActivity.class);
		} else if (activity instanceof PCLoginActivity) {
			intent = new Intent((PCLoginActivity) activity, PCSelectCountryActivity.class);
		}
		ArrayList<String> values = new ArrayList<String>(countryCodeMap.values());
		ArrayList<String> keys = new ArrayList<String>(countryCodeMap.keySet());
		intent.putStringArrayListExtra("VALUES", values);
		intent.putStringArrayListExtra("KEYS", keys);
		startActivityForResult(intent, PCPesachoiceConstant.PESACHOICE_SELECT_COUNTRY);
	}




	  public void showCountriesFlag(PCBaseActivity activity) {
		  Intent intent = null;
		  if (activity instanceof PCMainTabActivity) {
			  intent = new Intent((PCMainTabActivity) activity, PCSelectCountryActivity.class);
		  }
		  ArrayList<String> values = new ArrayList<String>(countryFlagMap.values());
		  ArrayList<String> keys = new ArrayList<String>(countryFlagMap.keySet());
		  intent.putStringArrayListExtra("VALUES", values);
		  intent.putStringArrayListExtra("KEYS", keys);
		  startActivityForResult(intent, PCPesachoiceConstant.PESACHOICE_SELECT_COUNTRY);
	  }


	/**
	 * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
	 *
	 * @param context Context reference to get the TelephonyManager instance from
	 * @return country code or null
	 */
	public String getUserCountryCode(Context context) {
		String countryCode;
		try {
			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			final String simCountry = tm.getSimCountryIso();

			if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
				return simCountry.toUpperCase(Locale.US);
			} else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
				String networkCountry = tm.getNetworkCountryIso();
				if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
					return networkCountry.toUpperCase(Locale.US);
				}
			} else {
				return "+1";
			}
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * used to trim all user inputs in this app
	 *
	 * @param userInput: input from the user
	 * @return trimmed string.
	 */
	public String trimUserInput(String userInput) {
		if (StringUtils.isEmpty(userInput)) {
			return userInput;
		}
		return userInput.trim();
	}

	public Map<String, String> getCountriesAndCodes() {
		return null;
	}

	private void makeCountriesCode() {

		try {
			JSONArray array = (JSONArray) parser.parse(new BufferedReader(
					new InputStreamReader(getAssets().open("country.json"))));
			for (Object o : array) {
				JSONObject country = (JSONObject) o;
				String country_name = (String) country.get("name");
				String country_dial_code = (String) country.get("dial_code");
				String country_code = (String) country.get("code");
				countryCodeMap.put(country_code, country_name + " " + country_dial_code);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void makeCountriesFlag () {

		try {
			JSONArray array = (JSONArray) parser.parse(new BufferedReader(
					new InputStreamReader(getAssets().open("country_flag_code.json"))));
			for (Object o : array) {
				JSONObject country = (JSONObject) o;
				String country_name = (String) country.get("name_official");
				String country_dial_code = (String) country.get("dial_code");
				String country_code = (String) country.get("code2l");
				String country_flag = (String) country.get("flag_32");
				countryFlagMap.put(country_flag, country_name + " " + country_dial_code + " " + country_code);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	protected void updatePaymentInfo(Fragment frag, PCBillPaymentData.PCPaymentType pcPaymentType) {
		if (frag instanceof PCUserAccountFragment) {
			PCUserAccountFragment userFrag = (PCUserAccountFragment) frag;
			userFrag.updatePaymentCardInfo(userFrag.getView(), pcPaymentType);
		}
	}

	/**
	 * used to return the default user account number.
	 */
	public String getAccountInfo(PCBillPaymentData.PCPaymentType paymentType) {
		String accountInfo = null;
		if (appUser != null && paymentType != null) {
			PCPaymentInfo paymentInfo = appUser.getPaymentInfo();
			if (paymentInfo != null) {
				PCCustomerMetadata defaultSource = paymentInfo.getDefaultSource();
				if (defaultSource != null && (!StringUtils.isEmpty(defaultSource.getPhoneNumber()) || !StringUtils.isEmpty(defaultSource.getMaskedCardNumber()))) {
					accountInfo = paymentType == MOBILE_MONEY ? defaultSource.getPhoneNumber() : defaultSource.getMaskedCardNumber();
				}
			}

			if (StringUtils.isEmpty(accountInfo)) {
				accountInfo = paymentType == MOBILE_MONEY ? appUser.getPhoneNumber() : appUser.getMaskedCardNumber();
			}
		}
		return accountInfo;
	}

	/**
	 * Save new credit card in boolean which was a void method I changed it in order to perform unit test.
	 *
	 * @param cardFragment the card fragment
	 * @return the boolean
	 */
	@SuppressWarnings("deprecation")
	public void saveNewCreditCard(PCCardPayment cardFragment) {
		PCCardInfo cardInfo = new PCCardInfo();

		cardInfo.setNumber(cardFragment.getCardNumber());
		cardInfo.setCvv(cardFragment.getCvv());
		cardInfo.setMonthOfExpiration(cardFragment.getExpMonth());
		cardInfo.setYearOfExpiration(cardFragment.getExpYear());
		cardInfo.setCardType(cardFragment.getCardType());

		if (isCountryUS()) {
			// server now requires billing address.
			PCBillingAddress billingAddress = cardFragment.getBillingAddress();
			cardInfo.setBillingAddress(billingAddress);

			//set billing info to the user!
			if (appUser != null) {//&& appUser.getBillingAddress() == null
				appUser.setBillingAddress(billingAddress);
			}
		}
		final PCCardServiceData cardServiceDataRequest = new PCCardServiceData();
		//TODO: what will happen if ping call has on returned?

		cardServiceDataRequest.setDeviceId(this.pingResults.getDeviceId());
		cardServiceDataRequest.setSender(this.getAppUser());
		cardServiceDataRequest.setCard(cardInfo);

		try {
			// If we already have a card attached, treat this as replacing an existing card,
			// otherwise we are adding a new card.
			if (this.getAppUser() != null && this.getAppUser().getMaskedCardNumber() != null &&
					getAppUser().getMaskedCardNumber().length() > 0) {
				this.currentServiceType = PCPesabusClient.PCServiceType.REPLACE_CARD;
			} else {
				this.currentServiceType = PCPesabusClient.PCServiceType.PAYMENT_PROFILE;
			}

			this.currentFragment = (Fragment) cardFragment;
			PCCardController cardController = (PCCardController)
					PCControllerFactory.constructController(
							PCControllerFactory.PCControllerType.CARD_CONTROLLER, this);
			if (cardController != null) {
				cardController.setActivity(((PCCardPaymentFragment) currentFragment).getCurrentActivity());
				cardController.setServiceType(currentServiceType);
				cardController.execute(cardServiceDataRequest);
			}

		} catch (Throwable exc) {
			// FIXME: Pass the correct method or type for this metric event
			PCPostPesaBillMetrics.postCardAttachmentEvent(this, getCountry(appUser), "", false);

			Log.e(clazzz, "Could not handle the process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}

			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError) exc;
				error.setNeedToGoBack(false);
			} else {
				error.setMessage(exc.getMessage());
			}
			this.presentError(error, "Error Saving Card Payment");
		}
	}


	public interface DataShare{
		public void shareData(String str);
	}

	/**
	 * use to temporay hold local card for transaction processing
	 *
	 * @param cardFragment
	 */



	public void holdLocalCardInfo(PCCardPayment cardFragment){
		PCCardInfo cardInfo = new PCCardInfo();
		cardInfo.setNumber(cardFragment.getCardNumber());
		cardInfo.setCvv(cardFragment.getCvv());
		cardInfo.setMonthOfExpiration(cardFragment.getExpMonth());
		cardInfo.setYearOfExpiration(cardFragment.getExpYear());
		cardInfo.setCardType(cardFragment.getCardType());
		if (this instanceof PCMainTabActivity) {
			PCMainTabActivity parentActivity = (PCMainTabActivity) this;
			//Add the masked card number into cardInfo
			cardInfo.setMaskedLocalCard(parentActivity.getMaskedLocalCard());
			parentActivity.setCardInfo(cardInfo);
			//Pop PCCardPayment fragment
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.popBackStack();
		}
	}

	/**
	 * use to retrieve service info from a given transaction
	 *
	 * @param transaction
	 * @return
	 */
	public PCServiceInfo getServiceInfo(PCTransaction transaction) {
		PCServiceInfo serviceInfo = null;
		if (transaction instanceof PCAirtimeTransaction) {
			serviceInfo = ((PCAirtimeTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCElectricityTransaction) {
			serviceInfo = ((PCElectricityTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCInternetTransaction) {
			serviceInfo = ((PCInternetTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCTVTransaction) {
			serviceInfo = ((PCTVTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCCallingTransaction) {
			serviceInfo = ((PCCallingTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCTuitionTransaction) {
			serviceInfo = ((PCTuitionTransaction) transaction).getServiceInfo();
		} else if (transaction instanceof PCWaterTransaction) {
			serviceInfo = ((PCWaterTransaction) transaction).getServiceInfo();
		}

		return serviceInfo;
	}

	public String getTranscationType(String serviceType) {
		String transactionType = null;
		switch (serviceType) {
			case "airtimeServiceInfo":
				transactionType = "AIRTIME";
				break;
			case "callingServiceInfo":
				transactionType = "CALLING CARD";
				break;
			case "electricityServiceInfo":
				transactionType = "ELECTRICITY";
				break;
			case "internetServiceInfo":
				transactionType = "INTERNET";
				break;
			case "moneyTransferServiceInfo":
				transactionType = "MONEY TRANSFER";
				break;
			case "agentServiceInfo":
				transactionType = "AGENT TRANSACTION";
				break;
			case "insuranceServiceInfo":
				transactionType = "INSURANCE";
				break;
			case "productServiceInfo":
				transactionType = "RETAIL PRODUCTS";
				break;
			case "ticketServiceInfo":
				transactionType = "EVENT TICKET";
				break;
			case "tuitionServiceInfo":
				transactionType = "PAY TUITION";
				break;
			case "tvServiceInfo":
				transactionType = "TV BILL";
				break;
			case "waterServiceInfo":
				transactionType = "WATER BILL";
				break;
			default :
				break;
		}
		return transactionType;

	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.e("ACTIVITY_STOPPED", "STOPPED");
	}

	/**
	 * used to handle results from activity started with startActivityForResult
	 *
	 * @param requestCode
	 * @param resultCode
	 * @param data:       intent with returned data e.g selected country details if the started activity was PCSelectCountryActivity
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data != null) {
			if (data.getExtras() != null && data.getExtras().containsKey("country")) {
				selectedCountry = data.getExtras().getString("country");
				if (!StringUtils.isEmpty(selectedCountry)) {
					int index = selectedCountry.indexOf("+");
					if (index != -1 && countryPicker1 != null) {
						countryPicker1.setText(selectedCountry.substring(index));
					}
				} else if(countryPickerImage != null) {
					      Bitmap selectedCountryImage = BitmapFactory.decodeResource(this.getResources(), R.id.country_picker_image);
						  countryPickerImage.setImageBitmap(selectedCountryImage );
					  }

			}
		} else  if (requestCode == REQUEST_INVITE) {
			if (resultCode == RESULT_OK) {
				// Get the invitation IDs of all sent messages
				String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
				for (String id : ids) {
					Log.d(TAG, "onActivityResult: sent invitation " + id);
				}
			} else {

				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Sending Invitation Failed")
						.setTitle("Invite Friend");

				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Log.d(clazzz, "Clicked on OK on Alert box");
					}
				});

				Dialog alert = builder.create();
				alert.show();


			}
		}
	}

	/**
	 * get logged in user currency symbol
	 *
	 * @return
	 */
	@NonNull
	public String getCurrencySymbol() {
		//get currency symbol user is using
		PCSpecialUser appUser = getAppUser();
		String currencySymbl = null;
		if (appUser != null && appUser.getBaseCurrency() != null) {
			currencySymbl = appUser.getBaseCurrency().getCurrencySymbol() + " ";
		}
		if (StringUtils.isEmpty(currencySymbl)) {
			currencySymbl = "$";
		}
		return currencySymbl;
	}

	public void straightLogout() {
		Intent intent = new Intent(this, PCAppLauncher.class);
		PCSpecialUser user = getLogedInUser();
		user.setTokenId("");
		this.saveUserDetails(user, false);
		startActivity(intent);
		finish();
	}

	protected String getCountry(PCSpecialUser user) {
		String country = null;
		if (user != null) {
			country = user.getCountry();
			if (StringUtils.isEmpty(country)) {
				PCCurrency currency = user.getBaseCurrency();
				if (currency != null) {
					country = currency.getCountry();
				}
			}
		}
		return country;
	}

	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	@Override
	public void onTaskStarted() {
		if (this.currentServiceType == PCPesabusClient.PCServiceType.LOGOUT) {
			progressDialog = ProgressDialog.show(this, "User Logout", "Logout", true);
		}
	}

	@Override
	public void onTaskCompleted(PCData data) {
		if (this.currentServiceType == PCPesabusClient.PCServiceType.LOGOUT) {
			straightLogout();
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}
	}
}

