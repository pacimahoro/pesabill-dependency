/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGeneralRequest;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCKeychainWrapper;

import java.util.Queue;

/**
 * Handles a user through resetting their password
 *
 * @author Pacifique Mahoro
 */
public class PCPasswordResetActivity extends PCBaseActivity {
	private final static String CLAZZ = PCPasswordResetActivity.class.getName();

	private ProgressDialog progressDialog;
	private EditText emailText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_activity_password_reset);
		emailText = (EditText) findViewById(R.id.email);
		PCUser user = getLogedInUser();
		emailText.setText(user.getEmail());
		try {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		} catch (NullPointerException e) {
			Log.d(CLAZZ, "Error enabling Up button");
		}

		// Listen for keyboard dismissal
		findViewById(R.id.password_reset_container).setOnClickListener(new DismissKeyboardListener(this));
	}

	public void onCodeRequestAction(View view) {
		// Get input fields

		EditText passwordText = (EditText) findViewById(R.id.password);
		EditText reEnteredPasswordText = (EditText) findViewById(R.id.re_entered_password);
		EditText codeText = (EditText) findViewById(R.id.code);

		String userName = emailText.getText().toString();
		userName = trimUserInput(userName);
		String password = passwordText.getText().toString();
		password = trimUserInput(password);
		String reEnteredPassowrd = reEnteredPasswordText.getText().toString();
		reEnteredPassowrd = trimUserInput(reEnteredPassowrd);
		String code = codeText.getText().toString();
		code = trimUserInput(code);
		try {
			this.validCustomerInfo(userName, password, reEnteredPassowrd, code);
			String pswd = "";
			// Throws NumberFormatException if the code is not an integer
			int accessCode = Integer.parseInt(code);

			// Set values: TODO ask the team if they're ok with this mechanism!
			if (TextUtils.isDigitsOnly(userName)) {
				//encypt password
				pswd = PCKeychainWrapper.securedAESEncryption(userName, pingResults.getApiKey(), passwordText.getText().toString());
			} else {
				// Encrypt the password
				pswd = PCKeychainWrapper.securedSHA256DigestHash(password, userName);
			}

			final PCGeneralRequest passwordResetRequest = new PCGeneralRequest();

			passwordResetRequest.setEmail(userName);
			passwordResetRequest.setPwd(pswd);
			passwordResetRequest.setAccessCode(accessCode);

			PCCredentialsController credentialsController = (PCCredentialsController)
					PCControllerFactory.constructController(
							PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, this);
			if (credentialsController != null) {
				credentialsController.setActivity(this);
				credentialsController.setServiceType(PCPesabusClient.PCServiceType.RESET_PASSWORD);
				credentialsController.execute(passwordResetRequest);
			}
		} catch (NumberFormatException numFormatExc) {
			PCGenericError error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.code_should_be_numeric));
			error.setField("code");
			this.presentError(error, this.getResources().getString(R.string.password_reset_error_title));
		} catch (Throwable exc) {
			Log.e(CLAZZ, "Could not handle the Password Reset process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError) exc;
			} else {
				error.setMessage(exc.getMessage());
			}
			this.presentError(error, "Password Rest Error");
		}
	}


	public void validCustomerInfo(String email, String password, String reEnteredPassowrd, String code) throws PCGenericError {
		PCGenericError error;
		if (email == null || email.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.password_recovery_missing_email));
			error.setField("email");
			throw error;
		}

		if (password == null || password.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.password_reset_missing_password));
			error.setField("password");
			throw error;
		}

		if (reEnteredPassowrd == null || reEnteredPassowrd.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.re_enter_password_reset_missing_password));
			error.setField("re_entered_password");
			throw error;
		}

		if (!password.equalsIgnoreCase(reEnteredPassowrd)) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.passwords_should_match));
			error.setField("password");
			throw error;
		}

		if (code == null || code.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.password_reset_missing_code));
			error.setField("code");
			throw error;
		}
	}

	@Override
	public void onTaskStarted() {
		progressDialog = ProgressDialog.show(this, "Password Reset", "Please wait while we reset new password", true);
	}

	@Override
	public void onTaskCompleted(PCData data) {
		if (data != null) {
			if (data.getErrorMessage() != null && !"".equals(data.getErrorMessage())) { // service call failed
				Log.e(CLAZZ, "An error occured: [" + data.getErrorMessage() + "]");
				PCGenericError error = new PCGenericError();
				error.setMessage(data.getErrorMessage());
				this.presentError(error, this.getResources().getString(R.string.password_reset_error_title));
			} else {
				final PCSpecialUser user = (PCSpecialUser) data;
				this.saveUserDetails(user, true);
				Log.d(CLAZZ, "Returning to main activity");
				Intent intent = new Intent(this, PCMainTabActivity.class);
				intent.putExtra(PCPesachoiceConstant.USER_INTENT_EXTRA, user);
				startActivity(intent);
			}
		} else {
			PCGenericError error = new PCGenericError();
			error.setMessage("Could not reset your password. Please contact support.");
			this.presentError(error, this.getResources().getString(R.string.password_reset_error_title));
		}

		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {
		//TODO
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
