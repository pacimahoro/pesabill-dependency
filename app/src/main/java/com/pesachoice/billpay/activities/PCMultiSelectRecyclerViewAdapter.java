package com.pesachoice.billpay.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pesachoice.billpay.SelectableAdapter;
import com.pesachoice.billpay.model.PCContactInfo;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * UI Adapter for list of friends/contact to invite and recommend them to use pesachoice
 *
 * Created by desire.aheza on 8/2/2016.
 */
public class PCMultiSelectRecyclerViewAdapter extends SelectableAdapter<PCMultiSelectRecyclerViewAdapter.ViewHolder> {

	private ArrayList<PCContactInfo> mArrayList;
	private Context mContext;
	private ViewHolder.ClickListener clickListener;
	private List<String> mSelectedItem = new ArrayList<>();
	// declare the builder object once..withBorder(4)
	private TextDrawable.IBuilder builder = TextDrawable.builder()
			.beginConfig()
			.useFont(Typeface.DEFAULT)
			.fontSize(30)
			.toUpperCase()
			.endConfig()
			.round();


	public PCMultiSelectRecyclerViewAdapter(Context context, ArrayList<PCContactInfo> arrayList, ViewHolder.ClickListener clickListener) {
		this.mArrayList = arrayList;
		this.mContext = context;
		this.clickListener = clickListener;

	}

	// Create new views
	@Override
	public PCMultiSelectRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {

		View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.pc_contact_listitem, null);

		ViewHolder viewHolder = new ViewHolder(itemLayoutView, clickListener);

		return viewHolder;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {

		PCContactInfo contactInfo = mArrayList.get(position);
		viewHolder.contactName.setText(contactInfo.getDisplayName());
		viewHolder.contactPhoneNumber.setText(contactInfo.getPhoneNumber());
		ColorGenerator generator = ColorGenerator.MATERIAL;
		int color1 = generator.getRandomColor();
		String contactName = contactInfo.getDisplayName();
		// reuse the builder specs to create multiple drawables
		TextDrawable ic1 = builder.build((contactName.length() >= 2 ? contactName.substring(0, 2) : contactName.substring(0, 1)), color1);

		viewHolder.contactPhoto.setImageDrawable(ic1);

		viewHolder.selectedItem.setVisibility(isSelected(position) ? View.VISIBLE : View.GONE);

	}

	@Override
	public int getItemCount() {
		return mArrayList.size();
	}

	public List<PCContactInfo> getItems() {
		return mArrayList;
	}

	public List<String> getSelectedContacts() {
		for (Integer i : getSelectedItems()) {
			// set selected item and remove special chars in phone number
			mSelectedItem.add(mArrayList.get(i) != null ? PCGeneralUtils.cleanPhoneString(mArrayList.get(i).getPhoneNumber()) : null);
		}
		return mSelectedItem;
	}

	public void addAll(List<PCContactInfo> contacts) {
		if (null != mArrayList) {
			mArrayList.clear();
			mArrayList.addAll(contacts);
			notifyDataSetChanged();
		}

	}

	public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
		public TextView contactName;
		public TextView contactPhoneNumber;
		public ImageView contactPhoto;
		private ClickListener listener;
		private ImageView selectedItem;

		public ViewHolder(View itemLayoutView, ClickListener listener) {
			super(itemLayoutView);

			this.listener = listener;

			contactName = (TextView) itemLayoutView.findViewById(R.id.textView_contact_name);
			contactPhoneNumber = (TextView) itemLayoutView.findViewById(R.id.textView_contact_number);
			contactPhoto = (ImageView) itemLayoutView.findViewById(R.id.imageView_contact_photo);
			selectedItem = (ImageView) itemLayoutView.findViewById(R.id.imageViewSelectedItem);
			itemLayoutView.setOnClickListener(this);

			itemLayoutView.setOnLongClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (listener != null) {
				listener.onItemClicked(getAdapterPosition());
			}
		}

		@Override
		public boolean onLongClick(View view) {
			if (listener != null) {
				return listener.onItemLongClicked(getAdapterPosition());
			}
			return false;
		}

		public interface ClickListener {
			public void onItemClicked(int position);

			public boolean onItemLongClicked(int position);
		}
	}
}