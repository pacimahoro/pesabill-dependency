/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */


package com.pesachoice.billpay.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.pesachoice.billpay.activities.helpers.DismissKeyboardListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGeneralRequest;
import com.pesachoice.billpay.model.PCGenericError;

import java.util.Queue;


/**
 * PCPasswordRecovery activity handles user requesting an access code
 * to reset their forgottten password.
 *
 * @author Pacifique Mahoro
 */

public class PCPasswordRecoveryActivity extends PCBaseActivity {
	private final static String CLAZZ = PCPasswordRecoveryActivity.class.getName();
	private EditText phoneText;
	private EditText emailText;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_activity_password_recovery);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
		setSupportActionBar(toolbar);
		// Get input fields
		phoneText = (EditText) findViewById(R.id.phone);
		emailText = (EditText) findViewById(R.id.email);
		phoneText.setText(getLogedInUser().getPhoneNumber());
		emailText.setText(getLogedInUser().getEmail());

		try {
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		} catch (NullPointerException e) {
			Log.d(CLAZZ, "Error enabling Up button");
		}

		// Listen for keyboard dismissal
		findViewById(R.id.password_recovery_container).setOnClickListener(new DismissKeyboardListener(this));
	}

	public void onCodeRequestAction(View view) {

		String email = emailText.getText().toString();
		email = trimUserInput(email);
		String phone = phoneText.getText().toString();
		phone = trimUserInput(phone);
		try {

			this.validCustomerInfo(email, phone);

			final PCGeneralRequest forgotPasswordRequest = new PCGeneralRequest();
			forgotPasswordRequest.setEmail(email);
			forgotPasswordRequest.setFeatures(new String[]{"password"});
			forgotPasswordRequest.setPhoneNumber(phone);
			PCCredentialsController credentialsController = (PCCredentialsController)
					PCControllerFactory.constructController(
							PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, this);
			if (credentialsController != null) {
				credentialsController.setActivity(this);
				credentialsController.setServiceType(PCPesabusClient.PCServiceType.FORGOT_PASSWORD);
				credentialsController.execute(forgotPasswordRequest);
			}
		} catch (Throwable exc) {
			Log.e(CLAZZ, "Could not handle the Password Recovery process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError) exc;
			} else {
				error.setMessage(exc.getMessage());
			}
			this.presentError(error, "Password Recovery Error");
		}

		// TODO: Take the next step
	}


	public void validCustomerInfo(String email, String phone) throws PCGenericError {
		PCGenericError error;
		//TODO: verify non-printable characters such as when someone enters spaces only,...
		if (email == null || email.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.password_recovery_missing_email));
			error.setField("email");
			throw error;
		}

		if (phone == null || phone.length() == 0) {
			error = new PCGenericError();
			error.setMessage(this.getResources().getString(R.string.password_recovery_missing_phone));
			error.setField("phone");
			throw error;
		}
	}

	@Override
	public void onTaskStarted() {
		progressDialog = ProgressDialog.show(this, "Requesting to reset password", "Please wait while we start process to reset password", true);
	}

	@Override
	public void onTaskCompleted(PCData data) {
		if (data != null) {
			if (data.getErrorMessage() != null && !"".equals(data.getErrorMessage())) { // service call failed
				Log.e(CLAZZ, "An error occured: [" + data.getErrorMessage() + "]");
				PCGenericError error = new PCGenericError();
				error.setMessage(data.getErrorMessage());
				this.presentError(error, this.getResources().getString(R.string.password_recovery_error_title));
			} else { // success
				Log.d(CLAZZ, "Open password reset activity");
				Intent intent = new Intent(this, PCPasswordResetActivity.class);
				startActivityForResult(intent, PASSWORD_RESET_ACTIVITY_REQUEST_CODE);
			}
		}
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {
		//TODO
	}
}
