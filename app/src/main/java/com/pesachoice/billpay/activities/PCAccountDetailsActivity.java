package com.pesachoice.billpay.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.appinvite.FirebaseAppInvite;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.fragments.PCActivityDetailFragment;
import com.pesachoice.billpay.fragments.PCCardPaymentFragment;
import com.pesachoice.billpay.fragments.PCContactsFragment;
import com.pesachoice.billpay.fragments.PCMyEventTicketFragment;
import com.pesachoice.billpay.fragments.PCProductFAQFragment;
import com.pesachoice.billpay.fragments.PCReportProblemFragment;
import com.pesachoice.billpay.fragments.PCWebViewFragment;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import java.util.Queue;

/**
 * This activity will be used to display fragments that show information related to the user account
 */
public class PCAccountDetailsActivity extends PCBaseActivity   {

	private String pesachoiceHelpUrl = "";

	private PCBillPaymentData pcBillPaymentData = null;

		private static final String TAG = PCAccountDetailsActivity.class.getSimpleName();
		private static final int REQUEST_INVITE = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			setContentView(R.layout.pc_activity_account_details);
			Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
			myToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
			setSupportActionBar(myToolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			String optionName = getIntent().getStringExtra(PCPesabusClient.OPTION);
			setupLayout(myToolbar, optionName);

/**
 * receiving invitation using firebase dynamic Links
 */
			FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
					.addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
						@Override
						public void onSuccess(PendingDynamicLinkData data) {
							if (data == null) {
								Log.d(TAG, "getInvitation: no data");
								return;
							}

							// Get the deep link
							Uri deepLink = data.getLink();

							// Extract invite
							FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(data);
							if (invite != null) {
								String invitationId = invite.getInvitationId();
							}

							// Handle the deep link
							// [START_EXCLUDE]
							Log.d(TAG, "deepLink:" + deepLink);
							if (deepLink != null) {
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setPackage(getPackageName());
								intent.setData(deepLink);

								startActivity(intent);
							}
							// [END_EXCLUDE]
						}
					})
					.addOnFailureListener(this, new OnFailureListener() {
						@Override
						public void onFailure(@NonNull Exception e) {
							Log.w(TAG, "getDynamicLink:onFailure", e);
						}
					});

	}

	protected void setupLayout(Toolbar myToolBar, String optionName) {
		Button sendReport = (Button) myToolBar.findViewById(R.id.button_send);
		TextView title = (TextView) myToolBar.findViewById(R.id.textView_action);
		title.setText(optionName);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		switch (optionName) {
			case PCPesabusClient.OPTION_PAYMENT:
				PCCardPaymentFragment paymentFragment = new PCCardPaymentFragment();
				ft.add(R.id.fragment_container, paymentFragment, PCPesabusClient.OPTION_PAYMENT);
				ft.commit();

				break;
			case PCPesabusClient.OPTION_REPORT_PROBLEM:
				sendReport.setVisibility(View.VISIBLE);
				Fragment reportProblemFragment = new PCReportProblemFragment();
				ft.add(R.id.fragment_container, reportProblemFragment, PCPesabusClient.OPTION_REPORT_PROBLEM);
				ft.commit();
				break;
			case PCPesabusClient.OPTION_HELP:
				PCProductFAQFragment faqsFragment = new PCProductFAQFragment();
				ft.add(R.id.fragment_container, faqsFragment, PCPesabusClient.OPTION_HELP);
				ft.commit();
				break;
			case PCPesabusClient.OPTION_PRIVACY:
				pesachoiceHelpUrl = PCPesabusClient.PRIVACY_URL;
				setUpWebViewFragment(ft);
				break;
			case PCPesabusClient.OPTION_TERMS_OF_SERVICES:
				pesachoiceHelpUrl = PCPesabusClient.TERMS_OF_SERVICE_URL;
				setUpWebViewFragment(ft);
				break;
			case PCPesabusClient.OPTION_CONTACT_US:
				pesachoiceHelpUrl = PCPesabusClient.CONTACT_URL;
				setUpWebViewFragment(ft);
				break;
			case PCPesabusClient.ACTIVITY_ITEM_CLICK:
				Fragment activityReportFragment = new PCActivityDetailFragment();
				pcBillPaymentData = (PCBillPaymentData) getIntent().getSerializableExtra("activity");
				ft.add(R.id.fragment_container, activityReportFragment, PCPesabusClient.ACTIVITY_ITEM_CLICK);
				ft.commit();
				break;
			case PCPesabusClient.OPTION_CONTACTS_FROM_PHONE:
				PCContactsFragment contactsFragment = new PCContactsFragment();
				ft.add(R.id.fragment_container, contactsFragment, PCPesabusClient.OPTION_CONTACTS_FROM_PHONE);
				ft.commit();
				break;
			case PCPesabusClient.OPTION_SHOW_MY_TICKETS:
				PCMyEventTicketFragment eventTicketFragment = new PCMyEventTicketFragment();
				ft.add(R.id.fragment_container, eventTicketFragment, PCPesabusClient.OPTION_SHOW_MY_TICKETS);
				ft.commit();
				break;
			default:
				break;
		}
	}

	private void setUpWebViewFragment(FragmentTransaction ft) {
		Fragment loadPrivacyPageFragment = new PCWebViewFragment();
		ft.add(R.id.fragment_container, loadPrivacyPageFragment);
		ft.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.account_detail_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
	    if (menuItem.getItemId() == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        else if (menuItem.getItemId() == R.id.menu_invite_trigger) {
            inviteFriendUsingFirebase ();
            return true;
        }

		return super.onOptionsItemSelected(menuItem);
	}

	private void inviteFriend() {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out Pesachoice app for your smartphone. Download it today from http://www.pesachoice.com/#home");
		sendIntent.setType("text/plain");
		Intent chooser = Intent.createChooser(sendIntent, "Invite friends");
		if (sendIntent.resolveActivity(getPackageManager()) != null) {
			startActivity(chooser);

		}
	}

	/**
	 * This method invite friend to install pesachoice app using firebase
	 */
	public void inviteFriendUsingFirebase() {
		Intent sendIntent = new AppInviteInvitation.IntentBuilder("Invite Friends")
				.setMessage(getResources().getString(R.string.invite_message))
				.setDeepLink(Uri.parse(getResources().getString(R.string.link_Details)))
				.setCustomImage(Uri.parse(getResources().getString(R.string.google_image_url)))
				.setCallToActionText("Install!")
				.build();

		startActivityForResult(sendIntent, REQUEST_INVITE);
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

		if (requestCode == REQUEST_INVITE) {
			if (resultCode == RESULT_OK) {
				// Get the invitation IDs of all sent messages
				String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
				for (String id : ids) {
					Log.d(TAG, "onActivityResult: sent invitation " + id);
				}
			} else {
				// Sending failed or it was canceled, show failure message to the user
				showMessage("Sending Invitation Failed");

			}
		}
	}


	private void showMessage(String msg) {
		Context context = getApplicationContext();
		CharSequence text = msg;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	public String getURL() {
		return pesachoiceHelpUrl;
	}

	public PCBillPaymentData getPcBillPaymentData() {
		return pcBillPaymentData;
	}

	@Override
	public void onTaskCompleted(Queue<? extends PCData> data) {

	}

	@Override
	public void onTaskStarted() {
		if (currentServiceType == PCPesabusClient.PCServiceType.PAYMENT_PROFILE ||
				currentServiceType == PCPesabusClient.PCServiceType.REPLACE_CARD) {
			progressDialog = ProgressDialog.show(this, "Card Attachment", "Saving", true);
		}
	}

	@Override
	public void onTaskCompleted(PCData data) {
		if (data != null && data.getErrorMessage() != null && data.getErrorMessage().length() > 0) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			error.setMessage(data.getErrorMessage());
			this.presentError(error, "Error");
			return;
		}
		if (currentServiceType == PCPesabusClient.PCServiceType.PAYMENT_PROFILE ||
				currentServiceType == PCPesabusClient.PCServiceType.REPLACE_CARD) {
			PCCustomerMetadata cardMeta = (PCCustomerMetadata) data;
			if (this.appUser != null && cardMeta != null) {
				this.appUser.setMaskedCardNumber(cardMeta.getMaskedCardNumber());
			}

			this.onBackPressed();
			finish();
		}
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}




}
