package com.pesachoice.billpay.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pesachoice.billpay.business.PCPesachoiceConstant;

/**
 * Handle push notifications
 */
public class PCNotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pc_notification_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pesa Notifications");
        //setup the back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ListView notificationsView=(ListView)findViewById(R.id.notifications);
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
                String[] notifications = (String[]) objBundle;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.pc_line_item_with_line_below,R.id.textViewNotification,notifications);
                notificationsView.setAdapter(adapter);
            }
        }
    }
}