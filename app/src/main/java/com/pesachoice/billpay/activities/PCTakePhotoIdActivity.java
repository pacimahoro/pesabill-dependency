package com.pesachoice.billpay.activities;

import android.*;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.pesachoice.billpay.activities.helpers.PCPhotoUpload;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCSavePhotoMetadataController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMetadata;
import com.pesachoice.billpay.model.PCSavePhotoIDMetadataRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;
import com.pesachoice.billpay.utils.PCProfilePictureUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Activity being used to help take user photo id
 * @author desire.aheza
 */
public class PCTakePhotoIdActivity extends AppCompatActivity implements PCPhotoUpload, PCAsyncListener {

	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
	final static String TAG = "PCTakePhotoIdActivity";
	private String userName = null;
	protected Bitmap mImageBitmap;
	protected Button takePhoto = null;
	private String photoIdName = "";
	private PCTakePhotoIdActivity cameraActivity = null;
	private String mCurrentPhotoPath;
	protected PCSpecialUser appUser = null;
	protected ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pc_take_photo_id_activity);
		takePhoto = (Button) findViewById(R.id.take_id_photo);
		cameraActivity = this;

		Intent intent = getIntent();

		if (intent != null) {
			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				Object objBundle = bundle.get(PCPesachoiceConstant.USER_INTENT_EXTRA);
				appUser = (PCSpecialUser) objBundle;
				if (appUser != null) {
					userName = appUser.getUserName();
				}
			}

		}

		if(takePhoto != null) {
			takePhoto.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					requestExternalStoragePermission();
				}

			});

		}

		requestExternalStoragePermission();
	}

	protected void sendProfilePicture() {
		try {
			if (mImageBitmap != null) {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
				byte[] byteArray = stream.toByteArray();
				//upload photo to FILEBASE
				PCProfilePictureUtil.initializeBucket(cameraActivity);
				photoIdName = userName + "_" + System.nanoTime();
				PCProfilePictureUtil.uploadPhoto(photoIdName, byteArray);
			}
		} catch (Exception e) {
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			e.printStackTrace();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	/**
	 * this is used to open camera intent
	 */
	private void startCameraIntent() {

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (cameraIntent.resolveActivity(getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File
				Log.i(TAG, "IOException");
			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
				startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
				/*try {
					Uri photoURI = FileProvider.getUriForFile(PCTakePhotoIdActivity.this,
							BuildConfig.APPLICATION_ID + ".provider",
							photoFile);
					cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);//Uri.fromFile(photoFile)
					startActivityForResult(cameraIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
				} catch (Exception e){
					e.printStackTrace();
				}*/
			}
		}
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
				imageFileName,  // prefix
				".jpg",         // suffix
				storageDir      // directory
		);

		// Save a file: path for use with ACTION_VIEW intents
		mCurrentPhotoPath = "file:" + image.getAbsolutePath();
		return image;
	}


	protected void requestExternalStoragePermission() {
		List<String> permissionNeeded=new ArrayList<>();
		// Here, thisActivity is the current activity
		if (ContextCompat.checkSelfPermission(this,
				android.Manifest.permission.WRITE_EXTERNAL_STORAGE )
				!= PackageManager.PERMISSION_GRANTED) {

			permissionNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
		}
		if (ContextCompat.checkSelfPermission(this,
				android.Manifest.permission.CAMERA )
				!= PackageManager.PERMISSION_GRANTED) {
			permissionNeeded.add(android.Manifest.permission.CAMERA);
		}
		if(permissionNeeded!=null && permissionNeeded.size() >0){
			ActivityCompat.requestPermissions(this,
					permissionNeeded.toArray(new String[permissionNeeded.size()]),
					CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		}
		else{
			//take picture
			startCameraIntent();
		}
	}




	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
			try {
				mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(mCurrentPhotoPath));
				//showPhotoImage.setImageBitmap(mImageBitmap);
				if (mImageBitmap != null) {

					onStartUploadingPhoto();
					sendProfilePicture();


				} else {
					Toast.makeText(cameraActivity, "no picture taken", Toast.LENGTH_LONG).show();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onStartUploadingPhoto() {
		progressDialog = ProgressDialog.show(this, "Uploading", "Uploading", true);

	}

	@Override
	public void onFinishedPhotoUploading(Uri uri) {

		if (uri != null) {
			sendRequestToSavePhotoIDMetadata(uri.toString());
		} else {
			dismissDialogAndBack();
		}
	}

	private void dismissDialogAndBack() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}

		cameraActivity.onBackPressed();
	}

	protected void sendRequestToSavePhotoIDMetadata(String photo_id_path) {
		try {
			PCSavePhotoIDMetadataRequest request = new PCSavePhotoIDMetadataRequest();
			request.setUser(appUser);
			PCMetadata metadata = new PCMetadata();
			metadata.setName(PCProfilePictureUtil.main_forlder + "/" + photoIdName);
			metadata.setDownloadURL(photo_id_path);
			request.setPhotoMetadata(metadata);
			PCSavePhotoMetadataController savePhotoMetadataController = (PCSavePhotoMetadataController) PCControllerFactory.constructController(
					PCControllerFactory.PCControllerType.SAVE_PHOTO_METADATA, this);
			if (savePhotoMetadataController != null) {
				savePhotoMetadataController.setActivity(this);
				savePhotoMetadataController.setServiceType(PCPesabusClient.PCServiceType.SAVE_PHOTO_METADATA);
				savePhotoMetadataController.execute(request);
			}
		} catch (Throwable exc) {
			Log.e(TAG, "Could not handle Saving Photo metadata process because [" + exc.getMessage() + "]");
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			PCGenericError error = new PCGenericError();
			if (exc instanceof PCGenericError) {
				error = (PCGenericError)exc;
			}
			else
				error.setMessage(exc.getMessage());
			presentError(error, "Error while saving photo metadata");
		}
	}

	@Override
	public void onTaskStarted() {

	}

	@Override
	public void onTaskCompleted(PCData data) {
		final PCUser user = (PCUser) data;
		if (user.getErrorMessage() != null && !"".equals(user.getErrorMessage())) { // service call failed
			Log.e(PCTakePhotoIdActivity.class.getName(), "An error occured: [" + user.getErrorMessage() + "]");
			PCGenericError error = new PCGenericError();
			error.setMessage(user.getErrorMessage());
			this.presentError(error, "Error when saving photo metadata");
		}
		// close dialog and go back to main
		dismissDialogAndBack();

	}

	public void presentError(PCGenericError error, String title) {
		String message = error.getMessage();
		if (isFinishing() || message == null) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setTitle(title);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Log.d(PCTakePhotoIdActivity.class.getName(), "Clicked on OK on Alert box");
			}
		});

		Dialog alert = builder.create();
		alert.show();
	}
}
