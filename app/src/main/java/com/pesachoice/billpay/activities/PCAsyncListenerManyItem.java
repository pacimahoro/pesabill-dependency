package com.pesachoice.billpay.activities;

import com.pesachoice.billpay.model.PCData;

import java.util.Queue;

/**
 * Created by desire.aheza on 12/13/2015.
 */
public interface PCAsyncListenerManyItem extends PCAsyncListener {
    /**
     * This used by components to do post-processing work after a call to a particular service.
     * Anything that a component needs to do with results from the service is implemented in this
     * method.
     * @param data result data need for further processing
     */

    void onTaskCompleted(Queue<? extends PCData> data);
}
