/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Holds operators data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCOperator extends PCUser {
    public static final String TIGO_RWANDA = "TIGO Rwanda Network";
    public static final String MTN_RWANDA = "MTN Rwanda Network";
    public static final String ONATEL_BURUNDI = "ONATELL MOBILE GSM Network";
    public static final String ECONET_BURUNDI = "ECONET GSM Network";
    public static final String AIRTEL = "AIRTEL Network";
    public static final String UMEME = "UMEME";
    public static final String DSTV = "DSTV";
    public static final String ORANGE_INTERNET = "Orange-Internet";

    private String name;               // Name of the service provider
    private ActionType[] services;     // Services offered (could be electricity, internet, water, airtime, tuition, tv, ...)
    private PCLocation headQuarters;   // Location of the service provider
    private long minAmount;            // Minimum amount that can be sent in a single transaction
    private long maxAmount;            // Maximum amount that can be sent in a single transaction
    private long multiplierAmount;     // base multiplier amount
    private List<PCPrice> prices;      // price list for this service provider

    // TODO: Decide the best place for these 3rd party fields later.
    private String durationTime;
    private String durationType;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PCLocation getHeadQuarters() {
        return headQuarters;
    }

    public void setHeadQuarters(PCLocation headQuarters) {
        this.headQuarters = headQuarters;
    }

    public ActionType[] getServices() {
        return services;
    }

    public void setServices(ActionType[] services) {
        this.services = services;
    }

    public final long getMinAmount() {
        return minAmount;
    }

    public final void setMinAmount(long minAmount) {
        this.minAmount = minAmount;
    }

    public final long getMaxAmount() {
        return maxAmount;
    }

    public final void setMaxAmount(long maxAmount) {
        this.maxAmount = maxAmount;
    }

    public final long getMultiplierAmount() {
        return multiplierAmount;
    }

    public final void setMultiplierAmount(long multiplierAmount) {
        this.multiplierAmount = multiplierAmount;
    }

    public final List<PCPrice> getPrices() {
        return prices;
    }

    public final void setPrices(List<PCPrice> prices) {
        this.prices = prices;
    }

    public boolean hasService(ActionType actionType) {
        boolean result = false;
        for (ActionType serviceType : this.services) {
            if (serviceType == actionType) {
                result = true;
            }
        }

        return result;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getDurationType() {
        return durationType;
    }

    public void setDurationType(String durationType) {
        this.durationType = durationType;
    }
}
