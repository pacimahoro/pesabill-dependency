/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.util.StringUtils;

import java.sql.Timestamp;

/**
 * Generic activity
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCActivity extends PCData {
    private Long transactionId;		// before transaction identifier
    private Long outTransactionId;  // after service has completed identifier
    private Timestamp createdTime;

    /*
     * Enumerates all types of payment that are handled
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public enum PCPaymentProcessingType {
        MONEY_TRANSFER("Money Transfer"),
        INTERNET_BILL("Internet"),
        ELECTRICITY_BILL("Electricity"),
        TV_BILL("TV Bill"),
        TUITION ("Tuition"),
        AIR_TIME ("Airtime"),
        CALLING("Calling Card"),
        WATER("Water"),
        EVENT_TICKETS("Pay Events"),
        RETAIL_PRODUCTS("Sell"),
        AGENT_TRANSACTION("Agent Transaction"),
       // INSURANCE("Agent Transaction"),
        DEFAULT("");


        private final String processingType;

        /**
         * @param proceType
         */
        private PCPaymentProcessingType(final String proceType) {
            this.processingType = proceType;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return processingType;
        }

        @JsonCreator
        public static PCPaymentProcessingType transform(final String processingType) {
            PCPaymentProcessingType paymentProcessingType = PCPaymentProcessingType.DEFAULT;
            PCPaymentProcessingType[] processingTypes = PCPaymentProcessingType.values();
            for ( PCPaymentProcessingType pType : processingTypes) {
                if (pType.toString().equalsIgnoreCase(processingType)) {
                    paymentProcessingType = pType;
                    break;
                }
            }

            return paymentProcessingType;
        }

        public static PCPaymentProcessingType getProcessingType(String serviceInfo){
            PCPaymentProcessingType processingType=null;
            switch (serviceInfo) {
                case "airtimeServiceInfo":
                    processingType = PCPaymentProcessingType.AIR_TIME;
                    break;
                case "callingServiceInfo":
                    processingType = PCPaymentProcessingType.CALLING;
                    break;
                case "electricityServiceInfo":
                    processingType = PCPaymentProcessingType.ELECTRICITY_BILL;
                    break;
                case "internetServiceInfo":
                    processingType = PCPaymentProcessingType.INTERNET_BILL;
                    break;
                case "moneyTransferServiceInfo":
                    processingType = PCPaymentProcessingType.MONEY_TRANSFER;
                    break;
                case "productServiceInfo":
                    processingType = PCPaymentProcessingType.RETAIL_PRODUCTS;
                    break;
                case "ticketServiceInfo":
                    processingType = PCPaymentProcessingType.EVENT_TICKETS;
                    break;
                case "tuitionServiceInfo":
                    processingType = PCPaymentProcessingType.TUITION;
                    break;
                case "tvServiceInfo":
                    processingType = PCPaymentProcessingType.TV_BILL;
                    break;
                case "waterServiceInfo":
                    processingType = PCPaymentProcessingType.WATER;
                    break;
                case "agentServiceInfo":
                    processingType = PCPaymentProcessingType.AGENT_TRANSACTION;
                    break;
                default:
                    break;
            }
            return processingType;
        }
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getOutTransactionId() {
        return outTransactionId;
    }

    public void setOutTransactionId(Long outTransactionId) {
        this.outTransactionId = outTransactionId;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

}
