/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * Contains the most generic/common data that most objects should be sharing.
 *
 * @author Odilon Senyana
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCData implements Serializable {
	protected String id;
	protected String classType;
	protected String errorMessage;
	protected String actionType;
	protected String userAgent;
	protected String apiLocation;
	protected PCLinks links;
	protected volatile Boolean success;

	/**
	 * Enumerates actions that can be taken
	 */
	@JsonIgnoreProperties(ignoreUnknown = true)
	public enum ActionType {
		PING,
		COUNTRY_PROFILE,
		AUTHORIZATION,
		RESET,
		SIGNUP,
		VERIFICATION,
		CARD,
		USER,
		INSURANCE,
		AGENT_TRANSACTION,
		PAYMENT,
		TUITION,
		ELECTRICITY,
		TV,
		WATER,
		AIRTIME,
		INTERNET,
		MONEY_TRANSFER,
		TICKETS,
		SELL,
		CREATE_USER,
		ALL_SERVICES;
	}

	public static String getActionType(PCActivity.PCPaymentProcessingType processingType) {
		ActionType actionType = null;

		switch (processingType) {
			case AIR_TIME :
				actionType = ActionType.AIRTIME;
				break;

            case AGENT_TRANSACTION:
				actionType = ActionType.AGENT_TRANSACTION;
				break;
			case MONEY_TRANSFER :
				actionType = ActionType.MONEY_TRANSFER;
				break;
			case INTERNET_BILL :
				actionType = ActionType.INTERNET;
				break;
			case ELECTRICITY_BILL :
				actionType = ActionType.ELECTRICITY;
				break;
			case TV_BILL :
				actionType = ActionType.TV;
				break;
			case TUITION :
				actionType = ActionType.TUITION;
				break;
			case CALLING :
				actionType = ActionType.CARD;
				break;
			case WATER :
				actionType = ActionType.WATER;
				break;
			case EVENT_TICKETS :
				actionType = ActionType.TICKETS;
				break;
			case RETAIL_PRODUCTS :
				actionType = ActionType.SELL;
				break;
			default :
				actionType = ActionType.ALL_SERVICES;
				break;
		}
		return actionType != null ? actionType.toString() : null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public Boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getApiLocation() {
		return apiLocation;
	}

	public void setApiLocation(String apiLocation) {
		this.apiLocation = apiLocation;
	}

	@JsonIgnore
	public PCLinks getLinks() {
		return links;
	}

	public void setLinks(PCLinks links) {
		this.links = links;
	}
}
