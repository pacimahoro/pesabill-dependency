/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * represents a merchants profile
 * @author Pacifique Mahoro
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCMerchantsProfile  extends PCData {

    private List<PCOperator> merchants;
    private PCLinks links;
    private String currencySymbol;
    private String country;
    private String actionType;

    @Override
    public PCLinks getLinks() {
        return links;
    }

    @Override
    public void setLinks(PCLinks links) {
        this.links = links;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String getActionType() {
        return actionType;
    }

    @Override
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public List<PCOperator> getMerchants() {
        return merchants;
    }

    public void setMerchants(List<PCOperator> merchants) {
        this.merchants = merchants;
    }

    public List<String> getOperatorNamesForService(ActionType actionType) {
        List<String> operatorNames = new ArrayList<>();

        if (this.merchants == null || this.merchants.size() == 0) {
            return operatorNames;
        }

        for (PCOperator operator : this.merchants) {
            if (operator.hasService(actionType)) {
                operatorNames.add(operator.getName());
            }
        }

        return operatorNames;
    }

    private List<String> products;
}

