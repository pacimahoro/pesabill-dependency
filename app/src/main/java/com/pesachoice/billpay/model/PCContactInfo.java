package com.pesachoice.billpay.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * holds data for a contact
 * Created by desire.aheza on 1/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCContactInfo implements Parcelable {

    public PCContactInfo(String phoneNumber, String displayName, String photoPath) {
        this.phoneNumber = phoneNumber;
        this.displayName = displayName;
        this.photoPath = photoPath;
    }

    private String phoneNumber;
    private String displayName;
    private String photoPath;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    // Parcelling part
    public PCContactInfo(Parcel in){
        String[] data = new String[3];

        in.readStringArray(data);
        this.displayName = data[0];
        this.phoneNumber = data[1];
        this.photoPath = data[2];
    }

    @Override
    public int describeContents(){
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.displayName,
                this.phoneNumber,
                this.photoPath});
    }
    public static final Creator CREATOR = new Creator() {
        public PCContactInfo createFromParcel(Parcel in) {
            return new PCContactInfo(in);
        }

        public PCContactInfo[] newArray(int size) {
            return new PCContactInfo[size];
        }
    };

}