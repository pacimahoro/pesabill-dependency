/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.util.StringUtils;

/**
 * Enumeration for possible transaction statuses, which include:
 * <li>new: used whenever the transaction is still in the newly created state</li>
 * <li>success: used whenever a transaction succeeds</li>
 * <li>failure: used whenever a transaction fails</li>
 * <li>in progress: used whenever a transaction is still in progress status</li>
 * <li>cancelled: used to indicate that a transaction is cancelled</li>
 * <li>unknown: used whenever the state of the transaction is unknown</li>
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public enum  PCTransactionStatus {

    NEW("new"),
    SUCCESS("success"),
    COMPLETE("completed"),
    FAILURE("failure"),
    IN_PROGRESS("in progress"),
    CANCELLED("cancelled"),
    UNKNOWN("unknown"),
    ON_HOLD("on hold");

    private String message;

    private PCTransactionStatus(String msg) {
        this.message = msg;
    }


    /**
     * Transforms a value into a transaction enumeration object
     * @param status
     * 				status value to transform
     * @return <b>transactionStatus</b>
     * 				transaction enumeration object representing the value entered
     */
    @JsonCreator
    public static PCTransactionStatus transform(final String status) {
        PCTransactionStatus transactionStatus = null;
        if (!StringUtils.isEmpty(status)) {
            PCTransactionStatus[] transactions = PCTransactionStatus.values();
            for (PCTransactionStatus transaction : transactions) {
                if (status.equalsIgnoreCase(transaction.toString())) {
                    transactionStatus = transaction;
                    break;
                }
            }

            if (transactionStatus == null && (status.equalsIgnoreCase("in_progress") || status.equalsIgnoreCase("in progress"))) {
                transactionStatus = IN_PROGRESS;
            }
        }

        if (transactionStatus == null) {
            transactionStatus = PCTransactionStatus.UNKNOWN;
        }
        return transactionStatus;
    }

    @Override
    public String toString() {
        return this.message;
    }
}

