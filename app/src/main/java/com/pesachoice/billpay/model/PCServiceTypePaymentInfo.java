package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

/**
 * @author Desire Aheza
 * @author Pacifique Mahoro
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCServiceTypePaymentInfo {
    private BigDecimal serviceFee;
    private BigDecimal minimumAllowedAmount;
    private BigDecimal maximumAllowedAmount;
    private String currencySymbol;
    private String serviceTypeName;
    private String receiverCurrencySymbl;
    private double exchangeRate;


    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public BigDecimal getMinimumAllowedAmount() {
        return minimumAllowedAmount;
    }

    public void setMinimumAllowedAmount(BigDecimal minimumAllowedAmount) {
        this.minimumAllowedAmount = minimumAllowedAmount;
    }

    public BigDecimal getMaximumAllowedAmount() {
        return maximumAllowedAmount;
    }

    public void setMaximumAllowedAmount(BigDecimal maximumAllowedAmount) {
        this.maximumAllowedAmount = maximumAllowedAmount;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("serviceFee: [" + serviceFee + "], ");
        sb.append("minimumAllowedAmount: [" + minimumAllowedAmount + "], ");
        sb.append("maximumAllowedAmount: [" + maximumAllowedAmount + "], ");
        sb.append("currencySymbol: [" + currencySymbol + "]");

        return sb.toString();
    }

    public String getReceiverCurrencySymbl() {
        return receiverCurrencySymbl;
    }

    public void setReceiverCurrencySymbl(String receiverCurrencySymbl) {
        this.receiverCurrencySymbl = receiverCurrencySymbl;
    }

  }
