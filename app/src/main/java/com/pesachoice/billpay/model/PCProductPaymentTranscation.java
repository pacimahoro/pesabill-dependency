package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Hold product payment transaction data
 * Created by desire.aheza on 9/21/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCProductPaymentTranscation extends PCTransaction {
    public PCProductServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(PCProductServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    private PCProductServiceInfo serviceInfo;
}
