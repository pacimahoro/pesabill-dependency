/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds profile data for all operators and service providers per country
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCountryProfile extends PCData {
    private double exchangeRate;  // used as a value for exchange rate
    private double baseFee;
    private double percentageFee;
    private List<PCServiceFee> serviceFees;
    private List<PCOperator> operators;
    private PCLinks links;
    private String currencySymbol;
    private String country;
    private String actionType;
    private List<String> products;
    private List<PCCurrency> currencies;
    private double selectedCountryExchangeRate;
    private String countryPhoneCode;
    private String companyId;

    public void setSelectedCountryExchangeRate(double selectedCountryExchangeRate) {
        this.selectedCountryExchangeRate = selectedCountryExchangeRate;
    }

    //TODO: still missing a lot of properties / instance members

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public double getBaseFee() {
        return baseFee;
    }

    public void setBaseFee(double baseFee) {
        this.baseFee = baseFee;
    }

    public double getPercentageFee() {
        return percentageFee;
    }

    public void setPercentageFee(double percentageFee) {
        this.percentageFee = percentageFee;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public List<PCOperator> getOperators() {
        return operators;
    }

    public void setOperators(List<PCOperator> operators) {
        this.operators = operators;
    }


    public List<PCServiceFee> getServiceFees() {
        return serviceFees;
    }

    public void setServiceFees(List<PCServiceFee> serviceFees) {
        this.serviceFees = serviceFees;
    }

    public PCLinks getLinks() {
        return links;
    }

    public void setLinks(PCLinks links) {
        this.links = links;
    }

    public List<PCCurrency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<PCCurrency> currencies) {
        this.currencies = currencies;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }


      public String getCountryCode (){
        return  countryPhoneCode;
      }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * Compute the Fee amount
     * ***************************************************************
     * Original transaction fee calculation
     * ***************************************************************
     * The business logic formula we are using now is as following
     * 4.9% of each transaction amount plus $0.30 for processing.
     * ***************************************************************
     * New transaction fee calculation
     * ***************************************************************
     * we're checking if amount is less than $15, then if yes we're adding a 50 cents fee
     * @param usd
     * @return double
     *          The fee amount
     */
    public double computeFeeAmount(double usd) {
        return usd < PCPesachoiceConstant.PESACHOICE_MAXMUM_MONEY_TO_PAY_TRANSACTION_FEE?this.baseFee:0.0;
    }

    public double convertTolocalCurrency(double usd) {
        return this.exchangeRate * usd;
    }

    public double convertToLocalUsingCalculatedExchangeRate(double usd){
        return this.selectedCountryExchangeRate * usd;
    }


    public List<String> getOperatorNamesForService(ActionType actionType) {
        List<String> operatorNames = new ArrayList<>();

        for (PCOperator operator : this.operators) {
            if (operator.hasService(actionType)) {
                operatorNames.add(operator.getName());
            }
        }

        return operatorNames;
    }


    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "PCCountryProfile{" +
                "exchangeRate=" + exchangeRate +
                ", baseFee=" + baseFee +
                ", percentageFee=" + percentageFee +
                ", operators=" + operators +
                ", links=" + links +
                ", currencySymbol='" + currencySymbol + '\'' +
                ", country='" + country + '\'' +
                ", actionType='" + actionType + '\'' +
                ", products=" + products +
                '}';
    }
}

