/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with internet payment data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCInternetPaymentData extends PCBillPaymentData {

    private PCInternetServiceInfo internetServiceInfo;


    public PCInternetServiceInfo getInternetServiceInfo() {
        return internetServiceInfo;
    }


    public void setInternetServiceInfo(PCInternetServiceInfo internetServiceInfo) {
        super.setServiceInfo(internetServiceInfo);
        this.internetServiceInfo = internetServiceInfo;
    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo){
        this.internetServiceInfo = (PCInternetServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getInternetServiceInfo();
    }
}
