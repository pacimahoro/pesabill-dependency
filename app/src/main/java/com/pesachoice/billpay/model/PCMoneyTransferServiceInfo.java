/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds Money Transfer data
 * @author Desire AHEZA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCMoneyTransferServiceInfo extends PCServiceInfo {

    private String receiverNumber;
    private String receiverName;
    private String currencySymbol;

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverNumber) {
        this.receiverName = receiverNumber;
    }

    public String getReceiverNumber() {
        return receiverNumber;
    }

    public void setReceiverNumber(String receiverNumber) {
        this.receiverNumber = receiverNumber;
    }

    @Override
    public boolean hasNecessaryContent() {
        return false;
    }

    @Override
    public String failedMessage() {
        return null;
    }
}

