/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * General bill payment data for various services.
 *
 * @author Odilon Senyana
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PCBillPaymentData extends PCActivity implements PCTransactionable, Serializable {
	protected PCSpecialUser sender;
	protected PCSpecialUser receiver;
	protected PCPaymentProcessingType paymentProcessingType;
	protected Double usd;
	protected Double totalUsd;
	protected Double fees;
	protected PCOperator operator;
	protected String maskedCardNumber;
	protected Double totalLocalCurrency;
	protected String promoCode;
	protected Double appliedDiscount;

	protected PCPaymentInfo paymentInfo;
	protected PCServiceInfo serviceInfo;
	protected PCCurrency baseCurrency;
	protected String uniqueID;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public enum PCPaymentType {
		MOBILE_MONEY("mobile_money"),
		PAYMENT_CARD("debit_card"),
		CASH("pay_cash"),
		DEFAULT("");

		private final String paymentType;

		private PCPaymentType(final String paymentType) {
			this.paymentType = paymentType;
		}

		@JsonValue
		public String getPaymentType() {
			return paymentType;
		}

		public String getDisplayName() {
			return paymentType.replace("_", " ");
		}

		@Override
		public String toString() {
			return this.paymentType;
		}

		@JsonCreator
		public static PCPaymentType transform(final String paymentTypeStr) {
			PCPaymentType paymentType = PCPaymentType.DEFAULT;
			PCPaymentType[] paymentTypes = PCPaymentType.values();
			for (PCPaymentType p : paymentTypes) {
				if (p.toString().equalsIgnoreCase(paymentTypeStr)) {
					paymentType = p;
					break;
				}
			}

			return paymentType;
		}
	}

	public PCServiceInfo getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(PCServiceInfo serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public PCSpecialUser getSender() {
		return sender;
	}

	public void setSender(PCSpecialUser sender) {
		this.sender = sender;
	}

	public PCSpecialUser getReceiver() {
		return receiver;
	}

	public void setReceiver(PCSpecialUser receiver) {
		this.receiver = receiver;
	}

	public PCPaymentProcessingType getPaymentProcessingType() {
		return paymentProcessingType;
	}

	public void setPaymentProcessingType(
			PCPaymentProcessingType paymentProcessingType) {
		this.paymentProcessingType = paymentProcessingType;
	}

	public Double getUsd() {
		return usd;
	}

	public void setUsd(Double usd) {
		this.usd = usd;
	}

	public Double getTotalUsd() {
		return totalUsd;
	}

	public void setTotalUsd(Double totalUsd) {
		this.totalUsd = totalUsd;
	}

	public Double getFees() {
		return fees;
	}

	public void setFees(Double fees) {
		this.fees = fees;
	}

	public PCOperator getOperator() {
		return operator;
	}

	public void setOperator(PCOperator operator) {
		this.operator = operator;
	}

	public String getMaskedCardNumber() {
		return maskedCardNumber;
	}

	public void setMaskedCardNumber(String maskedCardNumber) {
		this.maskedCardNumber = maskedCardNumber;
	}

	public void setTotalLocalCurrency(Double totalLocalCurrency) {
		this.totalLocalCurrency = totalLocalCurrency;
	}

	public Double getTotalLocalCurrency() {
		return this.totalLocalCurrency;
	}

	@Override
	public PCServiceInfo retrieveServiceInfo() {
		return null;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Double getAppliedDiscount() {
		return appliedDiscount;
	}

	public void setAppliedDiscount(Double appliedDiscount) {
		this.appliedDiscount = appliedDiscount;
	}

	public PCPaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PCPaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public PCCurrency getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(PCCurrency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

}

