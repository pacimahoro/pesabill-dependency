package com.pesachoice.billpay.model;

/**
 * Created by emmy on 18/03/2018.
 */

public class PCImageData {

    private String imageName;
    private String path;
    private String createdTime;
    private String imageCategory;

//    public PCImageMetadata(String imageName, String path, String createdTime, String imageCategory) {
//        this.imageName = imageName;
//        this.path = path;
//        this.createdTime = createdTime;
//        this.imageCategory = imageCategory;
//    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getImageCategory() {
        return imageCategory;
    }

    public void setImageCategory(String imageCategory) {
        this.imageCategory = imageCategory;
    }
}
