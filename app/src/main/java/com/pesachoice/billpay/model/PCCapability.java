package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Map;

/**
 * Created by desire.aheza on 4/28/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCapability extends PCData {
    private String capabilityToken;
    private String expirationDateTime;
    private Map<Object,Object>[] additionalInfo;// array of objects (key value pairs) representing any additional information
    private String actionType;

    public String getCapabilityToken() {
        return capabilityToken;
    }

    public void setCapabilityToken(String capabilityToken) {
        this.capabilityToken = capabilityToken;
    }

    public String getExpirationDateTime() {
        return expirationDateTime;
    }

    public void setExpirationDateTime(String expirationDateTime) {
        this.expirationDateTime = expirationDateTime;
    }

    public Map<Object, Object>[] getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(Map<Object, Object>[] additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
