/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds Service Info data for internet
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCInternetServiceInfo extends PCServiceInfo {

    @Override
    public boolean hasNecessaryContent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String failedMessage() {
        // TODO Auto-generated method stub
        return null;
    }

}