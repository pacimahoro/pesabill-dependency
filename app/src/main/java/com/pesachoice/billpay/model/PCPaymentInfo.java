package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Holds information regarding a customer's payment
 *
 * @author Pacifique Mahoro
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCPaymentInfo extends PCData {
	// List of all mobile money accounts a user has on file
	private List<PCCustomerMetadata> mobileAccounts;

	private String maskedCardNumber;

	// Default payment type.
	// This is information about account (card or mobile money)
	// that will be charged if a transaction was made.
	private PCCustomerMetadata defaultSource;

	// List of all payment types.
	private List<PCCustomerMetadata> sources;

	private PCBillPaymentData.PCPaymentType paymentType;

    @JsonIgnore
	public List<PCCustomerMetadata> getMobileAccounts() {
		return mobileAccounts;
	}

	public void setMobileAccounts(List<PCCustomerMetadata> mobileAccounts) {
		this.mobileAccounts = mobileAccounts;
	}

	public PCCustomerMetadata getDefaultSource() {
		return defaultSource;
	}

	public void setDefaultSource(PCCustomerMetadata defaultSource) {
		this.defaultSource = defaultSource;
	}

	public List<PCCustomerMetadata> getSources() {
		return sources;
	}

	public void setSources(List<PCCustomerMetadata> sources) {
		this.sources = sources;
	}

	public PCBillPaymentData.PCPaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PCBillPaymentData.PCPaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getMaskedCardNumber() {
		return maskedCardNumber;
	}

	public void setMaskedCardNumber(String maskedCardNumber) {
		this.maskedCardNumber = maskedCardNumber;
	}

}
