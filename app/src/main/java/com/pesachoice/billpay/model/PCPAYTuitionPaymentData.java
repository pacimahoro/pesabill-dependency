/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * used to create tuition payment model
 * Created by desire.aheza on 2/21/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCPAYTuitionPaymentData extends PCBillPaymentData {

    private PCTuitionServiceInfo tuitionServiceInfo;

    public PCTuitionServiceInfo getTuitionServiceInfo() {
        return tuitionServiceInfo;

    }

    public void setTuitionServiceInfo(PCTuitionServiceInfo tuitionServiceInfo) {
        super.setServiceInfo(tuitionServiceInfo);
        this.tuitionServiceInfo = tuitionServiceInfo;

    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.tuitionServiceInfo = (PCTuitionServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return getTuitionServiceInfo();
    }
}
