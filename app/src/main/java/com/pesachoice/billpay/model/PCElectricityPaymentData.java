/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with electricity payment data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCElectricityPaymentData extends PCBillPaymentData {

    private PCElectricityServiceInfo electricityServiceInfo;

    public PCElectricityServiceInfo getElectricityServiceInfo() {
        return electricityServiceInfo;
    }

    public void setElectricityServiceInfo(
            PCElectricityServiceInfo electricityServiceInfo) {
        super.setServiceInfo(electricityServiceInfo);
        this.electricityServiceInfo = electricityServiceInfo;
    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.electricityServiceInfo = (PCElectricityServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getElectricityServiceInfo();
    }
}
