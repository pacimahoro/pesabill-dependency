package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by desire.aheza on 12/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCEventVerifyResponse extends PCData {
}
