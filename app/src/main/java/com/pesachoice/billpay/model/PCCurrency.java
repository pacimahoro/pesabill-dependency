package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * hold curreny details of a given country
 * @author Desire Aheza
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCurrency implements Serializable {
	private String currencySymbol;
	private String exchangeRate;
	private String country;
	private String countrySymbol;
	private List<PCServiceFee> crossBorderFees;
	private Map<String, Double> interCountryExchangeRates;

	// Use the amount and totalAmount are useful
	// especially if the currency is not USD
	private double totalAmount;
	private double amount;

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountrySymbol() {
		return countrySymbol;
	}

	public void setCountrySymbol(String countrySymbol) {
		this.countrySymbol = countrySymbol;
	}


	public List<PCServiceFee> getCrossBorderFees() {
		return crossBorderFees;
	}

	public void setCrossBorderFees(List<PCServiceFee> crossBorderFees) {
		this.crossBorderFees = crossBorderFees;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}


	public Map<String, Double> getInterCountryExchangeRates() {
		return interCountryExchangeRates;
	}

	public void setInterCountryExchangeRates(Map<String, Double> interCountryExchangeRates) {
		this.interCountryExchangeRates = interCountryExchangeRates;
	}
}
