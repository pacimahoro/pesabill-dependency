/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds a default error object
 * Created by Pacifique Mahoro on 11/1/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCGenericError extends Exception {
    private String message;
    private String field;
    private String title;
    private boolean needToGoBack = true;
    public enum ErrorType{NETWORK_CONNECTIVITY}
    private ErrorType errorType;
    private int code;

    public String getMessage() { return this.message; }
    public void setMessage(String message) { this.message = message; }

    public String getField() { return field; }
    public void setField(String field) { this.field = field; }

    public String getTitle() { return this.title; }
    public void setTitle(String title) { this.title = title; }

    public int getCode() { return code; }

    public void setCode(int code) { this.code = code; }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public boolean isNeedToGoBack() {
        return needToGoBack;
    }

    public void setNeedToGoBack(boolean needToGoBack) {
        this.needToGoBack = needToGoBack;
    }



}
