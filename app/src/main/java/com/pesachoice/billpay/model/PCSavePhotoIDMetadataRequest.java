package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * Request to save photo photoMetadata
 * Created by desire.aheza on 2/5/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCSavePhotoIDMetadataRequest implements Serializable {
    public PCSpecialUser getUser() {
        return user;
    }

    public void setUser(PCSpecialUser user) {
        this.user = user;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public PCMetadata getPhotoMetadata() {
        return photoMetadata;
    }

    public void setPhotoMetadata(PCMetadata photoMetadata) {
        this.photoMetadata = photoMetadata;
    }

    private PCSpecialUser user;
    private String actionType = "PHOTO_ID_METADATA";
    private PCMetadata photoMetadata;
}
