package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * holds details of an referral contact
 * Created by desire.aheza on 8/4/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCReferralData extends PCUser {

    private String actionType = "REFERRAL_PROGRAM";
    private boolean setup = true;
    private double accumulatedDiscount = 2;
    private double percentageDiscount;
    private List<String> referrals;
    private List<PCExcludedReferrals> excludedReferrals;

    @Override
    public String getActionType() {
        return actionType;
    }

    @Override
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public boolean isSetup() {
        return setup;
    }

    public void setSetup(boolean setup) {
        this.setup = setup;
    }

    public double getAccumulatedDiscount() {
        return accumulatedDiscount;
    }

    public void setAccumulatedDiscount(double accumulatedDiscount) {
        this.accumulatedDiscount = accumulatedDiscount;
    }

    public List<String> getReferrals() {
        return referrals;
    }

    public void setReferrals(List<String> referrals) {
        this.referrals = referrals;
    }

    public List<PCExcludedReferrals> getExcludedReferrals() {
        return excludedReferrals;
    }

    public void setExcludedReferrals(List<PCExcludedReferrals> excludedReferrals) {
        this.excludedReferrals = excludedReferrals;
    }

    public double getPercentageDiscount() {
        return percentageDiscount;
    }

    public void setPercentageDiscount(double percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
    }

}
