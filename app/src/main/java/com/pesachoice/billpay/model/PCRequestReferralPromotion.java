package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 * Referral Promotion request object for web service
 * Created by desire.aheza on 7/31/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCRequestReferralPromotion extends PCUser {

    private String promoCode;
    private String amount;

    private PCCurrency currency;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PCCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(PCCurrency currency) {
        this.currency = currency;
    }

}
