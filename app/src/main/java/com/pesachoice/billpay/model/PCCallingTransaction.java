package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by desire.aheza on 6/19/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCallingTransaction extends PCTransaction {
    private PCCallingServiceInfo serviceInfo;

    public PCCallingServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(PCCallingServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }
}
