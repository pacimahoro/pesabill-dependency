package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * details about service fees for a given transaction
 * Created by desire.aheza on 3/19/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCServiceFee implements Serializable {
    private PCBillPaymentData.PCPaymentType paymentType;
    private List<String> services;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private BigDecimal feeAmount;

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public PCBillPaymentData.PCPaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PCBillPaymentData.PCPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }
}
