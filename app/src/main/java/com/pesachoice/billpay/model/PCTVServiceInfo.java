/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds Service Info data for TV subscription
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTVServiceInfo extends PCServiceInfo {
    //FIXME: this needs to be removed in the near future because accountNumber in PCServiceInfo is used instead
    private String decoderSerialNumber;

    public String getDecoderSerialNumber() {
        return decoderSerialNumber;
    }

    public void setDecoderSerialNumber (String decoderSerialNumber) {
        this.decoderSerialNumber = decoderSerialNumber;
    }

    @Override
    public boolean hasNecessaryContent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String failedMessage() {
        // TODO Auto-generated method stub
        return null;
    }

}
