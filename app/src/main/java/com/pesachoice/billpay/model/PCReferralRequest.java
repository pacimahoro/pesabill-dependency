package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.List;

/**
 * holds details of an referral request
 * Created by desire.aheza on 8/4/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCReferralRequest implements Serializable {
    private PCUser user;
    private List<String> referrals;

    public PCUser getUser() {
        return user;
    }

    public void setUser(PCUser user) {
        this.user = user;
    }

    public List<String> getReferrals() {
        return referrals;
    }

    public void setReferrals(List<String> referrals) {
        this.referrals = referrals;
    }
}
