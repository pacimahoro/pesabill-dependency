/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Contains metadata to use as reference to Authorize.Net's database which is
 * PCI compliant and holds the card information data
 *
 * @author Odilon Senyana
 * @author Pacifique Mahoro
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public final class PCCustomerMetadata extends PCData {
    private String email;
    private String customerProfileId;
    private Long customerPaymentProfileId;
    private String maskedCardNumber;
    private String phoneNumber;
    private boolean verified;
    private PCCardInfo cardDetails;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerProfileId() {
        return customerProfileId;
    }

    public void setCustomerProfileId(String customerProfileId) {
        this.customerProfileId = customerProfileId;
    }

    public Long getCustomerPaymentProfileId() {
        return customerPaymentProfileId;
    }

    public void setCustomerPaymentProfileId(Long customerPaymentProfileId) {
        this.customerPaymentProfileId = customerPaymentProfileId;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public PCCardInfo getCardDetails() {
        return cardDetails;
    }

    public void setCardDetails(PCCardInfo cardDetails) {
        this.cardDetails = cardDetails;
    }
}
