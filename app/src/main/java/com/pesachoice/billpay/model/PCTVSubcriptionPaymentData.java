/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with TV subscription payment data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class  PCTVSubcriptionPaymentData extends PCBillPaymentData {

    private PCTVServiceInfo tvServiceInfo;


    public PCTVServiceInfo getTvServiceInfo() {
        return tvServiceInfo;
    }


    public void setTvServiceInfo(PCTVServiceInfo tvServiceInfo) {
        super.setServiceInfo(tvServiceInfo);
        this.tvServiceInfo = tvServiceInfo;
    }
    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.tvServiceInfo = (PCTVServiceInfo)serviceInfo;
    }
    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getTvServiceInfo();
    }
}
