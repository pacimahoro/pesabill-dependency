package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * holds details of an excluded referral
 * Created by desire.aheza on 8/4/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCExcludedReferrals implements Serializable {
    private String phoneNumber;
    private String message;
    private PCUser referredBy;


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PCUser getReferredBy() {
        return referredBy;
    }

    public void setReferredBy(PCUser referredBy) {
        this.referredBy = referredBy;
    }


}
