package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Hold promotion data
 * Created by desire.aheza on 7/31/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCPromotion extends PCData {
    private String email;
    private String promoCode;
    private String percentageDiscount;// percent for which this appliedDiscount applies
    private String baseDiscount; // base if the percent is not used, and rather dollar amount is considered
    private String originalAmount;  // original amount that the user was sending
    private Double appliedDiscount; // applied appliedDiscount after considering either base or percent appliedDiscount
    private Double amountDue;        // amount left after applying appliedDiscount. This is what the customer will end up paying
    private String description;
    private String expirationDate;
    private PCCurrency currency;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPercentageDiscount() {
        return percentageDiscount;
    }

    public void setPercentageDiscount(String percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
    }

    public String getBaseDiscount() {
        return baseDiscount;
    }

    public void setBaseDiscount(String baseDiscount) {
        this.baseDiscount = baseDiscount;
    }

    public String getOriginalAmoun() {
        return originalAmount;
    }

    public void setOriginalAmount(String originalAmount) {
        this.originalAmount = originalAmount;
    }

    public Double getAppliedDiscount() {
        return appliedDiscount;
    }

    public void setAppliedDiscount(Double appliedDiscount) {
        this.appliedDiscount = appliedDiscount;
    }

    public Double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(Double amountDue) {
        this.amountDue = amountDue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public PCCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(PCCurrency currency) {
        this.currency = currency;
    }
}
