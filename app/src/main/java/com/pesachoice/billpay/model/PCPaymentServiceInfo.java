package com.pesachoice.billpay.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds information on payment processing. Could be Mobile Money -- momo or any
 * other type of payment that requires payment info data such as phone number,
 * price customer name,...
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

/**
 * Created by emmy on 18/03/2018.
 */

public class PCPaymentServiceInfo extends PCServiceInfo {
    private Double price;
    private String customerPhoneNumber;
    private String customerName;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public boolean hasNecessaryContent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String failedMessage() {
        // TODO Auto-generated method stub
        return null;
    }
}
