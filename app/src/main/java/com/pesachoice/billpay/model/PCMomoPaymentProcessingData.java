package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used for mobile money payment processing when working with Third Party companies
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
/**
 * Created by emmy on 18/03/2018.
 */

public class PCMomoPaymentProcessingData extends PCPaymentProcessingData {
    private PCPaymentServiceInfo paymentServiceInfo;

    public synchronized PCPaymentServiceInfo getPaymentServiceInfo() {
        return paymentServiceInfo;
    }

    public synchronized void setPaymentServiceInfo(PCPaymentServiceInfo paymentServiceInfo) {
        this.paymentServiceInfo = paymentServiceInfo;
    }
}
