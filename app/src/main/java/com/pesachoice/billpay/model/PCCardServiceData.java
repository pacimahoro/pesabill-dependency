/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds input data for card service
 * @author Odilon Senyana
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCardServiceData extends PCData {
    private String deviceId;
    private PCCardInfo card;
    private PCUser sender;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public PCCardInfo getCard() {
        return card;
    }

    public void setCard(PCCardInfo card) {
        this.card = card;
    }

    public PCUser getSender() {
        return sender;
    }

    public void setSender(PCUser sender) {
        this.sender = sender;
    }
}