/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.pesachoice.billpay.business.PCPesabusClient;

/**
 * Used to create the appropriate serviceInfo object based on the right type
 */
public class PCServiceInfoFactory {

    public static PCServiceInfo constructServiceInfo(PCPesabusClient.PCServiceType serviceType) {
        PCServiceInfo serviceInfo = null;
        if (serviceType != null) {
            switch (serviceType) {
                case SEND_AIRTIME:
                    serviceInfo = new PCAirtimeServiceInfo();
                    break;
                case CALLING_REFIL:
                    serviceInfo=new PCCallingServiceInfo();
                    break;
                case PAY_INTERNET:
                    serviceInfo = new PCInternetServiceInfo();
                    break;
                case PAY_TV:
                    serviceInfo = new PCTVServiceInfo();
                    break;
                case PAY_ELECTRICITY:
                    serviceInfo = new PCElectricityServiceInfo();
                    break;
                case PAY_TUITION:
                    serviceInfo = new PCTuitionServiceInfo();
                    break;
                case PAY_WATER:
                     serviceInfo = new PCWaterServiceInfo();
                    break;
                case MONEY_TRANSFER:
                    serviceInfo = new PCMoneyTransferServiceInfo();
                    break;
                case SELL:
                    serviceInfo = new PCProductServiceInfo();
                    break;
                case AGENT_TRANSACTION:
                    // FIXME: We should be using PCAgentServiceInfo rather than the moneyTransferInfo
                    // At the moment there is a mismatch between how the agent is understood on the client and server
                    // Setting the money transfer therefore allow us to get things to work in the short term.
                    serviceInfo = new PCProductServiceInfo();
                    break;
                case INSURANCE:
                    // FIXME: We should be using PCAgentServiceInfo rather than the moneyTransferInfo
                    // At the moment there is a mismatch between how the agent is understood on the client and server
                    // Setting the money transfer therefore allow us to get things to work in the short term.
                    serviceInfo = new PCProductServiceInfo();
                default:
                    break;
            }
        }

        return serviceInfo;
    }
}
