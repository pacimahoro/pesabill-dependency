/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with airtime payment data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCAirtimePaymentData extends PCBillPaymentData {
    private String phoneSimCardValue;
    private PCAirtimeServiceInfo airtimeServiceInfo;

    public String getPhoneSimCardValue() {
        return phoneSimCardValue;
    }


    public void setPhoneSimCardValue(String phoneSimCardValue) {
        this.phoneSimCardValue = phoneSimCardValue;
    }


    public PCAirtimeServiceInfo getAirtimeServiceInfo() {
        return airtimeServiceInfo;
    }


    public void setAirtimeServiceInfo(PCAirtimeServiceInfo airtimeServiceInfo) {
        super.setServiceInfo(airtimeServiceInfo);
        this.airtimeServiceInfo = airtimeServiceInfo;
    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.airtimeServiceInfo = (PCAirtimeServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getAirtimeServiceInfo();
    }
}

