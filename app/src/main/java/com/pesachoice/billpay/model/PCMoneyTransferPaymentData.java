/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with airtime payment data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCMoneyTransferPaymentData extends PCBillPaymentData {

    private PCMoneyTransferServiceInfo moneyTransferServiceInfo;

    public PCMoneyTransferServiceInfo getMoneyTransferServiceInfo() {
        return moneyTransferServiceInfo;
    }

    public void setMoneyTransferServiceInfo(PCMoneyTransferServiceInfo moneyTransferServiceInfo) {
        super.setServiceInfo(moneyTransferServiceInfo);
        this.moneyTransferServiceInfo = moneyTransferServiceInfo;
    }
    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo){
        this.moneyTransferServiceInfo = (PCMoneyTransferServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getMoneyTransferServiceInfo();
    }
}

