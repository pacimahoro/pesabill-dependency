package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * hold data of pesabill user
 * Created by desire.aheza on 6/30/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCDetailedUser extends PCUser {
    private long callingCardId;
    private double callingBalance;
    private String callingPaymentCard;

    public String getCallingPaymentCard() {
        return callingPaymentCard;
    }

    public void setCallingPaymentCard(String callingPaymentCard) {
        this.callingPaymentCard = callingPaymentCard;
    }

    public double getCallingBalance() {
        return callingBalance;
    }

    public void setCallingBalance(double callingBalance) {
        this.callingBalance = callingBalance;
    }

    public long getCallingCardId() {
        return callingCardId;
    }

    public void setCallingCardId(long callingCardId) {
        this.callingCardId = callingCardId;
    }

}
