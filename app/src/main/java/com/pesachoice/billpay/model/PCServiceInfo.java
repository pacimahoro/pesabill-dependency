package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds data for service information
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class PCServiceInfo extends PCData {
    protected PCTransactionStatus transactionStatus;
    protected PCMessageType messageType;
    protected double usd;
    protected double totalUsd;
    protected double fees;
    protected double rate;
    protected double totalLocalCurrency;
    private String accountNumber;
    protected String outTransactionId;
    protected String receiverCountry;
    protected String companyName;
    private String membershipType;
    protected String maskedCardNumber;
    protected double amount;
    protected double totalAmount;


    public  PCServiceInfo () {

    }
    public abstract boolean hasNecessaryContent();

    public abstract String failedMessage();

    public PCTransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public final void setTransactionStatus(PCTransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getOutTransactionId() {
        return outTransactionId;
    }

    public void setOutTransactionId(String outTransactionId) {
        this.outTransactionId = outTransactionId;
    }

    public PCMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(PCMessageType messageType) {
        this.messageType = messageType;
    }

    public double getUsd() {
        return usd;
    }

    public void setUsd(double usd) {
        this.usd = usd;
    }

    public final double getTotalUsd() {
        return totalUsd;
    }

    public final void setTotalUsd(double totalUsd) {
        this.totalUsd = totalUsd;
    }

    public final double getFees() {
        return fees;
    }

    public final void setFees(double fees) {
        this.fees = fees;
    }

    public final double getRate() {
        return rate;
    }

    public final void setRate(double rate) {
        this.rate = rate;
    }

    public final double getTotalLocalCurrency() {
        return totalLocalCurrency;
    }

    public final void setTotalLocalCurrency(double totalLocalCurrency) {
        this.totalLocalCurrency = totalLocalCurrency;
    }

    public String getReceiverCountry() {
        return receiverCountry;
    }

    public void setReceiverCountry(String receiverCountry) {
        this.receiverCountry = receiverCountry;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    // TODO: We shouldn't need to hard code this.
    public static String getCurrencySymbol(String countryName) {
        String countryCode = "";
        if("rwanda".equalsIgnoreCase(countryName))
            countryCode = "RFW";
        else if("uganda".equalsIgnoreCase(countryName))
            countryCode = "UGX";
        else if ("nigeria".equalsIgnoreCase(countryName))
            countryCode = "NGN";
        else if ("kenya".equalsIgnoreCase(countryName))
            countryCode = "KES";
        return countryCode;
    }
}
