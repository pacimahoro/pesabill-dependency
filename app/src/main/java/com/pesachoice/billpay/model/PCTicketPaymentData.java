/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to hold ticket payment data
 * @author Desire AHEZA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTicketPaymentData extends PCBillPaymentData {

    private String deviceId;
    private PCTicketServiceInfo ticketServiceInfo;//ticket information
    private PCSpecialUser sender;//ticket buyer information

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public PCTicketServiceInfo getTicketServiceInfo() {
        return ticketServiceInfo;
    }

    public void setTicketServiceInfo(PCTicketServiceInfo ticketServiceInfo) {
        super.setServiceInfo(ticketServiceInfo);
        this.ticketServiceInfo = ticketServiceInfo;
    }
    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo){
        this.ticketServiceInfo = (PCTicketServiceInfo)serviceInfo;
    }
    @Override
    public PCSpecialUser getSender() {
        return sender;
    }

    @Override
    public void setSender(PCSpecialUser sender) {
        this.sender = sender;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getTicketServiceInfo();
    }
}
