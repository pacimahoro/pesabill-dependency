/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Holds Card Information data
 * NOTE: The data on the address info is not saved anywhere in our database. It
 * is passed along to a PCI compliant entity like Authorize.Net for processing and management
 *
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PCCardInfo extends PCCard {
    private volatile String classType;
    //private PCAddress address;
    private String maskedLocalCard;

    private PCBillingAddress billingAddress;

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public PCBillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(PCBillingAddress address) {
        this.billingAddress = address;
    }

    public String getMaskedLocalCard() {
        return maskedLocalCard;
    }

    public void setMaskedLocalCard(String maskedLocalCard) {
        this.maskedLocalCard = maskedLocalCard;
    }
}