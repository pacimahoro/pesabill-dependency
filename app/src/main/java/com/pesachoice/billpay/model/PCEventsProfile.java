/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * represents a Events profile
 *
 * @author Desire Aheza
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCEventsProfile extends PCData {

	private List<PCEvent> events;
	private String country;
	private PCEvent ticket;
	private PCSpecialUser user;

	public PCSpecialUser getUser() {
		return user;
	}

	public void setUser(PCSpecialUser user) {
		this.user = user;
	}

	public List<PCEvent> getEvents() {
		return events;
	}

	public void setEvents(List<PCEvent> events) {
		this.events = events;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public PCEvent getTicket() {
		return ticket;
	}

	public void setTicket(PCEvent ticket) {
		this.ticket = ticket;
	}



}

