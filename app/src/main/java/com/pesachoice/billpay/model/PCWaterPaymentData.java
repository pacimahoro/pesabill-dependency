/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used to deal with Water payment data
 * @author Desire AHEZA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCWaterPaymentData extends PCBillPaymentData {
    private PCWaterServiceInfo waterServiceInfo;


    public PCWaterServiceInfo getWaterServiceInfo() {
        return waterServiceInfo;
    }

    public void setWaterServiceInfo(
            PCWaterServiceInfo waterServiceInfo) {
        super.setServiceInfo(waterServiceInfo);
        this.waterServiceInfo = waterServiceInfo;
    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo){
        this.waterServiceInfo = (PCWaterServiceInfo)serviceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return this.getWaterServiceInfo();
    }
}
