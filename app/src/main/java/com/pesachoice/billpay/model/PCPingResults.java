package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Hold Ping Results data
 * Created by desire.aheza on 4/29/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCPingResults extends PCUser {
    
    private String deviceId;
    private String apiKey;
    private String currentVersion;
    private String minVersion;
    private List<PCCustomerServiceInfo> custServiceContacts;
    public String getMinVersion() {
        return minVersion;
    }
    public List<String> authorizedTestEmails;

    public PCPingResults() {}

    public void setMinVersion(String minVersion) {
        this.minVersion = minVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public List<PCCustomerServiceInfo> getCustServiceContacts() {
        return custServiceContacts;
    }

    public void setCustServiceContacts(List<PCCustomerServiceInfo> custServiceContacts) {
        this.custServiceContacts = custServiceContacts;
    }

    public List<String> getAuthorizedTestEmails() {
        return authorizedTestEmails;
    }

    public void setAuthorizedTestEmails(List<String> authorizedTestEmails) {
        this.authorizedTestEmails = authorizedTestEmails;
    }
}
