/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Hold details for tuition transaction details
 * Created by desire.aheza on 2/29/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTuitionTransaction extends PCTransaction {
    private PCTuitionServiceInfo serviceInfo;

    public PCTuitionServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(PCTuitionServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }
}
