package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * class user device details
 * Created by desire.aheza on 7/15/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCUserDeviceDetails extends PCLocation {
	private String ipv4Address;
	private String ipv6Address;
	private String macWlanAddress;
	private String macEthAddress;
	private String country;
	private String phoneModel;
	private String phoneManufacture;
	private String osVersion;

	public String getIpv4Address() {
		return ipv4Address;
	}

	public void setIpv4Address(String ipv4Address) {
		this.ipv4Address = ipv4Address;
	}

	public String getIpv6Address() {
		return ipv6Address;
	}

	public void setIpv6Address(String ipv6Address) {
		this.ipv6Address = ipv6Address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneModel() {
		return phoneModel;
	}

	public void setPhoneModel(String phoneModel) {
		this.phoneModel = phoneModel;
	}

	public String getPhoneManufacture() {
		return phoneManufacture;
	}

	public void setPhoneManufacture(String phoneManufacture) {
		this.phoneManufacture = phoneManufacture;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getMacWlanAddress() {
		return macWlanAddress;
	}

	public void setMacWlanAddress(String macWlanAddress) {
		this.macWlanAddress = macWlanAddress;
	}

	public String getMacEthAddress() {
		return macEthAddress;
	}

	public void setMacEthAddress(String macEthAddress) {
		this.macEthAddress = macEthAddress;
	}


}
