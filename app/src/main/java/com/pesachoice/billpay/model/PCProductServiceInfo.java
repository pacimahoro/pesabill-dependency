/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Hold product payment service data
 * @author Pacifique Mahoro
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCProductServiceInfo extends PCServiceInfo {

    private String companyCode;
    private String productDescription;
    private PCCardInfo card;

    public PCCardInfo getCard() {
        return card;
    }

    public void setCard(PCCardInfo card) {
        this.card = card;
    }


    @Override
    public boolean hasNecessaryContent() {
        return false;
    }

    @Override
    public String failedMessage() {
        return null;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
