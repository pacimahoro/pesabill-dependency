/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

/**
 * Provides various messages to be used when communicating error messages
 * @author Odilon Senyana
 *
 */
public enum PCMessageType {
    INACTIVE_USER("User is not active"),
    NAME_NOT_MATCHING("Name not matching"),
    AMOUNT_MISSING("Amount and/or total amount missing"),
    INSUFFICIENT_FUNDS("Insufficient funds in Pesachoice's account"),
    RECEIVER_PHONE_NO_LONGER_IN_SERVICE("Phone no longer in service"),
    TRANSACTION_FAILED("Unable to process payment card or complete bill pay transaction."),
    NOT_AUTHORIZED_TO_RECEIVE("Cannot allow transfer. The receiver is flagged");

    private String msg;
    private PCMessageType(String msg) {
        this.msg = msg;
    }
    @Override
    public String toString() {
        return msg;
    }
}

