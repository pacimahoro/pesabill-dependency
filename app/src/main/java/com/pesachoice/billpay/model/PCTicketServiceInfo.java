package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * class holds service info for ticket transaction
 * Created by desire.aheza on 10/5/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTicketServiceInfo extends PCServiceInfo {

	private String name;//event name
	private Integer quantity;
	private String id;
	private String currencySymbl;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean hasNecessaryContent() {
		return false;
	}

	@Override
	public String failedMessage() {
		return null;
	}

	public String getCurrencySymbl() {
		return currencySymbl;
	}

	public void setCurrencySymbl(String currencySymbl) {
		this.currencySymbl = currencySymbl;
	}

}
