package com.pesachoice.billpay.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds data for Agent info for wallet.
 *
 *
 *
 */
/**
 * Created by emmy on 18/03/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCAgentServiceInfo extends PCServiceInfo{
    protected String agentId;
    protected String agentPhoneNumber;
    protected BigDecimal balance;
    protected BigDecimal approvedBalance;
    protected BigDecimal deposit;
    protected BigDecimal withdrawal;
    protected PCCurrency baseCurrency;
    protected Boolean approved;
    protected PCPaymentInfo paymentInfo;
    protected String approvedBy;
    protected String description;
    private PCImageData receiptMetadata;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public PCImageData getReceiptMetadata() {
        return receiptMetadata;
    }

    public void setReceiptMetadata(PCImageData receiptMetadata) {
        this.receiptMetadata = receiptMetadata;
    }

    /**
     * @return the approvedBy
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy
     *            the approvedBy to set
     */
    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId
     *            the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    public void setAgentPhoneNumber(String agentPhoneNumber) {
        this.agentPhoneNumber = agentPhoneNumber;
    }

    /**
     * @return the balance
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * @param balance
     *            the balance to set
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return the approvedBalance
     */
    public BigDecimal getApprovedBalance() {
        return approvedBalance;
    }

    /**
     * @param approvedBalance
     *            the approvedBalance to set
     */
    public void setApprovedBalance(BigDecimal approvedBalance) {
        this.approvedBalance = approvedBalance;
    }

    /**
     * @return the deposit
     */
    public BigDecimal getDeposit() {
        return deposit;
    }

    /**
     * @param deposit
     *            the deposit to set
     */
    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    /**
     * @return the withdrawal
     */
    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    /**
     * @param withdrawal
     *            the withdrawal to set
     */
    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

    /**
     * @return the baseCurrency
     */
    public PCCurrency getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * @param baseCurrency
     *            the baseCurrency to set
     */
    public void setBaseCurrency(PCCurrency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    /**
     * @return the approved
     */
    public Boolean getApproved() {
        return approved;
    }

    /**
     * @param approved
     *            the approved to set
     */
    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    /**
     * @return the paymentInfo
     */
    public PCPaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * @param paymentInfo
     *            the paymentInfo to set
     */
    public void setPaymentInfo(PCPaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    @Override
    public boolean hasNecessaryContent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String failedMessage() {
        // TODO Auto-generated method stub
        return null;
    }
}
