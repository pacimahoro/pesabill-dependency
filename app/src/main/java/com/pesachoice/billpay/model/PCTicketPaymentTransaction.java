/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class used for ticket transaction
 * @author Desire Aheza
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTicketPaymentTransaction extends PCTransaction {
    private PCTicketServiceInfo serviceInfo;

    public PCTicketServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(PCTicketServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }
}
