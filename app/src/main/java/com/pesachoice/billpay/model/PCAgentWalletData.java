package com.pesachoice.billpay.model;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;



/**
 * Class used to deal with agent transactions - when an agent performs a transaction on behalf of
 * a customer
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

/**
 * Created by emmy on 18/03/2018.
 */

public class PCAgentWalletData extends PCBillPaymentData{

    private PCSpecialUser sender; // override super-classes sender object to make sure detailed data is used
    private String agentId;       // This is the email of the agent making transaction on behalf of customers
    private String agentTokenId;
    private String agentPhoneNumber;
    private BigDecimal deposit;
    private BigDecimal withdrawal;
    private String companyId;
    private String country;

    /*
     * Services that are possible with an agent
     */
    private PCProductServiceInfo productServiceInfo;




    @Override
    public PCSpecialUser getSender() {
        return sender;
    }

    @Override
    public void setSender(PCSpecialUser sender) {
        this.sender = sender;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentTokenId() {
        return agentTokenId;
    }

    public void setAgentTokenId(String agentTokenId) {
        this.agentTokenId = agentTokenId;
    }

    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    public void setAgentPhoneNumber(String agentPhoneNumber) {
        this.agentPhoneNumber = agentPhoneNumber;
    }

    public PCProductServiceInfo getProductServiceInfo() {
        return productServiceInfo;
    }

    public void setProductServiceInfo(PCProductServiceInfo productServiceInfo) {
        this.productServiceInfo = productServiceInfo;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }



    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        // FIXME: We should check through all the object to figure out which one is not null
        // since an agent could do more than one service. Money transfer is the one at the moment.
        return productServiceInfo;
    }
}
