package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import org.springframework.util.StringUtils;
import org.w3c.dom.ProcessingInstruction;

/**
 * Created to handle incompatibilities with clients who doesn't update their app. this will contain Pesachoice user details
 * Created by desire.aheza on 10/23/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCSpecialUser extends PCDetailedUser {

    private PCBillingAddress billingAddress;

    private PCCurrency baseCurrency;

    private PCPaymentInfo paymentInfo;

    private boolean hasMobileMoneyAccount;

    private boolean customerRep;

    private String currentUserAccount;

    private PCUserDeviceDetails pcUserDeviceDetails;

    //TODO: Remove this once the server fix the issue with login for agent
    private boolean isAgent = true;

    private boolean thirdParty;

    public boolean getHasMobileMoneyAccount() {
        return hasMobileMoneyAccount;
    }

    private PCBillPaymentData.PCPaymentType paymentType;

    public PCBillPaymentData.PCPaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PCBillPaymentData.PCPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public boolean isCustomerRep() {
        return customerRep;
    }

    public void setCustomerRep(boolean customerRep) {
        this.customerRep = customerRep;
    }

    public void setHasMobileMoneyAccount(boolean hasMobileMoneyAccount) {
        this.hasMobileMoneyAccount = hasMobileMoneyAccount;
    }

    public PCBillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(PCBillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public PCCurrency getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(PCCurrency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    @JsonIgnore
    public PCPaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PCPaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getCurrentUserAccount() {
        //am setting the initial user mobile money account as the number they used to login.
        if(!PCGeneralUtils.isCountryUS(getCountry())&& StringUtils.isEmpty(currentUserAccount)) {
            currentUserAccount = getPhoneNumber();
        }
        return currentUserAccount;
    }

    public void setCurrentUserAccount(String currentUserAccount) {
        this.currentUserAccount = currentUserAccount;
    }

    public PCUserDeviceDetails getPcUserDeviceDetails() {
        return pcUserDeviceDetails;
    }

    public void setPcUserDeviceDetails(PCUserDeviceDetails pcUserDeviceDetails) {
        this.pcUserDeviceDetails = pcUserDeviceDetails;
    }

    public boolean isAgent() {
        return isAgent;
    }

    public void setAgent(boolean agent) {
        isAgent = agent;
    }

    public boolean isThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(boolean thirdParty) {
        this.thirdParty = thirdParty;
    }
}
