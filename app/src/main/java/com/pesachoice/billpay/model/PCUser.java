/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Holds User Data
 *
 * @author Odilon Senyana
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCUser extends PCData implements Serializable {
	private boolean isAuthenticated = false;
	private boolean registrationComplete = false;
	private String userName;
	private String email;
	private String firstName;
	private String lastName;
	private String fullName;
	private String pwd;
	private String phoneNumber;
	private String tokenId;
	private long accessCode;
	private long createdTime;                     // Time stamp elapsed in milliseconds since January 1st, 1970
	private long lastLoginTime;                   // Time stamp elapsed in milliseconds since January 1st, 1970
	private Timestamp createdNanoSecsTimeStamp;   // Time stamp elapsed in nanoseconds @see java.sql.TimeStamp
	private Timestamp lastLoginNonSecsTimeStamp;  // Time stamp elapsed in nanoseconds @see java.sql.TimeStamp
	private String maskedCardNumber;
	private String country;


	// TODO: missing field for country where the user is located

	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public boolean isRegistrationComplete() {
		return registrationComplete;
	}

	public void setRegistrationComplete(boolean registrationComplete) {
		this.registrationComplete = registrationComplete;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public final String getFullName() {
		return firstName + " " + lastName;
	}

	public final void setFullName(String fullName) {
		this.fullName = StringUtils.capitalize(fullName);
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public long getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(long accessCode) {
		this.accessCode = accessCode;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(long lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Timestamp getCreatedNanoSecsTimeStamp() {
		return createdNanoSecsTimeStamp;
	}

	public void setCreatedNanoSecsTimeStamp(Timestamp createdNanoSecsTimeStamp) {
		this.createdNanoSecsTimeStamp = createdNanoSecsTimeStamp;
	}

	public Timestamp getLastLoginNanoSecsTimeStamp() {
		return lastLoginNonSecsTimeStamp;
	}

	public void setLastLoginNanoSecsTimeStamp(Timestamp lastLoginNonSecsTimeStamp) {
		this.lastLoginNonSecsTimeStamp = lastLoginNonSecsTimeStamp;
	}

	public String getMaskedCardNumber() {
		return maskedCardNumber;
	}

	public void setMaskedCardNumber(String maskedCardNumber) {
		this.maskedCardNumber = maskedCardNumber;
	}

	public String getCountry() {
		return country;
	}

	public PCSpecialUser setCountry(String country) {
		this.country = country;
        return null;
    }

	@Override
	public String toString() {
		return "User Name [" + userName + "], Email [" + email + "], First Name [" + firstName + "], Last Name [" + lastName +
				"], Created Time [" + createdTime + "], Last Login Time [" + lastLoginTime + "]";
	}
}
