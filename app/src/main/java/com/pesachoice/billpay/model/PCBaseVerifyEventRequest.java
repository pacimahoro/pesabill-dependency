package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class that holds event details
 * Created by desire.aheza on 8/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCBaseVerifyEventRequest extends PCRequest {

    private PCSpecialUser organizer;
    private String ticketNumber;
    private boolean hasScannedQR;
    private boolean confirmTicketUsed;

    public PCSpecialUser getOrganizer() {
        return organizer;
    }

    public void setOrganizer(PCSpecialUser organizer) {
        this.organizer = organizer;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public boolean isHasScannedQR() {
        return hasScannedQR;
    }

    public void setHasScannedQR(boolean hasScannedQR) {
        this.hasScannedQR = hasScannedQR;
    }

    public boolean isConfirmTicketUsed() {
        return confirmTicketUsed;
    }

    public void setConfirmTicketUsed(boolean confirmTicketUsed) {
        this.confirmTicketUsed = confirmTicketUsed;
    }


}
