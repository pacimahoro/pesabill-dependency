package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Class that holds event details
 * Created by desire.aheza on 12/22/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCVerifyEventRequest extends PCBaseVerifyEventRequest {

    private PCTicketServiceInfo ticketServiceInfo;
    private PCData.ActionType actionType = PCData.ActionType.TICKETS;

    public PCTicketServiceInfo getTicketServiceInfo() {
        return ticketServiceInfo;
    }

    public void setTicketServiceInfo(PCTicketServiceInfo ticketServiceInfo) {
        this.ticketServiceInfo = ticketServiceInfo;
    }

    public PCData.ActionType getActionType() {
        return actionType;
    }

    public void setActionType(PCData.ActionType actionType) {
        this.actionType = actionType;
    }

}
