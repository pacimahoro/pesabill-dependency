package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

/**
 * Created by emmy on 18/03/2018.
 */

public class PCCardPaymentProcessingData extends PCPaymentProcessingData {
    private PCPaymentServiceInfo paymentServiceInfo;
    private PCCardInfo card;

    public PCPaymentServiceInfo getPaymentServiceInfo() {
        return paymentServiceInfo;
    }

    public void setPaymentServiceInfo(PCPaymentServiceInfo paymentServiceInfo) {
        this.paymentServiceInfo = paymentServiceInfo;
    }

    public PCCardInfo getCard() {
        return card;
    }

    public void setCard(PCCardInfo card) {
        this.card = card;
    }


}
