/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pesachoice.billpay.business.PCPesabusClient;

/**
 * Class for data related to a complete transaction. A transaction is
 * considered as a unit of sender, receiver, service info and operator information
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCTransaction extends PCActivity {
    private PCSpecialUser sender ;
    private PCSpecialUser receiver = new PCSpecialUser() ;
    private PCTransactionStatus transactionStatus;
    private String serviceType;
    private PCTicketServiceInfo ticketServiceInfo;
    private PCCallingServiceInfo callingServiceInfo;
    private PCAirtimeServiceInfo airtimeServiceInfo;
    private PCMoneyTransferServiceInfo moneyTransferServiceInfo;
    private PCWaterServiceInfo waterServiceInfo;
    private PCElectricityServiceInfo electricityServiceInfo;
    private PCTVServiceInfo tvServiceInfo;
    private PCInternetServiceInfo internetServiceInfo;
    private PCProductServiceInfo productServiceInfo;
    private PCTuitionServiceInfo tuitionServiceInfo;
    private PCOperator operator;
    private boolean cardAlreadyProcessed;
    private PCServiceInfo serviceInfo;
    private PCCurrency baseCurrency;

    public  PCTransaction () {

    }


    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setServiceInfo(PCServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    public PCTicketServiceInfo getTicketServiceInfo() {
        return ticketServiceInfo;
    }

    public void setTicketServiceInfo(PCTicketServiceInfo ticketServiceInfo) {
        this.ticketServiceInfo = ticketServiceInfo;
    }

    public PCCallingServiceInfo getCallingServiceInfo() {
        return callingServiceInfo;
    }

    public void setCallingServiceInfo(PCCallingServiceInfo callingServiceInfo) {
        this.callingServiceInfo = callingServiceInfo;
    }

    public PCAirtimeServiceInfo getAirtimeServiceInfo() {
        return airtimeServiceInfo;
    }

    public void setAirtimeServiceInfo(PCAirtimeServiceInfo airtimeServiceInfo) {
        this.airtimeServiceInfo = airtimeServiceInfo;
    }

    public PCMoneyTransferServiceInfo getMoneyTransferServiceInfo() {
        return moneyTransferServiceInfo;
    }

    public void setMoneyTransferServiceInfo(PCMoneyTransferServiceInfo moneyTransferServiceInfo) {
        this.moneyTransferServiceInfo = moneyTransferServiceInfo;
    }

    public PCWaterServiceInfo getWaterServiceInfo() {
        return waterServiceInfo;
    }

    public void setWaterServiceInfo(PCWaterServiceInfo waterServiceInfo) {
        this.waterServiceInfo = waterServiceInfo;
    }

    public PCElectricityServiceInfo getElectricityServiceInfo() {
        return electricityServiceInfo;
    }

    public void setElectricityServiceInfo(PCElectricityServiceInfo electricityServiceInfo) {
        this.electricityServiceInfo = electricityServiceInfo;
    }

    public PCTVServiceInfo getTvServiceInfo() {
        return tvServiceInfo;
    }

    public void setTvServiceInfo(PCTVServiceInfo tvServiceInfo) {
        this.tvServiceInfo = tvServiceInfo;
    }

    public PCInternetServiceInfo getInternetServiceInfo() {
        return internetServiceInfo;
    }

    public void setInternetServiceInfo(PCInternetServiceInfo internetServiceInfo) {
        this.internetServiceInfo = internetServiceInfo;
    }

    public PCProductServiceInfo getProductServiceInfo() {
        return productServiceInfo;
    }


    public void setProductServiceInfo(PCProductServiceInfo productServiceInfo) {
        this.productServiceInfo = productServiceInfo;
    }

    public PCTuitionServiceInfo getTuitionServiceInfo() {
        return tuitionServiceInfo;
    }

    public void setTuitionServiceInfo(PCTuitionServiceInfo tuitionServiceInfo) {
        this.tuitionServiceInfo = tuitionServiceInfo;
    }

    public PCTransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(PCTransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public PCSpecialUser getSender() {
        return sender;
    }

    public void setSender(PCSpecialUser sender) {
        this.sender = sender;
    }

    public PCSpecialUser getReceiver() {
        return receiver;
    }

    public void setReceiver(PCSpecialUser receiver) {
        this.receiver = receiver;
    }

    public PCOperator getOperator() {
        return operator;
    }

    public void setOperator(PCOperator operator) {
        this.operator = operator;
    }

    public boolean isCardAlreadyProcessed() {
        return cardAlreadyProcessed;
    }

    public void setCardAlreadyProcessed(boolean cardAlreadyProcessed) {
        this.cardAlreadyProcessed = cardAlreadyProcessed;
    }

    public PCCurrency getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(PCCurrency baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public PCServiceInfo getServiceInfo() {

        PCServiceInfo serviceInfoData = null;

        if (serviceInfo != null) {
            return serviceInfo;
        }

        if ("callingServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData = this.callingServiceInfo;
        }
        else if("airtimeServiceInfo".equalsIgnoreCase(serviceType)){
            serviceInfoData = this.airtimeServiceInfo;
        }
        else if("ticketServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData = this.ticketServiceInfo;
        }
        else if("moneyTransferServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData = this.moneyTransferServiceInfo;
        }
        else if("electricityServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData = this.electricityServiceInfo;
        }
        else if("waterServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData = this.waterServiceInfo;
        }
        else if("tvServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData=this.tvServiceInfo;
        }
        else if("tuitionServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData=this.tuitionServiceInfo;
        }
        else if("internetServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData=this.internetServiceInfo;
        }
        else if("electricityServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData=this.electricityServiceInfo;
        }
        else if("productServiceInfo".equalsIgnoreCase(serviceType)) {
            serviceInfoData=this.productServiceInfo;
        } else if("agentServiceInfo".equalsIgnoreCase("agentServiceInfo")) {
            serviceInfoData = this.productServiceInfo;
        }

        if(serviceInfoData == null){
            serviceInfoData = this.serviceInfo;
        }
        return serviceInfoData;
    }
}
