package com.pesachoice.billpay.model;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/**
 * Created by emmy on 18/03/2018.
 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCAgentWalletTransaction extends PCTransaction {

    public PCAgentWalletTransaction () {

    }
    /**
     * Email of the agent making, reading or modifying a wallet transaction
     */
    private String agentId;

    /**
     * Total balance including balance that is not approved yet
     */
    private BigDecimal balance;

    /**
     * Available balance that the agent has access to
     */
    private BigDecimal approvedBalance;

    /**
     * Amount the agent is deposting to the wallet account
     */
    private BigDecimal deposit;

    /**
     * Amount the agent is withdrawing from the wallet account
     */
    private BigDecimal withdrawal;

    /**
     * True if approved by PesaChoice admin or exec, false otherwise
     */
    private Boolean approved;

    /**
     * Currency information associated with agent registered default currency
     */
    private PCCurrency currencyInfo;

    /**
     * Email of the admin or PesaChoice executive who approved the current
     * transaction
     */
    private String approvedBy;

    /**
     * All fields of receiptMetadata
     */
    @JsonProperty("serviceInfo")
    private PCProductServiceInfo agentServiceInfo;
    private PCImageData receiptMetadata;

    public PCImageData getReceiptMetadata() {
        return receiptMetadata;
    }

    public void setReceiptMetadata(PCImageData receiptMetadata) {
        this.receiptMetadata = receiptMetadata;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getApprovedBalance() {
        return approvedBalance;
    }

    public void setApprovedBalance(BigDecimal approvedBalance) {
        this.approvedBalance = approvedBalance;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

    public Boolean getApproaved() {
        return approved;
    }

    public void setApproaved(Boolean approaved) {
        this.approved = approaved;
    }

    public PCCurrency getCurrencyInfo() {
        return currencyInfo;
    }

    public void setCurrencyInfo(PCCurrency currencyInfo) {
        this.currencyInfo = currencyInfo;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public PCProductServiceInfo getAgentServiceInfo() {
        return agentServiceInfo;
    }
    @JsonProperty("serviceInfo")
    public void setAgentServiceInfo(PCProductServiceInfo agentServiceInfo) {
        this.agentServiceInfo = agentServiceInfo;
    }
}
