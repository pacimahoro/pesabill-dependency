/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Holds card data
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCard {
    private String number;
    private String monthOfExpiration;
    private String yearOfExpiration;
    private String cvv;
    private String cardType;



    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMonthOfExpiration() {
        return monthOfExpiration;
    }

    public void setMonthOfExpiration(String monthOfExpiration) {
        this.monthOfExpiration = monthOfExpiration;
    }

    public String getYearOfExpiration() {
        return yearOfExpiration;
    }

    public void setYearOfExpiration(String yearOfExpiration) {
        this.yearOfExpiration = yearOfExpiration;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}

