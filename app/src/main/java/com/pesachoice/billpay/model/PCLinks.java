/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * Class used to provide and hold data related to links used by all services throughout the application
 * @author Odilon Senyana
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCLinks implements Serializable {
    private String ping;
    private String signup;
    private String completeSignup;
    private String verifyPhone;
    private String verifyEmail;
    private String login;
    private String forgotCredentials;
    private String resetCredentials;
    private String isauth;
    private String logout;
    private String createCard;
    private String user;
    private String tuition;
    private String tv;
    private String electricity;
    private String airtime;
    private String water;
    private String internet;
    private String moneyTransfer;
    private String countryProfile;
    private String exchangeRate;
    private String callingRefill;

    public String getCallingRefill() {
        return callingRefill;
    }

    public void setCallingRefill(String callingRefill) {
        this.callingRefill = callingRefill;
    }

    public final String getPing() {
        return ping;
    }

    public final void setPing(String ping) {
        this.ping = ping;
    }

    public final String getSignup() {
        return signup;
    }

    public final String getCompleteSignup() {
        return completeSignup;
    }

    public final void setCompleteSignup(String completeSignup) {
        this.completeSignup = completeSignup;
    }

    public final void setSignup(String signup) {
        this.signup = signup;
    }

    public final String getLogin() {
        return login;
    }

    public final void setLogin(String login) {
        this.login = login;
    }

    public final String getForgotCredentials() {
        return forgotCredentials;
    }

    public final void setForgotCredentials(String forgotCredentials) {
        this.forgotCredentials = forgotCredentials;
    }

    public final String getResetCredentials() {
        return resetCredentials;
    }

    public final void setResetCredentials(String resetCredentials) {
        this.resetCredentials = resetCredentials;
    }

    public final String getVerifyPhone() {
        return verifyPhone;
    }

    public final void setVerifyPhone(String verifyPhone) {
        this.verifyPhone = verifyPhone;
    }

    public final String getVerifyEmail() {
        return verifyEmail;
    }

    public final void setVerifyEmail(String verifyEmail) {
        this.verifyEmail = verifyEmail;
    }
    public final String getIsauth() {
        return isauth;
    }

    public final void setIsauth(String isauth) {
        this.isauth = isauth;
    }

    public final String getLogout() {
        return logout;
    }

    public final void setLogout(String logout) {
        this.logout = logout;
    }

    public final String getCreateCard() {
        return createCard;
    }

    public final void setCreateCard(String createCard) {
        this.createCard = createCard;
    }

    public final String getUser() {
        return user;
    }

    public final void setUser(String user) {
        this.user = user;
    }

    public final String getTuition() {
        return tuition;
    }

    public final void setTuition(String tuition) {
        this.tuition = tuition;
    }

    public final String getTv() {
        return tv;
    }

    public final void setTv(String tv) {
        this.tv = tv;
    }

    public final String getElectricity() {
        return electricity;
    }

    public final void setElectricity(String electricity) {
        this.electricity = electricity;
    }

    public final String getAirtime() {
        return airtime;
    }

    public final void setAirtime(String airtime) {
        this.airtime = airtime;
    }

    public final String getWater() {
        return water;
    }

    public final void setWater(String water) {
        this.water = water;
    }

    public final String getInternet() {
        return internet;
    }

    public final void setInternet(String internet) {
        this.internet = internet;
    }

    public final String getMoneyTransfer() {
        return moneyTransfer;
    }

    public final void setMoneyTransfer(String moneyTransfer) {
        this.moneyTransfer = moneyTransfer;
    }

    public String getCountryProfile() {
        return countryProfile;
    }

    public void setCountryProfile(String countryProfile) {
        this.countryProfile = countryProfile;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
