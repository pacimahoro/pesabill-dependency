package com.pesachoice.billpay.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.sql.Timestamp;

/**
 * Class used to hold generic data for payment processing. Sub-classes should be
 * more granular and specific to deal with card payment, mobile money
 * payment,...
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
/**
 * Created by emmy on 18/03/2018.
 */

public class PCPaymentProcessingData extends PCBillPaymentData {
    private String country;
    private String products;
    private String contactEmail;
    private String contactPhoneNumber;
    private Double price;
    private Timestamp date;

    /**
     * Lists all product types that third party companies can have that apply to
     * PesaChoice services.
     *
     */
    public enum PCThirdPartyProductType {
        Airtime,
        Calling,
        Electricity,
        Internet,
        Event,
        MoneyTransfer,
        Retail,
        Ticket,
        Tuition,
        Tv,
        Water;

        /**
         * Given in string product type, this method makes sure it maps to an
         * appropriate product type
         *
         * @param prodType
         *            Product type input to map to
         * @return PCThirdPartyProductType Internal representation of product type as it
         *         is known within PesaChoice. If a match cannot be found then null is
         *         returned
         */
        public static PCThirdPartyProductType mapValue(String prodType) {
            PCThirdPartyProductType productType = null;
            if (prodType != null && ! "".equals(prodType)) {
                PCThirdPartyProductType[] allProductTypes = PCThirdPartyProductType.values();
                if (allProductTypes != null && allProductTypes.length > 0)
                    for (PCThirdPartyProductType currentType : allProductTypes)
                        if (prodType.equalsIgnoreCase(currentType.toString()))
                            productType = currentType;
            }
            return productType;
        }
    }

    public synchronized String getCountry() {
        return country;
    }

    public synchronized void setCountry(String country) {
        this.country = country;
    }

    public synchronized String getProducts() {
        return products;
    }

    public synchronized void setProducts(String products) {
        this.products = products;
    }

    public synchronized String getContactEmail() {
        return contactEmail;
    }

    public synchronized void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public synchronized String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public synchronized void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public synchronized Double getPrice() {
        return price;
    }

    public synchronized void setPrice(Double price) {
        this.price = price;
    }

    public synchronized Timestamp getDate() {
        return date;
    }

    public synchronized void setDate(Timestamp date) {
        this.date = date;
    }

	/*
	 * TODO: Class missing overridden methods for equals, hashCode and toString
	 */
}
