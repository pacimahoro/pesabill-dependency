/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Hold product payment data
 * @author Pacifique Mahoro
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCProductPaymentData extends PCBillPaymentData {
    private PCProductServiceInfo productServiceInfo;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    private String deviceId;
    public void setProductServiceInfo(PCProductServiceInfo productServiceInfo) {
        super.setServiceInfo(productServiceInfo);
        this.productServiceInfo = productServiceInfo;
    }

    @Override
    public void setServiceInfo(PCServiceInfo serviceInfo){
        this.productServiceInfo = (PCProductServiceInfo)serviceInfo;
    }

    public PCProductServiceInfo getProductServiceInfo() {
        return productServiceInfo;
    }

    @Override
    public PCServiceInfo retrieveServiceInfo() {
        return getProductServiceInfo();
    }
}
