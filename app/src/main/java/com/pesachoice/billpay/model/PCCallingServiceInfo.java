package com.pesachoice.billpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * this class is used to hold calling service information
 * Created by desire.aheza on 6/5/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PCCallingServiceInfo extends PCServiceInfo {
    private String credit;
    private long cardId;
    private double balance;
    private boolean callingCustomer;

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isCallingCustomer() {
        return callingCustomer;
    }

    public void setCallingCustomer(boolean callingCustomer) {
        this.callingCustomer = callingCustomer;
    }

    @Override
    public boolean hasNecessaryContent() {
        return false;
    }

    @Override
    public String failedMessage() {
        return null;
    }
}
