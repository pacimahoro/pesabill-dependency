/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package com.pesachoice.billpay.model;

/**
 * Defines the characteristics of a transaction.
 * @author Pacifique Mahoro
 *
 */
public interface PCTransactionable {

    /**
     * Get the service info object.
     * While we might different type of service info based on the service,
     * they all inherit from PCServiceInfo.
     *
     * @return PCServiceInfo
     *              The service info used.
     */
    public PCServiceInfo retrieveServiceInfo();
}
