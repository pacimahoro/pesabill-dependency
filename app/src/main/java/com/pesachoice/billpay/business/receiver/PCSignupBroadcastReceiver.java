/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCUser;

/**
 * @author Odilon Senyana
 */
public class PCSignupBroadcastReceiver extends BroadcastReceiver {

    private final static String CLAZZ = PCSignupBroadcastReceiver.class.getName();

    private static volatile boolean singletonIsCreated = false;

    private Activity activity;
    /*
     * Using a singleton to ensure only one object is created for all callers.
     * This is used because we want only one object to handle the onReceive method.
     */
    private static PCSignupBroadcastReceiver signupBroadcastReceiver = null;

    public static PCSignupBroadcastReceiver createOrGetBroadcastReceiver(Activity activity) {
        if (singletonIsCreated) {
            return signupBroadcastReceiver;
        }
        else {
            singletonIsCreated = true;
            return new PCSignupBroadcastReceiver(activity);
        }
    }

    private PCSignupBroadcastReceiver(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (activity != null) {
            Bundle userDataBundle = intent.getBundleExtra(PCPesabusClient.EXTRA_USER_DATA_BUNDLE);
            Object content = userDataBundle.get(PCPesabusClient.EXTRA_USER_DATA);

            Log.v(CLAZZ, "booyeah");
            if (content != null && content instanceof PCUser) {
                Log.v(CLAZZ, "is it here");
                PCUser user = (PCUser)content;
                Log.v(CLAZZ, "first name" + user.getFirstName());
            }
        }
    }
}
