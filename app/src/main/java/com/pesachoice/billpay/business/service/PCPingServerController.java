/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPingResults;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for ping the server
 * so far it will be usable to get some data including API KEY
 * 
 * @author Desire Aheza
 */
public class PCPingServerController extends PCBaseController {

	private final static String CLAZZ = PCPingServerController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCPingServerController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCPingResults doInBackground(Object... data) {
        PCPingResults userResponseData = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCPingResults) {
                    userResponseData =
                            (PCPingResults) this.manageService(PCPesabusClient.PCServiceType.PING, (PCPingResults)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return userResponseData;
	}

	@Override
	protected void onPostExecute(Object user) {
        if (user != null && user instanceof PCPingResults) {
            Log.v(CLAZZ, "Processing on post execute for ping server");
            asyncListener.onTaskCompleted((PCPingResults)user);
        }
        else {
            Log.e(CLAZZ, "Missing response data result for pinging server.");
            PCPingResults missingUser = new PCPingResults();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
        }
	}


    /**
     * This method is specific to singing up new users.
     * {@inheritDoc}
     */
	@Override
    protected PCPingResults workWithService() {
        PCPingResults pingRespData = null;
		if (this.request != null) {
            try {
                String request = "{}";
                Log.v(CLAZZ, "pinging the server request {}");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);

                HttpEntity<String> entity = new HttpEntity<String>(request,headers);
                ResponseEntity<PCPingResults> response = restTemplate.postForEntity(PCPesabusClient.PING_SERVER_RELATIVE_PATH, entity, PCPingResults.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    pingRespData = response.getBody();
                    // case 0: no response data
                    if (pingRespData ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (pingRespData.getErrorMessage() != null && ! "".equalsIgnoreCase(pingRespData.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + pingRespData.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after pinging server[" + pingRespData + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                pingRespData = new PCPingResults();
                pingRespData.setErrorMessage(exc.getMessage());

            }
		}
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
		return pingRespData;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
