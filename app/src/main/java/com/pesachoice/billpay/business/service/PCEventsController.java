package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.fragments.PCUserEventsFragment;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCEventsProfile;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMerchantsProfile;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCVerifyEventRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Handle retrieving a list of events
 * @author Desire Aheza
 *
 */
public class PCEventsController extends PCBaseController {
    private final static String CLAZZ = PCEventsController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCEventsController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    public void clearAllInstances() {

    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCData doInBackground(Object... data) {
        PCData eventsDataResponse = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCRequest) {
                    eventsDataResponse =
                            (PCData) this.manageService((PCRequest)dataElement);
                }

            }
        }
        catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return eventsDataResponse;
    }

    @Override
    protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCEventsProfile) {
            Log.v(CLAZZ, "Processing on post execute after retrieving events profile");
            asyncListener.onTaskCompleted((PCEventsProfile)response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for country profile.");
            // ((PCMainTabActivity)activity).straightLogout();
            PCEventsProfile missingEventsProfile = new PCEventsProfile();
            //TODO: Need to handle this a lil better:
            missingEventsProfile.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingEventsProfile);
        }
    }

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCData}
     * 				service result data for events profile
     * @throws PCGenericError
     */
    public PCData manageService(Object requestData) throws PCGenericError {
        PCData eventsData = null;
        if (activity == null) {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
            throw genericError;
        }
        try {
            this.request = requestData;
            eventsData = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return eventsData;
    }

    /**
     * This method uses the service to fetch events data
     * {@inheritDoc}
     */
    @Override
    protected PCData workWithService() {
        PCData eventsDataResponse = null;
        if (this.request != null) {
            try {

                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                PCRequest eventDataRequestData = null;
                String requestURL="";
                if (request instanceof PCVerifyEventRequest) {
                    eventDataRequestData = (PCVerifyEventRequest) request;
                    requestURL=PCPesabusClient.VERIFY_EVENTS_RELATIVE_PATH;
                }
                else {
                    eventDataRequestData = (PCRequest) request;
                    requestURL = PCPesabusClient.GET_EVENTS_RELATIVE_PATH;
                }
                Log.d(CLAZZ, "Request data for events [" + eventDataRequestData + "]");

                ResponseEntity<PCEventsProfile> response = restTemplate.postForEntity(
                        PCPesabusClient.URL + requestURL, eventDataRequestData, PCEventsProfile.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    eventsDataResponse = response.getBody();
                    // case 0: no response data
                    if (eventsDataResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (eventsDataResponse.getErrorMessage() != null && ! "".equals(eventsDataResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + eventsDataResponse.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after getting events profile [" + eventsDataResponse + "]");
                    }
                }
                else {
                    //TODO: close app not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return eventsDataResponse;
    }
}
