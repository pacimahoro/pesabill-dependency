/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business;

import android.app.Activity;

import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import java.util.Queue;

/**
 * Interface reserved for any implementation that deal with service client for calling,
 * and managing services.
 * @author Desire AHEZA
 */
public interface PCControllableManyItem<T extends PCData> extends PCControllable {


	/**
	 * Main component to use when accessing the service client code base. This method triggers the
	 * client and manages it to get the corresponding data from a particular service.
	 * //TODO
	 * @param serviceType
	 * @param requestData
	 * @return
	 * @throws PCGenericError
	 */
	Queue<? extends PCData> manageServiceManyItem(PCPesabusClient.PCServiceType serviceType, Object requestData) throws PCGenericError;


	/**
	 *
	 * @param serviceType
	 * @param requestData
	 * @param activity
	 * @return
	 * @throws PCGenericError
	 */
	Queue<? extends PCData> manageServiceManyItem(PCPesabusClient.PCServiceType serviceType, Object requestData, Activity activity) throws PCGenericError;


}
