package com.pesachoice.billpay.business;

/**
 * This class holds all constants that will be used in this application
 * Created by desire.aheza on 1/4/2016.
 */
public class PCPesachoiceConstant {

	public static String UNABLE_TO_PROCESS_REQUESTS = "Unable to process request.";
	public static String UNABLE_TO_COMPLETE_SERVICE_CALL = "Unable to complete service call. Status code: ";
	public static String ERROR_IN_REQUEST = "Unfortunately, input data is missing.";
	public static String ERROR_MISSING_DATA = "Missing crucial data, such as request data and/or service type and/or activity.";
	public static String ERROR_NULL_ACTIVITY = "Activity object cannot be null. It needs to be set.";
	public static String ERROR_MISSING_IMPLEMENTATION = "Missing implemetation";
	public static String CONTACT_RESULTS = "contactresults";
	public static String SHARED_PREF_USER_EMAIL = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_EMAIL";
	public static String SHARED_PREF_USER_TOKEN = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_TOKEN";
	public static String SHARED_PREF_USER_LOGGED_IN = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_LOGGED_IN";
	public static String USER_AUTO_LOGGED_IN = "user_auto_logedin";
	public static String SHARED_PREF_USER_VERIFIED_PHONE = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_VERIFIED_PHONE";
	public static String SHARED_PREF_USER_USERNAME = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_USERNAME";
	public static String SHARED_PREF_USER_PHONE_NUMBER = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_PHONE_NUMBER";
	public static String SHARED_PREF_USER_MASKED_CREDIT_CARD = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_MASKED_CREDIT_CARD";
	public static String SHARED_PREF_USER_LASTNAME = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_LASTNAME";
	public static String SHARED_PREF_USER_FIRSTNAME = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_FIRSTNAME";
	public static String SHARED_PREF_USER_IS_CUSTOMER_REP = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_IS_CUSTOMER_REP";
	public static String SHARED_PREF_USER_ADDRESS_CITY = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_ADDRESS_CITY";
	public static String SHARED_PREF_USER_ADDRESS_STATE = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_ADDRESS_STATE";
	public static String SHARED_PREF_USER_ADDRESS_ZIP_CODE = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_ADDRESS_ZIP_CODE";
	public static String SHARED_PREF_USER_ADDRESS_COUNTRY = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_ADDRESS_COUNTRY";
	public static String SHARED_PREF_USER_ADDRESS_STREET_NAME = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_ADDRESS_STREET_NAME";
	public static String SHARED_PREF_USER_PHOTO_ID_STORAGE_TOKEN = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_PHOTO_ID_STORAGE_TOKEN";
	public static String SHARED_PREF_USER_DEFAULT_ACCOUNT = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_DEFAULT_ACCOUNT";
	public static String SHARED_PREF_USER_DEFAULT_TRACKING_KEY = "com.pesachoice.billpay.PCPesachoice.SHARED_PREF_USER_DEFAULT_TRACKING_KEY";
	public static String USER_INTENT_EXTRA = "user";
	public static String PC_APPROVED_RECOMMENDED_FRIENDS = "Recommended Approved Friends";
	public static String PC_NOT_APPROVED_RECOMMENDED_FRIENDS = "Recommended Not Approved";
	public static String USER_INTENT_EXTRA_COUNTRY = "sendToCountry";
	public static String PESACHOICE_PHONE_NUMBER = "+1(310) 776-5044";
	public static String PESACHOICE_EMAIL = "info@pesachoice.com";
	public static String STORAGE_ID = "pesachoice-80aec.appspot.com";
	public static String MAIN_FOLDER = "pesachoice_ids";
	public static int PESACHOICE_SELECT_COUNTRY = 1;
	public static int PESACHOICE_MINIMUM_MONEY = 5;
	public static String MOBILE_MONEY_PAYMENT_TYPE = "mobile_money";
	public static String CARD_PAYMENT_TYPE = "debit_card";
	public static int PESACHOICE_MAXMUM_MONEY_TO_PAY_TRANSACTION_FEE = 15;
	public static String FAILED_IN_AES_ENCRIPTION = "Exception while setting up password";
	public static int NUMBER_OF_FRAGMENT_IN_PCMAINTABACTIVITY = 3;
}