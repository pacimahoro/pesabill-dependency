/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.widget.BaseAdapter;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCAsyncListenerManyItem;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.business.PCControllable;
import com.pesachoice.billpay.business.PCControllableManyItem;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Use to create/construct appropriate controller objects using controller type as indicator
 * to determine which controller to build.
 * @author Odilon Senyana
 */
public class PCControllerFactory extends AsyncTask<Void, Integer, Boolean> {
    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean isOnline = isOnline();
        return isOnline;
    }

    /**
     * Enumeration for supported controllers
     */
    public enum PCControllerType {
        INFO_CONTROLLER,
        SIGN_UP_CONTROLLER,
        CREDENTIALS_CONTROLLER,
        VERIFY_CONTROLLER,
        COUNTRY_PROFILE,
        MERCHANTS_PROFILE,
        CALLING_PROFILE_CONTOLLER,
        USER_CONTROLLER,
        CARD_CONTROLLER,
        TRANSACTION_CONTROLLER,
        PING_SERVER,
        SAVE_PHOTO_METADATA,
        REFERRAL_PROMOTION_CONTROLLER,
        USER_RECEIVERS_CONTROLLER,
        USER_EVENTS_CONTROLLER,
        SEND_MESSAGE,
        USER_MANY_ITEM_CONTROLLER,
        AGENT_TRANSACTION_CONTROLLER;
    }

    /**
     * Given the desired controller type, this method constructs a corresponding controller
     * that the caller can subsequently use to call and manage services.
     * @param controllerType
     *                  Type of the controller desired
     * @param activity
     *                  Activity being used
     * @return <b>controllable</b>
     *                  Desired controller
     */
    public static PCControllable<? extends PCData> constructController(
            PCControllerType controllerType,
            PCAsyncListener activity) throws PCGenericError {//throws Throwable
       //check if we have internet if not tell users that they have to turn on their internet.

       PCControllable<? extends PCData>  controllable = null;
       try {
           if (isOnline()) {
               if (controllerType != null) {
                   switch (controllerType) {
                       case INFO_CONTROLLER:
                           //TODO: missing implementation
                           break;
                       case PING_SERVER:
                           controllable = new PCPingServerController(activity);
                           break;
                       case SIGN_UP_CONTROLLER:
                           controllable = new PCSignupController(activity);
                           break;
                       case CREDENTIALS_CONTROLLER:
                           controllable = new PCCredentialsController(activity);
                           break;
                       case VERIFY_CONTROLLER:
                           controllable = new PCVerifyController(activity);
                           break;
                       case COUNTRY_PROFILE:
                           controllable = new PCCountryProfileController(activity);
                           break;
                       case MERCHANTS_PROFILE:
                           controllable = new PCMerchantsProfileController(activity);
                           break;
                       case CALLING_PROFILE_CONTOLLER:
                           controllable = new PCCallingProfileController(activity);
                           break;
                       case USER_CONTROLLER:
                           controllable = new PCUserController(activity);
                           break;
                       case USER_MANY_ITEM_CONTROLLER:
                           controllable = new PCUserActivityController((PCAsyncListenerManyItem) activity);
                           break;
                       case CARD_CONTROLLER:
                           controllable = new PCCardController(activity);
                           break;
                       case TRANSACTION_CONTROLLER:
                           controllable = new PCTransactionController(activity);
                           break;
                       case USER_RECEIVERS_CONTROLLER:
                           controllable = new PCGetReceiversDetailsController((PCAsyncListenerManyItem) activity);
                           break;
                       case USER_EVENTS_CONTROLLER:
                           controllable = new PCEventsController(activity);
                           break;
                       case REFERRAL_PROMOTION_CONTROLLER:
                           controllable = new PCReferralPromotionController(activity);
                           break;
                       case SAVE_PHOTO_METADATA:
                           controllable = new PCSavePhotoMetadataController(activity);
                           break;
                       case SEND_MESSAGE:
                           controllable = new PCSendSMSViaPesaBusController(activity);
                           break;
                       case AGENT_TRANSACTION_CONTROLLER:
                           controllable = new PCAgentTransactionController(activity);
                           break;
                       default:
                           break; // case not supported yet
                   }
               }
           } else {
               throw new UnknownHostException();
           }
       } catch (Throwable message) {
           if (message instanceof UnknownHostException && activity != null) {
               String msg = "It seems like you're not connected to the internet, please verify your settings and try again";
               message = new UnknownHostException(msg);
 //              throw message;
               PCGenericError error = new PCGenericError();
               error.setMessage(msg);
               error.setTitle("Unable to connect to the network");
               error.setErrorType(PCGenericError.ErrorType.NETWORK_CONNECTIVITY);
               throw error;
           }
           Log.d("PCControllerFactory", message.getMessage());
       }
        return controllable;
    }

    /**
     * //TODO
     * @param controllerType
     * @param activity
     * @return
     */
    public static PCControllableManyItem<? extends PCData> constructControllerManyType(
            PCControllerType controllerType,
            PCAsyncListenerManyItem activity) {

        PCControllableManyItem<? extends PCData>  controllable = null;
        if (controllerType != null) {
            switch(controllerType) {
                case USER_MANY_ITEM_CONTROLLER:
                    controllable = new PCUserActivityController(activity);
                    break;

                default:
                    break; // case not supported yet
            }
        }
        return controllable;
    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess =  runtime.exec("ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;

    }




}
