/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business;

import android.app.Activity;

import com.pesachoice.billpay.business.PCPesabusClient.PCServiceType;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

/**
 * Interface reserved for any implementation that deal with service client for calling,
 * and managing services.
 * @author Odilon Senyana
 */
public interface PCControllable<T extends PCData> {


	/**
	 * Main component to use when accessing the service client code base. This method triggers the
	 * client and manages it to get the corresponding data from a particular service.
	 * The response object is saved in the instance variable reserved for response.
	 * Check implementing classes for more information.
	 * For a method that uses call back functionality,
	 * @see PCControllable#manageService(PCServiceType serviceType, Object requestData, Activity activity)
	 * @param serviceType
	 * 				Type of service requested
	 * @param requestData
	 * 				Data used for request
	 * @return {@link PCData}
	 * 				service result data
	 * @throws PCGenericError
	 */
	T manageService(PCServiceType serviceType, Object requestData) throws PCGenericError;

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
	 * For a simpler version, check @see PCControllable#manageService(PCServiceType serviceType, Object requestData)
	 * @param serviceType
	 * 				Type of service requested
	 * @param requestData
	 * 				Data used for request
	 * @param activity
	 * 				activity object, which has a method to use for callback
	 * @return {@link PCData}
	 * 				service result data
	 * @throws PCGenericError
	 */
	T manageService(PCServiceType serviceType, Object requestData, Activity activity) throws PCGenericError;


	/**
	 * Clears all instance variables that are used prior to re-processing.
	 */
	void clearAllInstances();


	/**
	 * Setter for service type.
	 * @param serviceType
	 * 				The type of service needed.
	 */
	public void setServiceType(PCServiceType serviceType);


	/**
	 * Setter for user request data.
	 * @param request
	 * 				request data for the service.
	 */
	public void setRequest(Object request);


	/**
	 * Setter for activity that is triggering the service calls.
	 * @param activity
	 * 				Activity that caused the service call.
	 */
	void setActivity(Activity activity);
}
