/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPingResults;
import com.pesachoice.billpay.model.PCSavePhotoIDMetadataRequest;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for saving photo ID metadata
 * 
 * @author Desire Aheza
 */
public class PCSavePhotoMetadataController extends PCBaseController {

	private final static String CLAZZ = PCSavePhotoMetadataController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCSavePhotoMetadataController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCUser doInBackground(Object... data) {
        PCUser photoResponseData = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCSavePhotoIDMetadataRequest) {
                    photoResponseData =
                            (PCUser) this.manageService(PCPesabusClient.PCServiceType.SAVE_PHOTO_METADATA, (PCSavePhotoIDMetadataRequest)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return photoResponseData;
	}

	@Override
	protected void onPostExecute(Object user) {
        if (user != null && user instanceof PCUser) {
            Log.v(CLAZZ, "Processing on post execute for saving photo metadata on the server");
            asyncListener.onTaskCompleted((PCUser)user);
        }
        else {
            Log.e(CLAZZ, "Missing response data result for saving photo metadata on the server");
            PCUser missingUser = new PCUser();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
        }
	}


    /**
     * This method is specific to uploading photo metadata.
     * {@inheritDoc}
     */
	@Override
    protected PCUser workWithService() {
        PCUser saveMetaDataRespData = null;
		if (this.request != null) {
            try {
                PCSavePhotoIDMetadataRequest request = (PCSavePhotoIDMetadataRequest)this.request;
                Log.v(CLAZZ, "Saving photo metadata on the server request [" + request + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCUser> response = restTemplate.postForEntity(PCPesabusClient.URL + PCPesabusClient.SAVE_PHOTO_METADATA_PATH, request, PCUser.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    saveMetaDataRespData = response.getBody();
                    // case 0: no response data
                    if (saveMetaDataRespData ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (saveMetaDataRespData.getErrorMessage() != null && ! "".equalsIgnoreCase(saveMetaDataRespData.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + saveMetaDataRespData.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after saving photo metadata on the server[" + saveMetaDataRespData + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                saveMetaDataRespData = new PCPingResults();
                saveMetaDataRespData.setErrorMessage(exc.getMessage());
            }
		}
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
		return saveMetaDataRespData;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
