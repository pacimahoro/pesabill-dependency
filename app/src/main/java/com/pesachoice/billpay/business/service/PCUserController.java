/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Queue;

/**
 * Handles the mechanism for reading user data, checking they are authenticated, all user activities, among other features.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 *
 * @author Odilon Senyana
 */
public class PCUserController extends PCBaseController {

    private final static String CLAZZ = PCUserController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCUserController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCData doInBackground(Object... data) {
        PCData responseData = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];

                // For reading user data, checking if the user is authenticated,...
                if (dataElement instanceof PCRequest) {
                    responseData =
                            (PCData)this.manageService(super.serviceType, (PCRequest)dataElement);
                }

            }
        }
        catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return responseData;
    }

    @Override
    protected void onPostExecute(Object response) {
        if (response != null) {
            if (response instanceof PCUser) {
                Log.d(CLAZZ, "Processing on post execute in user controller.");
                asyncListener.onTaskCompleted((PCUser)response);
            }
            else if (response instanceof Queue) {
                Log.d(CLAZZ, "Processing on post execute.");
            }
        }
        else {
            Log.e(CLAZZ, "Missing response data for user controller.");
            PCData missingUser = new PCData();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingUser);
        }
    }

    /**
     * This method is specific to user login, logout, forgot password feature,....
     * {@inheritDoc}
     */
    @Override
    protected PCData workWithService() {
        PCData serviceResponse = null;
        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCSpecialUser> userResponseData = null;
                switch (this.serviceType) {
                    case USER_INFO:
                        PCRequest userDataRequest = (PCRequest)request;
                        Log.d(CLAZZ, "Request data for reading user data [" + userDataRequest + "]");
                        userResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.USER_INFO_RELATIVE_PATH, userDataRequest, PCSpecialUser.class);
                        break;
                    case IS_AUTHENTICATED:
                        PCRequest isAuthenticatedReqData = (PCRequest)request;
                        Log.d(CLAZZ, "Request data to check if the user is authenticated [" + isAuthenticatedReqData + "]");
                        userResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.IS_USER_AUTHENTICATED_RELATIVE_PATH, isAuthenticatedReqData, PCSpecialUser.class);
                        ((PCMainTabActivity)activity).currentServiceTypeAuth=PCPesabusClient.PCServiceType.IS_AUTHENTICATED;
                        break;
                    default:
                        break; // case not handled
                }
                HttpStatus statusCode = userResponseData.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    /*
                     * Need to check using class hierachy to avoid superclasses from being prematurely absorbed:
                     */
                    if (userResponseData != null) {
                        serviceResponse = (PCDetailedUser)userResponseData.getBody();
                    }

                    if (serviceResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                        serviceResponse.setErrorMessage(PCPesachoiceConstant.UNABLE_TO_PROCESS_REQUESTS);
                    }
                    // case 1: an error occured
                    else if (serviceResponse.getErrorMessage() != null && ! "".equals(serviceResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + serviceResponse.getErrorMessage() + "]");
                        serviceResponse.setErrorMessage(serviceResponse.getErrorMessage());
                    }
                    // case 2: some generalResponseData data is present
                    else {
                        Log.d(CLAZZ, "Results [" + serviceResponse + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException(PCPesachoiceConstant.UNABLE_TO_COMPLETE_SERVICE_CALL + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return serviceResponse;
    }

    @Override
    public void clearAllInstances() {
        //TODO: Missing implementation
    }
}
      