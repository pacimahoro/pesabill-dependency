/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCCardServiceData;
import com.pesachoice.billpay.model.PCCustomerMetadata;
import com.pesachoice.billpay.model.PCGenericError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism to create profile of the card used for payments.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 * 
 * @author Odilon Senyana
 */

public class PCCardController extends PCBaseController {

	private final static String CLAZZ = PCCardController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCCardController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCCustomerMetadata doInBackground(Object... data) {
        PCCustomerMetadata customerMetadata = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCCardServiceData) {
                    customerMetadata =
                            (PCCustomerMetadata) this.manageService((PCCardServiceData)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return customerMetadata;
	}

	@Override
	protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCCustomerMetadata) {
            Log.v(CLAZZ, "Processing on post execute for card feature.");
            asyncListener.onTaskCompleted((PCCustomerMetadata)response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for card feature");
            PCCustomerMetadata missingCustomerMetadata = new PCCustomerMetadata();
            //TODO: Need to handle this a lil better:
            missingCustomerMetadata.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingCustomerMetadata);
        }
	}

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCCustomerMetadata}
     * 				service result data for customer card meta data
     * @throws PCGenericError
     */
	public PCCustomerMetadata manageService(Object requestData) throws PCGenericError {
        PCCustomerMetadata customerMetada = null;
		if (activity == null) {
			PCGenericError genericError = new PCGenericError();
			genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
			throw genericError;
		}
		try {
			this.request = requestData;
            customerMetada = workWithService();
		} catch (Throwable exc) {
			PCGenericError error = new PCGenericError();
			error.setMessage(exc.getMessage());
		}
		return customerMetada;
	}

    /**
     * This method uses the service to create the profile of the card used for payments.
     * {@inheritDoc}
     */
	@Override
    protected PCCustomerMetadata workWithService() {
        PCCustomerMetadata customerMetadataResponse = null;
        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCCustomerMetadata> response = null;
                switch (this.serviceType) {
                    case PAYMENT_PROFILE:
                        PCCardServiceData cardServiceReqData = (PCCardServiceData)request;
                        Log.d(CLAZZ, "Request data to create profile for card used for payments [" + cardServiceReqData + "]");
                        response = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.CREATE_PAYMENT_PROFILE_RELATIVE_PATH, cardServiceReqData, PCCustomerMetadata.class);
                        break;
                    case REPLACE_CARD:
                        PCCardServiceData replaceCardReqData = (PCCardServiceData)request;
                        Log.d(CLAZZ, "Request data to replace card used for payment [" + replaceCardReqData + "]");
                        response = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.REPLACE_EXISTING_PAYMENT_RELATIVE_PATH, replaceCardReqData, PCCustomerMetadata.class);
                        break;
                    default:
                        break; // case not supported
                }
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    customerMetadataResponse = response.getBody();
                    if (customerMetadataResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    else if (customerMetadataResponse.getErrorMessage() != null && ! "".equals(customerMetadataResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + customerMetadataResponse.getErrorMessage() + "]");
                    }
                    else {
                        Log.d(CLAZZ, "Service results [" + customerMetadataResponse + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return customerMetadataResponse;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
