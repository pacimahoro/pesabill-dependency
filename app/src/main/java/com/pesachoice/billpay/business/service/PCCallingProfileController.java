/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCCallingServiceInfo;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperatorRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for obtaining Calling profile.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with getting calling profile details.
 * 
 * @author Desire AHEZA
 */
public class PCCallingProfileController extends PCBaseController {

	private final static String CLAZZ = PCCallingProfileController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCCallingProfileController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCCallingServiceInfo doInBackground(Object... data) {
        PCCallingServiceInfo callingProfileResp = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCOperatorRequest) {
                    callingProfileResp =
                            (PCCallingServiceInfo) this.manageService((PCOperatorRequest)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return callingProfileResp;
	}

	@Override
	protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCCallingServiceInfo) {
            Log.v(CLAZZ, "Processing on post execute after retrieving calling profile");
            asyncListener.onTaskCompleted((PCCallingServiceInfo)response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for calling profile.");
           // ((PCMainTabActivity)activity).straightLogout();
            PCCallingServiceInfo missingCallingProfileInfo = new PCCallingServiceInfo();
            //TODO: Need to handle this a lil better:
            missingCallingProfileInfo.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingCallingProfileInfo);
        }
	}

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCCallingServiceInfo}
     * 				service result data for calling profile
     * @throws PCGenericError
     */
	public PCCallingServiceInfo manageService(Object requestData) throws PCGenericError {
        PCCallingServiceInfo callingProfile = null;
		if (activity == null) {
			PCGenericError genericError = new PCGenericError();
			genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
			throw genericError;
		}
		try {
			this.request = requestData;
            callingProfile = workWithService();
		} catch (Throwable exc) {
			PCGenericError error = new PCGenericError();
			error.setMessage(exc.getMessage());
		}
		return callingProfile;
	}

    /**
     * This method uses the service to fetch a given country profile.
     * {@inheritDoc}
     */
	@Override
    protected PCCallingServiceInfo workWithService() {
        PCCallingServiceInfo callingProfileResponse = null;
        if (this.request != null) {
            try {
                PCOperatorRequest callingProfileRequestData = (PCOperatorRequest)request;
                Log.d(CLAZZ, "Request data for calling profile [" + callingProfileRequestData + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCCallingServiceInfo> response = restTemplate.postForEntity(
                        PCPesabusClient.URL + PCPesabusClient.CALLING_PROFILE_RELATIVE_PATH, callingProfileRequestData, PCCallingServiceInfo.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    callingProfileResponse = response.getBody();

                    if (callingProfileResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }

                    else if (callingProfileResponse.getErrorMessage() != null && ! "".equals(callingProfileResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + callingProfileResponse.getErrorMessage() + "]");
                    }

                    else {
                        Log.d(CLAZZ, "Results after getting calling profile [" + callingProfileResponse + "]");
                    }
                }
                else {
                    //TODO: close app not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return callingProfileResponse;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
