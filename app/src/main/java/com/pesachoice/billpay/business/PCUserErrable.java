/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business;

import com.pesachoice.billpay.model.PCGenericError;

/**
 * To be implemented by an activity that takes user input
 * Created by pacifique on 11/1/15.
 */
public interface PCUserErrable {

    /**
     * It should be invoked to present an error
     * @param error
     *              Error Element to consider
     */
    public void presentError(PCGenericError error, String title);
}
