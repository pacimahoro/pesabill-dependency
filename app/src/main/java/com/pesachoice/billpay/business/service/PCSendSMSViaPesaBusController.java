/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCSendSMSRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for saving photo ID metadata
 * 
 * @author Desire Aheza
 */
public class PCSendSMSViaPesaBusController extends PCBaseController {

	private final static String CLAZZ = PCSendSMSViaPesaBusController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCSendSMSViaPesaBusController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCData doInBackground(Object... data) {
        PCData smsResponseData = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCSendSMSRequest) {
                    smsResponseData =
                            (PCData) this.manageService(PCPesabusClient.PCServiceType.SEND_MESSAGE, (PCSendSMSRequest)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return smsResponseData;
	}

	@Override
	protected void onPostExecute(Object data) {
        if (data != null && data instanceof PCData) {
            Log.v(CLAZZ, "Processing on post execute for sending message via pesabus");
            asyncListener.onTaskCompleted((PCData) data);
        }
        else {
            Log.e(CLAZZ, "Missing response data result for sending message via pesabus");
            PCData pcData = new PCData();
            //TODO: Need to handle this a lil better:
            pcData.setErrorMessage("Unable to process request.");
        }
	}


    /**
     * This method is specific to uploading photo metadata.
     * {@inheritDoc}
     */
	@Override
    protected PCData workWithService() {
        PCData smsDataRespData = null;
		if (this.request != null) {
            try {
                PCSendSMSRequest request = (PCSendSMSRequest)this.request;
                Log.v(CLAZZ, "Saving photo metadata on the server request [" + request + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCData> response = restTemplate.postForEntity(PCPesabusClient.URL + PCPesabusClient.SEND_MESSAGE, request, PCData.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    smsDataRespData = response.getBody();
                    // case 0: no response data
                    if (smsDataRespData ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (smsDataRespData.getErrorMessage() != null && ! "".equalsIgnoreCase(smsDataRespData.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + smsDataRespData.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after sending message on the server[" + smsDataRespData + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                smsDataRespData = new PCData();
                smsDataRespData.setErrorMessage(exc.getMessage());
            }
		}
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
		return smsDataRespData;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}

