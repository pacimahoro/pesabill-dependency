/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.app.Activity;
import android.os.AsyncTask;

import com.pesachoice.billpay.activities.PCAsyncListenerManyItem;
import com.pesachoice.billpay.business.PCControllableManyItem;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import java.util.Queue;
import java.util.concurrent.Callable;

/**
 * Base class for all controllers that request a collections of Objects
 * @author Desire AHEZA
 */
public abstract class PCBaseControllerManyItemResp extends AsyncTask<Object, Void, Queue<? extends PCData>> implements PCControllableManyItem<PCData>, Callable<Queue<? extends PCData>> {
   /*
    * Need to make the class generic enough and not make it rely on PCUser as base object for data
    */
    private final static String CLAZZ = PCBaseControllerManyItemResp.class.getName();

    protected PCPesabusClient.PCServiceType serviceType;
    protected Object request;
    protected Activity activity = new Activity();
    protected PCAsyncListenerManyItem asyncListener;

    public PCBaseControllerManyItemResp(PCAsyncListenerManyItem asyncListener){
        this.asyncListener = asyncListener;
    }
         
    @Override
    public final synchronized void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public final synchronized void setRequest(Object request) {
        this.request = request;
    }

    /**
     * Used set reference to the activity that is requesting data
     * @param activity
     *              screen where the request comes from
     */
    @Override
    public final synchronized void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public Queue<? extends PCData> call() throws PCGenericError {
        if(activity != null && serviceType != null && request != null) {
            return manageServiceManyItem(serviceType, request);
        }
        else {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_MISSING_DATA);
            StringBuilder field = new StringBuilder("");
            if (activity == null) {
                field.append("activity, ");
            }
            if (serviceType == null) {
                field.append("serviceType, ");
            }
            if (request == null) {
                field.append("request");
            }
            throw genericError;
        }
    }

    /**
     * This method is called from child controllers to make service call request of specified service type and return a collection of Object extending {@link PCData}
     * @param serviceType
     *                  Type of service we need to perform
     * @param requestData
     *                  Request
     * @return
     * @throws PCGenericError
     *                  Error
     */
    @Override
    public Queue<? extends PCData> manageServiceManyItem(PCPesabusClient.PCServiceType serviceType, Object requestData) throws PCGenericError {
        Queue<? extends PCData> responseData = null;
        if (activity == null) {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
            throw genericError;
        }
        try {
            this.request = requestData;
            responseData = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return responseData;
    }

    @Override
    public Queue<PCData>  manageServiceManyItem(PCPesabusClient.PCServiceType serviceType, Object requestData, Activity activity) throws PCGenericError {
        Queue<PCData>  userResponseData = null;
        PCGenericError genericError = new PCGenericError();
        genericError.setMessage(PCPesachoiceConstant.ERROR_MISSING_IMPLEMENTATION);
        throw genericError;
    }

    /**
     * Uses the pesabus ESB (Enterprise Service Bus) for various service tasks that include but not limited to:
     * <ol>
         * <li>retrieve all transactions</li>
     * </ol>
     *
     * @return T
     * 		Object extending {@link PCData} representing data results after service call depending on the service requested
     */
    abstract protected Queue<? extends PCData> workWithService();
}
