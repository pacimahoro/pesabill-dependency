/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.app.Activity;
import android.os.AsyncTask;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCControllable;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import java.util.concurrent.Callable;

/**
 * Base class for all controllers.
 * @author Odilon Senyana
 */
public abstract class PCBaseController extends AsyncTask<Object, Void, Object> implements PCControllable<PCData>, Callable<PCData> {
   /*
    * Need to make the class generic enough and not make it rely on PCUser as base object for data
    */
    private final static String CLAZZ = PCBaseController.class.getName();
    protected PCPesabusClient.PCServiceType serviceType;
    protected Object request;
    protected Activity activity = new Activity();
    protected PCAsyncListener asyncListener;

    public PCBaseController(PCAsyncListener asyncListener){
        this.asyncListener = asyncListener;
    }

    @Override
    public final synchronized void setServiceType(PCPesabusClient.PCServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public final synchronized void setRequest(Object request) {
        this.request = request;
    }

    @Override
    public final synchronized void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public PCData call() throws PCGenericError {
        if(activity != null && serviceType != null && request != null) {
            return manageService(serviceType, request);
        }
        else {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_MISSING_DATA);
            StringBuilder field = new StringBuilder("");
            if (activity == null) {
                field.append("activity, ");
            }
            if (serviceType == null) {
                field.append("serviceType, ");
            }
            if (request == null) {
                field.append("request");
            }
            throw genericError;
        }
    }

    @Override
    public PCData manageService(PCPesabusClient.PCServiceType serviceType, Object requestData) throws PCGenericError {
        PCData responseData = null;
        if (activity == null) {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
            throw genericError;
        }
        try {
            this.request = requestData;
            responseData = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return responseData;
    }

    @Override
    public PCData manageService(PCPesabusClient.PCServiceType serviceType, Object requestData, Activity activity) throws PCGenericError {
        PCData userResponseData = null;
        PCGenericError genericError = new PCGenericError();
        genericError.setMessage(PCPesachoiceConstant.ERROR_MISSING_IMPLEMENTATION);
        throw genericError;
    }

    /**
     * Uses the pesabus ESB (Enterprise Service Bus) for various service tasks that include but not limited to:
     * <ol>
         * <li>ping</li>
         * <li>registration</li>
         * <li>login</li>
         * <li>logout</li>
         * <li>forgot credentials</li>
         * <li>reset credentials</li>
         * <li>is authenticated</li>
         * <li>get user</li>
         * <li>create card</li>
         * <li>replace card</li>
         * <li>airtime transaction</li>
         * <li>pay electricity</li>
         * <li>pay tuition</li>
         * <li>pay tv</li>
         * <li>pay water bill</li>
         * <li>pay internet</li>
         * <li>retrieve all transactions</li>
         * <li>country profile for providers</li>
     * </ol>
     *
     * @return T
     * 		Object extending {@link PCData} representing data results after service call depending on the service requested
     */
    abstract protected <T extends PCData> T workWithService();
}
