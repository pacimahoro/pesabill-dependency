package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMerchantsProfile;
import com.pesachoice.billpay.model.PCOperatorRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Handle retrieving a merchants (list of sellers) profile
 * @author Pacifique Mahoro
 *
 */
public class PCMerchantsProfileController extends PCBaseController {
    private final static String CLAZZ = PCMerchantsProfileController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCMerchantsProfileController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    public void clearAllInstances() {

    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCMerchantsProfile doInBackground(Object... data) {
        PCMerchantsProfile countryProfileResp = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCOperatorRequest) {
                    countryProfileResp =
                            (PCMerchantsProfile) this.manageService((PCOperatorRequest)dataElement);
                }
            }
        }
        catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return countryProfileResp;
    }

    @Override
    protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCMerchantsProfile) {
            Log.v(CLAZZ, "Processing on post execute after retrieving country profile");
            asyncListener.onTaskCompleted((PCMerchantsProfile)response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for country profile.");
            PCMerchantsProfile missingCountryProfile = new PCMerchantsProfile();
            //TODO: Need to handle this a lil better:
            missingCountryProfile.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingCountryProfile);
        }
    }

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCMerchantsProfile}
     * 				service result data for country profile
     * @throws PCGenericError
     */
    public PCMerchantsProfile manageService(Object requestData) throws PCGenericError {
        PCMerchantsProfile countryProfile = null;
        if (activity == null) {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
            throw genericError;
        }
        try {
            this.request = requestData;
            countryProfile = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return countryProfile;
    }

    /**
     * This method uses the service to fetch a given country profile.
     * {@inheritDoc}
     */
    @Override
    protected PCMerchantsProfile workWithService() {
        PCMerchantsProfile countryProfileResponse = null;
        if (this.request != null) {
            try {
                PCOperatorRequest countryProfileRequestData = (PCOperatorRequest)request;
                Log.d(CLAZZ, "Request data for country profile [" + countryProfileRequestData + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCMerchantsProfile> response = restTemplate.postForEntity(
                        PCPesabusClient.URL + PCPesabusClient.MERCHANTS_PROFILE_RELATIVE_PATH, countryProfileRequestData, PCMerchantsProfile.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    countryProfileResponse = response.getBody();
                    // case 0: no response data
                    if (countryProfileResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (countryProfileResponse.getErrorMessage() != null && ! "".equals(countryProfileResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + countryProfileResponse.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after getting country profile [" + countryProfileResponse + "]");
                    }
                }
                else {
                    //TODO: close app not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return countryProfileResponse;
    }
}
    