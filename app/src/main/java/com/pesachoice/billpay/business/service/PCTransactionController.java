/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCAgentWalletTransaction;
import com.pesachoice.billpay.model.PCAirtimePaymentData;
import com.pesachoice.billpay.model.PCAirtimeTransaction;
import com.pesachoice.billpay.model.PCCallingPaymentData;
import com.pesachoice.billpay.model.PCCallingTransaction;
import com.pesachoice.billpay.model.PCElectricityPaymentData;
import com.pesachoice.billpay.model.PCElectricityTransaction;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCInternetPaymentData;
import com.pesachoice.billpay.model.PCInternetTransaction;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferTransaction;
import com.pesachoice.billpay.model.PCPAYTuitionPaymentData;
import com.pesachoice.billpay.model.PCProductPaymentData;
import com.pesachoice.billpay.model.PCProductPaymentTranscation;
import com.pesachoice.billpay.model.PCTVSubcriptionPaymentData;
import com.pesachoice.billpay.model.PCTVTransaction;
import com.pesachoice.billpay.model.PCTicketPaymentData;
import com.pesachoice.billpay.model.PCTicketPaymentTransaction;
import com.pesachoice.billpay.model.PCTransaction;
import com.pesachoice.billpay.model.PCTuitionTransaction;
import com.pesachoice.billpay.model.PCWaterPaymentData;
import com.pesachoice.billpay.model.PCWaterTransaction;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles client side of the payment features that include:
 * <ol>
 *     <li>Airtime topup</li>
 *     <li>Pay electricity</li>
 *     <li>Pay TV subscription</li>
 *     <li>Pay Internet</li>
 *     <li>Pay Internet</li>
 *     <li>Calling cards</li>
 *     <li>Pay insurance</li>
 *     <li>Buy tickets for event(s)</li>
 *     <li>Pay tuition</li>
 *     <li>Pay water bill</li>
 *     <li>All other possible payment types made available by PesaChoice</li>
 * </ol>
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 *
 * @author Odilon Senyana
 */
public class PCTransactionController extends PCBaseController {

	private final static String CLAZZ = PCTransactionController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCTransactionController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCTransaction doInBackground(Object... data) {
		PCTransaction transaction = null;
		try {
			if (data != null && data.length > 0) {
				// We are only concerned with the first element of the array 'data'
				Object dataElement = data[0];

				// airtime:
				if (dataElement instanceof PCAirtimePaymentData) {
					transaction =
							(PCAirtimeTransaction) this.manageService((PCAirtimePaymentData) dataElement);
				}
				// Refilling via KIVUTEL
				else if (dataElement instanceof PCCallingPaymentData) {
					transaction =
							(PCCallingTransaction) this.manageService((PCCallingPaymentData) dataElement);
				}

				// electricity payment:
				else if (dataElement instanceof PCElectricityPaymentData) {
					transaction =
							(PCElectricityTransaction) this.manageService((PCElectricityPaymentData) dataElement);
				}

				// internet payment:
				else if (dataElement instanceof PCInternetPaymentData) {
					transaction =
							(PCInternetTransaction) this.manageService((PCInternetPaymentData) dataElement);
				}

				// TV payment:
				else if (dataElement instanceof PCTVSubcriptionPaymentData) {
					transaction =
							(PCTVTransaction) this.manageService((PCTVSubcriptionPaymentData) dataElement);
				}

				//Tuition Payment:
				else if (dataElement instanceof PCPAYTuitionPaymentData) {
					transaction = (PCTuitionTransaction) this.manageService((PCPAYTuitionPaymentData) dataElement);
				}
				//Tuition Water:
				else if (dataElement instanceof PCWaterPaymentData) {
					transaction = (PCWaterTransaction) this.manageService((PCWaterPaymentData) dataElement);
				}
				//Ticket Payment
				else if (dataElement instanceof PCTicketPaymentData) {
					transaction = (PCTicketPaymentTransaction) this.manageService((PCTicketPaymentData) dataElement);
				}
				//Money Transfer transaction:
				else if (dataElement instanceof PCMoneyTransferPaymentData) {

					try {
						ObjectMapper mapper = new ObjectMapper();
						Log.d("MONEY_TRANSFER_REQUEST",mapper.writeValueAsString((PCMoneyTransferPaymentData) dataElement));
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					transaction = (PCMoneyTransferTransaction) this.manageService((PCMoneyTransferPaymentData) dataElement);
				}
				//RETAIL_PRODUCTS Product transaction
				else if (dataElement instanceof PCProductPaymentData) {
					transaction = (PCProductPaymentTranscation) this.manageService((PCProductPaymentData) dataElement);
				}
			}
		} catch (PCGenericError error) {
			//TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
		return transaction;
	}

	@Override
	protected void onPostExecute(Object response) {
		if (response != null && response instanceof PCTransaction) {
			Log.v(CLAZZ, "Processing on post execute after payment transaction");
			asyncListener.onTaskCompleted((PCTransaction) response);
		} else {
			Log.e(CLAZZ, "Missing response data after payment transaction.");
			PCTransaction missingTransactionResponse = new PCTransaction();

			//TODO: Need to handle this a lil better:
			String errorMessage = "Unable to process request.";
			if (response instanceof PCGenericError) {
				errorMessage = ((PCGenericError) response).getMessage();
			}
			missingTransactionResponse.setErrorMessage(errorMessage);
			asyncListener.onTaskCompleted(missingTransactionResponse);
		}
	}

	/**
	 * Main component to use when accessing the service client code base. This method triggers the
	 * client and manages it to get the corresponding data from payment transaction service.
	 * The response object is saved in the instance variable reserved for response.
	 * @param requestData
	 * 				Data used for request
	 * @return {@link PCTransaction}
	 * 				service result data for payment service that includes airtime, electricity, tv and internet
	 * @throws PCGenericError
	 */
	public PCTransaction manageService(Object requestData) throws PCGenericError {
		PCTransaction transactionResp = null;
		if (activity == null) {
			PCGenericError genericError = new PCGenericError();
			genericError.setMessage("Activity object cannot be null. It needs to be set.");
			throw genericError;
		}
		try {
			this.request = requestData;
			transactionResp = workWithService();
		} catch (Throwable exc) {
			PCGenericError error = new PCGenericError();
			error.setMessage(exc.getMessage());
		}
		return transactionResp;
	}

	/**
	 * This method uses the service to fetch a given country profile.
	 * {@inheritDoc}
	 */
	@Override
	protected PCTransaction workWithService() {
		PCTransaction transactionResponse = null;
		if (this.request != null) {
			try {
				restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
				ResponseEntity<PCAirtimeTransaction> airtime = null;
				ResponseEntity<PCCallingTransaction> calling = null;
				ResponseEntity<PCElectricityTransaction> electricity = null;
				ResponseEntity<PCInternetTransaction> internet = null;
				ResponseEntity<PCTVTransaction> tv = null;
				ResponseEntity<PCTuitionTransaction> tuition = null;
				ResponseEntity<PCWaterTransaction> water = null;
				ResponseEntity<PCMoneyTransferTransaction> moneyTransfer = null;
				ResponseEntity<PCProductPaymentTranscation> sellProduct = null;
				ResponseEntity<PCTicketPaymentTransaction> ticketPayment = null;
				ResponseEntity<PCAgentWalletTransaction> agentTransaction = null;
				ResponseEntity<PCAgentWalletTransaction> insuranceTransaction = null;

				switch (this.serviceType) {
					case SEND_AIRTIME:
						PCAirtimePaymentData airtimeRequest = (PCAirtimePaymentData) request;
						Log.d(CLAZZ, "Request data for sending airtime [" + airtimeRequest + "]");
						airtime = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_AIRTIME_RELATIVE_PATH, airtimeRequest, PCAirtimeTransaction.class);
						break;
					case CALLING_REFIL:
						PCCallingPaymentData callingRequest = (PCCallingPaymentData) request;
						Log.d(CLAZZ, "Request data for sending refilling credit [" + callingRequest + "]");
						calling = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_CREDIT_KIVUTEL_RELATIVE_PATH, callingRequest, PCCallingTransaction.class);
						break;
					case PAY_ELECTRICITY:
						PCElectricityPaymentData electricityRequest = (PCElectricityPaymentData) request;
						Log.d(CLAZZ, "Request data for paying electricity [" + electricityRequest + "]");
						electricity = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_ELECTRICITY_RELATIVE_PATH, electricityRequest, PCElectricityTransaction.class);
						break;
					case PAY_INTERNET:
						PCInternetPaymentData internetRequest = (PCInternetPaymentData) request;
						Log.d(CLAZZ, "Request data for paying internet [" + internetRequest + "]");
						internet = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_INTERNET_RELATIVE_PATH, internetRequest, PCInternetTransaction.class);
						break;
					case PAY_TV:
						PCTVSubcriptionPaymentData tvRequest = (PCTVSubcriptionPaymentData) request;
						Log.d(CLAZZ, "Request data for paying tv [" + tvRequest + "]");
						tv = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_TV_RELATIVE_PATH, tvRequest, PCTVTransaction.class);
						break;
					case PAY_TUITION:
						PCPAYTuitionPaymentData tuitionRequest = (PCPAYTuitionPaymentData) request;
						Log.d(CLAZZ, "Request data for paying tuition[" + tuitionRequest + "]");
						tuition = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_TUITION_RELATIVE_PATH, tuitionRequest, PCTuitionTransaction.class);
						break;
					case PAY_WATER:
						PCWaterPaymentData waterRequest = (PCWaterPaymentData) request;
						Log.d(CLAZZ, "Request data for paying water [" + waterRequest + "]");
						water = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_WATER_RELATIVE_PATH, waterRequest, PCWaterTransaction.class);
						break;
					case MONEY_TRANSFER:
						PCMoneyTransferPaymentData moneyTransferRequest = (PCMoneyTransferPaymentData) request;
						Log.d(CLAZZ, "Request data for Money Transfer [" + moneyTransferRequest + "]");
						moneyTransfer = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_MONEY_TRANSFER_RELATIVE_PATH, moneyTransferRequest, PCMoneyTransferTransaction.class);

						break;
					case SELL:
						PCProductPaymentData sellProductTrans = (PCProductPaymentData) request;
						Log.d(CLAZZ, "Request data for Selling product [" + sellProductTrans + "]");
						sellProduct = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_PRODUCT_TRANSACTION_RELATIVE_PATH, sellProductTrans, PCProductPaymentTranscation.class);

						break;
					case TICKETS:
						PCTicketPaymentData payTicketTrans = (PCTicketPaymentData) request;
						Log.d(CLAZZ, "Request data for paying ticket [" + payTicketTrans + "]");
						ticketPayment = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.PAY_TICKET_TRANSACTION_RELATIVE_PATH, payTicketTrans, PCTicketPaymentTransaction.class);
						break;
					case AGENT_TRANSACTION:
					PCAgentWalletData agentPay = (PCAgentWalletData) request;
					Log.d(CLAZZ, "Request data for agent transaction [" + agentPay + "]");
					agentTransaction = restTemplate.postForEntity(
							PCPesabusClient.URL + PCPesabusClient.TRANSACTION_BY_AGENT_ON_BEHALF_OF_CUSTOMER, agentPay, PCAgentWalletTransaction.class);
					break;
					case INSURANCE:
						PCAgentWalletData insurancePay = (PCAgentWalletData) request;
						Log.d(CLAZZ, "Request data for agent transaction [" +  insurancePay + "]");
						insuranceTransaction = restTemplate.postForEntity(
								PCPesabusClient.URL + PCPesabusClient.TRANSACTION_BY_AGENT_ON_BEHALF_OF_CUSTOMER,  insurancePay, PCAgentWalletTransaction.class);
						break;
					default:
						break; // case not handled
				}

				HttpStatus statusCode = null;

				if (airtime != null) {
					statusCode = airtime.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCAirtimeTransaction) airtime.getBody();
					}
				} else if (calling != null) {
					statusCode = calling.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCCallingTransaction) calling.getBody();
					}
				} else if (electricity != null) {
					statusCode = electricity.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCElectricityTransaction) electricity.getBody();
					}
				} else if (internet != null) {
					statusCode = internet.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCInternetTransaction) internet.getBody();
					}
				} else if (tv != null) {
					statusCode = tv.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCTVTransaction) tv.getBody();
					}
				} else if (tuition != null) {
					statusCode = tuition.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCTuitionTransaction) tuition.getBody();
					}
				} else if (water != null) {
					statusCode = water.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCWaterTransaction) water.getBody();
					}
				} else if (moneyTransfer != null) {
					statusCode = moneyTransfer.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCMoneyTransferTransaction) moneyTransfer.getBody();
					}
				} else if (sellProduct != null) {
					statusCode = sellProduct.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCProductPaymentTranscation) sellProduct.getBody();
					}
				} else if (ticketPayment != null) {
					statusCode = ticketPayment.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCTicketPaymentTransaction) ticketPayment.getBody();
					}
				} else if (agentTransaction != null) {
					statusCode = agentTransaction.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCAgentWalletTransaction) agentTransaction.getBody();
					}

				} else if (insuranceTransaction != null) {
					statusCode = insuranceTransaction.getStatusCode();
					if (HttpStatus.OK == statusCode) {
						transactionResponse = (PCAgentWalletTransaction) insuranceTransaction.getBody();
					}

				}

				// case 0: no response data
				if (transactionResponse == null) {
					Log.e(CLAZZ, "Missing response data.");
				}
				// case 1: an error occured
				else if (transactionResponse.getErrorMessage() != null && !"".equals(transactionResponse.getErrorMessage())) {
					Log.e(CLAZZ, "Error response [" + transactionResponse.getErrorMessage() + "]");
				}
				// case 2: some response data is present
				else {
					Log.d(CLAZZ, "Results after payment transaction [" + transactionResponse + "]");
				}

				if (statusCode != null && statusCode != HttpStatus.OK) {
					//TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
					throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
				}
			} catch (Throwable exc) {
				Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
				PCGenericError error = new PCGenericError();
				error.setMessage(exc.getMessage());
				throw new RuntimeException(error.getMessage(), error);
			}
		} else {
			Log.e(CLAZZ, "Unfortunately, input data is missing.");
		}
		return transactionResponse;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
