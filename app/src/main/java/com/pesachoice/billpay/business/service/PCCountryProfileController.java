/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCCountryProfile;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperatorRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for obtaining country profile.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 * 
 * @author Odilon Senyana
 */
public class PCCountryProfileController extends PCBaseController {

	private final static String CLAZZ = PCCountryProfileController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCCountryProfileController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCCountryProfile doInBackground(Object... data) {
		PCCountryProfile countryProfileResp = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCOperatorRequest) {
                    countryProfileResp =
                            (PCCountryProfile) this.manageService((PCOperatorRequest)dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return countryProfileResp;
	}

	@Override
	protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCCountryProfile) {
            Log.v(CLAZZ, "Processing on post execute after retrieving country profile");
            asyncListener.onTaskCompleted((PCCountryProfile)response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for country profile.");
            PCCountryProfile missingCountryProfile = new PCCountryProfile();
            //TODO: Need to handle this a lil better:
            missingCountryProfile.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingCountryProfile);
        }
	}

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from a particular service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCCountryProfile}
     * 				service result data for country profile
     * @throws PCGenericError
     */
	public PCCountryProfile manageService(Object requestData) throws PCGenericError {
        PCCountryProfile countryProfile = null;
		if (activity == null) {
			PCGenericError genericError = new PCGenericError();
			genericError.setMessage(PCPesachoiceConstant.ERROR_NULL_ACTIVITY);
			throw genericError;
		}
		try {
			this.request = requestData;
            countryProfile = workWithService();
		} catch (Throwable exc) {
			PCGenericError error = new PCGenericError();
			error.setMessage(exc.getMessage());
		}
		return countryProfile;
	}

    /**
     * This method uses the service to fetch a given country profile.
     * {@inheritDoc}
     */
	@Override
    protected PCCountryProfile workWithService() {
        PCCountryProfile countryProfileResponse = null;
        if (this.request != null) {
            try {
                PCOperatorRequest countryProfileRequestData = (PCOperatorRequest)request;
                Log.d(CLAZZ, "Request data for country profile [" + countryProfileRequestData + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCCountryProfile> response = restTemplate.postForEntity(
                        PCPesabusClient.URL + PCPesabusClient.COUNTRY_PROFILE_RELATIVE_PATH, countryProfileRequestData, PCCountryProfile.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    countryProfileResponse = response.getBody();
                    if (countryProfileResponse ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    else if (countryProfileResponse.getErrorMessage() != null && ! "".equals(countryProfileResponse.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + countryProfileResponse.getErrorMessage() + "]");
                    }

                    else {
                        Log.d(CLAZZ, "Results after getting country profile [" + countryProfileResponse + "]");
                    }
                }
                else {
                    //TODO: close app not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return countryProfileResponse;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
