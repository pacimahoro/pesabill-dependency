package com.pesachoice.billpay.business.service;

import android.app.Activity;
import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListenerManyItem;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.pesachoice.billpay.business.PCPesabusClient.GET_RECEVERS_RELATIVE_PATH;
import static com.pesachoice.billpay.business.PCPesabusClient.PCServiceType;
import static com.pesachoice.billpay.business.PCPesabusClient.URL;


/**
 * Handles the mechanism for requesting a collection of Object extending {@Link PCData}
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with getting a collection of {@Link PCData}.
 * <p/>
 * Currently this controller is used to request a collection of user {@Link PCUSER} which are
 * all Details of all Receivers that a user has send transaction to.
 * Future work will be done to improve the way it handles the response
 * <p/>
 * Created by desire.aheza on 09/08/2016.
 */
public class PCGetReceiversDetailsController extends PCBaseControllerManyItemResp {

    private final static String CLAZZ = PCGetReceiversDetailsController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCGetReceiversDetailsController(PCAsyncListenerManyItem asyncListener) {
        super(asyncListener);
    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected Queue<PCData> doInBackground(Object... data) {
        Queue<PCData> responseData = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                // For user receivers
                if (dataElement instanceof PCRequest) {
                    responseData =
                            (Queue<PCData>) this.manageServiceManyItem(super.serviceType, (PCRequest) dataElement);
                }

            }

        } catch (PCGenericError error) {
            //TODO
            Log.e(CLAZZ, error.getMessage());
        }
        return responseData;
    }
       

    protected void onPostExecute(Queue<? extends PCData> response) {
        if (response != null) {
            if (response instanceof Queue) {
                Log.d(CLAZZ, "Processing on post execute." + response.size());
                asyncListener.onTaskCompleted((Queue<? extends PCData>) response);
            }
        } else {
            Log.e(CLAZZ, "Missing response data for user controller.");
            PCData missingUser = new PCData();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingUser);
        }
    }

    @Override
    protected Queue<? extends PCData> workWithService() {
        Queue<? extends PCData> serviceResponse = null;
        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<Queue<? extends PCUser>> receiversResponseData = null;
                switch (this.serviceType) {
                    case GET_ALL_RECEIVERS:
                        PCRequest getReceiversRequest = (PCRequest) request;
                        Log.d(CLAZZ, "Request data to retrieve Receivers that user send transactions to [" + getReceiversRequest + "]");
                        final HttpHeaders requestHeaders = new HttpHeaders();
                        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
                        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                        HttpEntity<PCRequest> httpEntityRqst = new HttpEntity<>(getReceiversRequest, requestHeaders);
                        receiversResponseData = restTemplate.exchange(URL + GET_RECEVERS_RELATIVE_PATH, HttpMethod.POST, httpEntityRqst, new ParameterizedTypeReference<Queue<? extends PCUser>>() {});
                        break;
                    default:
                        break; // case not handled
                }
                HttpStatus statusCode = (receiversResponseData != null) ? receiversResponseData.getStatusCode() : HttpStatus.FAILED_DEPENDENCY;
                if (HttpStatus.OK == statusCode) {
                    if (receiversResponseData != null) {
                        serviceResponse = receiversResponseData.getBody();
                    }
                } else if (HttpStatus.NO_CONTENT == statusCode) {
                    serviceResponse = new LinkedBlockingQueue<>();
                }
                 else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
        return serviceResponse;
    }

    @Override
    public PCData manageService(PCServiceType serviceType, Object requestData) throws PCGenericError {
        //TODO
        return null;
    }

    @Override
    public PCData manageService(PCServiceType serviceType, Object requestData, Activity activity) throws PCGenericError {
        //TODO
        return null;
    }

    @Override
    public void clearAllInstances() {

    }

}
