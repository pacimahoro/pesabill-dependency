package com.pesachoice.billpay.business;

import com.pesachoice.billpay.model.PCActivity;

/**
 * Created by desire.aheza on 10/12/2016.
 */
public class PCPesabusClientImpl implements PCPesabusClient {
    @Override
    public boolean isDeviceConnected() {
        return false;
    }

    public static PCServiceType getServiceType(PCActivity.PCPaymentProcessingType processingType) {
        PCServiceType serviceType = null;
        if (processingType == PCActivity.PCPaymentProcessingType.AIR_TIME) {
            serviceType = PCServiceType.SEND_AIRTIME;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.CALLING) {
            serviceType = PCServiceType.CALLING_REFIL;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL) {
           serviceType = PCServiceType.PAY_ELECTRICITY;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.INTERNET_BILL) {
            serviceType = PCServiceType.PAY_INTERNET;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.WATER) {
            serviceType = PCServiceType.PAY_WATER;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.MONEY_TRANSFER) {
            serviceType = PCServiceType.MONEY_TRANSFER;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.TUITION) {
            serviceType = PCServiceType.PAY_TUITION;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.TV_BILL) {
            serviceType = PCServiceType.PAY_TV;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.RETAIL_PRODUCTS) {
            serviceType = PCServiceType.SELL;
        }
        else if (processingType == PCActivity.PCPaymentProcessingType.EVENT_TICKETS) {
            serviceType = PCServiceType.TICKETS;
        }
       return serviceType;
    }
}
