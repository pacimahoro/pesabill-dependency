/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCDetailedUser;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for verifing phones and emails and completing signup process.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 *
 * @author Odilon Senyana
 */
public class PCVerifyController extends PCBaseController {

    private final static String CLAZZ = PCVerifyController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCVerifyController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCUser doInBackground(Object... data) {
        PCUser userResponseData = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCRequest && serviceType == PCPesabusClient.PCServiceType.VERIFY_PHONE) {
                    userResponseData =
                            (PCUser) this.manageService(PCPesabusClient.PCServiceType.VERIFY_PHONE, (PCRequest) dataElement);
                }
               else if (dataElement instanceof PCRequest) {
                    userResponseData =
                            (PCSpecialUser) this.manageService(PCPesabusClient.PCServiceType.COMPLETE_REGISTRATION, (PCRequest) dataElement);
                }
            }
        } catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return userResponseData;
    }

    @Override
    protected void onPostExecute(Object user) {
        if (user != null && user instanceof PCUser && serviceType == PCPesabusClient.PCServiceType.VERIFY_PHONE ) {
            Log.v(CLAZZ, "Processing on post execute in verify controller");
            asyncListener.onTaskCompleted((PCUser) user);
        }
        else if (user != null && user instanceof PCDetailedUser) {
            Log.v(CLAZZ, "Processing on post execute in verify controller");
            asyncListener.onTaskCompleted((PCDetailedUser) user);
        }
        else {
            Log.e(CLAZZ, "Missing response data for verify controller.");
            PCUser missingUser = new PCUser();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
        }
    }


    /**
     * This method is specific to singing up new users.
     * {@inheritDoc}
     */
    @Override
    protected PCUser workWithService() {
        PCUser userRespData = null;
        ResponseEntity<PCUser> response = null;
        ResponseEntity<PCSpecialUser> verificationCompleteResp = null;
        if (this.request != null) {
            try {
                Log.d(CLAZZ, "Request data [" + request + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                String relativePath = "";
                switch (serviceType) {
                    case VERIFY_PHONE:
                        relativePath = PCPesabusClient.VERIFY_PHONE_RELATIVE_PATH;
                        response = restTemplate.postForEntity(
                                PCPesabusClient.URL + relativePath, request, PCUser.class);
                        break;
                    case COMPLETE_REGISTRATION:
                        PCRequest verifyRequest = (PCRequest) request;
                        relativePath = PCPesabusClient.COMPLETE_REGISTRATION_RELATIVE_PATH;
                        verificationCompleteResp = restTemplate.postForEntity(
                                PCPesabusClient.URL + relativePath, verifyRequest, PCSpecialUser.class);
                        break;
                    default:
                        break; // not supported
                }
                HttpStatus statusCode = null;
                if (response != null) {
                    statusCode = response.getStatusCode();
                    if (HttpStatus.OK == statusCode) {
                        userRespData = response.getBody();
                    }
                } else if (verificationCompleteResp != null) {
                    statusCode = verificationCompleteResp.getStatusCode();
                    if (HttpStatus.OK == statusCode) {
                        userRespData = verificationCompleteResp.getBody();
                    }
                }
                // case 0: no response data
                if (userRespData == null) {
                    Log.e(CLAZZ, "Missing response data.");
                }
                // case 1: an error occured
                else if (StringUtils.isEmpty(userRespData.getErrorMessage())) {
                    Log.e(CLAZZ, "Error response [" + userRespData.getErrorMessage() + "]");
                }
                // case 2: some response data is present
                else {
                    Log.d(CLAZZ, "Results [" + userRespData + "]");
                }

                if (HttpStatus.OK != statusCode) {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        } else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
        return userRespData;
    }

    @Override
    public void clearAllInstances() {
        //TODO: Missing implementation
    }
}
