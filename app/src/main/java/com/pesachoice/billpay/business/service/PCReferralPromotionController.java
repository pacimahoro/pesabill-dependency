package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPromotion;
import com.pesachoice.billpay.model.PCReferralData;
import com.pesachoice.billpay.model.PCReferralRequest;
import com.pesachoice.billpay.model.PCRequestReferralPromotion;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by desire.aheza on 7/31/2016.
 */
public class PCReferralPromotionController extends PCBaseController {

    public PCReferralPromotionController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    private final static String CLAZZ = PCReferralPromotionController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();


    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCData doInBackground(Object... data) {
        PCData promotionAndReferral = null;
        try {
            if(data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];

                // promotional:
                if (dataElement instanceof PCRequestReferralPromotion && serviceType == PCPesabusClient.PCServiceType.VERIFY_PROMOTION_CODE) {
                    promotionAndReferral = this.manageService((PCRequestReferralPromotion)dataElement);
                }
                else if (dataElement instanceof PCReferralRequest && serviceType == PCPesabusClient.PCServiceType.REFERRAL_USERS) {
                   //Refer new users pesachoice via pesabus
                    promotionAndReferral = this.manageService((PCReferralRequest)dataElement);
                }
                //TODO: handle referral program here
            }
        }
        catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return promotionAndReferral;
    }

    @Override
    protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCPromotion && serviceType == PCPesabusClient.PCServiceType.VERIFY_PROMOTION_CODE ) {
            Log.v(CLAZZ, "Processing on post execute in verify promotion code controller");
            asyncListener.onTaskCompleted((PCPromotion) response);
        }
        else if (response != null && response instanceof PCReferralData && serviceType == PCPesabusClient.PCServiceType.REFERRAL_USERS) {
            Log.v(CLAZZ, "Processing on post execute in referral controller");
            asyncListener.onTaskCompleted((PCReferralData) response);
        }
        else {
            Log.e(CLAZZ, "Missing response data for verify controller.");
            //TODO: Need to handle this a lil better

            if (serviceType == PCPesabusClient.PCServiceType.REFERRAL_USERS) {
                PCReferralData dataD = new PCReferralData();
                dataD.setErrorMessage("Unable to process request.");
                asyncListener.onTaskCompleted(dataD);
            }
            else {
                PCPromotion dataD = new PCPromotion();
                dataD.setErrorMessage("Unable to process request.");
                asyncListener.onTaskCompleted(dataD);
            }
        }
    }

    /**
     *
     */
    public PCData manageService(Object requestData) throws PCGenericError {
        PCData transactionResp = null;

        try {
            this.request = requestData;
            transactionResp = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return transactionResp;
    }

    /**
     * This method uses the service to fetch a given country profile.
     * {@inheritDoc}
     */
    @Override
    protected PCData workWithService() {
        PCData pcDataResponse = null;
        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCPromotion> promotion = null;
                ResponseEntity<PCReferralData> referralData=null;
                String relativePath="";

                switch (this.serviceType) {

                    case VERIFY_PROMOTION_CODE:
                        PCRequestReferralPromotion verifyPromotionCodeRequest = (PCRequestReferralPromotion) request;
                        relativePath = PCPesabusClient.VERIFY_PROMOTION_CODE_PATH;
                        promotion = restTemplate.postForEntity(
                                PCPesabusClient.URL + relativePath, verifyPromotionCodeRequest, PCPromotion.class);
                        break;

                    case REFERRAL_USERS:
                        PCReferralRequest referralUserRequest = (PCReferralRequest)request;
                        relativePath = PCPesabusClient.SUBMIT_REFERRAL_PATH;
                        referralData = restTemplate.postForEntity(
                                PCPesabusClient.URL + relativePath,referralUserRequest,PCReferralData.class);

                    default:
                        break; // case not handled
                }

                HttpStatus statusCode = null;

                if (promotion != null) {
                    statusCode = promotion.getStatusCode();
                    if (HttpStatus.OK == statusCode) {
                        pcDataResponse = (PCPromotion) promotion.getBody();
                    }
                }
                else if (referralData != null) {
                    statusCode = referralData.getStatusCode();
                    if (statusCode == HttpStatus.OK){
                        pcDataResponse = (PCReferralData)referralData.getBody();
                    }
                }

                // case 0: no response data
                if (pcDataResponse ==  null) {
                    Log.e(CLAZZ, "Missing response data.");
                }
                // case 1: an error occured
                else if (pcDataResponse.getErrorMessage() != null && ! "".equals(pcDataResponse.getErrorMessage())) {
                    Log.e(CLAZZ, "Error response [" + pcDataResponse.getErrorMessage() + "]");
                }
                // case 2: some response data is present
                else {
                    Log.d(CLAZZ, "Results after referral/promotion action  [" + pcDataResponse + "]");
                }

                if (statusCode != null && statusCode != HttpStatus.OK) {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        }
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }

        return pcDataResponse;
    }

    @Override
    public void clearAllInstances() {
        //TODO: Missing implementation
    }
}
