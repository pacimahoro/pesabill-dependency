/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited. 
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 * Handles the mechanism for registering new customers.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 * 
 * @author Odilon Senyana
 */
public class PCSignupController extends PCBaseController {

	private final static String CLAZZ = PCSignupController.class.getName();

	private final RestTemplate restTemplate = new RestTemplate();

	public PCSignupController(PCAsyncListener asyncListener) {
		super(asyncListener);
	}

	@Override
	protected void onPreExecute() {
		asyncListener.onTaskStarted();
	}

	@Override
	protected PCUser doInBackground(Object... data) {
        PCUser userResponseData = null;
		try {
			if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                if (dataElement instanceof PCUser) {
                    userResponseData =
                            (PCUser) this.manageService(PCPesabusClient.PCServiceType.SIGN_UP, dataElement);
                }
			}
		}
		catch (PCGenericError error) {
            //TODO: handle the error
			Log.e(CLAZZ, error.getMessage());
		}
        return userResponseData;
	}

	@Override
	protected void onPostExecute(Object user) {
        if (user != null && user instanceof PCUser) {
            Log.v(CLAZZ, "Processing on post execute for sign up");
            asyncListener.onTaskCompleted((PCUser)user);
        }
        else {
            Log.e(CLAZZ, "Missing response data for signing up new user.");
            PCUser missingUser = new PCUser();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingUser);
        }
	}


    /**
     * This method is specific to singing up new users.
     * {@inheritDoc}
     */
	@Override
    protected PCUser workWithService() {
		PCUser userRespData = null;
		if (this.request != null) {
            try {
                PCUser userRequestData = (PCUser)request;
                Log.v(CLAZZ, "Request data for signing up [" + userRequestData + "]");
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCUser> response = restTemplate.postForEntity(
                        PCPesabusClient.URL + PCPesabusClient.SIGNUP_RELATIVE_PATH, userRequestData, PCUser.class);
                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.OK == statusCode) {
                    userRespData = response.getBody();
                    // case 0: no response data
                    if (userRespData ==  null) {
                        Log.e(CLAZZ, "Missing response data.");
                    }
                    // case 1: an error occured
                    else if (userRespData.getErrorMessage() != null && ! "".equalsIgnoreCase(userRespData.getErrorMessage())) {
                        Log.e(CLAZZ, "Error response [" + userRespData.getErrorMessage() + "]");
                    }
                    // case 2: some response data is present
                    else {
                        Log.d(CLAZZ, "Results after signing up [" + userRespData + "]");
                    }
                }
                else {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
		}
        else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
		return userRespData;
	}

	@Override
	public void clearAllInstances() {
		//TODO: Missing implementation
	}
}
