package com.pesachoice.billpay.business.service;
import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.model.PCAgentWalletData;
import com.pesachoice.billpay.model.PCAgentWalletTransaction;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCardPaymentProcessingData;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCMomoPaymentProcessingData;
import com.pesachoice.billpay.model.PCPaymentProcessingData;
import com.pesachoice.billpay.model.PCTransaction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;

/**
 * Able to handle client side of the payment features that include specific to when agents want to
 * do to a transaction for users:
 * <ol>
 *     <li>Energy topup by agents for customers</li>
 *     <li>Airtime topup by agents for customers</li>
 *     <li>Pay electricity for customers by agents</li>
 *     <li>Pay TV subscription for customers by agents</li>
 *     <li>Pay Internet for customers by agents</li>
 *     <li>Pay Internet for customers by agents</li>
 *     <li>Calling cards for customers by agents</li>
 *     <li>Pay insurance for customers by agents</li>
 *     <li>Buy tickets for event(s) for customers by agents</li>
 *     <li>Pay tuition for customers by agents</li>
 *     <li>Pay water bill for customers by agents</li>
 *     <li>All other possible payment types made available by PesaChoice</li>
 * </ol>
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with the transaction at hand.
 *
 */

/**
 * Created by emmy on 18/03/2018.
 */

public class PCAgentTransactionController  extends PCBaseController {

    private final static String CLAZZ = PCTransactionController.class.getName();
    private final RestTemplate restTemplate = new RestTemplate();

    public PCAgentTransactionController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCTransaction doInBackground(Object... data) {
        PCTransaction transaction = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];

                // Case when the payment is done by cash -- agent is doing it on behalf of the customer
                if (dataElement instanceof PCAgentWalletData) {
                    transaction =
                            (PCTransaction) this.manageService((PCAgentWalletData) dataElement);
                }
                // Case when the payment is done via mobile money or bank debit card  -- agent is doing it on behalf of the customer
                else if(dataElement instanceof PCPaymentProcessingData) {
                    if(dataElement instanceof PCMomoPaymentProcessingData)
                        transaction =
                                (PCAgentWalletTransaction) this.manageService((PCMomoPaymentProcessingData) dataElement);
                    else if(dataElement instanceof PCCardPaymentProcessingData)
                        transaction =
                                (PCAgentWalletTransaction) this.manageService((PCCardPaymentProcessingData) dataElement);
                }
            }
        } catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return transaction;
    }

    @Override
    protected void onPostExecute(Object response) {
        if (response != null && response instanceof PCTransaction) {
            Log.v(CLAZZ, "Processing on post execute after payment transaction for agent on behalf of customer");
            asyncListener.onTaskCompleted((PCTransaction) response);
        } else {
            Log.e(CLAZZ, "Missing response data after payment transaction by agent on behalf of customer.");
            PCTransaction missingTransactionResponse = new PCTransaction();

            //TODO: Need to handle this a lil better:
            String errorMessage = "Unable to process payment request, attempted by agent on behalf of customer.";
            if (response instanceof PCGenericError) {
                errorMessage = ((PCGenericError) response).getMessage();
            }
            missingTransactionResponse.setErrorMessage(errorMessage);
            asyncListener.onTaskCompleted(missingTransactionResponse);
        }
    }

    /**
     * Main component to use when accessing the service client code base. This method triggers the
     * client and manages it to get the corresponding data from payment transaction service.
     * The response object is saved in the instance variable reserved for response.
     * @param requestData
     * 				Data used for request
     * @return {@link PCTransaction}
     * 				service result data for payment service that includes airtime, electricity, tv and internet, momo payment,...
     * @throws PCGenericError
     */
    public PCTransaction manageService(Object requestData) throws PCGenericError {
        PCTransaction transactionResp = null;
        if (activity == null) {
            PCGenericError genericError = new PCGenericError();
            genericError.setMessage("Activity object cannot be null. It needs to be set.");
            throw genericError;
        }
        try {
            this.request = requestData;
            transactionResp = workWithService();
        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            error.setMessage(exc.getMessage());
        }
        return transactionResp;
    }

    /**
     * This method uses the service to fetch a given country profile.
     * {@inheritDoc}
     */
    @Override
    protected PCTransaction workWithService() {
        PCTransaction transactionResponse = null;

        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCAgentWalletTransaction> agentTransaction = null;
                PCBillPaymentData agentTransactionRequest = null;
                List<PCAgentWalletData> agentTransactions = new ArrayList<>();
                switch (this.serviceType) {
                    case AGENT_TRANSACTION:
                        // Case when paying with cash -- agent is doing it on behalf of the customer
                        if (request instanceof PCAgentWalletData) {
                            agentTransactionRequest = (PCAgentWalletData) request;
                            // Case when the payment is done via mobile money or bank debit card  -- agent is doing it on behalf of the customer
                        } else if(request instanceof PCPaymentProcessingData )
                            agentTransactionRequest = (PCAgentWalletData) request;
                        Log.d(CLAZZ, "Request data for agent transaction [" + agentTransactionRequest + "]");
                        agentTransaction = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.TRANSACTION_BY_AGENT_ON_BEHALF_OF_CUSTOMER, agentTransactionRequest, PCAgentWalletTransaction.class);
                        break;
                    case INSURANCE:

                        // Case when paying with cash -- agent is doing it on behalf of the customer
                        if (request instanceof PCAgentWalletData) {
                            agentTransactionRequest = (PCAgentWalletData) request;
                            // Case when the payment is done via mobile money or bank debit card  -- agent is doing it on behalf of the customer
                        } else if(request instanceof PCPaymentProcessingData )
                            agentTransactionRequest = (PCAgentWalletData) request;
                        Log.d(CLAZZ, "Request data for agent transaction [" + agentTransactionRequest + "]");
                        agentTransaction = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.TRANSACTION_BY_AGENT_ON_BEHALF_OF_CUSTOMER, agentTransactionRequest, PCAgentWalletTransaction.class);
                        break;

                    default:
                        break; // case not handled
                }
                HttpStatus statusCode = null;

                if (agentTransaction != null) {
                    statusCode = agentTransaction.getStatusCode();
                    if (HttpStatus.OK == statusCode) {
                        transactionResponse = (PCAgentWalletTransaction) agentTransaction.getBody();
                    }
                }
                // case 0: no response data
                if (transactionResponse == null) {
                    Log.e(CLAZZ, "Missing response data.");
                }
                // case 1: an error occured
                else if (transactionResponse.getErrorMessage() != null && !"".equals(transactionResponse.getErrorMessage())) {
                    Log.e(CLAZZ, "Error response [" + transactionResponse.getErrorMessage() + "]");
                }
                // case 2: some response data is present
                else {
                    Log.d(CLAZZ, "Results after payment transaction [" + transactionResponse + "]");
                }

                if (statusCode != null && statusCode != HttpStatus.OK) {
                    //TODO: not all status codes are to be treated as exception though. This needs to be revisited for sure
                    throw new RuntimeException("Unable to complete service call. Status code: " + statusCode);
                }
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        } else {
            Log.e(CLAZZ, "Unfortunately, input data is missing.");
        }
        return transactionResponse;
    }

    @Override
    public void clearAllInstances() {
        //TODO: Missing implementation
    }
}
