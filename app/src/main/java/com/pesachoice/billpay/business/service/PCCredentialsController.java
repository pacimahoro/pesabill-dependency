/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package com.pesachoice.billpay.business.service;

import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCCapability;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGeneralRequest;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCOperatorRequest;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Handles the mechanism for login, logout, forgot password, and other credentials processing.
 * It connects to the back-end through internal Enterprise Service Bus (ESB),
 * which in turn takes care of the logic that goes with registering new customers.
 *
 * @author Odilon Senyana
 */
public class PCCredentialsController extends PCBaseController {

    private final static String CLAZZ = PCCredentialsController.class.getName();

    private final RestTemplate restTemplate = new RestTemplate();

    public PCCredentialsController(PCAsyncListener asyncListener) {
        super(asyncListener);
    }

    @Override
    protected void onPreExecute() {
        asyncListener.onTaskStarted();
    }

    @Override
    protected PCData doInBackground(Object... data) {
        PCData responseData = null;
        try {
            if (data != null && data.length > 0) {
                // We are only concerned with the first element of the array 'data'
                Object dataElement = data[0];
                // For login, logout,...
                if (dataElement instanceof PCRequest) {
                    responseData =
                            (PCData) this.manageService(super.serviceType, (PCRequest) dataElement);
                }
                // For features such as forgot password, reset password,...
                else if (dataElement instanceof PCGeneralRequest) {
                    responseData =
                            (PCData) this.manageService(super.serviceType, (PCGeneralRequest) dataElement);
                }
            }
        } catch (PCGenericError error) {
            //TODO: handle the error
            Log.e(CLAZZ, error.getMessage());
        }
        return responseData;
    }

    @Override
    protected void onPostExecute(Object user) {
        if (user != null) {
            Log.d(CLAZZ, "Processing on post execute on credentials management.");
            /*
             * Class hierarchy needs to be taken into account. Check for PCUser before PCData, because PCData would
             * mistakenly get every object, if we were to start with checking it
             */
            if (user instanceof PCSpecialUser) {
                asyncListener.onTaskCompleted((PCSpecialUser) user);
            } else if (user instanceof PCData) {
                asyncListener.onTaskCompleted((PCData) user);
            }
        } else {
            Log.e(CLAZZ, "Missing response data for credential controller.");
            PCData missingUser = new PCData();
            //TODO: Need to handle this a lil better:
            missingUser.setErrorMessage("Unable to process request.");
            asyncListener.onTaskCompleted(missingUser);
        }
    }

    /**
     * This method is specific to user login, logout, forgot password feature,....
     * {@inheritDoc}
     */
    @Override
    protected PCData workWithService() {
        PCData serviceResponse = null;
        if (this.request != null) {
            try {
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<PCSpecialUser> userResponseData = null;
                ResponseEntity<PCCapability> capabilityResponseData = null;
                ResponseEntity<PCData> generalResponseData = null;
                switch (this.serviceType) {
                    case LOGIN:
                        PCRequest loginRequestData = (PCRequest) request;
                        Log.d(CLAZZ, "Request data for login [" + loginRequestData + "]");
                        if (loginRequestData.getCompanyId().equalsIgnoreCase("embracesolar")) {
                            userResponseData = restTemplate.postForEntity(
                                    PCPesabusClient.URL + PCPesabusClient.LOGIN_RELATIVE_PATH_AGENT, loginRequestData, PCSpecialUser.class);
                        } else {
                            userResponseData = restTemplate.postForEntity(
                                    PCPesabusClient.URL + PCPesabusClient.LOGIN_RELATIVE_PATH, loginRequestData, PCSpecialUser.class);

                        }
                        break;
                    case LOGOUT:
                        PCRequest logoutRequestData = (PCRequest) request;
                        Log.d(CLAZZ, "Request data for logout [" + logoutRequestData + "]");
                        userResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.LOGOUT_RELATIVE_PATH, logoutRequestData, PCSpecialUser.class);
                        break;
                    case FORGOT_PASSWORD:
                        PCGeneralRequest forgotPwdRequestData = (PCGeneralRequest) request;
                        Log.d(CLAZZ, "Request data when user forgets the password [" + forgotPwdRequestData + "]");
                        generalResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.FORGOT_PASSWORD_RELATIVE_PATH, forgotPwdRequestData, PCData.class);
                        break;
                    case RESET_PASSWORD:
                        PCGeneralRequest resetPasswordReqData = (PCGeneralRequest) request;
                        Log.d(CLAZZ, "Request data when user is resetting the password [" + resetPasswordReqData + "]");
                        userResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.RESET_PASSWORD_RELATIVE_PATH, resetPasswordReqData, PCSpecialUser.class);
                        break;
                    case GET_CAPABILITY_TOKEN:
                        PCRequest capabilityTokenRequestData = (PCRequest) request;
                        Log.d(CLAZZ, "Request data for capability [" + capabilityTokenRequestData + "]");
                        capabilityResponseData = restTemplate.postForEntity(
                                PCPesabusClient.URL + PCPesabusClient.GET_CAPABILITY_TOKEN, capabilityTokenRequestData, PCCapability.class);
                        break;
                    default:
                        break; // case not handled
                }

                /*
                 * Need to check using class hierachy to avoid superclasses from being prematurely absorbed:
                 */
                if (userResponseData != null) {
                    serviceResponse = userResponseData.getBody();
                } else if (generalResponseData != null) {
                    serviceResponse = generalResponseData.getBody();
                } else if (capabilityResponseData != null) {
                    serviceResponse = capabilityResponseData.getBody();
                }
                // case 0: no generalResponseData data
                if (serviceResponse == null) {
                    serviceResponse = new PCData();
                    Log.e(CLAZZ, "Missing response data.");
                    serviceResponse.setErrorMessage("Unable to process request.");
                }
                // case 1: an error occured
                else if (serviceResponse.getErrorMessage() != null && !"".equals(serviceResponse.getErrorMessage())) {
                    Log.e(CLAZZ, "Error response [" + serviceResponse.getErrorMessage() + "]");
                    serviceResponse.setErrorMessage(serviceResponse.getErrorMessage());
                }
                // case 2: some generalResponseData data is present
                else {
                    Log.d(CLAZZ, "Results [" + serviceResponse + "]");
                }

            } catch (Throwable exc) {
                Log.e(CLAZZ, "Error while working with service: [" + exc.getMessage() + "]");
                PCGenericError error = new PCGenericError();
                error.setMessage(exc.getMessage());
                throw new RuntimeException(error.getMessage(), error);
            }
        } else {
            Log.e(CLAZZ, PCPesachoiceConstant.ERROR_IN_REQUEST);
        }
        return serviceResponse;
    }

    @Override
    public void clearAllInstances() {
        //TODO: Missing implementation
    }
}
