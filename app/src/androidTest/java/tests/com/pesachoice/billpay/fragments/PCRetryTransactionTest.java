package tests.com.pesachoice.billpay.fragments;


import android.test.ActivityInstrumentationTestCase2;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.fragments.PCActivityDetailFragment;
import com.pesachoice.billpay.fragments.PCSupportRepeatTransFragment;

import org.junit.Test;

/**
 * Created by emmy on 11/7/17.
 */

public class PCRetryTransactionTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {

     PCMainTabActivity mainTabActivity;
     private static final String CLAZZ = PCRetryTransactionTest.class.getName();


    public PCRetryTransactionTest() {
        super(CLAZZ,PCMainTabActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();

    }

    @Test
    public void testRepeatTransactionCorrect () {

         assertNotNull(mainTabActivity);

        PCSupportRepeatTransFragment transFragment = new PCActivityDetailFragment();
        boolean result = transFragment.initiateCheckout();
        assertTrue(result);

    }
}
