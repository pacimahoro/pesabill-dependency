/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCUserActivityController;
import com.pesachoice.billpay.model.PCActivityRequest;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved to retrieve user activities.
 *
 * @author Odilon Senyana
 */
public class PCUserActivitiesControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCUserActivitiesControllerTest.class.getName();

    private PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCUserActivitiesControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testUserActivitiesService() {
        assertNotNull(mainTabActivity);

        final PCActivityRequest activityRequest = new PCActivityRequest();
        final PCUser userData = new PCUser();
        userData.setEmail("some_user@pesachoice.com");
        userData.setTokenId("8795da5a-3ccb-4239-934c-e03727414595");

        activityRequest.setUser(userData);
        try {
            PCUserActivityController userController = (PCUserActivityController) PCControllerFactory.constructControllerManyType(
                    PCControllerFactory.PCControllerType.USER_MANY_ITEM_CONTROLLER, mainTabActivity);
            userController.setActivity(mainTabActivity);
            userController.setServiceType(PCPesabusClient.PCServiceType.USER_ACTIVITIES);
            userController.execute(activityRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(mainTabActivity);
            /*
             * TODO: uncomment the code below once login activity is completed
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
