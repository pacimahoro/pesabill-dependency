/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferServiceInfo;
import com.pesachoice.billpay.model.PCMoneyTransferTransaction;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTransactionStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;


/**
 * This class tests the service element reserved for money transfer transaction.
 * @author Desire Aheza
 */
public class PCMoneyTransferTransactionControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCMoneyTransferTransactionControllerTest.class.getName();

    PCMainTabActivity fragActivityUtil;


    // The class behaves as Activity to call the service
    public PCMoneyTransferTransactionControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(true);
        fragActivityUtil = getActivity();
    }

    @Test
    public void testMoneyTransferService() {
        assertNotNull(fragActivityUtil);

        try {
            final CountDownLatch signal = new CountDownLatch(1);
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        PCTransactionController transactionController = new PCTransactionController(new PCAsyncListener() {
                            @Override
                            public void onTaskStarted() {
                                Log.d(CLAZZ,"money transfer testing started");
                            }

                            @Override
                            public void onTaskCompleted(PCData data) {
                                Log.d(CLAZZ,"money transfer testing finished");
                                assertNotNull(data);
                                assertNull(data.getErrorMessage());
                                //check if service info is not null
                                PCMoneyTransferServiceInfo serviceInfo = ((PCMoneyTransferTransaction)data).getServiceInfo();
                                assertNotNull(serviceInfo);
                                assertEquals(PCTransactionStatus.COMPLETE, serviceInfo.getTransactionStatus());
                                signal.countDown();// notify the count down latch
                            }
                        });
                        PCSpecialUser sender = new PCSpecialUser();
                        sender.setClassType(PCSpecialUser.class.getSimpleName());
                        sender.setUserName("desire kaka");
                        sender.setEmail("desire@test.com");
                        sender.setFirstName("desire");
                        sender.setLastName("kaka");
                        sender.setTokenId("8e10f0ac-5fff-4de4-8c0d-016de0c1a1cc");
                        sender.setLastLoginTime(0);
                        sender.setPhoneNumber("14055323339");

                        PCSpecialUser receiver = new PCSpecialUser();
                        receiver.setClassType(PCSpecialUser.class.getSimpleName());
                        receiver.setFirstName("john");
                        receiver.setLastName("Doe");
                        receiver.setPhoneNumber("256782848212");

                        PCMoneyTransferServiceInfo moneyTransferServiceInfo = new PCMoneyTransferServiceInfo();
                        moneyTransferServiceInfo.setUsd(5);
                        moneyTransferServiceInfo.setTotalUsd(5.5);
                        moneyTransferServiceInfo.setFees(0.5);
                        moneyTransferServiceInfo.setRate(0.5);
                        moneyTransferServiceInfo.setMaskedCardNumber("XXX4242");
                        moneyTransferServiceInfo.setTotalLocalCurrency(15000);
                        moneyTransferServiceInfo.setReceiverCountry("Uganda");

                        PCOperator operator = new PCOperator();
                        operator.setName("MTN");

                        PCMoneyTransferPaymentData moneyTransferPaymentDataReq = new PCMoneyTransferPaymentData();
                        moneyTransferPaymentDataReq.setSender(sender);
                        moneyTransferPaymentDataReq.setReceiver(receiver);
                        moneyTransferPaymentDataReq.setMoneyTransferServiceInfo(moneyTransferServiceInfo);
                        moneyTransferPaymentDataReq.setOperator(operator);
                        transactionController.setActivity(fragActivityUtil);
                        transactionController.setServiceType(PCPesabusClient.PCServiceType.MONEY_TRANSFER);
                        transactionController.execute(moneyTransferPaymentDataReq);
                        signal.await();// wait for callback
                    } catch (InterruptedException e) {
                        fail();
                        e.printStackTrace();
                    }catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });


            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}