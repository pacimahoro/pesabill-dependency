/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCReferralPromotionController;
import com.pesachoice.billpay.model.PCRequestReferralPromotion;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for cases when the user forgets the password.
 *
 * @author Odilon Senyana
 */
public class PCPromotialDiscountControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCPromotialDiscountControllerTest.class.getName();

    private PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCPromotialDiscountControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testAppliedService() {
        assertNotNull(mainTabActivity);
        try {

            PCUser logedInUser = mainTabActivity.getAppUser();
            PCReferralPromotionController promotionController = (PCReferralPromotionController) PCControllerFactory.constructController(PCControllerFactory.PCControllerType.REFERRAL_PROMOTION_CONTROLLER,mainTabActivity);
            PCPesabusClient.PCServiceType currentServiceType = PCPesabusClient.PCServiceType.VERIFY_PROMOTION_CODE;
            promotionController.setServiceType(currentServiceType);
            PCRequestReferralPromotion request = new PCRequestReferralPromotion();

            request.setEmail("desire@test.com");

            String promotionalCodeText = "DAL072312321#@!6";//DAL072016
            if (promotionalCodeText != null) {
                request.setPromoCode(promotionalCodeText);
            }

            request.setTokenId("acf3d7a1-b5a9-4465-8ff6-16d70660276b");
            request.setAmount("15.5");
            promotionController.setRequest(request);
            promotionController.execute(request);

            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);
            /*
             * TODO: missing assertions
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
