/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;


import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.model.PCGenericError;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class PCLoginActivityTest extends ActivityInstrumentationTestCase2<PCLoginActivity>  {
    PCLoginActivity loginActivity = new PCLoginActivity();
    private static final String CLAZZ = PCSignupActivityTest.class.getName();

    public PCLoginActivityTest() {
        super(CLAZZ,PCLoginActivity.class);
    }

	/*
	 * TODO: test cases incomplete
    */



    @Before
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        loginActivity = getActivity();
    }

    @Test
    public void testMissingPasswordField() {

        /**
         * Test missing password field  with negative case.
         */
        try {


            loginActivity.validCustomerInfo("some_user23@pesachoice.com", "");

        } catch (PCGenericError error) {
            error = new PCGenericError();
            assertEquals(error.getMessage(), "Password is required");
        }


    }


    @Test
    public void testMissingUsernameField() throws PCGenericError   {

        /**
         * Test missing username field negative case.
         */
        try {

            loginActivity.validCustomerInfo("", "m9$JdcU4g!ospSF<?1uf3");
            System.out.println("Error message wait");
        } catch (PCGenericError error) {
            error = new PCGenericError();
            System.out.println("Error message" +"  " + error.getMessage());
            assertEquals(error.getMessage(), "Email is required");

        }

    }



    @Test
    public void testLoginWithInvalidNumber() throws PCGenericError {

        /**
         * Test login with invalid number negative case by putting invalid phoneNumber below 6 characters
         * and containing letters not digits.
         */
        try {
            loginActivity.loginUser("5dsan", "m9$JdcU4g!ospSF<?1uf3", "323232");
        } catch (Exception error) {
            assertEquals(error.getMessage(), PCPesachoiceConstant.FAILED_IN_AES_ENCRIPTION);
        }

        /**
         * Test login with invalid number negative case by putting invalid phoneNumber above 15 characters
         * and contain digits only.
         */
        try {
            loginActivity.loginUser("2507826479856265738934","m9$JdcU4g!ospSF<?1uf3","323232");

        } catch (Exception error) {

            assertEquals(error.getMessage(),PCPesachoiceConstant.FAILED_IN_AES_ENCRIPTION);

        }


    }
}


