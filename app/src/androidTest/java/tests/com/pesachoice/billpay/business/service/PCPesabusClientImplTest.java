/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.content.Context;
import android.content.Intent;
import android.test.ServiceTestCase;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCPesabusClientImpl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Odilon Senyana
 */
public class PCPesabusClientImplTest extends ServiceTestCase<PCPesabusClientImpl> {
    private final static String CLAZZ = PCPesabusClientImplTest.class.getName();

    private Context systemContext;
    private Intent intent;

    public PCPesabusClientImplTest() {
        super(PCPesabusClientImpl.class);
    }

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        systemContext = getSystemContext();
        intent = new Intent(systemContext, PCPesabusClientImpl.class);
        Log.d(CLAZZ, "Setup complete");
    }

        //TODO: incomplete


    @Test
    public void testOnStartCommand() throws Exception {
        //Just test pinging the server. Other unit test classes will test remaining features
        intent.putExtra(PCPesabusClient.EXTRA_SERVICE_TYPE, PCPesabusClient.PCServiceType.PING);
        startService(intent);
    }



    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the service");
        super.tearDown();
    }


}
