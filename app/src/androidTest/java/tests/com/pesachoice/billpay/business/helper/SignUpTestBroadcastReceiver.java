/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pesachoice.billpay.business.PCPesabusClient;

/**
 * @author Odilon Senyana
 */
public class SignUpTestBroadcastReceiver extends BroadcastReceiver {

    private final static String CLAZZ = SignUpTestBroadcastReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
//            Uri data = intent.getData();
        Log.v(CLAZZ, "Got the following");
        String type = intent.getStringExtra(PCPesabusClient.EXTRA_ACTION_TYPE);
        Log.v(CLAZZ, "Got the following : [" + type + "]");
    }
}