package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCMoneyTransferPaymentData;
import com.pesachoice.billpay.model.PCMoneyTransferServiceInfo;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Created by desire.aheza on 10/10/2016.
 */
public class PCMoneyTransferControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCMoneyTransferControllerTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCMoneyTransferControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testAirtimePaymentService() {
        assertNotNull(mainTabActivity);

        try {
            final CountDownLatch signal = new CountDownLatch(1);
            assertNotNull(mainTabActivity);

            final PCSpecialUser sender = new PCSpecialUser();
            mainTabActivity.currentServiceType = PCPesabusClient.PCServiceType.MONEY_TRANSFER;

            sender.setClassType(PCSpecialUser.class.getSimpleName());
            sender.setUserName("desire@test.com");
            sender.setFirstName("desire");
            sender.setLastName("test");
            sender.setEmail("desire@test.com");
            sender.setTokenId("75b715da-39da-4a35-b4e6-14ddfee7e7c5");
            sender.setPhoneNumber("4055323339");

            PCSpecialUser receiver = new PCSpecialUser();
            receiver.setClassType(PCSpecialUser.class.getSimpleName());
            receiver.setFirstName("John");
            receiver.setLastName("Doe");
            receiver.setPhoneNumber("256782848212");

            PCMoneyTransferServiceInfo moneyTransferServiceInfo = new PCMoneyTransferServiceInfo();
            moneyTransferServiceInfo.setUsd(5);
            moneyTransferServiceInfo.setTotalUsd(5.5);
            moneyTransferServiceInfo.setFees(0.5);
            moneyTransferServiceInfo.setRate(0.5);
            moneyTransferServiceInfo.setMaskedCardNumber("XXX4242");
            moneyTransferServiceInfo.setTotalLocalCurrency(3500);
            //to test if the service will throw error because no country was setup
           // moneyTransferServiceInfo.setReceiverCountry("Uganda");

            PCOperator operator = new PCOperator();
            operator.setName("MTN_MOBILE_WALLET");

            PCMoneyTransferPaymentData moneyTransferPaymentDataReq = new PCMoneyTransferPaymentData();
            moneyTransferPaymentDataReq.setSender(sender);
            moneyTransferPaymentDataReq.setReceiver(receiver);
            moneyTransferPaymentDataReq.setMoneyTransferServiceInfo(moneyTransferServiceInfo);
            moneyTransferPaymentDataReq.setOperator(operator);


            try {
                PCTransactionController transactionController = (PCTransactionController)
                        PCControllerFactory.constructController(
                                PCControllerFactory.PCControllerType.TRANSACTION_CONTROLLER, mainTabActivity);
                transactionController.setActivity(mainTabActivity);
                transactionController.setServiceType(PCPesabusClient.PCServiceType.MONEY_TRANSFER);
                transactionController.execute(moneyTransferPaymentDataReq);
            /*
             * The sleep call below is only specific to unit tests
             */
                //TODO: sleep is not needed but it is a patch for now
                Thread.sleep(30000L);

                //TODO: missing assertions

                Log.d(CLAZZ, "Done testing.");
            } catch (Throwable exc) {
                Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
                fail(exc.getMessage());
            }



        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
