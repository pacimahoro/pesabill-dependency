/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;

import com.pesachoice.billpay.activities.PCAppLauncher;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Odilon Senyana on 11/2/2015.
 */
public class PCAppLauncherTest extends ActivityInstrumentationTestCase2<PCAppLauncher> {
    private static final String CLAZZ = PCAppLauncher.class.getName();
    PCAppLauncher appLauncher;

	/*
	 * TODO: test cases incomplete
	 */

    public PCAppLauncherTest() {
        super(CLAZZ, PCAppLauncher.class);
    }

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        appLauncher = getActivity();
    }


    @Test
    public void testOverallMechanism() {
        assertNotNull(appLauncher);
        //TODO: incomplete test case
    }
}

