package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase;
import android.util.Log;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.fragments.PCCardPayment;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * Created by desire.aheza on 6/14/2016.
 */
public class PCRefillingCardControllerTest extends ActivityInstrumentationTestCase<PCMainTabActivity> {
    private static final String CLAZZ = PCRefillingCardControllerTest.class.getName();
    PCMainTabActivity mainTabActivity;

    public PCRefillingCardControllerTest() {
        super(CLAZZ,PCMainTabActivity.class);
    }

    // The class behaves as Activity to call the service


    @Before
    protected void setUp() throws Exception  {
        super.setUp();

        mainTabActivity = getActivity();
    }

    @Test
    public void testSaveCreditCard () {

        assertNotNull(mainTabActivity);


        try {
            final CountDownLatch signal = new CountDownLatch(1);
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        PCTransactionController transactionController = new PCTransactionController(new PCAsyncListener() {
                            @Override
                            public void onTaskStarted() {

                            }

                            @Override
                            public void onTaskCompleted(PCData data) {
                                assertNotNull(data);
                                assertNull(data.getErrorMessage());
                                signal.countDown();// notify the count down latch
                            }
                        });
                        PCSpecialUser sender = new PCSpecialUser();
                        sender.setClassType(PCUser.class.getSimpleName());
                        sender.setUserName("some user");
                        sender.setFirstName("some");
                        sender.setLastName("user");
                        sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
                        sender.setPhoneNumber("14055558654");



                        PCCardPayment crd = new PCCardPayment() {
                            @Override
                            public String getCardNumber() {
                                String card = "5242770000131859";
                                return card;
                            }


                            @Override
                            public String getCvv() {
                                String cvv = "658";
                                return cvv;
                            }

                            @Override
                            public String getExpMonth() {
                                String mth = "05";
                                return mth;
                            }

                            @Override
                            public String getExpYear() {
                                String exp = "20";
                                return exp;
                            }

                            @Override
                            public PCBillingAddress getBillingAddress() {
                                PCBillingAddress bill = new PCBillingAddress();
                                bill.setCountry("Rwanda");
                                bill.setState("kacyiru");
                                bill.setCity("Kigali");
                                bill.setZip("+ 8932");
                                bill.setStreet("KG AVE 5 37 ST");
                                return bill;
                            }

                            @Override
                            public String getCardType() {
                                String cardType = "Debit Card";
                                return cardType;
                            }
                        };


                        transactionController.setActivity(mainTabActivity);
                        transactionController.setServiceType(PCPesabusClient.PCServiceType.CALLING_REFIL);
                        transactionController.execute(crd);
                        signal.await();// wait for callback
                    } catch (InterruptedException e) {
                        fail();
                        e.printStackTrace();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });


            /*
             * The sleep call below is only specific to unit tests
             */

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }

    }

}
