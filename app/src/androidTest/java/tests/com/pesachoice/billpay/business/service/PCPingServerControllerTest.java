package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCPingServerController;
import com.pesachoice.billpay.model.PCPingResults;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by desire.aheza on 6/14/2016.
 * This class tests the service element that is being used to ping the server
 */

public class PCPingServerControllerTest extends ActivityInstrumentationTestCase2<PCLoginActivity> {
    PCLoginActivity loginActivity;
    private static final String CLAZZ = PCPingServerControllerTest.class.getName();

    public PCPingServerControllerTest(String pkg, Class<PCLoginActivity> activityClass) {
        super(pkg, activityClass);
    }

    public PCPingServerControllerTest(){
        super(PCLoginActivity.class);
    }

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        loginActivity = getActivity();
    }

    @Test
    public void testPingService() {
        assertNotNull(loginActivity);

        try {

            //make service call to ping the server
            PCPingResults pingResults = new PCPingResults();
            PCPingServerController pingServerController = (PCPingServerController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.PING_SERVER, loginActivity);
            pingServerController.setActivity(loginActivity);
            pingServerController.setServiceType(PCPesabusClient.PCServiceType.PING);
            pingServerController.execute(pingResults);
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);
            assertNotNull(pingResults);
            assertNotNull(pingResults.getApiKey());
            assertNotSame(pingResults.getApiKey(), "");
        } catch (Throwable exception) {
            Log.e(CLAZZ, "Could not ping the server because [" + exception.getMessage() + "]");
            fail(exception.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }

}
