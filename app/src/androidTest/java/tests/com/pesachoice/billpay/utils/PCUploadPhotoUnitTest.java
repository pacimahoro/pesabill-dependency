package tests.com.pesachoice.billpay.utils;


import android.test.AndroidTestCase;

import com.google.firebase.FirebaseApp;
import com.pesachoice.billpay.activities.PCTakePhotoIdActivity;
import com.pesachoice.billpay.activities.helpers.PCPhotoUpload;
import com.pesachoice.billpay.utils.PCProfilePictureUtil;

import org.junit.Test;

/**
 * Created by desire.aheza on 2/4/2017.
 */

public class PCUploadPhotoUnitTest  extends AndroidTestCase {


    @Override
    protected void setUp() throws Exception {

        super.setUp();


    }


    @Test
    public void testUploadPhotoCorrectness() {
        byte[] imageDetails = "TESTING TESTING".getBytes();
        FirebaseApp.initializeApp(getContext());
        PCPhotoUpload activity = new PCTakePhotoIdActivity();
        PCProfilePictureUtil.initializeBucket(activity);
        boolean uploaded = PCProfilePictureUtil.uploadPhoto("android_TEST",imageDetails );
        //TODO:currently this is failing bcz android test is not initialing firebase app.

        assertEquals(uploaded, true);
    }

    @Test
    public void testUploadPhotoFakeInputs() {
        byte[] imageDetails = null;
        boolean uploaded = PCProfilePictureUtil.uploadPhoto("",imageDetails );
        assertEquals(uploaded, false);
    }
}
