package tests.com.pesachoice.billpay.activities;

import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;
import android.test.AndroidTestCase;

import com.koushikdutta.ion.builder.Builders;
import com.pesachoice.billpay.activities.PCAppLauncher;
import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.fragments.PCCardPayment;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import java.util.Queue;

/**
 * Created by emmy on 10/2/17.
 */

public class PCBaseActivityTest extends AndroidTestCase {

    PCBaseActivity activity = new PCBaseActivity() {
        @Override
        public void onTaskCompleted(Queue<? extends PCData> data) {
            if (this.currentServiceType == PCPesabusClient.PCServiceType.LOGOUT) {
                straightLogout();
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        }
    };

	/*
	 * TODO: test cases incomplete
	 */



    /**
     * Test save new credit card correct positive case.
     */

    @Test
    public void testSaveNewCreditCardCorrect ()  {
        PCCardPayment crd = new PCCardPayment() {
            @Override
            public String getCardNumber() {
                String card = "5242770000131859";
                return card;
            }


            @Override
            public String getCvv() {
                String cvv = "658";
                return cvv;
            }

            @Override
            public String getExpMonth() {
                String mth = "05";
                return mth;
            }

            @Override
            public String getExpYear() {
                String exp = "20";
                return exp;
            }

            @Override
            public PCBillingAddress getBillingAddress() {
                PCBillingAddress bill = new PCBillingAddress();
                bill.setCountry("Rwanda");
                bill.setState("kacyiru");
                bill.setCity("Kigali");
                bill.setZip("+ 8932");
                bill.setStreet("KG AVE 5 37 ST");
                return bill;
            }

            @Override
            public String getCardType() {
                String cardType = "Debit Card";
                return cardType;
            }
        };

        try{
            activity.saveNewCreditCard(crd);

        } catch (Throwable exc) {
            PCGenericError error = new PCGenericError();
            if (exc instanceof PCGenericError) {
                error = (PCGenericError) exc;
                assertEquals(error.getMessage(), "Error Saving Card Payment");
            }
        }

    }


}
