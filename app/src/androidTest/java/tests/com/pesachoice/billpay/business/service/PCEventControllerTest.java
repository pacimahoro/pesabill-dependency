package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCEventsController;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCEvent;
import com.pesachoice.billpay.model.PCEventsProfile;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by emmy on 10/25/17.
 */

public class PCEventControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
     PCMainTabActivity mainTabActivity;
    private static final String CLAZZ = PCMainTabActivity.class.getName();



    public PCEventControllerTest() {
        super(CLAZZ,PCMainTabActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

       @Test
       public void testEventTicket () {


           assertNotNull(mainTabActivity);
           try {
               final CountDownLatch signal = new CountDownLatch(1);
               runTestOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       try {
                           PCEventsController eventController = new PCEventsController(new PCAsyncListener() {

                               @Override
                               public void onTaskStarted() {

                               }

                               @Override
                               public void onTaskCompleted(PCData data) {
                                   assertNotNull(data);
                                   assertNull(data.getErrorMessage());
                                   signal.countDown();// notify the count down latch
                               }
                           });
                           PCSpecialUser sender = new PCSpecialUser();
                           sender.setClassType(PCUser.class.getSimpleName());
                           sender.setUserName("some user");
                           sender.setFirstName("some");
                           sender.setLastName("user");
                           sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
                           sender.setPhoneNumber("14055558654");


                           PCEvent evt  = new PCEvent();
                           evt.setId("8346");
                           evt.setContactEmail("info@deskpesachoice");
                           evt.setContactPhoneNumber("250782647985");
                           evt.setName("Kigali Up");
                           evt.setPrice(10000.0);
                           evt.setTicketNumber("1");
                           PCEventsProfile profile = new PCEventsProfile();
                           List<PCEvent> event = new ArrayList<>();
                           event.add(evt);
                           profile.setCountry("Rwanda");
                           profile.setEvents(event);
                           profile.setUser(sender);
                           profile.setTicket(evt);

                           eventController.setActivity(mainTabActivity);
                           eventController.setServiceType(PCPesabusClient.PCServiceType.EVENTS);
                           eventController.execute(profile);
                           signal.await();// wait for callback
                       } catch (InterruptedException e) {
                           fail();
                           e.printStackTrace();
                       } catch (Exception e1) {
                           e1.printStackTrace();
                       }
                   }
               });


            /*
             * The sleep call below is only specific to unit tests
             */
               //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);
               //TODO: missing assertions

               Log.d(CLAZZ, "Done testing.");
           } catch (Throwable exc) {
               Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
               fail(exc.getMessage());
           }



       }

}
