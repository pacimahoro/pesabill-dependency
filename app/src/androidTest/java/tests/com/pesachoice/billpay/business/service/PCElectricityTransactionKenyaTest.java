/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCElectricityPaymentData;
import com.pesachoice.billpay.model.PCElectricityServiceInfo;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for kenya electricity payment transaction.
 * @author Desire Aheza
 */
public class PCElectricityTransactionKenyaTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCElectricityTransactionKenyaTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCElectricityTransactionKenyaTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testElectricityPaymentService() {
        assertNotNull(mainTabActivity);
        PCSpecialUser sender = new PCSpecialUser();
        sender.setClassType(PCSpecialUser.class.getSimpleName());
        sender.setUserName("desire@test.com");
        sender.setFirstName("desire");
        sender.setLastName("kaka");
        sender.setTokenId("14db0ff0-4957-4cfc-9d33-d0bf0e258b2f");
        sender.setPhoneNumber("14055323339");

        PCSpecialUser receiver = new PCSpecialUser();
        receiver.setClassType(PCSpecialUser.class.getSimpleName());
        receiver.setFirstName("John");
        receiver.setLastName("Doe");
        receiver.setPhoneNumber("254732234466");

        PCElectricityServiceInfo electricityServiceInfo = new PCElectricityServiceInfo();
        electricityServiceInfo.setCashPower("04225641978");
        electricityServiceInfo.setUsd(5);
        electricityServiceInfo.setTotalUsd(5.5);
        electricityServiceInfo.setFees(0.5);
        electricityServiceInfo.setRate(0.5);
        electricityServiceInfo.setMaskedCardNumber("XXX4242");
        electricityServiceInfo.setTotalLocalCurrency(451);
        electricityServiceInfo.setReceiverCountry("Kenya");

        PCOperator operator = new PCOperator();
        operator.setName("KPLC");

        PCElectricityPaymentData electricityPaymentData = new PCElectricityPaymentData();
        electricityPaymentData.setSender(sender);
        electricityPaymentData.setReceiver(receiver);
        electricityPaymentData.setElectricityServiceInfo(electricityServiceInfo);
        electricityPaymentData.setOperator(operator);

        try {
            PCTransactionController transactionController = (PCTransactionController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.TRANSACTION_CONTROLLER, mainTabActivity);
            transactionController.setActivity(mainTabActivity);
            transactionController.setServiceType(PCPesabusClient.PCServiceType.PAY_ELECTRICITY);
            transactionController.execute(electricityPaymentData);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}