/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCSignupActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCSignupController;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for the registration process.
 * @author Odilon Senyana
 */
public class PCSignupControllerTest extends ActivityInstrumentationTestCase2<PCSignupActivity> {
    private static final String CLAZZ = PCSignupControllerTest.class.getName();

    PCSignupActivity signupActivity;

    // The class behaves as Activity to call the service
    public PCSignupControllerTest() {
        super(CLAZZ, PCSignupActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        signupActivity = getActivity();
    }

    @Test
    public void testSignupService() {
        assertNotNull(signupActivity);

        final PCUser userData = new PCUser();
        //TODO: provide a dynamic way of creating a new user every time we are about to register a new customer
        userData.setUserName("some_user");
        userData.setEmail("some_user532@pesachoice.com");
        userData.setPwd("blablabla");
        userData.setFirstName("some");
        userData.setLastName("user");
        userData.setPhoneNumber("14055558654");
        try {
            PCSignupController signupController = (PCSignupController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.SIGN_UP_CONTROLLER, signupActivity);
            signupController.setActivity(signupActivity);
            signupController.setServiceType(PCPesabusClient.PCServiceType.SIGN_UP);
            signupController.execute(userData);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}