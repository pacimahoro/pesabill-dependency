/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCPasswordResetActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCGeneralRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for cases when the user wants to reset the password.
 *
 * @author Odilon Senyana
 */
public class PCResetPasswordCredentialsControllerTest extends ActivityInstrumentationTestCase2<PCPasswordResetActivity> {
    private static final String CLAZZ = PCResetPasswordCredentialsControllerTest.class.getName();

    private PCPasswordResetActivity passwordResetActivity;

    // The class behaves as Activity to call the service
    public PCResetPasswordCredentialsControllerTest() {
        super(CLAZZ, PCPasswordResetActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        passwordResetActivity = getActivity();
    }

    @Test
    public void testResetPasswordService() {
        assertNotNull(passwordResetActivity);

        final PCGeneralRequest passwordResetRequest = new PCGeneralRequest();
        passwordResetRequest.setEmail("some_user23@pesachoice.com");
        passwordResetRequest.setPwd("m9$JdcU4g!ospSF<?1uf3");
        // TODO: need to use access code sent by the server
        passwordResetRequest.setAccessCode(2905);
        try {
            PCCredentialsController credentialsController = (PCCredentialsController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, passwordResetActivity);
            credentialsController.setActivity(passwordResetActivity);
            credentialsController.setServiceType(PCPesabusClient.PCServiceType.RESET_PASSWORD);
            credentialsController.execute(passwordResetRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(passwordResetActivity);
            /*
             * TODO: missing assertions
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
