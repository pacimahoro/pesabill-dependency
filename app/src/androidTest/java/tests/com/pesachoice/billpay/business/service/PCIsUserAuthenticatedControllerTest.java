/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCUserController;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved to check if the user is authenticated.
 *
 * @author Odilon Senyana
 */
public class PCIsUserAuthenticatedControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCIsUserAuthenticatedControllerTest.class.getName();

    private PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCIsUserAuthenticatedControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }
    
    @Test
    public void testIsUserAuthenticatedService() {
        assertNotNull(mainTabActivity);

        final PCRequest isAuthenticatedRequest = new PCRequest();
        final PCUser userData = new PCUser();
        userData.setEmail("some_user23@pesachoice.com");
        userData.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");

        isAuthenticatedRequest.setUser(userData);
        try {
            PCUserController userController = (PCUserController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.USER_CONTROLLER, mainTabActivity);
            userController.setActivity(mainTabActivity);
            userController.setServiceType(PCPesabusClient.PCServiceType.IS_AUTHENTICATED);
            userController.execute(isAuthenticatedRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(mainTabActivity);

            assertNotNull(mainTabActivity);

            PCSpecialUser pcSpecialUser = mainTabActivity.getAppUser();

            PCBillingAddress address = pcSpecialUser.getBillingAddress();
            assertNotNull(address);
            assertNotNull(address.getCountry());
            assertNotNull(address.getCity());
            assertNotNull(address.getState());
            assertNotNull(address.getZip());
            assertNotNull(address.getStreet());

            /*
             * TODO: uncomment the code below once login activity is completed
             */


            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
