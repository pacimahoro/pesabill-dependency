package tests.com.pesachoice.billpay.fragments;
import android.app.Application;
import android.content.Context;
import android.test.AndroidTestCase;

import com.pesachoice.billpay.fragments.PCUserEventsFragment;
import com.pesachoice.billpay.model.PCEvent;
import com.pesachoice.billpay.model.PCTicketServiceInfo;

//import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by emmy on 10/9/17.
 */

public class PCUserEventFragment$PCEventAdapterTest extends AndroidTestCase {
    public static Context context = null;
    PCTicketServiceInfo pcTicketServiceInfo = new PCTicketServiceInfo();


    /**
     * Test adding contacts correct negative case.
     */
    @Test
    public void testAddingContactsCorrect() {
        PCUserEventsFragment eventAdapter = new PCUserEventsFragment();
        List<PCEvent> event = new ArrayList<>();
        MyApplication app = new MyApplication();
        context = app.getContext();
        PCUserEventsFragment.PCEventsAdapter event1 = eventAdapter.new PCEventsAdapter(event, context);
         boolean result = event1.checkOut(pcTicketServiceInfo);
         assertFalse(result);

    }

    public class MyApplication extends Application {

        private  Context mContext;

        @Override
        public void onCreate() {
            super.onCreate();
            mContext = getApplicationContext();
        }

        public  Context getContext() {
            return mContext;
        }
    }
}
