/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for login.
 *
 * @author Odilon Senyana
 */
public class PCLoginCredentialsControllerTest extends ActivityInstrumentationTestCase2<PCLoginActivity> {
    private static final String CLAZZ = PCLoginCredentialsControllerTest.class.getName();

    private PCLoginActivity loginActivity;

    // The class behaves as Activity to call the service
    public PCLoginCredentialsControllerTest() {
        super(CLAZZ, PCLoginActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        loginActivity = getActivity();
    }

    @Test
    public void testLoginService() {
        assertNotNull(loginActivity);

        final PCRequest loginRequest = new PCRequest();
        final PCUser userData = new PCUser();
        userData.setEmail("some_user23@pesachoice.com");
        userData.setPwd("m9$JdcU4g!ospSF<?1uf3");

        loginRequest.setUser(userData);
        try {
            PCCredentialsController credentialsController = (PCCredentialsController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, loginActivity);
            credentialsController.setActivity(loginActivity);
            credentialsController.setServiceType(PCPesabusClient.PCServiceType.LOGIN);
            credentialsController.execute(loginRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(loginActivity);
            /*
             * TODO: uncomment the code below once login activity is completed
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
