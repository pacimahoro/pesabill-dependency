package tests.com.pesachoice.billpay.business.service;


import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCUserController;
import com.pesachoice.billpay.model.PCActivityRequest;
import com.pesachoice.billpay.model.PCUser;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 10/26/17.
 */

public class PCUserControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
         private static final String CLAZZ = PCUserControllerTest.class.getName();

   private PCMainTabActivity mainTabActivity;



    public PCUserControllerTest() {
        super(CLAZZ,PCMainTabActivity.class);
    }

    @Before
    public void setUp () throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }



       @Test
       public void testUserAccounts () {
           assertNotNull(mainTabActivity);
           PCActivityRequest controlUser = new PCActivityRequest();
           PCUser user = new PCUser();
           user.setEmail("some user");
           user.setFullName("some user");
           user.setCountry("USA");
           user.setPhoneNumber("14055558654");
           user.setTokenId("8795da5a-3ccb-4239-934c-e03727414595");
            controlUser.setUser(user);

           try {

               PCUserController userController = (PCUserController) PCControllerFactory.constructControllerManyType(
                       PCControllerFactory.PCControllerType.USER_CONTROLLER, mainTabActivity);
               userController.setActivity(mainTabActivity);
               userController.setServiceType(PCPesabusClient.PCServiceType.USER_INFO);
               userController.execute(controlUser);
            /*
             * The sleep call below is only specific to unit tests
             */
               //TODO: sleep is not needed but it is a patch for now
               Thread.sleep(30000L);

               assertNotNull(mainTabActivity);
            /*
             * TODO: uncomment the code below once login activity is completed
             */

               Log.d(CLAZZ, "Done testing.");
           } catch (Throwable exc) {
               Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
               fail(exc.getMessage());
           }


       }


}
