/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */


package tests.com.pesachoice.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;

import com.pesachoice.billpay.activities.PCPasswordRecoveryActivity;

import org.junit.Before;
import org.junit.Test;

/**
 *
 * Tests the PCPasswordRecovery activity
 * @author Pacifique Mahoro
 *
 */
public class PCPasswordRecoveryActivityTest  extends ActivityInstrumentationTestCase2<PCPasswordRecoveryActivity> {
    private static final String CLAZZ = PCPasswordRecoveryActivity.class.getName();
    PCPasswordRecoveryActivity recoveryActivity;

	/*
	 * TODO: test cases incomplete
    */

    public PCPasswordRecoveryActivityTest() {
        super(CLAZZ, PCPasswordRecoveryActivity.class);
    }

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        recoveryActivity = getActivity();
    }


    @Test
    public void testMechanism() {
        // TODO: Need valid tests.
    }
}
