package tests.com.pesachoice.billpay.fragments;

import android.content.pm.ActivityInfo;
import android.test.ActivityInstrumentationTestCase;
import android.test.AndroidTestCase;

import com.pesachoice.billpay.activities.PCLoginActivity;
import com.pesachoice.billpay.fragments.PCActivityDetailFragment;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 9/28/17.
 */

  public class PCActivityDetailFragmentTest extends AndroidTestCase {

    PCActivityDetailFragment activity = new PCActivityDetailFragment();


    /**
     * Test populate receiver correctness positive case.
     */
    @Test
    public void testPopulateReceiverCorrectness() {

         boolean result = activity.populateReceiverName();

         assertTrue(result);


    }
}
