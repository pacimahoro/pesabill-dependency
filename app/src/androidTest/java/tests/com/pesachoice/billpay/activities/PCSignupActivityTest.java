/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.activities;

import android.test.ActivityInstrumentationTestCase2;
import android.test.AndroidTestCase;
import android.util.Log;
import android.widget.EditText;

import com.pesachoice.billpay.activities.PCSignupActivity;
import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.utils.PCGeneralUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PCSignupActivityTest extends ActivityInstrumentationTestCase2<PCSignupActivity> {
    private static final String CLAZZ = PCSignupActivityTest.class.getName();

	/*
	 * TODO: test cases incomplete
	 */

    public PCSignupActivityTest() {
        super(CLAZZ, PCSignupActivity.class);
    }

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        signupActivity = getActivity();
    }
   PCSignupActivity signupActivity = new PCSignupActivity();

    @Test
    public void testOverallMechanism() {
        assertNotNull(signupActivity);
        //TODO: incomplete test case
    }



    @Test
    public void testAllRequiredField() {
        /**
         * Test all required field positive case.
         */
        EditText emailText = (EditText) signupActivity.findViewById(R.id.email);
        EditText phoneText = (EditText) signupActivity.findViewById(R.id.phone);
        EditText firstNameText = (EditText) signupActivity.findViewById(R.id.firstName);
        EditText lastNameText = (EditText) signupActivity.findViewById(R.id.lastName);
        EditText pwdText = (EditText) signupActivity.findViewById(R.id.password);

        assertNotNull(emailText);
        assertNotNull(phoneText);
        assertNotNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);

        /**
         * Test all required field negative caase.
         */

          emailText = null;
          firstNameText = null;

        assertNull(emailText);
        assertNotNull(phoneText);
        assertNull(firstNameText);
        assertNotNull(lastNameText);
        assertNotNull(pwdText);


    }


    @Test
    public void testEmptyFieldValidation() throws PCGenericError {

        try {
         boolean result =  signupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), signupActivity.getResources().getString(R.string.sign_error_missing_info));
        }


        /**
         * Test empty field validation positive case.
         */

               String phone = "+1 4056667777";
             String countryCode = "+1";

             // First check for any empty fields or in wrong formart
             if (PCGeneralUtils.isValidPhone(phone.substring(1), countryCode)) {


                 assertTrue(signupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","emmanuel"));

             }

    }


    @Test
    public void testInvalidPhoneFieldValidation() throws PCGenericError{
        try {
           boolean result =  signupActivity.validCustomerInfo("George","Kagabo","john@smith.com","+1 4056667777","");
        } catch (PCGenericError error) {
            assertEquals(error.getMessage(), signupActivity.getResources().getString(R.string.sign_error_invalid_phone));
        }
        /**
         * Test invalid phone field validation positive case.
         */

          boolean result =  signupActivity.validCustomerInfo("Patrick","Ronald","patrick@ronald.com","+250782901278","emmanuel");
          assertTrue(result);

    }

    @Test
    public void testInvalidEmailFieldValidation() {
        signupActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText emailText = (EditText) signupActivity.findViewById(R.id.email);
                EditText phoneText = (EditText) signupActivity.findViewById(R.id.phone);
                EditText firstNameText = (EditText) signupActivity.findViewById(R.id.firstName);
                EditText lastNameText = (EditText) signupActivity.findViewById(R.id.lastName);
                EditText pwdText = (EditText) signupActivity.findViewById(R.id.password);

                emailText.setText("johnsmith.com");
                phoneText.setText("+1 4056667777");
                firstNameText.setText("George");
                lastNameText.setText("Camaro");
                pwdText.setText("test123");

                String first = firstNameText.getText().toString();
                String last = lastNameText.getText().toString();




                try {
                    signupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                } catch (Exception error) {
                    assertEquals(error.getMessage(), signupActivity.getResources().getString(R.string.sign_error_invalid_email));
                }





                /**
                 * Test invalid email field validation positive case.
                 */

                emailText.setText("john@smith.com");



                try {
                 boolean result = signupActivity.validCustomerInfo(
                            first, last,
                            emailText.getText().toString(),
                            phoneText.getText().toString(),
                            pwdText.getText().toString()
                    );
                    assertTrue(result);
                } catch (Exception error) {
                    error.getMessage();
                }
            }
        });
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
               