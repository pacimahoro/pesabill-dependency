package tests.com.pesachoice.billpay.utils;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.activities.PCBaseActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.PCPesabusClient.PCServiceType;
import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCElectricityServiceInfo;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.AbstractMap;
import java.util.Queue;

/**
 * Created by desire.aheza on 2/4/2017.
 */

public class PCPostPesaBillMetricsTest extends AndroidTestCase {
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Override
	protected void setUp() throws Exception {
		super.setUp();

	}


	@Test(expected = IllegalStateException.class)
	public void testPostPurchaseMetrics() {

		try {
			PCBillPaymentData pcBillPaymentData = new PCBillPaymentData();
			PCElectricityServiceInfo serviceInfo = new PCElectricityServiceInfo();
			serviceInfo.setActionType(PCData.ActionType.ELECTRICITY.name());
			serviceInfo.setTotalAmount(30);

			pcBillPaymentData.setServiceInfo(serviceInfo);


			PCActivity.PCPaymentProcessingType processingType = PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL;

			pcBillPaymentData.setPaymentProcessingType(processingType);

			PCSpecialUser user = new PCSpecialUser();
			user.setCountry("Rwanda");

			PCCurrency currency = new PCCurrency();
			currency.setCountry("Rwanda");
			currency.setCurrencySymbol("RWF");

			user.setBaseCurrency(currency);

			pcBillPaymentData.setSender(user);

			PCPaymentInfo paymentInfo = new PCPaymentInfo();
			paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);

			pcBillPaymentData.setPaymentInfo(paymentInfo);



			boolean eventSent = PCPostPesaBillMetrics.postPurchaseMetric(null, pcBillPaymentData, true);


		} catch (IllegalStateException e) {
			assertEquals("Must Initialize Fabric before using singleton()", e.getMessage());
		}

		/**
		 * Test post purchase metrics negative case by missing to submit user details.
		 */
		  try {
			  PCBillPaymentData pcBillPaymentData = new PCBillPaymentData();
			  PCElectricityServiceInfo serviceInfo = new PCElectricityServiceInfo();
			  serviceInfo.setActionType(PCData.ActionType.ELECTRICITY.name());
			  serviceInfo.setTotalAmount(30);

			  pcBillPaymentData.setServiceInfo(serviceInfo);


			  PCActivity.PCPaymentProcessingType processingType = PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL;

			  pcBillPaymentData.setPaymentProcessingType(processingType);
			  PCSpecialUser user = new PCSpecialUser();
			  user.setCountry("Rwanda");

			  PCCurrency currency = new PCCurrency();
			  currency.setCountry("Rwanda");
			  currency.setCurrencySymbol("RWF");
			  user.setBaseCurrency(currency);
			  boolean eventSent = PCPostPesaBillMetrics.postPurchaseMetric(null, pcBillPaymentData, true);
			  	assertEquals(eventSent, false);

		  } catch (Exception e) {
			  e.getMessage();

		  }



	}


	@Test
	public void testPostPurchaseMetricsMissingParms() {
		PCBillPaymentData pcBillPaymentData = new PCBillPaymentData();
		PCElectricityServiceInfo serviceInfo = new PCElectricityServiceInfo();
		serviceInfo.setActionType(PCData.ActionType.ELECTRICITY.name());
		serviceInfo.setTotalAmount(30);

		pcBillPaymentData.setServiceInfo(serviceInfo);

		PCActivity.PCPaymentProcessingType processingType = PCActivity.PCPaymentProcessingType.ELECTRICITY_BILL;

		pcBillPaymentData.setPaymentProcessingType(processingType);

		PCSpecialUser user = new PCSpecialUser();
		user.setCountry("Rwanda");

		PCCurrency currency = new PCCurrency();
		currency.setCountry("Rwanda");
		currency.setCurrencySymbol("RWF");

		user.setBaseCurrency(currency);




		PCPaymentInfo paymentInfo = new PCPaymentInfo();
		paymentInfo.setPaymentType(PCBillPaymentData.PCPaymentType.MOBILE_MONEY);

		pcBillPaymentData.setPaymentInfo(paymentInfo);


		assertFalse(PCPostPesaBillMetrics.postPurchaseMetric(null, pcBillPaymentData, true));

		/**
		 * Test post purchase metrics missing parms positive case.
		 */

		     try {
			     pcBillPaymentData.setSender(user);
				 assertTrue(PCPostPesaBillMetrics.postPurchaseMetric(null,pcBillPaymentData,true));
			 } catch (Exception e) {

				 e.getMessage();
			 }

	}


	@Test(expected = IllegalStateException.class)
	public void testPostLoginMetrics() {
		try {
			assertTrue(PCPostPesaBillMetrics.postLoginMetric(null, "USA", "EMAIL", false));
		} catch (IllegalStateException e) {
			assertEquals("Must Initialize Fabric before using singleton()", e.getMessage());
		}

	}



	@Test
	public void testPostLoginMetricsMissingParams() {

		boolean result = PCPostPesaBillMetrics.postLoginMetric(null,"","",false);

		assertEquals(result,true);

		/**
		 * Test post login metrics missing params positive case.
		 */

		result = PCPostPesaBillMetrics.postLoginMetric(null, "USA", "EMAIL", false);

		assertTrue(result);

	}


	@Test(expected = IllegalStateException.class)
	public void testPostCustomMetrics() {
		try {
			// FIXME: The method this is testing does nothing. Need to implement a custom REPEAT_TRANSACTION event
			PCPostPesaBillMetrics.postCustomEvent("Repeat Transaction", new AbstractMap.SimpleEntry<String, String>("AIRTIME", ""), new AbstractMap.SimpleEntry<String, String>(PCPostPesaBillMetrics.CUSTOM_EVENT_TAG.STATUS.getName(), "Success"));
		} catch (IllegalStateException e) {
			assertEquals("Must Initialize Fabric before using singleton()", e.getMessage());
		}


		/**
		 * Test post custom metrics positive case.
		 */
		try {

		boolean result = PCPostPesaBillMetrics.postCustomEvent("login", new AbstractMap.SimpleEntry<String, String>("AIRTIME", ""), new AbstractMap.SimpleEntry<String, String>(PCPostPesaBillMetrics.CUSTOM_EVENT_TAG.STATUS.getName(), "Success"));
		   assertEquals(result,true);
		} catch (Exception error) {

			error.getMessage();
		}






	}


	@Test
	public void testPostCustomMetricsMissingParms() {
		// FIXME: The method this is testing does nothing. Need to implement a custom REPEAT_TRANSACTION event
		assertFalse(PCPostPesaBillMetrics.postCustomEvent("", new AbstractMap.SimpleEntry<String, String>("AIRTIME", "")));
		assertFalse(PCPostPesaBillMetrics.postCustomEvent(null));

		/**
		 * Test post custom metrics missing parms positive case.
		 */

		assertTrue(PCPostPesaBillMetrics.postCustomEvent("login", new AbstractMap.SimpleEntry<String, String>("AIRTIME", "")));

	}

}
