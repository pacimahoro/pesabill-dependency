/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCWaterPaymentData;
import com.pesachoice.billpay.model.PCWaterServiceInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for water payment transaction.
 * @author Desire Aheza
 */
public class PCWaterTransactionControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCWaterTransactionControllerTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCWaterTransactionControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testWaterPaymentService() {
        assertNotNull(mainTabActivity);
        PCSpecialUser sender = new PCSpecialUser();
        sender.setClassType(PCSpecialUser.class.getSimpleName());
        sender.setUserName("some user");
        sender.setFirstName("some");
        sender.setLastName("user");
        sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
        sender.setPhoneNumber("14055558654");

        PCSpecialUser receiver = new PCSpecialUser();
        receiver.setClassType(PCSpecialUser.class.getSimpleName());
        receiver.setFirstName("John");
        receiver.setLastName("Doe");
        receiver.setPhoneNumber("256752874562");

        PCWaterServiceInfo waterServiceInfo = new PCWaterServiceInfo();

        //TODO: need to use service description doc in order to determine the right values and asserations
        waterServiceInfo.setAccountNumber("04217248493");
        waterServiceInfo.setUsd(1);
        waterServiceInfo.setTotalUsd(1.25);
        waterServiceInfo.setFees(0.25);
        waterServiceInfo.setRate(0.25);
        waterServiceInfo.setMaskedCardNumber("XXX0027");
        waterServiceInfo.setTotalLocalCurrency(3500);
        waterServiceInfo.setReceiverCountry("Uganda");

        PCOperator operator = new PCOperator();
        operator.setName("UG");

        PCWaterPaymentData waterPaymentData = new PCWaterPaymentData();
        waterPaymentData.setSender(sender);
        waterPaymentData.setReceiver(receiver);
        waterPaymentData.setWaterServiceInfo(waterServiceInfo);
        waterPaymentData.setOperator(operator);

        try {
            PCTransactionController transactionController = (PCTransactionController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.TRANSACTION_CONTROLLER, mainTabActivity);
            transactionController.setActivity(mainTabActivity);
            transactionController.setServiceType(PCPesabusClient.PCServiceType.PAY_WATER);
            transactionController.execute(waterPaymentData);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO:ASSERTATIONS
            /*
             *  have to use service description doc in order to determine the right assertions
             */
            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}