package tests.com.pesachoice.billpay.utils;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.model.PCActivity;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCCurrency;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCElectricityServiceInfo;
import com.pesachoice.billpay.model.PCGenericError;
import com.pesachoice.billpay.model.PCPaymentInfo;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.utils.PCCurrencyUtil;
import com.pesachoice.billpay.utils.PCPostPesaBillMetrics;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by desire.aheza on 2/4/2017.
 */

public class PCCurrencyUtilTest extends AndroidTestCase {

	static Map<String,Double> interCountryExchangeRale = new HashMap<>();

	static{
		interCountryExchangeRale.put("Rwanda",1.0);
		interCountryExchangeRale.put("Kenya",3213.0);
		interCountryExchangeRale.put("Uganda",4.0);
		interCountryExchangeRale.put("Congo",12.0);

	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();
    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    @Test(expected = PCGenericError.class)
    public void testCalculateExchangeRate() {
		try {
			PCCurrency currency = new PCCurrency();
			currency.setCountry("Rwanda");
			currency.setCurrencySymbol("RWF");
			currency.setCountrySymbol("RW");

			currency.setInterCountryExchangeRates(interCountryExchangeRale);
			double ex = PCCurrencyUtil.calculateExchangeRate(currency,"Rwanda");
			assertEquals(ex,1.0);

			currency.setCountry("Kenya");
			ex = PCCurrencyUtil.calculateExchangeRate(currency,"Kenya");
			assertEquals(ex,3213.0);

			currency.setCountry("Uganda");
            ex = PCCurrencyUtil.calculateExchangeRate(currency,"Uganda");
			assertEquals(ex,4.0);


		} catch(Exception e) {

            e.getMessage();

		}

    }


	@Test
    public void testCalculateExchangeRateMissingData() {
		try {
			double ex = PCCurrencyUtil.calculateExchangeRate(null,"Rwanda");
			assertEquals(ex,1.0);
		} catch (PCGenericError pcGenericError) {
			pcGenericError.printStackTrace();
		}

		/**
		 * Test calculate exchange rate missing data positive case.
		 */

		try {
			PCCurrency currency = new PCCurrency();
			currency.setCountry("Rwanda");

			double ex = PCCurrencyUtil.calculateExchangeRate(currency,"Rwanda");
			assertEquals(ex,1.0);

		} catch (Exception e) {
			e.getMessage();
		}

	}

}
