/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCSignupActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCVerifyController;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for completing the registration process.
 * @author Odilon Senyana
 */
public class PCCompleteRegistrationControllerTest extends ActivityInstrumentationTestCase2<PCSignupActivity> {
    private static final String CLAZZ = PCCompleteRegistrationControllerTest.class.getName();

    PCSignupActivity signupActivity;

    // The class behaves as Activity to call the service
    public PCCompleteRegistrationControllerTest() {
        super(CLAZZ, PCSignupActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        signupActivity = getActivity();
    }

    @Test
    public void testCompleteRegistrationService() {
        assertNotNull(signupActivity);

        final PCRequest completeRegistrationRequest = new PCRequest();
        final PCUser userData = new PCUser();
        userData.setEmail("some_user23@pesachoice.com");
        /*
         * TODO: Would need to log in first, get the token and then use it below
         */
        userData.setTokenId("21687f9b-d043-4670-937b-516d3c290ddb");
        // TODO: get the access code for the user
        userData.setAccessCode(2241);

        completeRegistrationRequest.setUser(userData);
        try {
            PCVerifyController completeRegistrationController = (PCVerifyController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.VERIFY_CONTROLLER, signupActivity);
            completeRegistrationController.setActivity(signupActivity);
            completeRegistrationController.setServiceType(PCPesabusClient.PCServiceType.COMPLETE_REGISTRATION);
            completeRegistrationController.execute(completeRegistrationRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}