package tests.com.pesachoice.billpay.activities;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTransaction;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emmy on 10/4/17.
 */

public class PCMainTabActivityTest extends AndroidTestCase {
    PCMainTabActivity activity = new PCMainTabActivity();

    /**
     * Test show transaction receiptcorrect positive case.
     */
    @Test
      public void testShowTransactionReceiptcorrect() {

          PCTransaction transaction = new PCTransaction();
          boolean result = activity.showTransactionReceipt(transaction);

          assertTrue(result);

     }
}
