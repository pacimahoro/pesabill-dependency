/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCCardController;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCCardInfo;
import com.pesachoice.billpay.model.PCCardServiceData;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for creating payment profile.
 * @author Odilon Senyana
 */
public class PCCreatePaymentProfileControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCCreatePaymentProfileControllerTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCCreatePaymentProfileControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testCountryProfileService() {
        assertNotNull(mainTabActivity);

        PCUser sender = new PCUser();
        sender.setClassType(PCUser.class.getSimpleName());
        sender.setUserName("some user");
        sender.setFirstName("some");
        sender.setLastName("user");
        sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
        sender.setPhoneNumber("14055558654");

        PCCardInfo cardInfo = new PCCardInfo();
        cardInfo.setClassType(PCCardInfo.class.getSimpleName());
        cardInfo.setNumber("4007000000027");
        cardInfo.setMonthOfExpiration("10");
        cardInfo.setYearOfExpiration("2017");
        cardInfo.setCvv("900");
        PCBillingAddress address = new PCBillingAddress();
        address.setStreet("12345 main street");
        address.setCity("Norman");
        address.setState("OK");
        address.setZip("73071");
        address.setCountry("USA");
        cardInfo.setBillingAddress(address);

        final PCCardServiceData cardServiceDataRequest = new PCCardServiceData();
        cardServiceDataRequest.setSender(sender);
        cardServiceDataRequest.setCard(cardInfo);

        try {
            PCCardController cardController = (PCCardController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.CARD_CONTROLLER, mainTabActivity);
            cardController.setActivity(mainTabActivity);
            cardController.setServiceType(PCPesabusClient.PCServiceType.PAYMENT_PROFILE);
            cardController.execute(cardServiceDataRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch  (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}