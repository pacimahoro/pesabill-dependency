package tests.com.pesachoice.billpay.fragments;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.fragments.PCConfirmationDialogFragment;

import org.junit.Test;

/**
 * Created by desire.aheza on 8/17/2016.
 */
public class PCConfirmationFragmentTest extends AndroidTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    //positive case
    @Test
    public void testCalculateTotalAfterDiscount() {
        double totalAmount = 5;//total usd;
        double discountAmount = 0.5;//discount;
        PCConfirmationDialogFragment dialogFragment = new PCConfirmationDialogFragment();
        double valueAfterDiscount = dialogFragment.getValueAfterDiscount(totalAmount,discountAmount);
        assertEquals(4.5, valueAfterDiscount);
    }

    //negative case
    @Test
    public void testCalculateTotalAfterDiscountNegative() {
        double totalAmount = 5;//total usd;
        double discountAmount = 0.5;//discount;
        PCConfirmationDialogFragment dialogFragment = new PCConfirmationDialogFragment();
        double valueAfterDiscount = dialogFragment.getValueAfterDiscount(totalAmount,discountAmount);
        assertFalse(5 == valueAfterDiscount);
    }
}
