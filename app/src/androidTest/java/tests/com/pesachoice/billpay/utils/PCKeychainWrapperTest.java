/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */

package tests.com.pesachoice.billpay.utils;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.business.PCPesachoiceConstant;
import com.pesachoice.billpay.utils.PCKeychainWrapper;

import org.junit.Test;

/**
 * Tests the keychain wrapper. We use the keychain wrapper for encryption
 *
 * @author Pacifique Mahoro
 * @author Desire Aheza
 */
public class PCKeychainWrapperTest extends AndroidTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }


    @Test
    public void testEncryptingCorrectness() {
        String userName = "desire@test.com";//"paci@pesachoice.com";
        String pwd = "sosoma";//"temp002";

        pwd = PCKeychainWrapper.securedSHA256DigestHash(pwd, userName);
        assertEquals(pwd, "95aa43e40209a14bc1e9429501d4fe2ca172f7477731b45da216a907b7f1577c");

        /**
         * Test encrypting correctness negative case.
         */


        userName = "desire@test.com";
        pwd ="koko";


        pwd = PCKeychainWrapper.securedSHA256DigestHash(pwd, userName);
        assertFalse(pwd,false);



    }

    /**
     * positive test case
     */
    @Test
    public  void testSecuredAESEncryption() {
        // username,input,password
        String pwd = PCKeychainWrapper.securedAESEncryption("test123","89a2d1a883b721a2", "yeah");
        assertEquals(pwd, "b9dfff726ccdb34cdbe9ea568622921b65a3e172e549ac6d0ee13ab073b190c5");//5884d44c44fe84fb7c3a3b6a48f71d36bb66b17c6a786f1cae320ebca7a87c8b
    }

    /**
     * Negative test case when password is wrong
     */
    @Test
    public  void testNegativeSecuredAESEncryption() {
        // username,input,password
        String pwd = PCKeychainWrapper.securedAESEncryption("test123","89a2d1a883b721a2", "yeah12");
        assertNotSame(pwd, "b9dfff726ccdb34cdbe9ea568622921b65a3e172e549ac6d0ee13ab073b190c5");
    }

    /**
     * Test if no key was generated from server
     */
    @Test
    public  void testSecuredAESEncryptionInvalidKey() {
        try {
            String pwd = PCKeychainWrapper.securedAESEncryption("test123", null, "yeah12");
            assertNull(pwd);
        }
        catch (Exception e) {
           assertEquals(PCPesachoiceConstant.FAILED_IN_AES_ENCRIPTION,e.getMessage());
        }
    }


    @Test
    public void testSecuredAESCardEncryption() {
        String cipher = PCKeychainWrapper.securedAESEncryption("4007000000027","89a2d1a883b721a2",null);

        assertEquals(cipher, "614dded0c77ce6b82dd6f1e179999aebe09e32d00e29a7505a9f6fbdaa1af50e");


        /**
         * Test secured aes card encryption Negative case.
         */
        cipher = PCKeychainWrapper.securedAESEncryption("bibi","89a2d1a883b721a2",null);

        assertFalse(cipher,false);
    }


    @Test
    public void testSecuredAESCardCvvEncryption() {
        String cipher = PCKeychainWrapper.securedAESEncryption("955","89a2d1a883b721a2",null);
        assertEquals(cipher, "614dded0c77ce6b82dd6f1e179999aeb25034d33652d67b868271a6608203334");

        /**
         * Test secured aes card cvv encryption Negative case.
         */
        cipher = PCKeychainWrapper.securedAESEncryption("S$ms34","89a2d1a883b721a2",null);
        assertFalse(cipher,false);
    }

    @Test
    public void testSecuredAESCardMonthEncryption() {
        String cipher = PCKeychainWrapper.securedAESEncryption("10","89a2d1a883b721a2",null);
        assertEquals(cipher, "614dded0c77ce6b82dd6f1e179999aeb22e3b43dad23669342dc7271760a9b4d");




        /**
         * Test secured aes card month encryption Negative case.
         */
        cipher = PCKeychainWrapper.securedAESEncryption("bit","89a2d1a883b721a2",null);
        assertFalse(cipher,false);

    }


    @Test
    public void testSecuredAESCardYearEncryption() {
        String cipher = PCKeychainWrapper.securedAESEncryption("16","89a2d1a883b721a2",null);
        assertEquals(cipher, "614dded0c77ce6b82dd6f1e179999aebce4096053dc71c2e53f1483d1cad833c");

        /**
         * Test secured aes card year encryption Negative case.
         */

        cipher = PCKeychainWrapper.securedAESEncryption("Emmanuel","89a2d1a883b721a2",null);
        assertFalse(cipher,false);

    }
}