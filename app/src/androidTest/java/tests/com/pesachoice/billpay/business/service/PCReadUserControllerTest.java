/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCUserController;
import com.pesachoice.billpay.model.PCBillingAddress;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for reading user data.
 *
 * @author Odilon Senyana
 */
public class PCReadUserControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCReadUserControllerTest.class.getName();

    private PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCReadUserControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testReadUserService() {
        assertNotNull(mainTabActivity);

        final PCRequest readUserRequest = new PCRequest();
        final PCUser userData = new PCUser();
        mainTabActivity.currentServiceTypeAuth= PCPesabusClient.PCServiceType.USER_INFO;
        userData.setEmail("desire@test.com");
        userData.setTokenId("75b715da-39da-4a35-b4e6-14ddfee7e7c5");

        readUserRequest.setUser(userData);
        try {
            PCUserController userController = (PCUserController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.USER_CONTROLLER, mainTabActivity);
            userController.setActivity(mainTabActivity);
            userController.setServiceType(PCPesabusClient.PCServiceType.USER_INFO);
            userController.execute(readUserRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(mainTabActivity);

            PCSpecialUser pcSpecialUser = mainTabActivity.getAppUser();
            assertNotNull(pcSpecialUser);
            PCBillingAddress address = pcSpecialUser.getBillingAddress();
            //this will currently fail as BACK END is not setting ADDRESS as it is supposed to do TODO:NEED TO FOLLOWUP WITH ODILON
            assertNotNull(address);
            assertNotNull(address.getCountry());
            assertNotNull(address.getCity());
            assertNotNull(address.getState());
            assertNotNull(address.getZip());
            assertNotNull(address.getStreet());
            /*
             * TODO: uncomment the code below once login activity is completed
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
