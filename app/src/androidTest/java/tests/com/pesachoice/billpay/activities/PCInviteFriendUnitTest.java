package tests.com.pesachoice.billpay.activities;


import android.test.ActivityInstrumentationTestCase2;
import com.pesachoice.billpay.activities.PCBaseActivity;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by emmy on 11/13/17.
 */

   public class PCInviteFriendUnitTest extends ActivityInstrumentationTestCase2<PCBaseActivity> {

    PCBaseActivity activity;

    private static final String CLAZZ = PCBaseActivity.class.getName();


    public PCInviteFriendUnitTest() {
        super(CLAZZ,PCBaseActivity.class);
    }
    @Before
    protected void setUp () throws Exception {
        super.setUp();
        setActivityInitialTouchMode(false);
        activity = getActivity();
    }

    @Test
    public void testInviteFriendsUsingFirebase() {

              try {
                  activity.inviteFriendUsingFirebase();
              } catch (Exception error) {
                 assertEquals(error.getMessage(),"Sending Invitation Failed");
              }

           }


}
