/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCVerifyPhoneActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCVerifyController;
import com.pesachoice.billpay.model.PCRequest;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for verifying phones.
 * @author Odilon Senyana
 */
public class PCVerifyPhoneControllerTest extends ActivityInstrumentationTestCase2<PCVerifyPhoneActivity> {
    private static final String CLAZZ = PCVerifyPhoneControllerTest.class.getName();

    PCVerifyPhoneActivity verifyPhoneActivity;

    // The class behaves as Activity to call the service
    public PCVerifyPhoneControllerTest() {
        super(CLAZZ, PCVerifyPhoneActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        verifyPhoneActivity = getActivity();
    }

    @Test
    public void testVerifyPhoneService() {
        assertNotNull(verifyPhoneActivity);

        final PCRequest verifyPhoneRequest = new PCRequest();
        final PCUser userData = new PCUser();
        userData.setEmail("some_user23@pesachoice.com");
        /*
         * TODO: Would need to log in first, get the token and then use it below
         */
        userData.setTokenId("21687f9b-d043-4670-937b-516d3c290ddb");
        userData.setPhoneNumber("14055558654");

        verifyPhoneRequest.setUser(userData);
        try {
            PCVerifyController verifyPhoneController = (PCVerifyController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.VERIFY_CONTROLLER, verifyPhoneActivity);
            verifyPhoneController.setActivity(verifyPhoneActivity);
            verifyPhoneController.setServiceType(PCPesabusClient.PCServiceType.VERIFY_PHONE);
            verifyPhoneController.execute(verifyPhoneRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}