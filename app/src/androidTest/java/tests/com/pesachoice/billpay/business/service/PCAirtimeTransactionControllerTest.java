/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCAsyncListener;
import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCAirtimePaymentData;
import com.pesachoice.billpay.model.PCAirtimeServiceInfo;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCUser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * This class tests the service element reserved for airtime transaction.
 * @author Odilon Senyana
 */
public class PCAirtimeTransactionControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCAirtimeTransactionControllerTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCAirtimeTransactionControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testAirtimePaymentService() {
        assertNotNull(mainTabActivity);

        try {
            final CountDownLatch signal = new CountDownLatch(1);
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        PCTransactionController transactionController = new PCTransactionController(new PCAsyncListener() {
                            @Override
                            public void onTaskStarted() {

                            }

                            @Override
                            public void onTaskCompleted(PCData data) {
                                assertNotNull(data);
                                assertNull(data.getErrorMessage());
                                signal.countDown();// notify the count down latch
                            }
                        });
                        PCSpecialUser sender = new PCSpecialUser();
                        sender.setClassType(PCUser.class.getSimpleName());
                        sender.setUserName("some user");
                        sender.setFirstName("some");
                        sender.setLastName("user");
                        sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
                        sender.setPhoneNumber("14055558654");

                        PCSpecialUser receiver = new PCSpecialUser();
                        receiver.setClassType(PCUser.class.getSimpleName());
                        receiver.setFirstName("John");
                        receiver.setLastName("Doe");
                        receiver.setPhoneNumber("256752874562");

                        PCAirtimeServiceInfo airtimeServiceInfo = new PCAirtimeServiceInfo();
                        airtimeServiceInfo.setUsd(1);
                        airtimeServiceInfo.setTotalUsd(1.25);
                        airtimeServiceInfo.setFees(0.25);
                        airtimeServiceInfo.setRate(0.25);
                        airtimeServiceInfo.setMaskedCardNumber("XXX0027");
                        airtimeServiceInfo.setTotalLocalCurrency(3500);
                        airtimeServiceInfo.setReceiverCountry("Uganda");

                        PCOperator operator = new PCOperator();
                        operator.setName("AIRTEL");

                        PCAirtimePaymentData airtimePaymentDataReq = new PCAirtimePaymentData();
                        airtimePaymentDataReq.setSender(sender);
                        airtimePaymentDataReq.setReceiver(receiver);
                        airtimePaymentDataReq.setAirtimeServiceInfo(airtimeServiceInfo);
                        airtimePaymentDataReq.setOperator(operator);
                        transactionController.setActivity(mainTabActivity);
                        transactionController.setServiceType(PCPesabusClient.PCServiceType.SEND_AIRTIME);
                        transactionController.execute(airtimePaymentDataReq);
                        signal.await();// wait for callback
                    } catch (InterruptedException e) {
                        fail();
                        e.printStackTrace();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

   
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
//            Thread.sleep(30000L);
            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}