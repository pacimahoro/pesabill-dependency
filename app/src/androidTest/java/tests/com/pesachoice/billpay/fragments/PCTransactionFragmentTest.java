package tests.com.pesachoice.billpay.fragments;

import android.app.Application;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.test.AndroidTestCase;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pesachoice.billpay.activities.R;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.fragments.PCTransactionFragment;
import com.pesachoice.billpay.model.PCBillPaymentData;
import com.pesachoice.billpay.model.PCData;
import com.pesachoice.billpay.model.PCGenericError;

import org.junit.Test;

/**
 * Created by emmy on 10/3/17.
 */

public class PCTransactionFragmentTest extends AndroidTestCase {


    public class MyApplication extends Application {

        private Context mContext;

        @Override
        public void onCreate() {
            super.onCreate();
            mContext = getApplicationContext();
        }

        public Context getContext() {
            return mContext;
        }


    }


    /**
     * Test set add button correctness negative case.
     */
    @Test
    public void testSetAddsButtonCorrectness() {
        MyApplication app = new MyApplication();
        Context cxt = app.getContext();
        View view = new View(cxt);

        // Mock the transaction fragment
        PCTransactionFragment mockFragment = new PCTransactionFragmentMock();

        // FIXME: What are we testing here? It's not very clear.
        boolean result = mockFragment.setAddContactsButton(view);
        assertFalse(result);
    }

    /**
     * Mock up transaction fragment.
     * Override methods that we don't need at the moment or which rely heavily on the UI.
     *
     */
    public static class PCTransactionFragmentMock extends PCTransactionFragment {
        @Override
        public PCData.ActionType getActionType() {
            return PCData.ActionType.SELL;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = onCreateView(inflater, container, savedInstanceState);

            return view;
        }

        @Override
        public void getServiceInfo(View v) throws PCGenericError {

        }
    }

    ;
}
