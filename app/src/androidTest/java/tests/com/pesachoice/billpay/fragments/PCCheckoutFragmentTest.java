package tests.com.pesachoice.billpay.fragments;

import android.test.AndroidTestCase;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.fragments.PCCheckoutFragment;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTransaction;

import org.junit.Test;

/**
 * Created by emmy on 10/2/17.
 */


public class PCCheckoutFragmentTest extends AndroidTestCase {
    private PCMainTabActivity activity = new PCMainTabActivity();


    /**
     * Test set card number correct positive case.
     */
    @Test
      public  void testSetCardNumberCorrect() {

          PCSpecialUser appUser = new PCSpecialUser();
             appUser.setEmail("some_user23@pesachoice.com");
              PCCheckoutFragment frag = new PCCheckoutFragment();
              boolean result = frag.setCardNumber(appUser);

              assertTrue(result);
          }


}
