/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCMainTabActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCTransactionController;
import com.pesachoice.billpay.model.PCOperator;
import com.pesachoice.billpay.model.PCPAYTuitionPaymentData;
import com.pesachoice.billpay.model.PCSpecialUser;
import com.pesachoice.billpay.model.PCTuitionServiceInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for tuition payment transaction.
 * @author Desire Aheza
 */
public class PCTuitionTransactionControllerTest extends ActivityInstrumentationTestCase2<PCMainTabActivity> {
    private static final String CLAZZ = PCTuitionTransactionControllerTest.class.getName();

    PCMainTabActivity mainTabActivity;

    // The class behaves as Activity to call the service
    public PCTuitionTransactionControllerTest() {
        super(CLAZZ, PCMainTabActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        mainTabActivity = getActivity();
    }

    @Test
    public void testTuitionPaymentService() {
        assertNotNull(mainTabActivity);

        //TODO: need to use service description doc in order to determine the right values and asserations
        PCSpecialUser sender = new PCSpecialUser();
        sender.setClassType(PCSpecialUser.class.getSimpleName());
        sender.setUserName("some user");
        sender.setFirstName("some");
        sender.setLastName("user");
        sender.setTokenId("1822e358-81d9-4156-8383-bc16a6611a54");
        sender.setPhoneNumber("14055558654");

        PCSpecialUser receiver = new PCSpecialUser();
        receiver.setClassType(PCSpecialUser.class.getSimpleName());
        receiver.setFirstName("John");
        receiver.setLastName("Doe");
        receiver.setPhoneNumber("256752874562");

        PCTuitionServiceInfo tuitionServiceInfo = new PCTuitionServiceInfo();
        tuitionServiceInfo.setAccountNumber("04217248493");
        tuitionServiceInfo.setUsd(1);
        tuitionServiceInfo.setTotalUsd(1.25);
        tuitionServiceInfo.setFees(0.25);
        tuitionServiceInfo.setRate(0.25);
        tuitionServiceInfo.setMaskedCardNumber("XXX0027");
        tuitionServiceInfo.setTotalLocalCurrency(3700);
        tuitionServiceInfo.setReceiverCountry("Uganda");

        PCOperator operator = new PCOperator();
        operator.setName("BK");

        PCPAYTuitionPaymentData tuitionPaymentData = new PCPAYTuitionPaymentData();
        tuitionPaymentData.setSender(sender);
        tuitionPaymentData.setReceiver(receiver);
        tuitionPaymentData.setTuitionServiceInfo(tuitionServiceInfo);
        tuitionPaymentData.setOperator(operator);

        try {
            PCTransactionController transactionController = (PCTransactionController)
            PCControllerFactory.constructController(
                PCControllerFactory.PCControllerType.TRANSACTION_CONTROLLER, mainTabActivity);
            transactionController.setActivity(mainTabActivity);
            transactionController.setServiceType(PCPesabusClient.PCServiceType.PAY_TUITION);
            transactionController.execute(tuitionPaymentData);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            //TODO: missing assertions

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}