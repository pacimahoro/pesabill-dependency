/**
 * Copyright 2015, Pesachoice LLC, All Rights Reserved. The content of this file is sole propriety of Pesachoice LLC.
 * Without formal approval from Pesachoice LLC,  copying, modifying, distributing or altering this file is prohibited.
 */
package tests.com.pesachoice.billpay.business.service;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.pesachoice.billpay.activities.PCPasswordRecoveryActivity;
import com.pesachoice.billpay.business.PCPesabusClient;
import com.pesachoice.billpay.business.service.PCControllerFactory;
import com.pesachoice.billpay.business.service.PCCredentialsController;
import com.pesachoice.billpay.model.PCGeneralRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the service element reserved for cases when the user forgets the password.
 *
 * @author Odilon Senyana
 */
public class PCForgotPasswordCredentialsControllerTest extends ActivityInstrumentationTestCase2<PCPasswordRecoveryActivity> {
    private static final String CLAZZ = PCForgotPasswordCredentialsControllerTest.class.getName();

    private PCPasswordRecoveryActivity passwordRecoveryActivity;

    // The class behaves as Activity to call the service
    public PCForgotPasswordCredentialsControllerTest() {
        super(CLAZZ, PCPasswordRecoveryActivity.class);
    }

    @Before
    protected void setUp() throws Exception  {
        super.setUp();
        setActivityInitialTouchMode(false);
        passwordRecoveryActivity = getActivity();
    }

    @Test
    public void testForgotPasswordService() {
        assertNotNull(passwordRecoveryActivity);

        final PCGeneralRequest forgotPasswordRequest = new PCGeneralRequest();
        forgotPasswordRequest.setEmail("some_user23@pesachoice.com");
        forgotPasswordRequest.setFeatures(new String []{"password"});
        forgotPasswordRequest.setPhoneNumber("14055558654");
        try {
            PCCredentialsController credentialsController = (PCCredentialsController)
                    PCControllerFactory.constructController(
                            PCControllerFactory.PCControllerType.CREDENTIALS_CONTROLLER, passwordRecoveryActivity);
            credentialsController.setActivity(passwordRecoveryActivity);
            credentialsController.setServiceType(PCPesabusClient.PCServiceType.FORGOT_PASSWORD);
            credentialsController.execute(forgotPasswordRequest);
            /*
             * The sleep call below is only specific to unit tests
             */
            //TODO: sleep is not needed but it is a patch for now
            Thread.sleep(30000L);

            assertNotNull(passwordRecoveryActivity);
            /*
             * TODO: missing assertions
             */

            Log.d(CLAZZ, "Done testing.");
        } catch (Throwable exc) {
            Log.e(CLAZZ, "Could not handle the process because [" + exc.getMessage() + "]");
            fail(exc.getMessage());
        }
    }

    @After
    public void tearDown() throws Exception {
        Log.d(CLAZZ, "Tearing down the activity ...");
        super.tearDown();
    }
}
